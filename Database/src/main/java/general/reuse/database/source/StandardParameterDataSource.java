package general.reuse.database.source;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class StandardParameterDataSource implements DataSource {

    private String classname = null;
    private String jdbcUrl = null;

    private String username = null;
    private String password = null;

    public String toString() {
        return "StandardParameterDataSource{"+
                "classname="+classname+", "+
                "jdbcUrl="+jdbcUrl+", "+
                "username="+username+", "+
                (password == null ? "password=null" : "password.length()="+password.length())+
                "}";
    }

    public StandardParameterDataSource(String classname, String jdbcUrl) {
        this.classname = classname;
        this.jdbcUrl = jdbcUrl;
        try {
            Class.forName(classname);
        }
        catch(Exception e) {

        }
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return getConnection(username, password);
    }

    @Override
    public Connection getConnection(String username_p, String password_p) throws SQLException {
        return DriverManager.getConnection(jdbcUrl, username_p, password_p);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return DriverManager.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter writer) throws SQLException {
        DriverManager.setLogWriter(writer);
    }

    @Override
    public void setLoginTimeout(int timeout) throws SQLException {
        DriverManager.setLoginTimeout(timeout);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return DriverManager.getLoginTimeout();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new SQLFeatureNotSupportedException("Unsupported method");
    }

    @Override
    public <T> T unwrap(Class<T> aClass) throws SQLException {
        throw new SQLException("Unsupported method");
    }

    @Override
    public boolean isWrapperFor(Class<?> aClass) throws SQLException {
        throw new SQLException("Unsupported method");
    }

}
