package general.reuse.database.parameters.domain;

public class PersistentValue {

	private Long id;
	private String key;
	private String value;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) throws Exception {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) throws Exception {
		this.value = value;
	}

}
