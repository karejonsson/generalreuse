package general.reuse.database.parameters.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.parameters.domain.PersistentValue;
import general.reuse.database.pool.JDBCConnectionPool;

public class PersistentValueDB {

    public static List<PersistentValue> getAllPersistentValues(JDBCConnectionPool pool) {
        List<PersistentValue> out = new ArrayList<PersistentValue>();

        Connection conn = null;

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = pool.reserveConnection();
            stmt = conn.prepareStatement("select id, key, value from persistentvalues");
            rs = stmt.executeQuery();
            while(rs.next()) {
            	PersistentValue p = new PersistentValue();
                p.setId(rs.getLong(1));
                p.setKey(rs.getString(2));
                p.setValue(rs.getString(3));
                out.add(p);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            if(conn != null) { pool.releaseConnection(conn); }
        }
        return out;
    }

	
	private static boolean updateValue(JDBCConnectionPool pool, String key, String value) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("update persistentvalues set value = ? where key = ?");
		    stmt.setString(1, value);
		    stmt.setString(2, key);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			//e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	private static boolean insertValue(JDBCConnectionPool pool, String key, String value) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("insert into persistentvalues ( key, value ) values ( ?, ? )");
		    stmt.setString(1, key);
		    stmt.setString(2, value);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Throwable e) {
			//e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}
	
	public static boolean setStringValue(JDBCConnectionPool pool, String key, String value) {
		if(!insertValue(pool, key, value)) {
			updateValue(pool, key, value);
		}
		return true;
	}

	public static boolean setLongValue(JDBCConnectionPool pool, String key, long value) {
		if(!insertValue(pool, key, ""+value)) {
			updateValue(pool, key, ""+value);
		}
		return true;
	}
	
	public static boolean setIntegerValue(JDBCConnectionPool pool, String key, int value) {
		if(!insertValue(pool, key, ""+value)) {
			updateValue(pool, key, ""+value);
		}
		return true;
	}
	
	public static boolean setBooleanValue(JDBCConnectionPool pool, String key, boolean value) {
		if(!insertValue(pool, key, ""+value)) {
			updateValue(pool, key, ""+value);
		}
		return true;
	}
	
	public static String getStringValue(JDBCConnectionPool pool, String key) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select value from persistentvalues where key = ?");
		    stmt.setString(1, key);
		    rs = stmt.executeQuery();
		    String out = null;
		    while(rs.next()) {
		    	out = rs.getString(1);
		    }
		    return out;
		} catch(Exception e) {
		    //e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}

	public static Long getLongValue(JDBCConnectionPool pool, String key) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select value from persistentvalues where key = ?");
		    stmt.setString(1, key);
		    rs = stmt.executeQuery();
		    String out = null;
		    while(rs.next()) {
		    	out = rs.getString(1);
		    }
		    return Long.parseLong(out);
		} catch(Exception e) {
		    //e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}
	
	public static Integer getIntegerValue(JDBCConnectionPool pool, String key) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select value from persistentvalues where key = ?");
		    stmt.setString(1, key);
		    rs = stmt.executeQuery();
		    String out = null;
		    while(rs.next()) {
		    	out = rs.getString(1);
		    }
		    return Integer.parseInt(out);
		} catch(Exception e) {
		    //e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}
	
	public static Boolean getBooleanValue(JDBCConnectionPool pool, String key) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select value from persistentvalues where key = ?");
		    stmt.setString(1, key);
		    rs = stmt.executeQuery();
		    String out = null;
		    while(rs.next()) {
		    	out = rs.getString(1);
		    }
		    return Boolean.parseBoolean(out);
		} catch(Exception e) {
		    //e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return null;
	}
	
	public static boolean removeValue(JDBCConnectionPool pool, String key) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from persistentvalues where key = ?");
		    stmt.setString(1, key);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

}
