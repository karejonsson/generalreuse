package general.reuse.database.pool;

import java.sql.Connection;

public interface ManagedConnection {

    String getName();
    Connection getConnection(String poolname);
    void returnConnection(Connection conn, String poolname);
    public void reset(String poolname);

}
