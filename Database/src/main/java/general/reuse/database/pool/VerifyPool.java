package general.reuse.database.pool;

import java.sql.Connection;

public class VerifyPool {

    public interface AnnounceErrors {
        void error(String message, Exception e);
    }

    public interface CreatePool {
        JDBCConnectionPool get() throws Exception;
    }

    public static boolean control(AnnounceErrors ae, String leadingName, CreatePool cp) {
        JDBCConnectionPool pool = null;
        try {
            pool = cp.get();
        }
        catch(Exception e) {
            ae.error("Kan inte anropa pool-skaparen. Ledande namn: "+leadingName, e);
            return false;
        }
        if(pool == null) {
            ae.error("Pool-objektet blir null. Ledande namn: "+leadingName, null);
            return false;
        }
        Connection conn = null;
        try {
            conn = pool.reserveConnection();
        }
        catch(Exception e) {
            ae.error("Kan inte få ut en connection. Ledande namn: "+leadingName, e);
            return false;
        }
        if(conn == null) {
            ae.error("Connection-objektet blir null. Ledande namn: "+leadingName, null);
            return false;
        }
        try {
            if(conn.isClosed()) {
                ae.error("Connection-objektet blir stängt från början. Ledande namn: "+leadingName, null);
                pool.releaseConnection(conn);
                return false;
            }
        }
        catch(Exception e) {
            ae.error("Kan inte kolla om connection-objektet är stängt. Ledande namn: "+leadingName, e);
            return false;
        }
        pool.releaseConnection(conn);
        return true;
    }

}
