package general.reuse.database.transaction;

import general.reuse.database.pool.JDBCConnectionPool;

import java.sql.Connection;

public class TransactionalExecutor {

	public interface Executor {
		public boolean run(Connection conn_tph, Connection conn_tpxi) throws Exception;
	}

	public interface ExecutorSingle {
		public boolean run(Connection conn) throws Exception;
	}

	public static boolean run(JDBCConnectionPool pool_tph, JDBCConnectionPool pool_tpxi, Executor exec) throws Exception {
		Connection conn_tph = null;
		Boolean autocommitstate_tph = null;
		
		try {
			conn_tph = pool_tph.reserveConnection();
		}
		catch(Exception e) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser kan ej reserveras.", e);
		}
		if(conn_tph == null) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser ej tillgängliga.");
		}
 		
		try {
			autocommitstate_tph = conn_tph.getAutoCommit();
			conn_tph.setAutoCommit(false);
		}
		catch(Exception e) {
			pool_tph.releaseConnection(conn_tph);
			throw new Exception("Databaskopplingen är inte fungerande. Det går ej att slå av auto-commit.", e);
		}
		
		Connection conn_tpxi = null;
		Boolean autocommitstate_tpxi = null;
		
		try {
			conn_tpxi = pool_tpxi.reserveConnection();
		}
		catch(Exception e) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser kan ej reserveras.", e);
		}
		if(conn_tpxi == null) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser ej tillgängliga.");
		}
 		
		try {
			autocommitstate_tpxi = conn_tpxi.getAutoCommit();
			conn_tpxi.setAutoCommit(false);
		}
		catch(Exception e) {
			try {
				conn_tph.rollback();
				conn_tph.setAutoCommit(autocommitstate_tph);
				pool_tph.releaseConnection(conn_tph);
			}
			catch(Exception ee) {
				throw new Exception("Akten kunde skapas men dokumentarkivet kunde inte slå av autocommit så det rullades tillbaka och då gick det fel igen.", ee);
			}
			pool_tpxi.releaseConnection(conn_tpxi);
			throw new Exception("Databaskopplingen är inte fungerande. Det går ej att slå av auto-commit.", e);
		}

		boolean shallDoRollback;
		Exception ep = null;
		
		try {
			exec.run(conn_tph, conn_tpxi); // Här görs nyttojobbet
			shallDoRollback = false;
		}
		catch(Exception e) {
			ep = e;
			shallDoRollback = true;
		}
		
		if(!shallDoRollback) {
			StringBuffer message = new StringBuffer();
			boolean throwException = false;
			Exception e1 = null;
			try {
				conn_tph.commit();
				conn_tph.setAutoCommit(autocommitstate_tph);
				pool_tph.releaseConnection(conn_tph);
			}
			catch(Exception e) {
				e.printStackTrace();
				message.append("Skrivningarna inuti transaktionen gjordes men vid transaktionens commit uppstod problem.\nFelorsak: "+e.getMessage()+"\n"+
							   "Vid commit uppstod nästa problem: "+e.getMessage());
				throwException = true;
				e1 = e;
			}
			Exception e2 = null;
			try {
				conn_tpxi.commit();
				conn_tpxi.setAutoCommit(autocommitstate_tpxi);
				pool_tpxi.releaseConnection(conn_tpxi);
			}
			catch(Exception e) {
				message.append("Skrivningarna misslyckades.\nFelorsak: "+e.getMessage());
				throwException = true;
				e2 = e;
			}
			if(throwException) {
				Exception firstException = e2;
				if(e1 != null) {
					firstException = e1;
				}
				throw new Exception("Allt gick bra fram till commit men då blev det fel.\n"+message.toString(), firstException);
			}
			return true;
		}
		
		// Rulla tillbaka
		StringBuffer message = new StringBuffer();
		message.append("Operationen misslyckades så viljan är nu att rulla tillbaka allt.\n\n");
		if(ep != null) {
			message.append("Det underliggande problemet har meddelandet: "+ep.getMessage()+"\n");
		}
		try {
			conn_tpxi.rollback();
			conn_tpxi.setAutoCommit(autocommitstate_tpxi);
			pool_tpxi.releaseConnection(conn_tpxi);
			message.append("Att rulla tillbaka det som skrivits gick bra.\n");
		}
		catch(Exception e) {
			message.append(
					"Att rulla tillbaka det som skrivits misslyckades.\n"+
					"Feltext "+e.getMessage()+"\n");
		}
		try {
			conn_tph.rollback();
			conn_tph.setAutoCommit(autocommitstate_tph);
			pool_tph.releaseConnection(conn_tph);
			message.append("Att rulla tillbaka det som skrivits gick bra.\n");
		}
		catch(Exception e) {
			message.append(
					"Att rulla tillbaka det som skrivits misslyckades.\n"+
					"Feltext "+e.getMessage()+"\n");
		}
		
		throw new Exception(message.toString(), ep);
	}

	public interface ProducingExecutor {
		public Object run(Connection conn_tph, Connection conn_tpxi) throws Exception;
	}

	public interface ProducingExecutorSingle {
		public Object run(Connection conn) throws Exception;
	}

	public static Object run(JDBCConnectionPool pool_tph, JDBCConnectionPool pool_tpxi, ProducingExecutor exec) throws Exception {
		Connection conn_tph = null;
		Boolean autocommitstate_tph = null;
		
		try {
			conn_tph = pool_tph.reserveConnection();
		}
		catch(Exception e) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser kan ej reserveras.", e);
		}
		if(conn_tph == null) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser ej tillgängliga.");
		}
 		
		try {
			autocommitstate_tph = conn_tph.getAutoCommit();
			conn_tph.setAutoCommit(false);
		}
		catch(Exception e) {
			pool_tph.releaseConnection(conn_tph);
			throw new Exception("Databaskopplingen är inte fungerande. Det går ej att slå av auto-commit.", e);
		}
		
		Connection conn_tpxi = null;
		Boolean autocommitstate_tpxi = null;
		
		try {
			conn_tpxi = pool_tpxi.reserveConnection();
		}
		catch(Exception e) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser kan ej reserveras.", e);
		}
		if(conn_tpxi == null) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser ej tillgängliga.");
		}
 		
		try {
			autocommitstate_tpxi = conn_tpxi.getAutoCommit();
			conn_tpxi.setAutoCommit(false);
		}
		catch(Exception e) {
			try {
				conn_tph.rollback();
				conn_tph.setAutoCommit(autocommitstate_tph);
				pool_tph.releaseConnection(conn_tph);
			}
			catch(Exception ee) {
				throw new Exception("Databaskopplingen är inte fungerande.  Kunde inte slå av autocommit så det rullades tillbaka och då gick det fel igen.", ee);
			}
			pool_tpxi.releaseConnection(conn_tpxi);
			throw new Exception("Databaskopplingen är inte fungerande. Det går ej att slå av auto-commit.", e);
		}

		boolean shallDoRollback;
		Exception ep = null;
		Object out = null;
		
		try {
			out = exec.run(conn_tph, conn_tpxi); // Här görs nyttojobbet
			shallDoRollback = false;
		}
		catch(Exception e) {
			ep = e;
			shallDoRollback = true;
		}
		
		if(!shallDoRollback) {
			StringBuffer message = new StringBuffer();
			boolean throwException = false;
			Exception e1 = null;
			try {
				conn_tph.commit();
				conn_tph.setAutoCommit(autocommitstate_tph);
				pool_tph.releaseConnection(conn_tph);
			}
			catch(Exception e) {
				e.printStackTrace();
				message.append("Skrivningarna inuti transaktionen gjordes men vid transaktionens commit uppstod problem.\nFelorsak: "+e.getMessage()+"\n"+
							   "Vid commit uppstod nästa problem: "+e.getMessage());
				throwException = true;
				e1 = e;
			}
			Exception e2 = null;
			try {
				conn_tpxi.commit();
				conn_tpxi.setAutoCommit(autocommitstate_tpxi);
				pool_tpxi.releaseConnection(conn_tpxi);
			}
			catch(Exception e) {
				message.append("Skrivningarna misslyckades.\nFelorsak: "+e.getMessage());
				throwException = true;
				e2 = e;
			}
			if(throwException) {
				Exception firstException = e2;
				if(e1 != null) {
					firstException = e1;
				}
				throw new Exception("Allt gick bra fram till commit men då blev det fel.\n"+message.toString(), firstException);
			}
			return out;
		}
		
		// Rulla tillbaka
		StringBuffer message = new StringBuffer();
		message.append("Operationen misslyckades så viljan är nu att rulla tillbaka allt.\n\n");
		if(ep != null) {
			message.append("Det underliggande problemet har meddelandet: "+ep.getMessage()+"\n");
		}
		try {
			conn_tpxi.rollback();
			conn_tpxi.setAutoCommit(autocommitstate_tpxi);
			pool_tpxi.releaseConnection(conn_tpxi);
			message.append("Att rulla tillbaka det som skrivits gick bra.\n");
		}
		catch(Exception e) {
			message.append(
					"Att rulla tillbaka det som skrivits misslyckades.\n"+
					"Feltext "+e.getMessage()+"\n");
		}
		try {
			conn_tph.rollback();
			conn_tph.setAutoCommit(autocommitstate_tph);
			pool_tph.releaseConnection(conn_tph);
			message.append("Att rulla tillbaka det som skrivits gick bra.\n");
		}
		catch(Exception e) {
			message.append(
					"Att rulla tillbaka det som skrivits misslyckades.\n"+
					"Feltext "+e.getMessage()+"\n");
		}
		
		throw new Exception(message.toString(), ep);
	}

	public static boolean run(JDBCConnectionPool pool, ExecutorSingle exec) throws Exception {
		Connection conn = null;
		Boolean autocommitstate = null;

		try {
			conn = pool.reserveConnection();
		}
		catch(Exception e) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser kan ej reserveras.", e);
		}
		if(conn == null) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser ej tillgängliga.");
		}

		try {
			autocommitstate = conn.getAutoCommit();
			conn.setAutoCommit(false);
		}
		catch(Exception e) {
			pool.releaseConnection(conn);
			throw new Exception("Databaskopplingen är inte fungerande. Det går ej att slå av auto-commit.", e);
		}

		boolean shallDoRollback;
		Exception ep = null;

		try {
			exec.run(conn); // Här görs nyttojobbet
			shallDoRollback = false;
		}
		catch(Exception e) {
			ep = e;
			shallDoRollback = true;
		}

		if(!shallDoRollback) {
			StringBuffer message = new StringBuffer();
			boolean throwException = false;
			Exception e1 = null;
			try {
				conn.commit();
				conn.setAutoCommit(autocommitstate);
				pool.releaseConnection(conn);
			}
			catch(Exception e) {
				e.printStackTrace();
				message.append("Skrivningarna inuti transaktionen gjordes men vid transaktionens commit uppstod problem.\nFelorsak: "+e.getMessage()+"\n"+
					"Vid commit uppstod nästa problem: "+e.getMessage());
				throwException = true;
				e1 = e;
			}
			Exception e2 = null;
			if(throwException) {
				Exception firstException = e2;
				if(e1 != null) {
					firstException = e1;
				}
				throw new Exception("Allt gick bra fram till commit men då blev det fel.\n"+message.toString(), firstException);
			}
			return true;
		}

		// Rulla tillbaka
		StringBuffer message = new StringBuffer();
		message.append("Operationen misslyckades så viljan är nu att rulla tillbaka allt.\n\n");
		if(ep != null) {
			message.append("Det underliggande problemet har meddelandet: "+ep.getMessage()+"\n");
		}
		try {
			conn.rollback();
			conn.setAutoCommit(autocommitstate);
			pool.releaseConnection(conn);
			message.append("Att rulla tillbaka det som skrivits gick bra.\n");
		}
		catch(Exception e) {
			message.append(
				"Att rulla tillbaka det som skrivits misslyckades.\n"+
					"Feltext "+e.getMessage()+"\n");
		}

		throw new Exception(message.toString(), ep);
	}

	public static Object run(JDBCConnectionPool pool, ProducingExecutorSingle exec) throws Exception {
		Connection conn = null;
		Boolean autocommitstate = null;

		try {
			conn = pool.reserveConnection();
		}
		catch(Exception e) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser kan ej reserveras.", e);
		}
		if(conn == null) {
			throw new Exception("Databaskopplingen är inte fungerande. Resurser ej tillgängliga.");
		}

		try {
			autocommitstate = conn.getAutoCommit();
			conn.setAutoCommit(false);
		}
		catch(Exception e) {
			pool.releaseConnection(conn);
			throw new Exception("Databaskopplingen är inte fungerande. Det går ej att slå av auto-commit.", e);
		}

		boolean shallDoRollback;
		Exception ep = null;
		Object out = null;

		try {
			out = exec.run(conn); // Här görs nyttojobbet
			shallDoRollback = false;
		}
		catch(Exception e) {
			ep = e;
			shallDoRollback = true;
		}

		if(!shallDoRollback) {
			StringBuffer message = new StringBuffer();
			boolean throwException = false;
			Exception e1 = null;
			try {
				conn.commit();
				conn.setAutoCommit(autocommitstate);
				pool.releaseConnection(conn);
			}
			catch(Exception e) {
				e.printStackTrace();
				message.append("Skrivningarna inuti transaktionen gjordes men vid transaktionens commit uppstod problem.\nFelorsak: "+e.getMessage()+"\n"+
					"Vid commit uppstod nästa problem: "+e.getMessage());
				throwException = true;
				e1 = e;
			}
			Exception e2 = null;
			if(throwException) {
				Exception firstException = e2;
				if(e1 != null) {
					firstException = e1;
				}
				throw new Exception("Allt gick bra fram till commit men då blev det fel.\n"+message.toString(), firstException);
			}
			return out;
		}

		// Rulla tillbaka
		StringBuffer message = new StringBuffer();
		message.append("Operationen misslyckades så viljan är nu att rulla tillbaka allt.\n\n");
		if(ep != null) {
			message.append("Det underliggande problemet har meddelandet: "+ep.getMessage()+"\n");
		}
		try {
			conn.rollback();
			conn.setAutoCommit(autocommitstate);
			pool.releaseConnection(conn);
			message.append("Att rulla tillbaka det som skrivits gick bra.\n");
		}
		catch(Exception e) {
			message.append(
				"Att rulla tillbaka det som skrivits misslyckades.\n"+
					"Feltext "+e.getMessage()+"\n");
		}

		throw new Exception(message.toString(), ep);
	}

}
