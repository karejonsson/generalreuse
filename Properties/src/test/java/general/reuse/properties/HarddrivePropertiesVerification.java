package general.reuse.properties;

public class HarddrivePropertiesVerification {
	
	public static void main(String[] args) throws Exception {
		HarddriveProperties hp = new HarddriveProperties(null, null);
		hp.getCLPProperties().addSystemPropertiesAsUserValues();
		hp.printProperties();
		System.out.println(hp.getString(HarddriveProperties.servletcontainer_configfolder));
	}

}

/*
Kör denna klass med 

mvn -e exec:java -DCONFIGFOLDER_ROOT=/allan/vallan -Dservletcontainer.configfolder='${CONFIGFOLDER_ROOT}/GRODAN_BOLL' -Dexec.mainClass="general.reuse.properties.HarddrivePropertiesVerification" -Dexec.classpathScope="test"

Utskrtiftens sista rad skall bli 

/allan/vallan/GRODAN_BOLL
*/