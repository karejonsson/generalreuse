package general.reuse.properties;

import general.reuse.properties.specifics.SystemPropertyAndLeadingFolderNameStrategy;

public class SystemPropertyAndLeadingFolderNameStrategyTest {

    public static void main(String[] args) {
        System.setProperty("somepropname", "/home/kare/ws/generalreuse/Properties/hiding");
        SystemPropertyAndLeadingFolderNameStrategy s = new SystemPropertyAndLeadingFolderNameStrategy("pproper", "somepropname");
        System.out.println(s.find("/nån/skit/före/other.properties"));
    }

}
