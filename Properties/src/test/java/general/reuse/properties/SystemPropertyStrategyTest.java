package general.reuse.properties;

import general.reuse.properties.specifics.SystemPropertyStrategy;

public class SystemPropertyStrategyTest {

    public static void main(String[] args) {
        System.setProperty("somepropname", "/home/kare/ws/generalreuse/Properties/hiding");
        SystemPropertyStrategy s = new SystemPropertyStrategy("somepropname");
        System.out.println(s.find("other.properties"));
    }

}
