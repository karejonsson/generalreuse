package general.reuse.properties;

public class HarddrivePropertiesTest {

	public static void main(String[] args) throws Exception {
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("", "/aa/bb/test.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("", "/aa/bb/other.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("", "test.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("", "other.properties"));
		System.setProperty("aaa.bbb", "/etc/prvconfig/test.properties");
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.bbb", "/aa/bb/test.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.bbb", "/aa/bb/other.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.bbb", "test.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.bbb", "other.properties"));
		
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.bbb", "/etc/prvconfig/aktimp.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.bbb", "/etc/prvconfig/x.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.bbb", "/etc/prvconfig/test.properties"));

		System.setProperty("aaa.ccc", "/etc/prvconfig/x.properties");
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.ccc", "/etc/prvconfig/aktimp.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.ccc", "/etc/prvconfig/test.properties"));
		
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.ccc", "/etc/prvconfig/groovy.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.ccc", "/etc/prvconfig/hidden.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.ccc", "groovy.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.ccc", "hidden.properties"));
		
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.ddd", "/etc/prvconfig/groovy.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.ddd", "/etc/prvconfig/hidden.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.ddd", "groovy.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy("aaa.ddd", "hidden.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy(null, "groovy.properties"));
		System.out.println(HarddriveProperties.getPathToConfigFileFullStrategy(null, "hidden.properties"));
	}
	
}
