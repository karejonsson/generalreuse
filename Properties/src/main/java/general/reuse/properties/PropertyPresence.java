package general.reuse.properties;

public class PropertyPresence {

    private String property;
    private Boolean outcome;
    private String value;
    private String[] arrayValue;
    private String message;
    private Exception e;

    public String toString() {
        return "PropertyPresence{property="+property+
            ", outcome="+outcome+
        ", value="+value+
        ", message="+message+
        ", e.getMessage()="+(e != null ? e.getMessage() : "<NULL>")+"}";
    }

    public String getProperty() {
        return property;
    }

    public String getMessage() {
        return message;
    }

    public Exception getException() {
        return e;
    }

    public Boolean isOutcome() {
        return outcome;
    }

    public String getValue() {
        return value;
    }

    public PropertyPresence(String property) {
        this.property = property;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setException(Exception e) {
        this.e = e;
    }

    public void setOutcome(Boolean outcome) {
        this.outcome = outcome;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setValue(String[] arrayValue) {
        this.arrayValue = arrayValue;
    }

}
