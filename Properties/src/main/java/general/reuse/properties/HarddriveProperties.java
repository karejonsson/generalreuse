package general.reuse.properties;

import general.reuse.properties.specifics.SpecificStrategy;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

public class HarddriveProperties {
	
	public static final String servletcontainer_configfolder = "servletcontainer.configfolder";

	private String pathToConfigFile_property = null;
	private String pathToConfigFile_default = null;
	private String pathToConfigFile = null;
	private CommandLineProperties clp = null;

	public HarddriveProperties(String pathToConfigFile_property, String pathToConfigFile_default) {
		this(pathToConfigFile_property, pathToConfigFile_default, null);
	}

	public HarddriveProperties(String pathToConfigFile_property, String pathToConfigFile_default, CommandLineProperties clp) {
		this.pathToConfigFile_property = pathToConfigFile_property;
		this.pathToConfigFile_default = pathToConfigFile_default;
		this.clp = clp;
	}

	private String getPathToConfigFile() throws Exception {
		if(pathToConfigFile != null) {
			return pathToConfigFile;
		}
		pathToConfigFile = getPathToConfigFileFullStrategy(pathToConfigFile_property, pathToConfigFile_default);
		return pathToConfigFile;
	}
	
	public static String getPathToConfigFileFullStrategy(String pathToConfigFile_property, String pathToConfigFile_default) throws Exception {
		String out = null;
		try {
			out = getPathToConfigFileProperty(pathToConfigFile_property);
			if(out != null) {
				System.out.println("Strategy by property: "+out);
				return out;
			}
			out = searchPathToConfigFileOnDiscLocalOrBelow(pathToConfigFile_default);
			if(out != null) {
				return out;
			}
			File servletEnginesPropertyFile = getByFullServletEngineExplicitPropertyStrategy(pathToConfigFile_default);
			if(servletEnginesPropertyFile != null) {
				out = servletEnginesPropertyFile.getCanonicalPath();
			}
			if(out != null) {
				return out;
			}
			for(SpecificStrategy strategy : SpecificStrategy.strategies) {
				File f = strategy.find(pathToConfigFile_property);
				if(f == null) {
					continue;
				}
				if(!f.exists()) {
					continue;
				}
				out = f.getCanonicalPath();
				if(out != null) {
					return out;
				}
			}
			System.out.println("Strategies all failed. pathToConfigFile_property="+pathToConfigFile_property+", pathToConfigFile_default="+pathToConfigFile_default);
			return null;
		}
		catch(Exception e) {
			throw new Exception("No configuration file found.", e);
		}
	}

	public static InputStream getStream(String filename, Class clazz) {
		System.out.println("Hämtar properties för: "+filename);
		try {
			String fullyQualifyingPath = searchPathToConfigFileOnDiscLocalOrBelow(filename);
			if(fullyQualifyingPath != null) {
				System.out.println("Läser från skivan: "+filename+" -> "+fullyQualifyingPath);
				File props = new File(fullyQualifyingPath);
				if(!props.exists()) {
					System.out.println("Existerar ej "+fullyQualifyingPath);
				}
				//System.setProperty("log4j.configuration", fullyQualifyingPath);
				//System.out.println("Propertysättning: log4j.configuration -> "+fullyQualifyingPath);
				FileInputStream out = new FileInputStream(fullyQualifyingPath);
				return out;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		File servletEnginesPropertyFile = getByFullServletEngineExplicitPropertyStrategy(filename);
		if(servletEnginesPropertyFile != null) {
			try {
				String canFilePath = servletEnginesPropertyFile.getCanonicalPath();
				//System.setProperty("log4j.configuration", canFilePath);
				//System.out.println("Propertysättning: log4j.configuration -> "+canFilePath);
				return new FileInputStream(canFilePath);
			}
			catch (Exception e) {
			}
		}

		System.out.println("Läser från interna resurser: "+filename);
		return clazz.getClassLoader().getResourceAsStream(filename);
	}

	public static File getFile(String filename) {
		System.out.println("Hämtar properties för: "+filename);
		try {
			String fullyQualifyingPath = searchPathToConfigFileOnDiscLocalOrBelow(filename);
			if(fullyQualifyingPath != null) {
				System.out.println("Läser från skivan: "+filename+" -> "+fullyQualifyingPath);
				File out = new File(fullyQualifyingPath);
				if(!out.exists()) {
					System.out.println("Existerar ej "+fullyQualifyingPath);
					return null;
				}
				return out;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		File servletEnginesPropertyFile = getByFullServletEngineExplicitPropertyStrategy(filename);
		if(servletEnginesPropertyFile != null) {
			try {
				String fullyQualifyingPath = servletEnginesPropertyFile.getCanonicalPath();
				File out = new File(fullyQualifyingPath);
				if(!out.exists()) {
					System.out.println("Existerar ej "+fullyQualifyingPath);
					return null;
				}
				return out;
			}
			catch (Exception e) {
			}
		}
		return null;
	}

	public static String searchPathToConfigFileOnDiscLocalOrBelow(String pathToConfigFile_default) throws Exception {
		String out = null;
		try {
			out = getPathToConfigFileLocalConfig(pathToConfigFile_default);
			if(out != null) {
				System.out.println("Disc strategy 'local or below'  1: "+out);
				return out;
			}
			out = getPathToConfigFileLocalCurrent(pathToConfigFile_default);
			if(out != null) {
				System.out.println("Disc strategy 'local or below' 2: "+out);
				return out;
			}
			out = getPathToConfigFileDefault(pathToConfigFile_default);
			if(out != null) {
				System.out.println("Disc strategy 'local or below' 3: "+out);
				return out;
			}
			out = getPathToConfigFileLocalHiddenCurrent(pathToConfigFile_default);
			if(out != null) {
				System.out.println("Disc strategy 'local or below' 4: "+out);
				return out;
			}
			out = getPathToConfigFileLocalHiddenConfig(pathToConfigFile_default);
			if(out != null) {
				System.out.println("Disc strategy 'local or below' 5: "+out);
				return out;
			}
			return null;
		}
		catch(Exception e) {
			throw new Exception("No configuration file found on disc. pathToConfigFile_default="+pathToConfigFile_default, e);
		}
	}
	
	public static String getPathToConfigFileLocalConfig(String pathToConfigFile_default) {
		File current = new File(".");
		File currentConfig = new File(current, "config");
		if(currentConfig.exists()) {
			String filename = getFilenameAtEndOfFullyQualifyingPath(pathToConfigFile_default);
			File actualPropertyFile = new File(currentConfig, filename);
			if(actualPropertyFile.exists()) {
				try {
					return actualPropertyFile.getCanonicalPath();
				}
				catch(Exception e) {
					return actualPropertyFile.getAbsolutePath();
				}
			}
		}
		return null;
	}
	
	public static String getPathToConfigFileLocalHiddenConfig(String pathToConfigFile_default) {
		File current = new File(".");
		File[] hiding = current.listFiles(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f.isDirectory();
			}			
		});
		if(hiding == null) {
			return null;
		}
		String filename = getFilenameAtEndOfFullyQualifyingPath(pathToConfigFile_default);
		for(File directory : hiding) {
			File currentConfig = new File(directory, "config");
			if(currentConfig.exists()) {
				File actualPropertyFile = new File(currentConfig, filename);
				if(actualPropertyFile.exists()) {
					try {
						return actualPropertyFile.getCanonicalPath();
					}
					catch(Exception e) {
						return actualPropertyFile.getAbsolutePath();
					}
				}
			}
		}
		return null;
	}
	
	public static String getFilenameAtEndOfFullyQualifyingPath(String pathToConfigFile_default) {
		int idx = -1;
		idx = Math.max(idx, pathToConfigFile_default.lastIndexOf("/"));
		idx = Math.max(idx, pathToConfigFile_default.lastIndexOf("\\"));
		return pathToConfigFile_default.substring(idx+1);
	}

	public static String getPathToConfigFileLocalCurrent(String pathToConfigFile_default) {
		String filename = getFilenameAtEndOfFullyQualifyingPath(pathToConfigFile_default);
		File current = new File(".");
		File actualPropertyFile = new File(current, filename);
		if(actualPropertyFile.exists()) {
			try {
				return actualPropertyFile.getCanonicalPath();
			}
			catch(Exception e) {
				return actualPropertyFile.getAbsolutePath();
			}
		}
		return null;
	}

	public static String getPathToConfigFileLocalHiddenCurrent(String pathToConfigFile_default) {
		File current = new File(".");
		File[] hiding = current.listFiles(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f.isDirectory();
			}			
		});
		if(hiding == null) {
			return null;
		}
		String filename = getFilenameAtEndOfFullyQualifyingPath(pathToConfigFile_default);
		for(File directory : hiding) {
			File actualPropertyFile = new File(directory, filename);
			if(actualPropertyFile.exists()) {
				try {
					return actualPropertyFile.getCanonicalPath();
				}
				catch(Exception e) {
					return actualPropertyFile.getAbsolutePath();
				}
			}
		}
		return null;
	}

	public static String getPathToConfigFileDefault(String pathToConfigFile_default) {
		File defaultLocation = new File(pathToConfigFile_default);
		if(defaultLocation.exists()) {
			return pathToConfigFile_default;
		}
		return null;
	}

	public static String getPathToConfigFileProperty(String pathToConfigFile_property) {
		String system = null;
		try {
			system = System.getProperty(pathToConfigFile_property);
		}
		catch(Exception e) {
			return null;
		}
		File configLocation = null;
		try {
			configLocation = new File(system);
		}
		catch(Exception e) {
			return null;
		}
 		if(configLocation.exists()) {
			return system;
		}
		return null;
	}

	public CommandLineProperties getCLPProperties() throws Exception {
		if (clp != null) {
			return clp;
		}
		String pathToConfig = null;
		try {
			pathToConfig = getPathToConfigFile();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		// System.out.println("HarddriveProperties.getCLPProperties -
		// pathToConfig="+pathToConfig);
		if(pathToConfig == null) {
			System.out.println("Cannot deduce property file to read. Third fall back is process properties");
			clp = PropertiesAssemble.collectFromProcessEnvironment();
			return clp;
		}
		clp = PropertiesAssemble.collectFromFile(pathToConfig);
		return clp;
	}

	public String getString(String propertyname) throws Exception {
		CommandLineProperties clp = getCLPProperties();
		if (clp == null) {
			return null;
		}
		return clp.getString(propertyname);
	}

	public String getString(String propertyname, final String defaultValue) {
		return getCLPProperty(propertyname, defaultValue);
	}
	
	public String[] getStringArray(String propertyname) throws Exception {
		CommandLineProperties clp = getCLPProperties();
		if (clp == null) {
			return null;
		}
		return clp.getStringArray(propertyname);
	}

	public String getCLPProperty(String propertyname, final String defaultValue) {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (clp == null) {
			return defaultValue;
		}
		String out = clp.getString(propertyname, defaultValue);
		if(out == null) {
			System.out.println("Parametern " + propertyname + " är inte satt i konfigfilen");
			return defaultValue;
		}
		return out;
	}

	public Boolean getBoolean(String propertyname, final Boolean defaultValue) {
		return getCLPProperty(propertyname, defaultValue);
	}

	public Boolean getCLPProperty(String propertyname, final Boolean defaultValue) {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		} catch (Exception e) {
		}
		if (clp == null) {
			return defaultValue;
		}
		Boolean out = clp.getBoolean_Immutable(propertyname, defaultValue);
		if (out == defaultValue) {
			//System.out.println("Parametern " + propertyname + " är inte satt i konfigfilen");
		}
		return out;
	}

	public Integer getInt(String propertyname, final Integer defaultValue) {
		return getCLPProperty(propertyname, defaultValue);
	}
	
	public Integer[] getIntArray(String propertyname) throws Exception {
		CommandLineProperties clp = getCLPProperties();
		if (clp == null) {
			return null;
		}
		return clp.getIntArray(propertyname);
	}

	public Long getLong(String propertyname, final Long defaultValue) {
		return getCLPProperty_Long(propertyname, defaultValue);
	}

	public Double getDouble(String propertyname, final Double defaultValue) {
		return getCLPProperty_Double(propertyname, defaultValue);
	}

	public Float getFloat(String propertyname, final Float defaultValue) {
		return getCLPProperty_Float(propertyname, defaultValue);
	}

	public Long[] getLongArray(String propertyname) throws Exception {
		CommandLineProperties clp = getCLPProperties();
		if (clp == null) {
			return null;
		}
		return clp.getLongArray(propertyname);
	}

	public Integer getCLPProperty(String propertyname, final Integer defaultValue) {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		} catch (Exception e) {
		}
		if (clp == null) {
			return defaultValue;
		}
		Integer out = clp.getInt_Immutable(propertyname, defaultValue);
		if(out == null) {
			return defaultValue;
		}
		return out;
	}

	public Long getCLPProperty_Long(String propertyname, final Long defaultValue) {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		} catch (Exception e) {
		}
		if (clp == null) {
			return defaultValue;
		}
		Long out = clp.getLong_Immutable(propertyname, defaultValue);
		if(out == null) {
			return defaultValue;
		}
		return out;
	}

	public Double getCLPProperty_Double(String propertyname, final Double defaultValue) {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		} catch (Exception e) {
		}
		if (clp == null) {
			return defaultValue;
		}
		Double out = clp.getDouble_Immutable(propertyname, defaultValue);
		if(out == null) {
			return defaultValue;
		}
		return out;
	}

	public Float getCLPProperty_Float(String propertyname, final Float defaultValue) {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		} catch (Exception e) {
		}
		if (clp == null) {
			return defaultValue;
		}
		Float out = clp.getFloat_Immutable(propertyname, defaultValue);
		if(out == null) {
			return defaultValue;
		}
		return out;
	}

	public Integer getInt(String propertyname) {
		return getCLPProperty(propertyname);
	}

	public Long getLong(String propertyname) {
		return getCLPProperty_Long(propertyname);
	}

	public Integer getCLPProperty(String propertyname) {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		} catch (Exception e) {
		}
		if (clp == null) {
			return null;
		}
		Integer out = null;
		try {
			out = clp.getInt_Immutable(propertyname);
		} catch (Exception e) {
		}
		return out;
	}
	
	public Long getCLPProperty_Long(String propertyname) {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		} catch (Exception e) {
		}
		if (clp == null) {
			return null;
		}
		Long out = null;
		try {
			out = clp.getLong_Immutable(propertyname);
		} catch (Exception e) {
		}
		return out;
	}
	
	public void printProperties() throws Exception {
		CommandLineProperties clp = null;
		try {
			clp = getCLPProperties();
		} catch (Exception e) {
		}
		if (clp == null) {
			return;
		}
		clp.printEvaluated();
	}

	public static void main(String[] args) throws Exception {
		/*
		HarddriveProperties hp = new HarddriveProperties("AJKSDHJHDG", "JKHUJHJKH");
		hp.printProperties();
		System.out.println("----");
		CommandLineProperties clp = hp.getCLPProperties();
		clp.printEvaluated();
		System.out.println("----");
        Properties properties = System.getProperties();
        // Java 8
        properties.forEach((k, v) -> System.out.println(k + ":" + v));
		*/
		/*
		testOne("A/B/C/abc.old");
		testOne("A/B/C/abc_old");
		testOne("A/B/C/old.abc");
		testOne("A/B/C/old_abc");
		testOne("A/B/C/abc.old");
		testOne("/A/B/C/x.prop");
		testOne("/A/B/C.old/x.prop");
		testOne("/A/B/C.old/");
		*/
		List<File> files = findFilesInThreeLevelsFrom(new File("/home/kare/ws/phoenixwebclient/tmp"), "apa");
		files.forEach(f -> System.out.println(f.toPath().toString()));
	}

	public static void testOne(String name) {
		System.out.println(name+"->"+containsDisqualifyingSequence(name));
	}

	public static boolean containsDisqualifyingSequence(String name) {
		if(containsDisqualifyingSequence(name, "old")) {
			return true;
		}
		if(containsDisqualifyingSequence(name, "skip")) {
			return true;
		}
		if(containsDisqualifyingSequence(name, "save")) {
			return true;
		}
		return false;
	}

	public static boolean containsDisqualifyingSequence(String name, String sequence) {
		if(name.endsWith("."+sequence)) {
			return true;
		}
		if(name.endsWith("_"+sequence)) {
			return true;
		}
		int idx = name.lastIndexOf(File.separator);
		if(idx == -1) {
			return false;
		}
		String reducedName = name.substring(idx+1).trim();
		if(reducedName.startsWith(sequence+".")) {
			return true;
		}
		if(reducedName.startsWith(sequence+"_")) {
			return true;
		}
		return false;
	}

	public static List<File> findFilesInThreeLevelsFrom(File start, String toMatch) {
		return findFilesInThreeLevelsFrom(start, new FilenameMatcher() {

			@Override
			public boolean match(String candidate) {
				return candidate.endsWith(File.separator+toMatch);
			}

			@Override
			public String getSequence() {
				return toMatch;
			}
		});
	}

	public interface FilenameMatcher {
		boolean match(String candidate);
		String getSequence();
	}

	public static List<File> findFilesInThreeLevelsFrom(File start, FilenameMatcher matcher) {
		List<File> out = new ArrayList<>();
		try {
			for(File below : start.listFiles()) {
				String fullname = below.getCanonicalPath();
				if(matcher.match(fullname) && !containsDisqualifyingSequence(fullname)) {
					out.add(below);
				}
				//sb.append(below.getCanonicalPath()+" <br>\n");
				if(below.isDirectory() && !containsDisqualifyingSequence(below.getAbsolutePath())) {
					for(File belowBelow : below.listFiles()) {
						fullname = belowBelow.getCanonicalPath();
						if(matcher.match(fullname) && !containsDisqualifyingSequence(fullname)) {
							out.add(belowBelow);
						}
						if(belowBelow.isDirectory() && !containsDisqualifyingSequence(belowBelow.getAbsolutePath())) {
							for(File belowBelowBelow : belowBelow.listFiles()) {
								fullname = belowBelowBelow.getCanonicalPath();
								if(matcher.match(fullname) && !containsDisqualifyingSequence(fullname)) {
									out.add(belowBelowBelow);
								}
							}
						}
					}
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		String startPath = null;
		try {
			startPath = start.getCanonicalPath();
		}
		catch(Exception e) {
			startPath = start.getAbsolutePath();
		}
		final int len = startPath.length();
		Collections.sort(out, new ConfigPathComparator(len, matcher.getSequence()));
		return out;
	}

	public static class ConfigPathComparator implements Comparator<File> {

		private int len = 0;
		private String matcher = null;

		public ConfigPathComparator(int len, String matcher) {
			this.len = len;
			this.matcher = matcher;
		}

		@Override
		public int compare(File f1, File f2) {
			String spf1 = null;
			try {
				spf1 = f1.getCanonicalPath().substring(len);
			}
			catch(Exception e) {
				spf1 = f1.getAbsolutePath().substring(len);
			}
			String spf2 = null;
			try {
				spf2 = f2.getCanonicalPath().substring(len);
			}
			catch(Exception e) {
				spf2 = f2.getAbsolutePath().substring(len);
			}
			Integer i = decisive(spf1, spf2, "config");
			if(i != null) {
				return i;
			}
			i = decisive(spf1, spf2, "conf");
			if(i != null) {
				return i;
			}
			i = decisive(spf1, spf2, "konfig");
			if(i != null) {
				return i;
			}
			i = decisive(spf1, spf2, "konf");
			if(i != null) {
				return i;
			}
			return spf1.compareTo(spf2);
		}

		private Integer decisive(String spf1, String spf2, String id) {
			if(spf1.contains(id) && !spf2.contains(id)) {
				return -1;
			}
			if(!spf1.contains(id) && spf2.contains(id)) {
				return 1;
			}
			if(spf1.contains(id) && spf2.contains(id)) {
				int posdiff = spf1.indexOf(id) - spf2.indexOf(id);
				if(posdiff < 0) {
					return spf1.indexOf(matcher) - spf2.indexOf(matcher);
				}
				return spf1.compareTo(spf2);
			}
			return null;
		}
	}
	
	public static File getByFullServletEngineExplicitPropertyStrategy(String pathToConfigFile_default) {
		String filename = getFilenameAtEndOfFullyQualifyingPath(pathToConfigFile_default);
		String servletcontainer_configurations_startpoint = System.getProperty(servletcontainer_configfolder);
		if(servletcontainer_configurations_startpoint == null || servletcontainer_configurations_startpoint.trim().length() < 1) {
			System.out.println("No value for symbol: "+servletcontainer_configfolder);
			return null;
		}
		File startpoint = new File(servletcontainer_configurations_startpoint);
		if(!startpoint.exists()) {
			System.out.println("Place on disc does not exist: "+servletcontainer_configurations_startpoint);
			return null;
		}

		List<File> findings = new ArrayList<>();
		try {
			findings = findFilesInThreeLevelsFrom(startpoint, filename);//getByKnownIdentifyingFolderStrategy(startpoint, filename);
			if(findings.size() == 1) {
				System.out.println("Servlet strategy from explicit property: "+findings.get(0));
				return findings.get(0);
			}
		}
		catch(Exception e) {
		}

		System.out.println("Servlet strategy from explicit property failed: "+findings.get(0)+", cannot chose. Found "+findings.size());
		return null;
	}
	
	/*
		String tomcat_domain = System.getProperty("tomcat.folderidentifier");
		List<File> findings = new ArrayList<>();
		StringBuffer sb = new StringBuffer();
		if(tomcat_domain != null && tomcat_domain.trim().length() > 1) {
			try {
				findings = getByFullServletEngineStrategy(start, tomcat_domain, "x.properties");
			}
			catch(Exception e) {
			}
		}
		else {
			try {
				findings = getByFullServletEngineStrategy(start, "webapps", "x.properties");
			}
			catch(Exception e) {
			}
			sb.append("Fann inte den förväntade parametern tomcat.domain <br>\n");
		}
		sb.append("Antal funna potentiella konfigurationsfiler "+findings.size()+"<br>\n");
		findings.forEach(it -> sb.append(it.getAbsoluteFile()+" <br>\n"));
	 */
	/*
	public static List<File> getByKnownIdentifyingFolderStrategy(File start, String matcher) {
		StringBuffer log = new StringBuffer();
		List<File> domainers = new ArrayList<>();
		List<File> some = findFilesInThreeLevelsFrom(start, matcher);
		if(some != null) {
			some.forEach(it -> domainers.add(it));
		}
		return allCandidates;
	}
	*/
}
