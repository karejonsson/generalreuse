package general.reuse.properties;

import java.util.ArrayList;
import java.util.List;

public class CollectDiagnose {

    public interface DiagnosisPerformer {
        boolean collect(VerifyProperties.RetrieveStringProperty rsp, VerifyProperties.RetrieveStringArrayProperty rsap, VerifyProperties.AnnounceOutcome log);
    }

    private PropertyPresence current = null;

    private List<PropertyPresence> runInternal(DiagnosisPerformer dp, VerifyProperties.RetrieveStringProperty rsp, VerifyProperties.RetrieveStringArrayProperty rsap) {
        List<PropertyPresence> out = new ArrayList<>();
        dp.collect(
            s -> {
                current = new PropertyPresence(s);
                out.add(current);
                String prop = null;
                try {
                    prop = rsp.get(s);
                }
                catch(Exception e) {
                    current.setException(e);
                    current.setOutcome(false);
                }
                current.setValue(prop);
                return prop;
            },
            s -> {
                current = new PropertyPresence(s);
                out.add(current);
                String[] prop = null;
                try {
                    prop = rsap.get(s);
                }
                catch(Exception e) {
                    current.setException(e);
                    current.setOutcome(false);
                }
                current.setValue(prop);
                return prop;
            },
            (b, m, e) -> {
                current.setOutcome(b);
                current.setMessage(m);
                current.setException(e);
            }
        );
        return out;
    }

    public static List<PropertyPresence> run(DiagnosisPerformer dp, VerifyProperties.RetrieveStringProperty rsp, VerifyProperties.RetrieveStringArrayProperty rsap) {
        CollectDiagnose cd = new CollectDiagnose();
        return cd.runInternal(dp, rsp, rsap);
    }

}
/*
    public static PropertyPresence shallPointToReadableFolder_diagnose(String property, RetrieveProperty rp, AnnounceErrors log) {
        PropertyPresence pp = new PropertyPresence(property);
        boolean out = shallPointToReadableFolder(property, rp, (m, e) -> {
            pp.setMessage(m);
            pp.setException(e);
            log.error(m, e);
        });
        pp.setOutcome(out);
        return pp;
    }
 */