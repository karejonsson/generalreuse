package general.reuse.properties;

import java.io.File;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VerifyProperties {

    public interface RetrieveStringProperty {
        String get(String property) throws Exception;
    }

    public interface RetrieveStringArrayProperty {
        String[] get(String property) throws Exception;
    }

    public interface AnnounceOutcome {
        void outcome(Boolean outcome, String message, Exception e);
    }

    public static boolean shallPointToReadableFolder(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        File f = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        }
        catch(Exception e) {
            log.outcome(false, "Propertyn "+prop+" kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn "+prop+" ger null", null);
            return false;
        }
        f = new File(value);
        if(!f.exists()) {
            log.outcome(false, "Propertyn "+prop+" pekar inte på något existerande. Värdet är <"+value+">", null);
            return false;
        }
        if(!f.isDirectory()) {
            log.outcome(false, "Propertyn "+prop+" pekar inte på en katalog. Värdet är <"+value+">", null);
            return false;
        }
        if(!f.canRead()) {
            log.outcome(false, "Propertyn "+prop+" leder till filen "+value+" som inte kan läsas", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallPointToWritableFolder(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        File f = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        }
        catch(Exception e) {
            log.outcome(false, "Propertyn "+prop+" kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn "+prop+" ger null", null);
            return false;
        }
        f = new File(value);
        if(!f.exists()) {
            log.outcome(false, "Propertyn "+prop+" pekar inte på något existerande. Värdet är <"+value+">", null);
            return false;
        }
        if(!f.isDirectory()) {
            log.outcome(false, "Propertyn "+prop+" pekar inte på en katalog. Värdet är <"+value+">", null);
            return false;
        }
        if(!f.canWrite()) {
            log.outcome(false, "Propertyn "+prop+" leder till filen "+value+" som inte kan skrivas", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallPointToReadAndWritableFolder(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        File f = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        }
        catch(Exception e) {
            log.outcome(false, "Propertyn "+prop+" kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn "+prop+" ger null", null);
            return false;
        }
        f = new File(value);
        if(!f.exists()) {
            log.outcome(false, "Propertyn "+prop+" pekar inte på något existerande. Värdet är <"+value+">", null);
            return false;
        }
        if(!f.isDirectory()) {
            log.outcome(false, "Propertyn "+prop+" pekar inte på en katalog. Värdet är <"+value+">", null);
            return false;
        }
        if(!f.canRead()) {
            log.outcome(false, "Propertyn "+prop+" leder till filen "+value+" som inte kan läsas", null);
            return false;
        }
        if(!f.canWrite()) {
            log.outcome(false, "Propertyn "+prop+" leder till filen "+value+" som inte kan skrivas", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallPointToReadableFile(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        File f = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        }
        catch(Exception e) {
            log.outcome(false, "Propertyn "+prop+" kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn "+prop+" ger null", null);
            return false;
        }
        f = new File(value);
        if(!f.exists()) {
            log.outcome(false, "Propertyn "+prop+" pekar inte på något existerande på skivan. Värdet är <"+value+">", null);
            return false;
        }
        if(f.isDirectory()) {
            log.outcome(false, "Propertyn "+prop+" pekar på en katalog, ej en fil. Värdet är <"+value+">", null);
            return false;
        }
        if(!f.canRead()) {
            log.outcome(false, "Propertyn "+prop+" leder till filen "+value+" som inte kan läsas", null);
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideExplicitBoolean(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        }
        catch(Exception e) {
            log.outcome(false, "Propertyn "+prop+" kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn "+prop+" skall peka på ett booleskt värde men ger null.", null);
            return false;
        }
        if(value.equalsIgnoreCase("true")) {
            log.outcome(true, "Svar true", null);
            return true;
        }
        if(value.equalsIgnoreCase("false")) {
            log.outcome(true, "Svar false", null);
            return true;
        }
        log.outcome(false, "Propertyn "+prop+" skulle ge ett booleskt värde men gav <"+value+">", null);
        return false;
    }

    public static boolean shallProvideStringTrimmedWithMaxLength(String prop, int maxlen, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(value.length() != value.trim().length()) {
            log.outcome(false, "Propertyn " + prop + " ger värdet inte ett trimmat värde", null);
            return false;
        }
        if(value.length() > maxlen) {
            log.outcome(false, "Propertyn " + prop + " ger ett för långt värde <"+value+">. "+value.length()+" tecken medan max var "+maxlen, null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideStringWithMaxLength(String prop, int maxlen, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(value.length() > maxlen) {
            log.outcome(false, "Propertyn " + prop + " ger ett för långt värde <"+value+">. "+value.length()+" tecken medan max var "+maxlen, null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideStringTrimmedWithMinLength(String prop, int minlen, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(value.length() != value.trim().length()) {
            log.outcome(false, "Propertyn " + prop + " ger värdet inte ett trimmat värde", null);
            return false;
        }
        if(value.length() < minlen) {
            log.outcome(false, "Propertyn " + prop + " ger ett för kort värde <"+value+">. "+value.length()+" tecken medan min var "+minlen, null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideStringTrimmed(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(value.length() != value.trim().length()) {
            log.outcome(false, "Propertyn " + prop + " ger värdet inte ett trimmat värde", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideStringWithMinLength(String prop, int minlen, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(value.length() < minlen) {
            log.outcome(false, "Propertyn " + prop + " ger ett för kort värde <"+value+">. "+value.length()+" tecken medan min var "+minlen, null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideString(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideInteger(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        try {
            int integer = Integer.parseInt(value);
        }
        catch(Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan ej parseas som heltal", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideIntegerOrNothing(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(true, "Propertyn " + prop + " ger värdet null. OK", null);
            return true;
        }
        if(value.trim().length() == 0) {
            log.outcome(true, "Propertyn " + prop + " ger inget. OK", null);
            return true;
        }
        try {
            int integer = Integer.parseInt(value);
        }
        catch(Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan ej parseas som heltal", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideIntegerArray(String prop, RetrieveStringArrayProperty rp, AnnounceOutcome log) {
        String[] value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        String piece = null;
        try {
            for(String pieceTemp : value) {
                piece = pieceTemp;
                int integer = Integer.parseInt(piece.trim());
            }
        }
        catch(Exception e) {
            log.outcome(false, "Propertyn " + prop + " har det icke integer-parsebara värdet <"+piece+">", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideStringOrNothing(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(true, "Propertyn " + prop + " ger värdet null. OK", null);
            return true;
        }
        if(value.trim().length() == 0) {
            log.outcome(true, "Propertyn " + prop + " ger inget. OK", null);
            return true;
        }
        log.outcome(true, null, null);
        return true;
    }

    private static final String regexEmail = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
    private static final String regexDomain = "^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$";

    //private static final String regexUrl = "((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)";
    //private static final String regexUrl = "((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+(:0-9+//)?~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)";
    private static final String regexUrl = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    private static final String regexFileUrl = "^(file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    private static final String regexJdbcUrl = "^(jdbc):([-a-zA-Z0-9]+:)*//[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

    public static void main(String[] args) {
        System.out.println("URL "+isValidUrl("http://prvntsv32:9086/PandoraArendeWeb/rest/edi"));
        System.out.println("URL "+isValidUrl("http://prvntsv32.se/PandoraArendeWeb/rest/edi"));
        //System.out.println("URL "+isValidUrl("http://prvntsv32:9086/PandoraArendeWeb/rest/edi?"));
        System.out.println("URL "+isValidUrl("http://prvntsv32:9086/PandoraArendeWeb/rest/edi?t=aaa&g=fff"));
        System.out.println("URL "+isValidJdbcUrl("jdbc:postgresql://127.0.0.1:5432/pegasus3db"));
    }


    private static final String regexFtpUrl = "^ftp://([^:]+):([^@]+)@(.*?)/(.*?)(\\?.*)?$";

    public static boolean isValid(String value, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        return matcher.matches();
    }

    public static boolean isValidEmail(String email) {
        return isValid(email, regexEmail);
    }

    public static boolean isValidDomain(String domain) {
        return isValid(domain, regexDomain);
    }

    public static boolean isValidUrl(String url) {
        return isValid(url, regexUrl);
    }
    public static boolean isValidFileUrl(String url) {
        return isValid(url, regexFileUrl);
    }

    public static boolean isValidJdbcUrl(String url) {
        return isValid(url, regexJdbcUrl);
    }


    public static boolean isValidFtpUrl(String ftpurl) {
        return isValid(ftpurl, regexFtpUrl);
    }

    public static boolean shallProvideEmailAddress(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(value.contains(":")) {
            String[] parts = value.split(":");
            int ctr = 0;
            for(String part : parts) {
                ctr++;
                if(part.length() < 6) {
                    log.outcome(false, "Propertyn " + prop + " och värde nr "+ctr+" ger värdet "+part+" som är för kort. "+part.length()+" tecken", null);
                    return false;
                }
                if(!isValidEmail(part)) {
                    log.outcome(false, "Propertyn " + prop + " och värde nr "+ctr+" ger värdet "+part+" och det borde innehålla ett @-tecken", null);
                    return false;
                }
            }
        }
        else {
            if(value.length() < 6) {
                log.outcome(false, "Propertyn " + prop + " ger värdet "+value+" som är för kort. "+value.length()+" tecken", null);
                return false;
            }
            if(!value.contains("@")) {
                log.outcome(false, "Propertyn " + prop + " ger värdet "+value+" och det borde innehålla ett @-tecken", null);
                return false;
            }
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideInternetDomain(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(!isValidDomain(value)) {
            log.outcome(false, "Propertyn " + prop + " ger värdet <"+value+"> vilket inte är i domänformat", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideInternetDomainOrNothing(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(true, "Propertyn " + prop + " ger värdet null. OK", null);
            return true;
        }
        if(value.trim().length() == 0) {
            log.outcome(true, "Propertyn " + prop + " ger inget. OK", null);
            return true;
        }
        if(!isValidDomain(value)) {
            log.outcome(false, "Propertyn " + prop + " ger värdet <"+value+"> vilket inte är i domänformat", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }



    public static boolean shallProvideInternetUrl(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(!isValidUrl(value)) {
            log.outcome(false, "Propertyn " + prop + " ger värdet <"+value+"> vilket inte är i url-format", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideInternetUrlOrNothing(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(true, "Propertyn " + prop + " ger värdet null. OK", null);
            return true;
        }
        if(value.trim().length() == 0) {
            log.outcome(true, "Propertyn " + prop + " ger inget. OK", null);
            return true;
        }
        if(!isValidUrl(value)) {
            log.outcome(false, "Propertyn " + prop + " ger värdet <"+value+"> vilket inte är i url-format", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideFtpUrl(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(!isValidFtpUrl(value)) {
            log.outcome(false, "Propertyn " + prop + " ger värdet <"+value+"> vilket inte är i ftp-url-format", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideFileUrl(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(!isValidFileUrl(value)) {
            log.outcome(false, "Propertyn " + prop + " ger värdet <"+value+"> vilket inte är i file-url-format", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideJdbcUrl(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(!isValidJdbcUrl(value)) {
            log.outcome(false, "Propertyn " + prop + " ger värdet <"+value+"> vilket inte är i jdbc-url-format", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideStringTrimmedWithMinMaxLength(String prop, int minlen, int maxlen, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(value.length() != value.trim().length()) {
            log.outcome(false, "Propertyn " + prop + " ger inte ett trimmat värde <"+value+">", null);
            return false;
        }
        if(value.length() < minlen) {
            log.outcome(false, "Propertyn " + prop + " ger ett för kort värde <"+value+">. "+value.length()+" tecken medan min var "+minlen, null);
            return false;
        }
        if(value.length() > maxlen) {
            log.outcome(false, "Propertyn " + prop + " ger ett för långt värde <"+value+">. "+value.length()+" tecken medan max var "+maxlen, null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideStringWithMinMaxLength(String prop, int minlen, int maxlen, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);//InstallationProperties.getString(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        if(value == null) {
            log.outcome(false, "Propertyn " + prop + " ger värdet null", null);
            return false;
        }
        if(value.length() < minlen) {
            log.outcome(false, "Propertyn " + prop + " ger ett för kort värde <"+value+">. "+value.length()+" tecken medan min var "+minlen, null);
            return false;
        }
        if(value.length() > maxlen) {
            log.outcome(false, "Propertyn " + prop + " ger ett för långt värde <"+value+">. "+value.length()+" tecken medan max var "+maxlen, null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvideLoadableClass(String prop, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        Class clazz = null;
        try {
            clazz = Class.forName(value);
        }
        catch(Exception e) {
            log.outcome(false, "Propertyn " + prop + " leder till "+value+" och det är inte en laddbar klass", e);
            return false;
        }
        if(clazz == null) {
            log.outcome(false, "Propertyn " + prop + " leder till "+value+" och det är inte en laddbart, ger null", null);
            return false;
        }
        log.outcome(true, null, null);
        return true;
    }

    public static boolean shallProvidePingableAddress(String prop, int timeout, RetrieveStringProperty rp, AnnounceOutcome log) {
        String value = null;
        try {
            value = rp.get(prop);
        } catch (Exception e) {
            log.outcome(false, "Propertyn " + prop + " kan inte utvärderas", e);
            return false;
        }
        InetAddress inet = null;
        try {
            inet = InetAddress.getByName(value);
        }
        catch(Exception e) {
            log.outcome(false, "Propertyn " + prop + " gav "+value+" vilket inte kan slås upp", e);
            return false;
        }
        try {
            if(inet.isReachable(timeout)) {
                log.outcome(true, null, null);
                return true;
            }
            else {
                log.outcome(false, "Propertyn " + prop + " gav "+value+" vilket inte kan pingas", null);
                return false;
            }
        }
        catch(Exception e) {
            log.outcome(false, "Propertyn " + prop + " gav "+value+" mot vilken pingning ger exception", e);
            return false;
        }
    }

    public static boolean shallProvideSocketConnection(String addressProp, String portProp, int timeout, RetrieveStringProperty rp, AnnounceOutcome log) {
        String addressValue = null;
        try {
            addressValue = rp.get(addressProp);
        } catch (Exception e) {
            log.outcome(false, "Adresspropertyn " + addressProp + " kan inte utvärderas", e);
            return false;
        }
        String portValue = null;
        try {
            portValue = rp.get(portProp);
        } catch (Exception e) {
            log.outcome(false, "Portpropertyn " + portProp + " kan inte utvärderas", e);
            return false;
        }
        Integer port = null;
        try {
            port = Integer.parseInt(portValue);
        }
        catch(Exception e) {
            log.outcome(false, "Portpropertyn " + portProp + " kan inte utvärderas till ett heltal", e);
            return false;
        }
        if(port == null) {
            log.outcome(false, "Portpropertyn " + portProp + " kan inte utvärderas till ett heltal utan ger null", null);
            return false;
        }
        Socket socket = null;
        try {
            socket = new Socket();
        }
        catch(Exception e) {
            log.outcome(false, "Socket kan inte skapas", e);
            return false;
        }
        if(socket == null) {
            log.outcome(false, "Socket kan inte skapas, gav null", null);
            return false;
        }
        InetSocketAddress socketAddress = null;
        try {
            socketAddress = new InetSocketAddress(addressValue, port);
        }
        catch(Exception e) {
            log.outcome(false, "Socketadress kan inte skapas", e);
            return false;
        }
        try {
            socket.connect(socketAddress, timeout);
        }
        catch(Exception e) {
            log.outcome(false, "Kan ej göra connect till socket", e);
            try {
                socket.close();
            } catch(Exception ee) {}
            return false;
        }
        try {
            socket.close();
        } catch(Exception ee) {}
        log.outcome(true, null, null);
        return true;
    }

}
