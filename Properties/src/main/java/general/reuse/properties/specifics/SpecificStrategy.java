package general.reuse.properties.specifics;

import java.io.File;

public interface SpecificStrategy {

    public static SpecificStrategy[] strategies = new SpecificStrategy[] {
            new SystemPropertyAndLeadingFolderNameStrategy("propert", "catalina.base"),
            new SystemPropertyStrategy("catalina.base")
    };

    public File find(String pathToConfigFile_default);

}
