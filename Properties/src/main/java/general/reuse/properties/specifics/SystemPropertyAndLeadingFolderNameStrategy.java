package general.reuse.properties.specifics;

import general.reuse.properties.HarddriveProperties;

import java.io.File;
import java.util.List;

public class SystemPropertyAndLeadingFolderNameStrategy implements SpecificStrategy {

    private String leadingName = null;
    private String systemProperty = null;

    public SystemPropertyAndLeadingFolderNameStrategy(String leadingName, String systemProperty) {
        this.leadingName = leadingName;
        this.systemProperty = systemProperty;
    }

    @Override
    public File find(String pathToConfigFile_default) {
        String catalina_base = System.getProperty(systemProperty);
        if(catalina_base == null) {
            System.out.println("systemPropertyn har inget värde - null");
            return null;
        }
        if(catalina_base.trim().length() == 0) {
            System.out.println("systemPropertyn har inget värde - noll");
            return null;
        }
        File f = new File(catalina_base);
        if(!f.exists()) {
            System.out.println("systemPropertyn pekar på en ickeexisternade plats");
            return null;
        }
        final File[] children = f.listFiles(ff -> {
            return ff.getName().toLowerCase().contains(leadingName.toLowerCase());
        });
        if(children == null) {
            System.out.println("Inga filnamn med givna antydningen - null");
            return null;
        }
        if(children.length == 0) {
            System.out.println("Inga filnamn med givna antydningen - noll");
            return null;
        }
        final String filename = HarddriveProperties.getFilenameAtEndOfFullyQualifyingPath(pathToConfigFile_default);
        System.out.println("Antalet kandiderande kataloger är "+children.length+", filename="+filename);
        for(final File sf : children) {
            List<File> candidates = HarddriveProperties.findFilesInThreeLevelsFrom(sf, new HarddriveProperties.FilenameMatcher() {

                @Override
                public boolean match(String candidate) {
                    if(HarddriveProperties.containsDisqualifyingSequence(candidate)) {
                        return false;
                    }
                    boolean out = candidate.toLowerCase().contains(filename);
                    System.out.println("candidate="+candidate+", filename="+filename+", out="+out);
                    return out;
                }

                @Override
                public String getSequence() {
                    return filename;
                }
            });
            if(candidates == null) {
                System.out.println("kandidaterna - null");
                return null;
            }
            if(candidates.size() == 0) {
                System.out.println("kandidaterna - noll");
                return null;
            }
            System.out.println("Ger ut det första av "+candidates.size());
            return candidates.get(0);
        }
        return null;
    }

    public String toString() {
        return "SystemPropertyAndLeadingFolderNameStrategy{leadingName="+leadingName+", systemProperty="+systemProperty+"}";
    }

}
