package general.reuse.properties.specifics;

import general.reuse.properties.HarddriveProperties;

import java.io.File;
import java.util.List;

public class SystemPropertyStrategy implements SpecificStrategy {

    private String systemProperty = null;

    public SystemPropertyStrategy(String systemProperty) {
        this.systemProperty = systemProperty;
    }

    @Override
    public File find(String pathToConfigFile_default) {
        String catalina_base = System.getProperty(systemProperty);
        if(catalina_base == null) {
            return null;
        }
        if(catalina_base.trim().length() == 0) {
            return null;
        }
        File f = new File(catalina_base);
        if(!f.exists()) {
            return null;
        }
        final String filename = HarddriveProperties.getFilenameAtEndOfFullyQualifyingPath(pathToConfigFile_default);
        List<File> candidates = HarddriveProperties.findFilesInThreeLevelsFrom(f, new HarddriveProperties.FilenameMatcher() {

            @Override
            public boolean match(String candidate) {
                if(HarddriveProperties.containsDisqualifyingSequence(candidate)) {
                    return false;
                }
                return candidate.toLowerCase().contains(filename);
            }

            @Override
            public String getSequence() {
                return filename;
            }
        });
        if(candidates == null) {
            return null;
        }
        if(candidates.size() == 0) {
            return null;
        }
        System.out.println("Ger ut det första av "+candidates.size());
        return candidates.get(0);
    }

    public String toString() {
        return "SystemPropertyStrategy{systemProperty="+systemProperty+"}";
    }

}
