package general.reuse.http.analyse;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Set;

/**
 * Class for HTTP request parsing as defined by RFC 2612:
 *
 * Request = Request-Line ; Section 5.1 (( general-header ; Section 4.5 |
 * request-header ; Section 5.3 | entity-header ) CRLF) ; Section 7.1 CRLF [
 * message-body ] ; Section 4.3
 *
 * @author izelaya
 *
 */
public class HttpRequestStructure {

    private String requestLine;
    private LinkedHashMap<String, String> requestHeaders = new LinkedHashMap<String, String>();
    private StringBuffer messageBody = new StringBuffer();

    private HttpRequestStructure() {
    }

    public static HttpRequestStructure parseRequest(String request) throws IOException, HttpFormatException {
        return parseRequest(new BufferedReader(new StringReader(request)));
    }

    public static HttpRequestStructure parseRequest(byte[] request) throws IOException, HttpFormatException {
        return parseRequest(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(request))));
    }

    public static HttpRequestStructure parseRequest(BufferedReader reader) throws IOException, HttpFormatException {
        HttpRequestStructure out = new HttpRequestStructure();
        out.setRequestLine(reader.readLine()); // Request-Line ; Section 5.1

        String header = reader.readLine();
        while (header.length() > 0) {
            out.appendHeaderParameter(header);
            header = reader.readLine();
        }

        String bodyLine = reader.readLine();
        while (bodyLine != null) {
            out.appendMessageBody(bodyLine);
            bodyLine = reader.readLine();
        }

        return out;
    }

    public String getRequestLine() {
        return requestLine;
    }

    public Integer[] getVersion() {
        String cmd[] = requestLine.split("\\s");
        if (cmd.length != 3) {
            return null;
        }
        String temp[] = null;

        if (cmd[2].indexOf("HTTP/") == 0 && cmd[2].indexOf('.') > 5) {
            temp = cmd[2].substring(5).split("\\.");
            try {
                return new Integer[] {Integer.parseInt(temp[0]), Integer.parseInt(temp[1])};
            } catch (NumberFormatException nfe) {
            }
        }
        return null;
    }

    public String getMethod() {
        return requestLine.split(" ")[0];
    }

    private void setRequestLine(String requestLine) throws HttpFormatException {
        if (requestLine == null || requestLine.length() == 0) {
            throw new HttpFormatException("Invalid Request-Line: " + requestLine);
        }
        this.requestLine = requestLine;
    }

    private void appendHeaderParameter(String header) throws HttpFormatException {
        int idx = header.indexOf(":");
        if (idx == -1) {
            throw new HttpFormatException("Invalid Header Parameter: " + header);
        }
        requestHeaders.put(header.substring(0, idx), header.substring(idx + 1, header.length()));
    }

    public String restoreHeader() {
        StringBuffer out = new StringBuffer();
        for(String key : requestHeaders.keySet()) {
            out.append(key+": "+requestHeaders.get(key)+eoln);
        }
        return out.toString();
    }

    public String getMessageBody() {
        return messageBody.toString();
    }

    private void appendMessageBody(String bodyLine) {
        messageBody.append(bodyLine).append(eoln);
    }

    public String getHeaderParam(String headerName){
        return requestHeaders.get(headerName);
    }

    public void setHeaderParam(String headerName, String paramValue){
        requestHeaders.put(headerName, paramValue);
    }

    public Set<String> getHeaderNames() {
        return requestHeaders.keySet();
    }

    public static final String eoln = "\r\n";

    public String restoreCall() {
        return requestLine+eoln+restoreHeader()+eoln+getMessageBody();
    }

}