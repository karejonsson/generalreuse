package general.reuse.http.analyse;

public class HttpFormatException extends Exception {

    public HttpFormatException(String message) {
        super(message);
    }

}
