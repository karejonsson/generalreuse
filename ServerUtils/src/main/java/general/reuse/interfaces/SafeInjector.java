package general.reuse.interfaces;

public interface SafeInjector<T, R> {
    void set(T t, R r);
}
