package general.reuse.interfaces;

public interface Treater<T, R> {
    R treat(T t) throws Exception;
}

