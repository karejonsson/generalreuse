package general.reuse.interfaces;

public interface RiskyInjector<T, R> {
    void set(T t, R r) throws Exception;
}
