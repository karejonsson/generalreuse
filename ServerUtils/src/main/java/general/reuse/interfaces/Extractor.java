package general.reuse.interfaces;

public interface Extractor<S, R> {
    R get(S s);
}
