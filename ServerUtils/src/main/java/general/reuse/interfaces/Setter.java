package general.reuse.interfaces;

public interface Setter<T> {
    void set(T s) throws Exception;
}
