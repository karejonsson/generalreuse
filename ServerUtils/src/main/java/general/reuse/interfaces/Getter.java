package general.reuse.interfaces;

public interface Getter<T> {
    T get();
}
