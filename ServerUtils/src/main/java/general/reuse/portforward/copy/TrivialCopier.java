package general.reuse.portforward.copy;

import general.reuse.portforward.engine.Cleaner;
import general.reuse.portforward.utils.ParseAdressUtils;
import general.reuse.portforward.utils.StreamUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.Socket;

public class TrivialCopier implements Copier {

    private static final Log log = LogFactory.getLog(TrivialCopier.class);

    private Socket in = null;
    private Socket out = null;
    private Cleaner cleaner = null;
    private boolean isCompleted = false;

    TrivialCopier(Socket in, Socket out, Cleaner cleaner) {
        this.in = in;
        this.out = out;
        this.cleaner = cleaner;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    @Override
    public void run() {
        try {
            StreamUtils.copyStream(in.getInputStream(), out.getOutputStream(), false);
        } catch (java.net.SocketException e) {
            ParseAdressUtils.shutdown(in);
            ParseAdressUtils.shutdown(out);
            log.debug(this, e);
        } catch (Exception e) {
            log.error(this, e);
        } finally {
            this.isCompleted = true;
            if (cleaner != null) synchronized (cleaner) {
                cleaner.notify();
            }
        }
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "(from " + in + " to " + out + ")";
    }

}
