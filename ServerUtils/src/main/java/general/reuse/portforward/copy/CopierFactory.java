package general.reuse.portforward.copy;

import general.reuse.portforward.engine.Cleaner;

import java.net.Socket;

public interface CopierFactory {

    Copier forRequest(Socket in, Socket out, Cleaner cleaner);
    Copier forReply(Socket in, Socket out, Cleaner cleaner);

}
