package general.reuse.portforward.copy;

public interface Copier extends Runnable {
    boolean isCompleted();
}
