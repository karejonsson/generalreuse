package general.reuse.portforward.copy;

import general.reuse.portforward.engine.Cleaner;

import java.net.Socket;

public class TrivialCopierFactory implements CopierFactory {

    @Override
    public Copier forRequest(Socket in, Socket out, Cleaner cleaner) {
        return new TrivialCopier(in, out, cleaner);
    }

    @Override
    public Copier forReply(Socket in, Socket out, Cleaner cleaner) {
        return new TrivialCopier(in, out, cleaner);
    }

}
