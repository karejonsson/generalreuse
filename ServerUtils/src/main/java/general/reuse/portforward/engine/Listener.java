/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package general.reuse.portforward.engine;

import general.reuse.portforward.copy.CopierFactory;
import general.reuse.portforward.copy.TrivialCopierFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Listener implements Runnable, Closeable {
    private static Log log = LogFactory.getLog(Listener.class);
    private ServerSocket serverSocket;
    private InetSocketAddress from, to;
    private CopierFactory copierFactory;

    private Throwable exception;
    private final Cleaner cleaner = new Cleaner();

    public Throwable getException() {
        return exception;
    }

    public Listener(InetSocketAddress from, InetSocketAddress to) throws IOException {
        this(from, to, new TrivialCopierFactory());
    }

    public Listener(InetSocketAddress from, InetSocketAddress to, CopierFactory copierFactory) throws IOException {
        this.from = from;
        this.to = to;
        this.copierFactory = copierFactory;
        serverSocket = new ServerSocket();
        serverSocket.setReuseAddress(true);
        serverSocket.bind(from);
        String hostname = from.getHostName();
        if (hostname == null) hostname = "*";
        log.info("Ready to accept client connection on " + hostname + ":" + from.getPort());
    }

    @Override
    public void run() {
        Socket source = null;
        new Thread(cleaner).start();
        while (true) {
            try {
                TargetConnector connector = new TargetConnector(to);
                source = serverSocket.accept();
                log.trace("accepted client connection");
                Socket target = connector.openSocket();
                new Processor(copierFactory, source, target, cleaner).process();
            } catch (IOException e) {
                String msg = "Failed to accept client connection on port " + from.getPort();
                log.error(msg, e);
                exception = e;
                return;
            }
        }
    }

    @Override
    public void close() {
        if (!serverSocket.isClosed()) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

}
