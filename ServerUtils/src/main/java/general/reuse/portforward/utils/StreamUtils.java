/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package general.reuse.portforward.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class StreamUtils {

    private static final Log log = LogFactory.getLog(StreamUtils.class);
    private static final int BUF_SIZE = 512;

    public static void copyStream(InputStream in, OutputStream out) throws IOException {
        copyStream(in, out, true);
    }

    public static void copyStream(InputStream in, OutputStream out, boolean closeOnFinish) throws IOException {
        byte[] buf = new byte[BUF_SIZE];
        int count;
        try {
            while ((count = in.read(buf)) != -1) {
                System.out.println("Count = "+count);
                out.write(buf, 0, count);
            }
        } finally {
            if(closeOnFinish) {
                close(in);
                close(out);
            }
        }
    }

    public static byte[] copyStreamWithEcho(InputStream in, OutputStream out, boolean closeOnFinish) throws IOException {
        byte[] buf = new byte[BUF_SIZE];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int count;
        try {
            while ((count = in.read(buf)) != -1) {
                System.out.println("--Count = "+count);
                out.write(buf, 0, count);
                baos.write(buf, 0, count);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            if(closeOnFinish) {
                try {
                    close(in);
                    close(out);
                    close(baos);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            byte[] BA = baos.toByteArray();
            System.out.println("Svarslängd = "+BA.length);
            return BA;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] getBytes(InputStream in, boolean closeOnFinish) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        copyStream(in, baos, closeOnFinish);
        return baos.toByteArray();
    }

    public static void close(InputStream in) {
        close(in, false);
    }

    public static void close(InputStream in, boolean logError) {
        if (in != null) {
            try {
                in.close();
            } catch (Throwable t) {
                if (logError) log.error(t.getMessage(), t);
                else log.debug(t.getMessage(), t);
            }
        }
    }

    public static void close(OutputStream out) {
        close(out, false);
    }

    public static void close(OutputStream out, boolean logError) {
        if (out != null) {
            try {
                out.close();
            } catch (Throwable t) {
                if (logError) log.error(t.getMessage(), t);
                else log.debug(t.getMessage(), t);
            }
        }
    }

}
