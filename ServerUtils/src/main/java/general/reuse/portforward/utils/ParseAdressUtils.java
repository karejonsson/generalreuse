/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package general.reuse.portforward.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.InetSocketAddress;
import java.net.Socket;

public final class ParseAdressUtils {

    private static final Log log = LogFactory.getLog(ParseAdressUtils.class);

    public static InetSocketAddress parseInetSocketAddress(String endPoint) {
        String hostname = null;
        String portString;
        int port;

        int index = endPoint.indexOf(":");
        if (index >= 0) {
            hostname = endPoint.substring(0, index);
            portString = endPoint.substring(index + 1);
        } else {
            portString = endPoint;
        }
        port = Integer.parseInt(portString);
        return hostname == null ? new InetSocketAddress(port) : new InetSocketAddress(hostname, port);
    }

    public static void close(Socket socket) {
        if (socket == null) return;

        if (!socket.isClosed()) {
            shutdown(socket);
            try {
                socket.close();
            } catch (Throwable e) {
                log.debug(e.getMessage(), e);
            }
        }
    }

    public static void shutdownInput(Socket socket) {
        if (socket == null) return;
        try {
            if (!socket.isInputShutdown()) socket.shutdownInput();
        } catch (Throwable e) {
            log.debug(e.getMessage(), e);
        }
    }

    public static void shutdownOutput(Socket socket) {
        if (socket == null) return;
        try {
            if (!socket.isOutputShutdown()) socket.shutdownOutput();
        } catch (Throwable e) {
            log.debug(e.getMessage(), e);
        }
    }

    public static void shutdown(Socket socket) {
        shutdownInput(socket);
        shutdownOutput(socket);
    }

}
