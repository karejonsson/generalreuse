package se.prv.mailing;

public interface ConfiguredMailer {

	void send(String email, String title, String message);
	
}
