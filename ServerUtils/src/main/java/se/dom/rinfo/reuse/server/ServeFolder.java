package se.dom.rinfo.reuse.server;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.resource.Directory;

public class ServeFolder {

	public static Component serveFolderOnPort(final String folder, int port) throws Exception {
		return serveFolderOnPort(folder, port, null, null);
	}

	
	public static Component serveFolderOnPort(final String folder, int port, Integer maxThreads, Boolean loggingOff) throws Exception {
		System.out.println("Starts serving folder "+folder+" on port "+port);
        Component component = new Component();
        component.getServers().add(Protocol.HTTP, port);
        component.getClients().add(Protocol.FILE);
        
        if(maxThreads != null) {
            component.getContext().getParameters().add("maxThreads", ""+maxThreads);
        }
        
        if(loggingOff != null && loggingOff == true) {
            Logger logger = component.getContext().getLogger();
            logger.setLevel(Level.OFF);
        }

        component.getDefaultHost().attach("", new Application() {
            public Restlet createRoot() { 
				return new Directory(getContext(), "file://"+folder);
            }});
        component.start();
        return component;
	}

}
