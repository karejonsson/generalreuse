package general.reuse.portforward.copy;

import general.reuse.http.analyse.HttpRequestStructure;
import general.reuse.portforward.engine.Cleaner;
import general.reuse.portforward.utils.ParseAdressUtils;
import general.reuse.portforward.utils.StreamUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import general.reuse.webmanipulation.Download;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.Socket;

public class HttpInspectCopier implements Copier {

    private static final Log log = LogFactory.getLog(TrivialCopier.class);

    private Socket in = null;
    private Socket out = null;
    private Cleaner cleaner = null;
    private boolean isCompleted = false;

    HttpInspectCopier(Socket in, Socket out, Cleaner cleaner) {
        this.in = in;
        this.out = out;
        this.cleaner = cleaner;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    @Override
    public void run() {
        try {
            /*
            System.out.println("1");
            InputStream is = in.getInputStream();
            System.out.println("2");
            byte[] ba = StreamUtils.getBytes(is, false);
            System.out.println("3");
            is.close();
            System.out.println("4");
            HttpRequestStructure struct = HttpRequestStructure.parseRequest(ba);
            System.out.println("5");
            String call = struct.restoreCall();
            System.out.println("6 - "+call);
            System.out.println("7");
            ByteArrayInputStream bais = new ByteArrayInputStream(call.getBytes());
            System.out.println("8");
            StreamUtils.copyStream(bais, out.getOutputStream(), false);
            System.out.println("9 - ok");
             */
            byte[] echo = StreamUtils.copyStreamWithEcho(in.getInputStream(), out.getOutputStream(), false);
            HttpRequestStructure struct = HttpRequestStructure.parseRequest(echo);
            String call = struct.restoreCall();
            System.out.println("HTTP Anrop "+call);
        } catch (java.net.SocketException e) {
            ParseAdressUtils.shutdown(in);
            ParseAdressUtils.shutdown(out);
            log.debug(this, e);
        } catch (Exception e) {
            log.error(this, e);
        } finally {
            log.info("OK");
            this.isCompleted = true;
            if (cleaner != null) synchronized (cleaner) {
                cleaner.notify();
            }
        }
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "(from " + in + " to " + out + ")";
    }

}
