package general.reuse.portforward.start;

import general.reuse.portforward.copy.HttpInspectCopierFactory;
import general.reuse.portforward.engine.Listener;
import general.reuse.portforward.utils.ParseAdressUtils;

public class HttpInspectForwarderTest {

    public static void main(String[] args) throws Exception {
        Listener listener = new Listener(
                ParseAdressUtils.parseInetSocketAddress("8282"),
                ParseAdressUtils.parseInetSocketAddress("localhost:8080"),
                new HttpInspectCopierFactory());
        listener.run();
        listener.close();
    }

}
