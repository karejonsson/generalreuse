package general.reuse.swingutils.illustration;

public class HeatSpotSinus implements HeatSpot {

	private static final long twoPiMillis = (long) (2*Math.PI*1000);
	private long val = 1000l*((long) (2*Math.PI*Math.random()));
	private long start;
	private double freq;
	
	public HeatSpotSinus() {
		init();
	}
	
	private void init() {
		start = System.currentTimeMillis();
		freq = 0.5+1.0*Math.random();
	}
	
	public double getHeat() {
		long elapsed = System.currentTimeMillis() - start;
		if(elapsed > 10000) {
			init();
		}
		double secs = ((double) ((val + elapsed) % twoPiMillis)) / 1000;
		double out = 0.5+0.5*Math.sin((double) secs*freq);
		return out;
	}

}
