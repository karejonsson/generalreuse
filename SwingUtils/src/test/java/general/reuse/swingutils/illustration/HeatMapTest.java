package general.reuse.swingutils.illustration;

import java.awt.Color;

import javax.swing.JFrame;

import general.reuse.swingutils.colors.VariousGenerators;

public class HeatMapTest {

	public static void main(String args[]) {
		JFrame f = new JFrame();
		f.setSize(1200, 800);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.setVisible(true);
		VariousGenerators vg = new VariousGenerators(Color.red, Color.blue);
		//VariousGenerators vg = new VariousGenerators(Color.white, Color.black);
		HeatMap hm = new HeatMap(
				getGrid(3, 2),
				true, 
				vg.getColors(100)); 
		f.getContentPane().add(hm);

		f.invalidate();
		f.validate();
		iter(hm);
	}

	public static HeatSpot[] getColumn(int w) {
		HeatSpot[] out = new HeatSpot[w];
		for(int i = 0 ; i < w ; i++) {
			out[i] = new HeatSpotSinus();
		}
		return out;
	}

	public static HeatSpot[][] getGrid(int w, int h) {
		HeatSpot[][] out = new HeatSpot[w][];
		for(int i = 0 ; i < w ; i++) {
			out[i] = getColumn(h);
		}
		return out;
	}

	public static void iter(HeatMap hm) {
		while(true) {
			try { Thread.sleep(100); } catch (InterruptedException e) { }
			hm.updateData();	  
		}
	}

}
