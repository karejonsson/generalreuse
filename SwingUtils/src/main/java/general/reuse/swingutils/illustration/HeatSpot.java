package general.reuse.swingutils.illustration;

public interface HeatSpot {
	
	public double getHeat();
	
}

