package general.reuse.swingutils.colors;

import java.awt.Color;

public class VariousGenerators {

	public Color to;
	public Color base;
	
	public VariousGenerators(Color base, Color to) {
		this.base = base; 
		this.to= to;
	}

	public Color getColor(double d) {
		int red = base.getRed()+((int) (d*(to.getRed()-base.getRed())));
		int green = base.getGreen()+((int) (d*(to.getGreen()-base.getGreen())));
		int blue = base.getBlue()+((int) (d*(to.getBlue()-base.getBlue())));
		//System.out.println("R "+red+", G "+green+", B "+blue);
		return new Color(red, green, blue);
	}

	public Color[] getColors(int steps) {
		double frac = 1.0f/steps;
		Color[] out = new Color[steps];
		for(int i = 0 ; i < steps ; i++) {
			out[i] = getColor(frac*i);
		}
		return out;
	}

}
