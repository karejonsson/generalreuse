package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dialog.Dialog;

public class WindowV8ish extends Dialog {
    public WindowV8ish() {
        super();
    }

    public WindowV8ish(String title) {
        super();
    }

    public void setContent(Component c) {
        removeAll();
        add(c);
    }

    public void center() {
    }

    public void setPrimaryStyleName(String cssname) {
    }

}
