package general.reuse.vaadinreusable.managedapp.navigation;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.icon.VaadinIcon;
import general.reuse.vaadinreusable.component.ShowLongText;
import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;
import general.reuse.vaadinreusable.managedapp.factories.PanelFactory;
import general.reuse.vaadinreusable.managedapp.functionality.BaseAdminFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.functionality.CommonRunnable;
import general.reuse.vaadinreusable.managedapp.internalchat.ChatButton;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;
import general.reuse.vaadinreusable.vaadin8ish.*;
import org.apache.log4j.Logger;

import java.util.Map;

public class NavigationPanel extends PanelV8ish {

	private static Logger logg = Logger.getLogger(NavigationPanel.class);
	private static final long serialVersionUID = -1335626300083283997L;
	private BaseAdminFunctionalityProvider adminFunctionality = null;

	private BaseFunctionalityProvider functionality = null;
	private CommonRunnable onBack = null;
	private UIV8ish ui = null;

	public NavigationPanel(BaseAdminFunctionalityProvider adminFunctionality, UIV8ish ui, CommonRunnable onBack) {
		PanelFactory.styles(this);
		this.ui = ui;
		this.adminFunctionality = adminFunctionality;
		functionality = adminFunctionality.getFunctionality();
		adminFunctionality.updateUidname();
		
		this.onBack = onBack;
		VerticalLayoutV8ish vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		//vl.addComponent(LabelFactory.create("AdminPanel"));
		
		HorizontalLayoutV8ish collegues = LayoutFactory.createHorizontal(functionality.getHaveMargins());
		collegues.setCaption("Medarbetare");
		
		ButtonV8ish activeUsersBtn = ButtonFactory.create(ButtonTexts.activeUsers);
		activeUsersBtn.addClickListener(e -> adminFunctionality.openActiveManagement(new CommonRunnable(() -> adminFunctionality.openAdmin(onBack))));
		collegues.addComponent(activeUsersBtn);

		vl.addComponent(collegues);

		HorizontalLayoutV8ish infrastructure = LayoutFactory.createHorizontal(functionality.getHaveMargins());
		infrastructure.setCaption("Infrastruktur");

		ButtonV8ish chatBtn = ChatButton.createAlwaysStateConsidered(adminFunctionality.getFunctionality());
		if(chatBtn != null) {
			infrastructure.addComponent(chatBtn);
		}

		ButtonV8ish fileHandlingBtn = ButtonFactory.create(ButtonTexts.fileHandling);
		fileHandlingBtn.addClickListener(e -> adminFunctionality.openFileHandling(new CommonRunnable(() -> adminFunctionality.openAdmin(onBack))));
		infrastructure.addComponent(fileHandlingBtn);

		ButtonV8ish statesBtn = ButtonFactory.create(ButtonTexts.states);
		statesBtn.addClickListener(e -> UsersActivePanel.manageGlobalSettings(functionality));
		infrastructure.addComponent(statesBtn);

		vl.addComponent(infrastructure);

		HorizontalLayoutV8ish flow = LayoutFactory.createHorizontal(functionality.getHaveMargins());
		
		ButtonV8ish backBtn = ButtonFactory.create(ButtonTexts.revert);
		backBtn.addClickListener(e -> onBack.run());
		flow.addComponent(backBtn);
		
		ButtonV8ish startBtn = ButtonFactory.create(ButtonTexts.start);
		startBtn.addClickListener(e -> adminFunctionality.start());
		flow.addComponent(startBtn);
		
		ButtonV8ish logoutBtn = ButtonFactory.create(ButtonTexts.logout, VaadinIcon.POWER_OFF);
		logoutBtn.addClickListener(e -> adminFunctionality.getFunctionality().logout(adminFunctionality.getFunctionality().getRunnableForReturnToCurrentContent()));
		flow.addComponent(logoutBtn);

		vl.addComponent(flow);
		
		setContent(vl);
	}

	private interface CountersGetter {
		Map<String, Long> get();
	}

	private Component getCounter(String name, final CountersGetter getter) {
		ButtonV8ish outBtn = ButtonFactory.create(name);
		Map<String, Long> m = getter.get();
		StringBuffer sb = new StringBuffer();
		sb.append("Alla tabeller: "+m.get("all")+"\n");
		for(String key : m.keySet()) {
			if(key.equals("all")) {
				continue;
			}
			sb.append(key+": "+m.get(key)+"\n");
		}
		String text = sb.toString();
		outBtn.addClickListener(e -> {
			ShowLongText slt = new ShowLongText(ui, "Räknare", "DB: "+name, ButtonTexts.close, text, true);
			functionality.addWindow(slt, "Räknare: "+name);
		});
		return outBtn;
	}

}
