package general.reuse.vaadinreusable.parameters;

import general.reuse.database.parameters.domain.PersistentValue;

import java.util.List;

public interface PersistentValuePersister {

	List<PersistentValue> getAllPersistentValues() throws Exception;
	PersistentValue getNewPersistentValue();
	void removeValue(String key) throws Exception;
	void saveAll();

}
