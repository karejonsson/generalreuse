package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.splitlayout.SplitLayout;

public class HorizontalSplitPanelV8ish extends SplitLayout {

    public HorizontalSplitPanelV8ish() {
        setOrientation(Orientation.HORIZONTAL);
    }

    public void setCaption(String caption) {
    }

    public void setSplitPosition(int i, UnitV8ish unit) {
        unit.set(this, i);
    }

    public void setFirstComponent(Component first) {
        addToPrimary(first);
    }

    public void setSecondComponent(Component second) {
        addToSecondary(second);
    }

    public void setPrimaryStyleName(String csshorizontalname) {
        setClassName(csshorizontalname);
    }

}
