package general.reuse.vaadinreusable.component;

import general.reuse.vaadinreusable.vaadin8ish.*;

public class EnterLongText extends WindowV8ish {

	private static final long serialVersionUID = -5028474051361059691L;
	public final static double outer = 0.8;
	//public final static double outer_W = outer;
	//public final static double outer_H = outer;
	public final static double inner = 0.68;
	//public final static double inner_W = inner;
	//public final static double inner_H = inner;

	private UIV8ish ui = null;
	private TextAreaV8ish ta = null;
	private Runnable onDoneCancel = null;
	private Runnable onDoneOk = null;
	
	private double internal_inner;
	private double internal_outer;

	public EnterLongText(UIV8ish ui, String text, Runnable onDoneOk) {
		this(ui, "", "Specifikation", "Avbryt", "Spara", text, null, null, onDoneOk, inner, outer);
	}

	public EnterLongText(UIV8ish ui, String frame, String text, Runnable onDoneOk) {
		this(ui, frame, "Specifikation", "Avbryt", "Spara", text, null, null, onDoneOk, inner, outer);
	}

	public EnterLongText(UIV8ish ui, String frame, String componentText, String cancelButtonText, String okButtonText, String text, Integer maxLength) {
		this(ui, frame, componentText, cancelButtonText, okButtonText, text, maxLength, null, null, inner, outer);
	}

	public EnterLongText(UIV8ish ui, String frame, String componentText, String cancelButtonText, String okButtonText, String text, Integer maxLength, double internal_inner, double internal_outer) {
		this(ui, frame, componentText, cancelButtonText, okButtonText, text, maxLength, null, null, internal_inner, internal_outer);
	}

	public EnterLongText(UIV8ish ui, String frame, String componentText, String cancelButtonText, String okButtonText, String text, Integer maxLength, Runnable onDoneCancel, Runnable onDoneOk, double internal_inner, double internal_outer) {
		//super(frame);
		this.ui = ui;
		setModal(true);
		this.onDoneCancel = onDoneCancel;
		this.onDoneOk = onDoneOk;
		this.internal_inner = internal_inner;
		this.internal_outer = internal_outer;

		FormLayoutV8ish content = new FormLayoutV8ish();
		content.setWidth(""+(((int) (ui.getBrowserWindowWidth()*internal_outer))-0)+"px");
		content.setHeight(""+(((int) (ui.getBrowserWindowHeight()*internal_outer))-0)+"px");

		ta = new TextAreaV8ish(componentText);
		if(maxLength != null) {
			ta.setMaxLength(maxLength);
		}
		ta.setValue(text);
		ta.setWidth(""+(((int) (ui.getBrowserWindowWidth()*internal_inner))-85)+"px");
		ta.setHeight(""+(((int) (ui.getBrowserWindowHeight()*internal_inner))-70)+"px");
		content.addComponent(ta);

		HorizontalLayoutV8ish buttons = new HorizontalLayoutV8ish();

		ButtonV8ish doneBtn = new ButtonV8ish(okButtonText);
		doneBtn.addClickListener(e -> {
			close();
			if(this.onDoneOk != null) {
				this.onDoneOk.run();
			}
		});

		ButtonV8ish cancelBtn = new ButtonV8ish(cancelButtonText);
		cancelBtn.addClickListener(e -> {
			ta = null;
			close();
			if(this.onDoneCancel != null) {
				this.onDoneCancel.run();
			}
		});

		buttons.addComponent(doneBtn);
		buttons.addComponent(cancelBtn);

		content.addComponent(buttons);
		setContent(content);
		ta.focus();
	}
	
	public void setOnDone(Runnable onDone) {
		this.onDoneOk = onDone;
	}

	public void setOnDoneCancel(Runnable onDoneCancel) {
		this.onDoneCancel = onDoneCancel;
	}

	public void setOnDoneOk(Runnable onDoneOk) {
		this.onDoneOk = onDoneOk;
	}

	public String getText() {
		if(ta == null) {
			return null;
		}
		String out = ta.getValue();
		return out;
	}

	public void setEnabled(boolean enabled) {
		ta.setEnabled(enabled);
	}

	public void setMaxLength(int maxlen) {
		ta.setMaxLength(maxlen);
	}

}

