package general.reuse.vaadinreusable.choser;

import com.vaadin.flow.component.icon.VaadinIcon;
import general.reuse.vaadinreusable.vaadin8ish.*;

public class QuestionAndButtonsWindow extends WindowV8ish {
	
	private static final long serialVersionUID = 1845565644184742832L;
	
	private String title;
	private String question;
	private HorizontalLayoutV8ish hl = new HorizontalLayoutV8ish();

	public QuestionAndButtonsWindow(String title, String question) {
		this.title = title;
		this.question = question;
		VerticalLayoutV8ish vl = new VerticalLayoutV8ish();
		vl.addComponent(new LabelV8ish(question));
		vl.addComponent(hl);
		setContent(vl);
	}

	public void addButtonWithClose(String text, Runnable onPress) {
		addButtonWithClose(null, text, onPress);
	}

	public void addButtonWithClose(VaadinIcon icon, Runnable onPress) {
		addButtonWithClose(icon, null, onPress);
	}

	public void addButtonWithClose(VaadinIcon icon, String text, Runnable onPress) {
		ButtonV8ish b = null;
		if(icon != null && text != null) {
			b = new ButtonV8ish(text, icon);
		}
		else if(icon == null) {
			b = new ButtonV8ish(text);
		}
		else if(text == null) {
			b = new ButtonV8ish(icon);
		}
		else {
			b = new ButtonV8ish();
		}
		final ButtonV8ish bf = b;
		b.addClickListener(e -> { bf.setEnabled(false); close() ; onPress.run(); } );
		hl.addComponent(b);
	}

	public void addButtonWithoutClose(String text, Runnable onPress) {
		addButtonWithoutClose(null, text, onPress);
	}

	public void addButtonWithoutClose(VaadinIcon icon, Runnable onPress) {
		addButtonWithoutClose(icon, null, onPress);
	}

	public void addButtonWithoutClose(VaadinIcon icon, String text, Runnable onPress) {
		ButtonV8ish b = null;
		if(icon != null && text != null) {
			b = new ButtonV8ish(text, icon);
		}
		else if(icon == null) {
			b = new ButtonV8ish(text);
		}
		else if(text == null) {
			b = new ButtonV8ish(icon);
		}
		else {
			b = new ButtonV8ish();
		}
		final ButtonV8ish bf = b;
		b.addClickListener(e -> { bf.setEnabled(false); onPress.run(); } );
		hl.addComponent(b);
	}

}
