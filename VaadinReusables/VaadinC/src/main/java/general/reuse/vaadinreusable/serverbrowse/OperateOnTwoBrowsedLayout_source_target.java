package general.reuse.vaadinreusable.serverbrowse;

import general.reuse.vaadinreusable.vaadin8ish.*;
import org.apache.log4j.Logger;

import java.io.File;

public class OperateOnTwoBrowsedLayout_source_target extends VerticalLayoutV8ish {

    private static Logger logg = Logger.getLogger(OperateOnTwoBrowsedLayout_source_target.class);

    private ForeignFrameworksResources functionality = null;
    private OperateOnTwoBrowsed_source_target[] oots = null;

    private TextFieldV8ish source = null;
    private TextFieldV8ish target = null;

    public OperateOnTwoBrowsedLayout_source_target(ForeignFrameworksResources functionality, OperateOnTwoBrowsed_source_target[] oots) {
        functionality.styles(this);
        this.functionality = functionality;
        this.oots = oots;
        setupLayout();
    }

    private void setupLayout() {
        addComponent(new LabelV8ish(functionality.getTwoParametersText()));
        HorizontalLayoutV8ish sourceLayout = new HorizontalLayoutV8ish();
        sourceLayout.setWidthFull();
        source = new TextFieldV8ish("Källfil");
        source.setWidthFull();
        source.setHeight("40px");
        sourceLayout.addComponent(source);
        ButtonV8ish b1Btn = new ButtonV8ish(functionality.getBrowseText());
        b1Btn.addClickListener(e -> browse(source));
        sourceLayout.addComponent(b1Btn);
        addComponent(sourceLayout);

        HorizontalLayoutV8ish targetLayout = new HorizontalLayoutV8ish();
        targetLayout.setWidthFull();
        target = new TextFieldV8ish("Mål");
        target.setWidthFull();
        target.setHeight("40px");
        targetLayout.addComponent(target);
        ButtonV8ish b2Btn = new ButtonV8ish(functionality.getBrowseText());
        b2Btn.addClickListener(e -> browse(target));
        targetLayout.addComponent(b2Btn);
        addComponent(targetLayout);

        for(OperateOnTwoBrowsed_source_target oot : oots) {
            ButtonV8ish actionBtn = new ButtonV8ish(oot.getName());
            actionBtn.addClickListener(e -> execute(oot));
            actionBtn.setDescription(oot.getDescription());
            addComponent(actionBtn);
        }
    }

    private void browse(TextFieldV8ish tf) {
        TreeV8ish<File> tree = OperationsReuse.getBrowser(tf);
        WindowV8ish win = new WindowV8ish();
        win.setWidth("80%");
        win.setHeight("80%");
        VerticalLayoutV8ish l = new VerticalLayoutV8ish();
        l.addComponent(tree);
        win.setContent(l);
        functionality.addWindow(win, "Bläddra");
    }

    private void execute(OperateOnTwoBrowsed_source_target oot) {
        try {
            oot.operate(new File(source.getValue()), target.getValue());
        }
        catch(Exception e) {
            String msg = "Oväntat fel vid operationen. Underliggande felmeddelande\n"+e.getMessage();
            logg.error(msg+". source.getValue()="+source.getValue()+", target.getValue()="+target.getValue(), e);
            functionality.showError(msg);
            return;
        }
    }

}
