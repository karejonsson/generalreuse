package general.reuse.vaadinreusable.managedapp.factories;

import general.reuse.vaadinreusable.vaadin8ish.TabSheetV8ish;

public class TabSheetFactory {

	public final static String cssname = "cmdlwebtabsheet";
	private static final boolean isStyled = false;
	
	public static TabSheetV8ish styles(TabSheetV8ish in) {
		if(isStyled) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}
	
	public static TabSheetV8ish create() {
		return styles(new TabSheetV8ish());
	}

}
