package general.reuse.vaadinreusable.managedapp.factories;

import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;

public class WindowFactory {
	
	public final static String cssname = "cmdlwebwindow";
	private static final boolean isStyled = false;
	
	public static WindowV8ish styles(WindowV8ish in) {
		if(isStyled) {
			// Vaadin 20 in.setThemeName(cssname);
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static WindowV8ish create() {
		return styles(new WindowV8ish());
	}

}
