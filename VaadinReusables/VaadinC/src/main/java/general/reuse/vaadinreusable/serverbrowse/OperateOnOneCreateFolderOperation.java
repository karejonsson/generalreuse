package general.reuse.vaadinreusable.serverbrowse;

import org.apache.log4j.Logger;

import java.io.File;

public class OperateOnOneCreateFolderOperation implements OperateOnOneBrowsed {

    private static Logger logg = Logger.getLogger(OperateOnOneCreateFolderOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnOneCreateFolderOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Skapa katalog";
    }

    @Override
    public String getDescription() {
        return "Skapar en katalog";
    }

    @Override
    public Runnable getOperate(String filepath) {
        return () -> {
            operate(filepath);
        };
    }

    @Override
    public void operate(String filepath) {
        logg.info("filepath="+filepath);

        File f = new File(filepath);
        if(f.exists()) {
            String msg = "Katalogen/filen finns redan";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        File p = f.getParentFile();
        if(!p.exists()) {
            String msg = "Nivån ett stteg upp finns inte";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        if(f.mkdir()) {
            String msg = "Katalogen är skapad";
            logg.info(msg+". filepath="+filepath);
            functionality.showAssistive(msg);
            return;
        }
        else {
            String msg = "Katalogen kunde inte skapas";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
    }

}
