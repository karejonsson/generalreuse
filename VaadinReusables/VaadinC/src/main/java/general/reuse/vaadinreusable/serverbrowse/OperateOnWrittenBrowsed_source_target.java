package general.reuse.vaadinreusable.serverbrowse;

import java.io.File;

public interface OperateOnWrittenBrowsed_source_target {

    String getName();
    String getDescription();
    void operate(String written, File folder);
    Runnable getOperate(String written, File folder);

    // https://stackoverflow.com/questions/3411480/how-to-resume-an-interrupted-download

}
/*
import java.io.*;
import java.net.*;


public class HttpUrlDownload {

    public static void main(String[] args) {
       String strUrl = "http://VRSDLSCEN001:80//DLS//lib//clics.jar";
       String DESTINATION_PATH = "clics.jar";

       int count = 0;
       while (true) {
          count++;
          if (download(strUrl, DESTINATION_PATH) == true || count > 20) {
             break;
          }
       }
    }

    public static boolean download(String strUrl, String DESTINATION_PATH) {
        BufferedInputStream in = null;
        FileOutputStream fos = null;
        BufferedOutputStream bout = null;
        URLConnection connection = null;

        int downloaded = 0;

        try {
           System.out.println("mark ... download start");
           URL url = new URL(strUrl);

           connection = url.openConnection();

           File file=new File(DESTINATION_PATH);
           if(file.exists()){
               downloaded = (int) file.length();
           }
           if (downloaded == 0) {
               connection.connect();
           }
           else {
               connection.setRequestProperty("Range", "bytes=" + downloaded + "-");
               connection.connect();
           }

           try {
               in = new BufferedInputStream(connection.getInputStream());
           } catch (IOException e) {
               int responseCode = 0;
               try {
                   responseCode = ((HttpURLConnection)connection).getResponseCode();
               } catch (IOException e1) {
                 e1.printStackTrace();
               }

               if (responseCode == 416) {
                   return true;
               } else {
                   e.printStackTrace();
                   return false;
               }
           }

           fos=(downloaded==0)? new FileOutputStream(DESTINATION_PATH): new FileOutputStream(DESTINATION_PATH,true);
           bout = new BufferedOutputStream(fos, 1024);

           byte[] data = new byte[1024];
           int x = 0;
           while ((x = in.read(data, 0, 1024)) >= 0) {
              bout.write(data, 0, x);
           }

           in.close();
           bout.flush();
           bout.close();
           return false;

       } catch (IOException e) {
           e.printStackTrace();
           return false;
       } finally {
           if (in != null) {
               try {
                   in.close();
               } catch (IOException e) {
               }
           }
           if (fos != null) {
               try {
                   fos.close();
               } catch (IOException e) {
               }
           }
           if (bout != null) {
               try {
                   bout.close();
               } catch (IOException e) {
               }
           }

           if (connection != null) {
              ((HttpURLConnection)connection).disconnect();
           }
       }
    }
}
 */
