package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class VerticalLayoutV8ish extends VerticalLayout {

    public Component getComponent(int index) {
        return super.getComponentAt(index);
    }

    public void addComponent(Component c) {
        add(c);
    }

    public void removeComponent(Component c) {
        remove(c);
    }

    public void setPrimaryStyleName(String cssverticalname) {
        setClassName(cssverticalname);
    }

    public void setExpandRatio(Component c, float v) {
        setFlexGrow(v, c);
    }

    public void removeAllComponents() {
        removeAll();
    }

    public void setCaption(String onCurrentText) {
    }

    public void setComponentAlignment(Component comp, AlignmentV8ish alm) {
        
    }

}
