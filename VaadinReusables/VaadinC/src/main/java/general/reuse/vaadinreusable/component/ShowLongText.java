package general.reuse.vaadinreusable.component;

import general.reuse.vaadinreusable.vaadin8ish.*;

public class ShowLongText extends WindowV8ish {
	
	private static final long serialVersionUID = 7884499119391716345L;
	public final static double outer = 0.8;
	public final static double outer_W = outer;
	public final static double outer_H = outer;
	public final static double inner = 0.68;
	public final static double inner_W = inner;
	public final static double inner_H = inner;

	private UIV8ish ui = null;
	private TextAreaV8ish layout = null;

	public ShowLongText(UIV8ish ui, String text) {
		this(ui, "", text, false);
	}

	public ShowLongText(UIV8ish ui, String frame, String text) {
		this(ui, frame, text, false);
	}

	public ShowLongText(UIV8ish ui, String text, boolean enabled) {
		this(ui, "", "Specifikation", "Avbryt", text, enabled);
	}

	public ShowLongText(UIV8ish ui, String frame, String text, boolean enabled) {
		this(ui, "", "Specifikation", "Avbryt", text, enabled);
	}
	
	public ShowLongText(UIV8ish ui, String frame, String componentText, String closeButtonText, String text, boolean enabled) {
		//super(frame);
		this.ui = ui;
		setModal(true);

		FormLayoutV8ish content = new FormLayoutV8ish();
		content.setWidth(""+((int) ui.getBrowserWindowWidth()*outer)+"px");
		content.setHeight(""+((int) ui.getBrowserWindowHeight()*outer)+"px");
		
		layout = new TextAreaV8ish(componentText);
		layout.setValue(text);
		layout.setEnabled(enabled);
		layout.setWidth(""+((int) ui.getBrowserWindowWidth()*inner_W)+"px");
		layout.setHeight(""+((int) ui.getBrowserWindowHeight()*inner_H)+"px");
		content.addComponent(layout);

		HorizontalLayoutV8ish buttons = new HorizontalLayoutV8ish();
		
		ButtonV8ish cancelBtn = new ButtonV8ish(closeButtonText);
		cancelBtn.addClickListener(e -> close());
		buttons.addComponent(cancelBtn);
		
		content.addComponent(buttons);
		setContent(content);
	}
	
    public void setEnabledText(boolean enabledState) {
    	layout.setEnabled(enabledState);
    }   

}

