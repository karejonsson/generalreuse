package general.reuse.vaadinreusable.serverbrowse;

import com.vaadin.flow.component.Component;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;

public interface ForeignFrameworksResources {

    boolean getHaveMargins();
    void showWarning(String s);
    void showAssistive(String s);
    void showError(String s);
    void addWindow(WindowV8ish win);
    void addWindow(WindowV8ish win, String winType);
    void styles(Component components);

    String getParseProgramAsGroovyText();
    String getExecuteProgramAsGroovyText();
    String getBrowseText();
    String getEditText();
    String getCancelText();
    String getSaveText();
    String getOneParameterText();
    String getFileText();
    String getRevertText();
    String getOnCurrentText();
    String getTwoParametersText();
}
