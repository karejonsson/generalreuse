package general.reuse.vaadinreusable.serverbrowse;

import general.reuse.vaadinreusable.vaadin8ish.LinkV8ish;
//import general.reuse.vaadinreusable.vaadin8ish.StreamResourceV8ish;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;

public class OperateOnOneDownloadOperation implements OperateOnOneBrowsed {

    private static Logger log = Logger.getLogger(OperateOnOneDownloadOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnOneDownloadOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Nedladdning";
    }

    @Override
    public String getDescription() {
        return "Ladda ner fil";
    }

    @Override
    public Runnable getOperate(String filepath) {
        return () -> {
            operate(filepath);
        };
    }

    @Override
    public void operate(String filepath) {
        log.info("filepath="+filepath);

        File f = new File(filepath);
        if(!f.exists()) {
            String msg = "Filen finns inte";
            log.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }

        if(f.isDirectory()) {
            final String msg = "Det är en katalog";
            log.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }

        DocumentLink receiver = null;
        try {
            receiver = new DocumentLink(new File(filepath));
        }
        catch(Exception e) {
            final String msg = "Kunde inte skapa en länk";
            log.error(msg+". filepath="+filepath, e);
            functionality.showError(msg);
            return;
        }
        WindowV8ish win = new WindowV8ish();
        win.setHeight("60px");
        win.setWidth("40%");
        win.setContent(receiver);
        functionality.addWindow(win, "Fil att ladda ner");
    }

    public class DocumentLink extends LinkV8ish {

        private static final long serialVersionUID = -861816214400624227L;
        private File file;

        public DocumentLink(File file) throws FileNotFoundException {
            super("Nedladdning av fil", file.getName(), file);
            this.file = file;
            //setCaption("Nedladdning av fil");
            //setDescription("Hämta fil till din dator");
            //setTargetName("_blank");
        }

        /*
        @Override
        public void attach() {
            super.attach(); // Must call.

            StreamResourceV8ish.StreamSource source = new StreamResourceV8ish.StreamSource() {
                public InputStream getStream() {
                    try {
                        return new FileInputStream(file);
                    }
                    catch(Exception e) {
                        String msg = "Nedladdningen misslyckades. Underliggande felmeddelande\n"+e.getMessage();
                        logg.error(msg + ". filepath=" + file.getAbsolutePath(), e);
                        functionality.showError(msg);
                        return null;
                    }
                }
            };
            StreamResourceV8ish resource = new StreamResourceV8ish(source, file.getName());
            //resource.setMIMEType(df.getMimetype());
            resource.setCacheTime(0);
            setResource(resource);
        }
         */
    }

}

