package general.reuse.vaadinreusable.managedapp.factories;

import general.reuse.vaadinreusable.vaadin8ish.TextFieldV8ish;

public class TextFieldFactory {
	
	public final static String cssname = "cmdlwebtextfield";
	private static final boolean isStyled = false;
	
	public static TextFieldV8ish styles(TextFieldV8ish in) {
		if(isStyled) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static TextFieldV8ish create(String caption) {
		return styles(new TextFieldV8ish(caption));
	}

	public static TextFieldV8ish create() {
		return styles(new TextFieldV8ish());
	}
	
}
