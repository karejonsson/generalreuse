package general.reuse.vaadinreusable.managedapp.reuse;

import general.reuse.vaadinreusable.vaadin8ish.ComboBoxV8ish;

import java.util.Optional;

public class HandleRowListItem implements Runnable {
	
	private Runnable runnable;
	private String tostring;
	
	public HandleRowListItem(Runnable runnable, String tostring) {
		this.runnable = runnable;
		this.tostring = tostring;
	}
	
	public String toString() {
		return tostring;
	}
	
	public void run() {
		runnable.run();
	}
	
	public static HandleRowListItem getCurrentHandleRowListItem(ComboBoxV8ish<HandleRowListItem> handlingChoser) {
		HandleRowListItem out = null;
		try {
			Optional<HandleRowListItem> op = handlingChoser.getSelectedItem();
			out = op.get();
		}
		catch(Exception e) {
		}
		return out;
	}

}

