package general.reuse.vaadinreusable.chat;

import com.vaadin.flow.component.Component;
import general.reuse.vaadinreusable.vaadin8ish.*;

import java.text.SimpleDateFormat;
import java.util.*;

public class ChatPanel extends VerticalSplitPanelV8ish {

	private static final long serialVersionUID = 6589184305486446790L;

	private ChatParticipant participant = null;
	private Runnable onBack = null;
	private Runnable onContinue = null;
	
	private VerticalLayoutV8ish usersActive = null;
	private HorizontalSplitPanelV8ish stateOfChat = null;
	private PanelV8ish messagesDisplayPanel = null;
	private VerticalLayoutV8ish messagesDisplay = null;
	private HorizontalLayoutV8ish userActions = null;
	private TextFieldV8ish editing = null;
	private ButtonV8ish sendBtn = null;
	
	private Map<String, LabelV8ish> userLookup = new HashMap<>();
	private BroadcastListener follower = null;
	private String name = null;
	
	public ChatPanel(ChatParticipant participant, Runnable onBack) {
		this(participant, onBack, null);
	}

	public ChatPanel(ChatParticipant participant, Runnable onBack, Runnable onContinue) {
		setCaption("Chat for all currentUsers");
		this.participant = participant;
		this.onBack = onBack;
		this.onContinue = onContinue;
		name = participant.getUsername();
		setSplitPosition(80, UnitV8ish.PERCENTAGE);
		stateOfChat = new HorizontalSplitPanelV8ish();
		setFirstComponent(stateOfChat);
		usersActive = new VerticalLayoutV8ish();
		if(!participant.getHaveMargins()) {
			usersActive.setMargin(false);
		}
		stateOfChat.setSecondComponent(usersActive);
		messagesDisplayPanel = new PanelV8ish();
		messagesDisplay = new VerticalLayoutV8ish();
		if(!participant.getHaveMargins()) {
			messagesDisplay.setMargin(false);
		}
		messagesDisplayPanel.setContent(messagesDisplay);
		stateOfChat.setFirstComponent(messagesDisplayPanel);
		stateOfChat.setSplitPosition(85, UnitV8ish.PERCENTAGE);;
		messagesDisplay.setSpacing(false);
		messagesDisplay.setSizeFull();
		userActions = new HorizontalLayoutV8ish(); // !!!
		if(!participant.getHaveMargins()) {
			userActions.setMargin(false);
		}
		setSecondComponent(userActions);
		setSplitPosition(93, UnitV8ish.PERCENTAGE);;
		//chatting.setComponentAlignment(sending, Alignment.BOTTOM_CENTER);
		editing = new TextFieldV8ish();
		editing.setWidth("600px");
		userActions.addComponent(editing);
		sendBtn = new ButtonV8ish("Send");
		sendBtn.addClickListener(e -> sendMessage());
		participant.addReturnKeyListener(editing, () -> sendMessage());
		userActions.addComponent(sendBtn);

		if(onBack != null) {
			ButtonV8ish backBtn = new ButtonV8ish("Revert");
			backBtn.addClickListener(e -> {
				Broadcaster.leave(name);
				Broadcaster.unregister(follower);
				onBack.run();
			});
			userActions.addComponent(backBtn);
		}

		if(onContinue != null) {
			ButtonV8ish continueBtn = new ButtonV8ish("Continue");
			continueBtn.addClickListener(e -> {
				Broadcaster.leave(name);
				Broadcaster.unregister(follower);
				onContinue.run();
			});
			userActions.addComponent(continueBtn);
		}

		if(participant.hasChatAnyway()) {
			ButtonV8ish clearBtn = new ButtonV8ish("Clear chat");
			clearBtn.addClickListener(e -> Broadcaster.clear());
			userActions.addComponent(clearBtn);
		}
		
		List<String> currentUsers = Broadcaster.users();
		for(String currentUser : currentUsers) {
			LabelV8ish l = new LabelV8ish(currentUser);
			userLookup.put(currentUser, l);
			usersActive.addComponent(l);
		}
		
		follower = new BroadcastListener() {

			@Override
			public void message(ChatHolder ch) { addMessage(ch); }

			@Override
			public void join(String username) {
				addUserToListAtRight(username);
			}

			@Override
			public void leave(String username) {
				removeUserFromListAtRight(username);
			}
			
			@Override
			public String getUsername() {
				return name;
			}

			@Override
			public boolean isFunctional() {
				return participant.isFunctional();
			}

		};
		Broadcaster.register(follower);
		messagesDisplayPanel.setHeight(100, UnitV8ish.PERCENTAGE);
		fillDummyMessages();
		Broadcaster.replayAmount(name, 10);
		participant.setChatSince(System.currentTimeMillis());
		verifyConsistency_alwaysRunInOtherUsersAcces();
		/*
		AktaNotification.showWarning(
				"Notera att detta är en chatt-funktion och att\n"+
				"allt som skrivs här syns för alla\n"+
				"som är inloggade i Akta, nu eller senare.\n"+
				"Ta bort detta meddelande genom att klicka på dess yta");
		 */
	}

	private void sendMessage() {
		String message = editing.getValue();
		Broadcaster.message(name, message);
		editing.setValue("");
	}
	
	private List<LabelV8ish> messagesVisible = new ArrayList<>();
	public static final int maxVisible = 25;
	
	private void fillDummyMessages() {
		while(messagesVisible.size() <= maxVisible) {
			LabelV8ish l = new LabelV8ish("");
			messagesVisible.add(l);
			messagesDisplay.addComponent(l);
		}
	}

	private void addMessage(ChatHolder ch) {
		String timeOfEvent = "";
		long delay = System.currentTimeMillis() - ch.time;
		if(delay < 1800000) {
			// Senaste halvtimmen med minut och sekund
			timeOfEvent = new SimpleDateFormat("mm:ss").format(new Date(ch.time));
		}
		else if(delay < 5*3600000) {
			// Från en halvtimme till 5 timmar även timme
			timeOfEvent = new SimpleDateFormat("HH:mm:ss").format(new Date(ch.time));
		}
		else {
			// Efter 5 timmar datum och rubbet
			timeOfEvent = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(ch.time));
		}

		LabelV8ish m = new LabelV8ish(ch.username+" ["+timeOfEvent+"]: "+ch.message);
		messagesVisible.add(m);
		while(messagesVisible.size() != 0 && messagesVisible.size() > maxVisible) {
			try { messagesDisplay.removeComponent(messagesVisible.get(0)); } catch(Exception e) {}
			messagesVisible.remove(0);
		}
		participant.access(() -> messagesDisplay.addComponent(m));
	}

	private void verifyConsistency_alwaysRunInOtherUsersAcces() {
		List<String> users = Broadcaster.users();

		// Lägga till de som av någon anledning ändå saknas
		for(String user : users) {
			LabelV8ish e = userLookup.get(user);
			if(e != null) {
				continue;
			}
			e = new LabelV8ish(user);
			userLookup.put(user, e);
			usersActive.addComponent(e);
		}

		// Kolla vilka som är med i lista trots att de inte är med i chatten
		List<String> toRemove = new ArrayList<>();
		for(String user : userLookup.keySet()) {
			if(!users.contains(user)) {
				toRemove.add(user);
			}
		}

		// Ta bort de som hittades närvarande trots att de inte är med
		for(String userToRemove : toRemove) {
			LabelV8ish e = userLookup.get(userToRemove);
			usersActive.removeComponent(e);
			userLookup.remove(userToRemove);
		}

		// Efter detta är det interna registret fixat och det borde vara rätt namn på listen
		// Nu kollar vi att det är sant att de som är ute är det rätta.

		// Letar upp sådana som är ute men inte borde vara där
		List<Component> componentsToRemove = new ArrayList<>();
		for(int i = 0 ; i < usersActive.getComponentCount() ; i++) {
			Component comp = usersActive.getComponent(i);
			if(!(comp instanceof LabelV8ish)) {
				componentsToRemove.add(comp);
				continue;
			}
			LabelV8ish l = (LabelV8ish) comp;
			String text = l.getValue();
			if(userLookup.get(text) == null) {
				componentsToRemove.add(l);
			}
		}

		// Tar bort de som är utlagda som inte borde vara där.
		for(Component comp : componentsToRemove) {
			usersActive.removeComponent(comp);
		}
	}

	private void addUserToListAtRight(String username) {
		LabelV8ish e = userLookup.get(username);
		if(e != null) {
			participant.access(() -> verifyConsistency_alwaysRunInOtherUsersAcces());
			return;
		}
		LabelV8ish l = new LabelV8ish(username);
		userLookup.put(username, l);
		participant.access(() -> {
			usersActive.addComponent(l);
			verifyConsistency_alwaysRunInOtherUsersAcces();
		});
	}

	private void removeUserFromListAtRight(String username) {
		LabelV8ish l = userLookup.get(username);
		if(l != null) {
			userLookup.remove(username);
			participant.access(() -> {
				usersActive.removeComponent(l);
				verifyConsistency_alwaysRunInOtherUsersAcces();
			});
		}
	}

}
