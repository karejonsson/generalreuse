package general.reuse.vaadinreusable.serverbrowse;

import org.apache.log4j.Logger;

import java.io.File;

public class OperateOnOneDeleteFolderOperation implements OperateOnOneBrowsed {

    private static Logger logg = Logger.getLogger(OperateOnOneDeleteFolderOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnOneDeleteFolderOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Radera katalog";
    }

    @Override
    public String getDescription() {
        return "Raderar en katalog";
    }

    @Override
    public Runnable getOperate(String filepath) {
        return () -> {
            operate(filepath);
        };
    }

    @Override
    public void operate(String filepath) {
        OperationsReuse.okForHardAction(functionality, "Verifiera att filen skall raderas",
                () -> deleteFolder_actionIsOK(filepath),
                () -> functionality.showAssistive("Operationen avbruten")
        );
    }

    private void deleteFolder_actionIsOK(String filepath) {
        logg.info("filepath="+filepath);

        File f = new File(filepath);
        if(!f.exists()) {
            String msg = "Katalogen finns inte";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        if(!f.isDirectory()) {
            String msg = "Det är inte en katalog. Radera som fil.";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        try {
            if(deleteDirectory(f)) {
                String msg = "Katalogen är raderad";
                logg.info(msg+". filepath="+filepath);
                functionality.showAssistive(msg);
                return;
            }
            else {
                String msg = "Katalogen kunde inte raderas";
                logg.info(msg+". filepath="+filepath);
                functionality.showAssistive(msg);
                return;
            }
        }
        catch(Exception e) {
            String msg = "Katalogen kunde inte raderas";
            logg.info(msg+". filepath="+filepath, e);
            functionality.showError(msg);
            return;
        }
    }

    public static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }
}
