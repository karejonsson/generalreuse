package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.datepicker.DatePicker;

public class DateFieldV8ish extends DatePicker {

    public DateFieldV8ish() {
        super();
    }

    public DateFieldV8ish(String caption) {
        super(caption);
    }

    public void setPrimaryStyleName(String cssname) {
        setClassName(cssname);
    }

    public void setCaption(String caption) {
        setLabel(caption);
    }

}

