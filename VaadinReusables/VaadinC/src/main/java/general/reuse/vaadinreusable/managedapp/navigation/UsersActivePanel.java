package general.reuse.vaadinreusable.managedapp.navigation;

import com.vaadin.flow.component.icon.VaadinIcon;
import general.reuse.vaadinreusable.chat.Broadcaster;
import general.reuse.vaadinreusable.choser.QuestionAndButtonsWindow;
import general.reuse.vaadinreusable.component.EnterLongText;
import general.reuse.vaadinreusable.component.ShowLongText;
import general.reuse.vaadinreusable.managedapp.factories.*;
import general.reuse.vaadinreusable.managedapp.functionality.BaseAdminFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.functionality.CommonRunnable;
import general.reuse.vaadinreusable.managedapp.functionality.UserInteractionEvent;
import general.reuse.vaadinreusable.managedapp.internalchat.ChatButton;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.managedapp.reuse.HandleRowListItem;
import general.reuse.vaadinreusable.managedapp.session.FunctionalityWrapper;
import general.reuse.vaadinreusable.managedapp.session.SessionRegistry;
import general.reuse.vaadinreusable.managedapp.texts.ActivityTexts;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;
import general.reuse.vaadinreusable.managedapp.texts.CaptionTexts;
import general.reuse.vaadinreusable.managedapp.texts.MessageTexts;
import general.reuse.vaadinreusable.vaadin8ish.*;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UsersActivePanel extends VerticalLayoutV8ish {
	
	private static final long serialVersionUID = -7431980192394999028L;
	private static Logger logg = Logger.getLogger(UsersActivePanel.class);

	private CommonRunnable onBack = null;
	private BaseAdminFunctionalityProvider adminFunctionality = null;
	private BaseFunctionalityProvider functionality = null;
	
	private GridV8ish<FunctionalityWrapper> grid = null;
	private List<FunctionalityWrapper> users = null;

	private LabelV8ish userCount = null;
	private final boolean chatActive = true;
	private CheckBoxV8ish verifyHardOperations = null;
	private UIV8ish ui = null;

	public UsersActivePanel(BaseAdminFunctionalityProvider adminFunctionality, UIV8ish ui, CommonRunnable onBack) {
		LayoutFactory.styles(this, adminFunctionality.getFunctionality().getHaveMargins());
		this.ui = ui;
		this.adminFunctionality = adminFunctionality;
		functionality = adminFunctionality.getFunctionality();
		this.onBack = onBack;
		//chatActive = true; //SessionRegistry.chatActive.getValue();
		init();
	}
	
	private void init() {
		adminFunctionality.updateUidname();
		userCount = LabelFactory.create();
		initForWork();
		
		addComponent(grid);
		setExpandRatio(grid, 1.0f);

		HorizontalLayoutV8ish buttonsUpper = LayoutFactory.createHorizontal(functionality.getHaveMargins());
		HorizontalLayoutV8ish buttonsLower = LayoutFactory.createHorizontal(functionality.getHaveMargins());

		ButtonV8ish refreshBtn = ButtonFactory.create(ButtonTexts.updatePage);
		refreshBtn.addClickListener(e -> updateGrid());
		buttonsUpper.addComponent(refreshBtn);

		final ComboBoxV8ish<HandleRowListItem> methodOfContactChoser = ComboBoxFactory.create();
		HandleRowListItem defaultMethodAtSelection = new HandleRowListItem(() -> {}, CaptionTexts.choseContactMethod);
		List<HandleRowListItem> methodOptions = new ArrayList<>();
		methodOptions.add(defaultMethodAtSelection);
		methodOfContactChoser.setItems(methodOptions);
		methodOfContactChoser.setEmptySelectionAllowed(false);
		methodOfContactChoser.setTextInputAllowed(false);
		methodOfContactChoser.setValue(defaultMethodAtSelection);
		if(chatActive) {
			methodOptions.add(new HandleRowListItem(() -> { functionality.updateUidname(); methodOfContactChoser.setSelectedItem(defaultMethodAtSelection); askAllToEnterChat_flashing(); }, ButtonTexts.askAllToEnterChat));
			methodOptions.add(new HandleRowListItem(() -> { functionality.updateUidname();methodOfContactChoser.setSelectedItem(defaultMethodAtSelection); forceAllToEnterChat(); }, ButtonTexts.forceAllToEnterChat));
		}

		methodOptions.add(new HandleRowListItem(() -> { functionality.updateUidname(); methodOfContactChoser.setSelectedItem(defaultMethodAtSelection); sendWrittenMessageToAll(); }, ButtonTexts.writtenMessageToAll));

		methodOfContactChoser.addSelectionListener(e -> executeChoice(methodOfContactChoser));
		methodOfContactChoser.addFocusListener(e -> executeChoice(methodOfContactChoser));
		methodOfContactChoser.setWidth("400px");

		buttonsLower.addComponent(methodOfContactChoser);

		ButtonV8ish throwOutAllOthersBtn = ButtonFactory.create(ButtonTexts.throwOutAllOthers);
		throwOutAllOthersBtn.addClickListener(e -> throwOutAllOthers());
		buttonsUpper.addComponent(throwOutAllOthersBtn);

		ButtonV8ish throwOutNotWulnerableBtn = ButtonFactory.create(ButtonTexts.throwOutNotWulnerable);
		throwOutNotWulnerableBtn.addClickListener(e -> throwOutAllNotWulnerable());
		buttonsUpper.addComponent(throwOutNotWulnerableBtn);

		ButtonV8ish chatBtn = ChatButton.createAlwaysStateNotConsidered(functionality);//, functionality.getRunnableForReturnToCurrentContent());
		if(chatBtn != null) {
			buttonsLower.addComponent(chatBtn);
			ButtonV8ish clearChatBtn = ButtonFactory.create(ButtonTexts.clearChat+"("+ Broadcaster.countMessages()+")");
			clearChatBtn.addClickListener(e -> {
				functionality.updateUidname();
				int messagesBefore = Broadcaster.countMessages();
				Broadcaster.clear();
				CommonNotification.showAssistive(functionality, ""+messagesBefore+" meddelanden rensade. Nu väntar "+Broadcaster.countMessages()+" stycken");
				clearChatBtn.setCaption(ButtonTexts.clearChat+"("+Broadcaster.countMessages()+")");
			});
			buttonsLower.addComponent(clearChatBtn);
		}

		if(onBack != null) {
			ButtonV8ish backBtn = ButtonFactory.create(ButtonTexts.revert);
			backBtn.addClickListener(e -> onBack.run());
			buttonsLower.addComponent(backBtn);
		}

		ButtonV8ish statesBtn = ButtonFactory.create(ButtonTexts.states);
		statesBtn.addClickListener(e -> manageGlobalSettings(functionality));
		buttonsUpper.addComponent(statesBtn);

		buttonsUpper.addComponent(userCount);
		//buttonsUpper.setComponentAlignment(userCount, AlignmentV8ish.MIDDLE_RIGHT);

		verifyHardOperations = new CheckBoxV8ish("Verifiera hårda operationer");
		verifyHardOperations.setValue(true);
		buttonsUpper.addComponent(verifyHardOperations);
		//buttonsUpper.setComponentAlignment(verifyHardOperations, AlignmentV8ish.MIDDLE_RIGHT);

		ButtonV8ish editWarningMessageBtn = ButtonFactory.create(ButtonTexts.warningMessage);
		editWarningMessageBtn.addClickListener(e ->setWrittenWarningMessage());
		buttonsLower.addComponent(editWarningMessageBtn);

		addComponent(buttonsUpper);
		addComponent(buttonsLower);
		setSizeFull();
	}

	private void initForWork() {
		grid = GridFactory.create();
		grid.addComponentColumn(row -> {
			return LabelFactory.create(row.getUsername());
		}).setComparator((r1, r2) -> {
			try {
				return r1.getUsername().compareTo(r2.getUsername());
			}
			catch(Exception e) {
				return 0;
			}
		}).setCaption(CaptionTexts.username);
		grid.addComponentColumn(row -> {
			return LabelFactory.create(row.getStateDescription());
		}).setComparator((r1, r2) -> {
			try {
				return r1.getStateDescription().compareTo(r2.getStateDescription());
			}
			catch(Exception e) {
				return 0;
			}
		}).setCaption(CaptionTexts.engagement);
		grid.addComponentColumn(row -> {
			HorizontalLayoutV8ish out = LayoutFactory.createHorizontal(functionality.getHaveMargins());
			String len = "_";
			try {
				UserInteractionEvent[] events = row.getActivityEvents();
				if(events != null) {
					len = ""+events.length;
				}
			}
			catch(Exception e) {}
			ButtonV8ish moreBtn = ButtonFactory.create(VaadinIcon.QUESTION, len);
			moreBtn.addClickListener(e -> showActivityDetails(row));
			out.addComponent(moreBtn);
			out.addComponent(LabelFactory.create(row.getActivity()));
			return out;
		}).setComparator((r1, r2) -> {
			try {
				return r1.getActivity().compareTo(r2.getActivity());
			}
			catch(Exception e) {
				return 0;
			}
		}).setCaption(CaptionTexts.activity);
		grid.addComponentColumn(row -> {
			return LabelFactory.create(row.isStateful() ? CaptionTexts.yes : CaptionTexts.no);
		}).setComparator((r1, r2) -> {
			if(r1.isStateful() && r2.isStateful()) {
				return 0;
			}
			if(!r1.isStateful() && !r2.isStateful()) {
				return 0;
			}
			if(r1.isStateful() && !r2.isStateful()) {
				return 1;
			}
			return -1;
		}).setCaption(CaptionTexts.memoryState);
		grid.addComponentColumn(row -> {
			String msg = row.possiblyVulnerable() ? CaptionTexts.yes : CaptionTexts.no;
			if(SessionRegistry.isWarned(row.getUsername())) {
				msg = msg+" (varnad)";
			}
			return LabelFactory.create(msg);
		}).setComparator((r1, r2) -> {
			if(r1.possiblyVulnerable() && r2.possiblyVulnerable()) {
				return 0;
			}
			if(!r1.possiblyVulnerable() && !r2.possiblyVulnerable()) {
				return 0;
			}
			if(r1.possiblyVulnerable() && !r2.possiblyVulnerable()) {
				return 1;
			}
			return -1;
		}).setCaption(CaptionTexts.sensitive);
		grid.addComponentColumn(row -> {
			int secs = +row.getSecondsIdle();
			String s = textForElapsedTime(secs);
			HorizontalLayoutV8ish out = LayoutFactory.createHorizontal(functionality.getHaveMargins());
			ButtonV8ish moreBtn = ButtonFactory.create(VaadinIcon.QUESTION);
			moreBtn.addClickListener(e -> {
				CommonNotification.showHumanized(functionality, "Inloggad: "+row.getStart()+"\nSenast: "+row.getLast());
			});
			out.addComponent(moreBtn);
			out.addComponent(LabelFactory.create(s));
			return out;
		}).setComparator((r1, r2) -> { 
			try {
				return r1.getSecondsIdle()-r2.getSecondsIdle();
			}
			catch(Exception e) {
				return 0;
			}
		}).setCaption(CaptionTexts.timeIdle);
		grid.addComponentColumn(row -> {
			return LabelFactory.create(row.getPrivilege());
		}).setComparator((r1, r2) -> { 
			try {
				return r1.getPrivilege().compareTo(r2.getPrivilege());
			}
			catch(Exception e) {
				return 0;
			}
		}).setCaption(CaptionTexts.userType);
		grid.addComponentColumn(row -> {
			HorizontalLayoutV8ish buttons = LayoutFactory.createHorizontal(functionality.getHaveMargins());

			final boolean isSelf = functionality.getUsername().trim().equals(row.getUsername().trim());

			if(!isSelf) {
				final ComboBoxV8ish<HandleRowListItem> handlingChoser = ComboBoxFactory.create();
				HandleRowListItem defaultAtSelection = new HandleRowListItem(() -> {}, CaptionTexts.choseContactMethod);
				List<HandleRowListItem> options = new ArrayList<>();
				options.add(defaultAtSelection);
				handlingChoser.setItems(options);
				handlingChoser.setEmptySelectionAllowed(false);
				handlingChoser.setTextInputAllowed(false);
				handlingChoser.setValue(defaultAtSelection);
				if(chatActive) {
					options.add(new HandleRowListItem(() -> { handlingChoser.setSelectedItem(defaultAtSelection); askToChat(row); }, CaptionTexts.askForChat));
					options.add(new HandleRowListItem(() -> { handlingChoser.setSelectedItem(defaultAtSelection); forceToChat(row); }, CaptionTexts.forceToChat));
				}
				options.add(new HandleRowListItem(() -> { handlingChoser.setSelectedItem(defaultAtSelection); sendWrittenMessageToOne(row); }, CaptionTexts.writtenMessage));

				handlingChoser.addSelectionListener(e -> executeChoice(handlingChoser));
				handlingChoser.addFocusListener(e -> executeChoice(handlingChoser));
				handlingChoser.setWidth("350px");
				buttons.addComponent(handlingChoser);
			}

			if(!isSelf) {
				ButtonV8ish throwOutActionBtn = ButtonFactory.create(ButtonTexts.throwOut);
				throwOutActionBtn.addClickListener(e -> throwOutAction(row));
				buttons.addComponent(throwOutActionBtn);
			}

			return buttons;
		}).setCaption(CaptionTexts.handling);
		
		grid.setWidth("100%");
		grid.setHeight("100%");
		updateGrid();
	}

	private void showActivityDetails(FunctionalityWrapper fw) {
		WindowV8ish win = new WindowV8ish();
		VerticalLayoutV8ish vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		GridV8ish<UserInteractionEvent> grid = GridFactory.create();
		grid.addComponentColumn(row -> {
			return LabelFactory.create(row.getWhen().toString());
		}).setComparator((r1, r2) -> {
			try {
				return (int) (r1.getWhen().getTime() - r2.getWhen().getTime());
			}
			catch(Exception e) {
				return 0;
			}
		}).setCaption("Tid vid inledande");
		grid.addComponentColumn(row -> {
			int secsSince = (int) ((System.currentTimeMillis()-row.getWhen().getTime())/1000);
			return LabelFactory.create(textForElapsedTime(secsSince));
		}).setComparator((r1, r2) -> {
			int secsSince1 = (int) ((System.currentTimeMillis()-r1.getWhen().getTime())/1000);
			int secsSince2 = (int) ((System.currentTimeMillis()-r2.getWhen().getTime())/1000);
			return secsSince1 - secsSince2;
		}).setCaption("Tid sedan händelse");
		grid.addComponentColumn(row -> {
			return LabelFactory.create(row.isStateful() ? CaptionTexts.yes : CaptionTexts.no);
		}).setComparator((r1, r2) -> {
			if(r1.isStateful() && r2.isStateful()) {
				return 0;
			}
			if(!r1.isStateful() && !r2.isStateful()) {
				return 0;
			}
			if(r1.isStateful() && !r2.isStateful()) {
				return 1;
			}
			return -1;
		}).setCaption(CaptionTexts.memoryState);
		grid.addComponentColumn(row -> {
			return LabelFactory.create(row.getWhat());
		}).setComparator((r1, r2) -> {
			try {
				return r1.getWhat().compareTo(r2.getWhat());
			}
			catch(Exception e) {
				return 0;
			}
		}).setCaption(CaptionTexts.activity);
		grid.setSizeFull();
		grid.setItems(fw.getActivityEvents());
		vl.addComponent(grid);
		vl.setExpandRatio(grid, 1.0f);

		HorizontalLayoutV8ish hl = LayoutFactory.createHorizontal(functionality.getHaveMargins());
		ButtonV8ish closeBtn = ButtonFactory.create(ButtonTexts.close);
		closeBtn.addClickListener(e -> win.close());
		hl.addComponent(closeBtn);

		ButtonV8ish exportBtn = ButtonFactory.create(ButtonTexts.export);
		exportBtn.addClickListener(e -> {
			//win.close();
			StringBuffer sb = new StringBuffer();
			for(UserInteractionEvent uie : fw.getActivityEvents()) {
				sb.append(uie.toString()+"\n");
			}
			String text = sb.toString();
			ShowLongText slt = new ShowLongText(ui, "Aktivitet", "Händelser", ButtonTexts.close, text, true);
			functionality.addWindow(slt, "Aktivitetsexport");
		});
		hl.addComponent(exportBtn);

		vl.addComponent(hl);

		vl.setSizeFull();
		win.setContent(vl);
		win.setWidth("50%");
		win.setHeight("70%");
		functionality.addWindow(win, "Aktivitetsdetaljer");
	}

	private void updateGrid() {
		functionality.updateUidname();
		users = SessionRegistry.getCurrentUserlist();
		Collections.sort(users, (u1, u2) -> {
			return u1.getSecondsIdle() - u2.getSecondsIdle();
		});
		grid.setItems(users);
		grid.getDataProvider().refreshAll();
		userCount.setValue("Antal "+users.size());
	}

	public static String textForElapsedTime(final int secs) {
		int mins = secs / 60;
		int puresecs = secs - 60 * mins;
		int hours = mins / 60;
		int puremins = mins - 60 * hours;
		int days = hours / 24;
		int purehours = hours - 24 * days;
		if (mins < 1) {
			return "" + secs + " sek";
		}
		if (hours < 1) {
			return "" + secs + " sek ~ " + puremins + " min, " + puresecs + " sek";
		}
		if (days < 2) {
			return "" + secs + " sek ~ " + hours + " tim, " + puremins + " min";
		}
		return "" + days + " dagar, " + purehours + " tim, " + puremins + " min";
	}

	private void okForHardAction(String question, Runnable onOK, Runnable onNok) {
		if(onOK == null && onNok == null) {
			// Konstigt men för all del. Programmet skall vara robust.
			return;
		}
		if(question == null || question.trim().length() == 0) {
			// Konstigt. Ingen fråga tillhandahållen. Gör inget då ingen verifiering kan göras.
			return;
		}
		if(!verifyHardOperations.getValue()) {
			// Användaren har bockat ur så det är OK rakt av
			if(onOK != null) {
				onOK.run();
			}
			return;
		}
		QuestionAndButtonsWindow confirm = new QuestionAndButtonsWindow("Bekräfta hård åtgärd", question);
		confirm.addButtonWithClose(VaadinIcon.THUMBS_UP, ButtonTexts.yes, onOK != null ? onOK : () -> {});
		confirm.addButtonWithClose(VaadinIcon.STOP, ButtonTexts.cancel, onNok != null ? onNok : () -> {});
		functionality.addWindow(confirm, "Bekräfta hård åtgärd");
	}

	private void throwOutAction(FunctionalityWrapper row) {
		okForHardAction("Verifiera att "+row.getUsername()+" skall kastas ut",
				() -> throwOutAction_actionIsOK(row),
				() -> CommonNotification.showAssistive(functionality, ActivityTexts.operationCancelled)
				);
	}

	private void throwOutAction_actionIsOK(FunctionalityWrapper row) {
		functionality.updateUidname();
		long timeBefore = System.currentTimeMillis();
		List<FunctionalityWrapper> toThrowOut = new ArrayList<>();
		toThrowOut.add(row);
		throwOutSelected(timeBefore, toThrowOut);
	}

	private void askAllToEnterChat_flashing() {
		flashMessageToAll(ActivityTexts.pleaseGoToChat);
	}

	private void flashOneMessage(FunctionalityWrapper wrapper, String message) {
		functionality.updateUidname();
		try {
			wrapper.flashMessage(message);
		}
		catch(Exception e) {
			logg.error("Fel när användare "+wrapper.getUsername()+" skulle få meddelande via push", e);
		}
	}

	private void flashMessageToAll(String message) {
		functionality.updateUidname();
		String username = functionality.getUsername();
		for(FunctionalityWrapper user : users) {
			if(!username.trim().equals(user.getUsername().trim())) {
				try {
					user.flashMessage(message);
				}
				catch(Exception e) {
					logg.error("Fel när användare "+user.getUsername()+" skulle få meddelande via push", e);
				}
			}
		}
	}

	private void forceAllToEnterChat() {
		functionality.updateUidname();
		String username = functionality.getUsername();
		for(FunctionalityWrapper user : users) {
			if(!username.trim().equals(user.getUsername().trim())) {
				forceToChat(user);
			}
		}
	}

	private void throwOutAllOthers() {
		okForHardAction("Verifiera att alla andra skall kastas ut",
				() -> throwOutAllOthers_actionIsOK(),
				() -> CommonNotification.showAssistive(functionality, ActivityTexts.operationCancelled)
		);
	}

	private void throwOutAllOthers_actionIsOK() {
		functionality.updateUidname();
		long timeBefore = System.currentTimeMillis();
		String username = functionality.getUsername();

		List<FunctionalityWrapper> toThrowOut = new ArrayList<>();
		for(FunctionalityWrapper user : users) {
			if(!username.trim().equals(user.getUsername().trim())) {
				toThrowOut.add(user);
			}
		}
		throwOutSelected(timeBefore, toThrowOut);
	}

	private void throwOutSelected(long timeBefore, List<FunctionalityWrapper> toThrowOut) {
		functionality.updateUidname();
		for(FunctionalityWrapper user : toThrowOut) {
			outstandingWorks(1);
			user.setLocationLogout(() -> {
				if(outstandingWorks(-1) == 0) {
					finishThrowout(timeBefore, toThrowOut);
				}
			});
		}
	}

	private int outstandingWorksCount = 0;

	private synchronized int outstandingWorks(int delta) {
		logg.info("Utestående jobb "+outstandingWorksCount+" ökas med "+delta);
		outstandingWorksCount += delta;
		return outstandingWorksCount;
	}


	private void throwOutAllNotWulnerable() {
		functionality.updateUidname();
		okForHardAction("Verifiera att alla okänsliga skall kastas ut",
				() -> throwOutAllNotWulnerable_actionIsOK(),
				() -> CommonNotification.showAssistive(functionality, ActivityTexts.operationCancelled)
		);
	}

	private void throwOutAllNotWulnerable_actionIsOK() {
		functionality.updateUidname();
		long timeBefore = System.currentTimeMillis();
		String username = functionality.getUsername();
		List<FunctionalityWrapper> toThrowOut = new ArrayList<>();
		for(FunctionalityWrapper user : users) {
			if(!username.trim().equals(user.getUsername().trim())) {
				if(!user.possiblyVulnerable()) {
					toThrowOut.add(user);
				}
			}
		}
		throwOutSelected(timeBefore, toThrowOut);
	}

	private void finishThrowout(long timeBefore, List<FunctionalityWrapper> toThrowOut) {
		functionality.updateUidname();
		for(FunctionalityWrapper user : toThrowOut) {
			user.throwOutMechanically();
		}

		long timeAfter = System.currentTimeMillis();

		users = SessionRegistry.getCurrentUserlist();
		grid.setItems(users);
		grid.getDataProvider().refreshAll();
		userCount.setValue("Antal "+users.size());
		if(timeAfter - timeBefore > 1500) {
			CommonNotification.showAssistive(functionality, ButtonTexts.done_t);//"Klart");
		}
	}

	private void askToChat(FunctionalityWrapper wrapper) {
		functionality.updateUidname();
		logg.info("Ber "+wrapper.getUsername()+" om chatt");
		try {
			wrapper.askToChat(MessageTexts.askToGoToChat);
		}
		catch(Exception e) {
			logg.error("Fel när användare "+wrapper.getUsername()+" skulle få ombedas gå till chatten via push", e);
		}
	}

	private void forceToChat(FunctionalityWrapper wrapper) {
		functionality.updateUidname();
		wrapper.forceToChat();
	}

	private void sendWrittenMessageToOne(FunctionalityWrapper wrapper) {
		functionality.updateUidname();
		final EnterLongText elt =
				new EnterLongText(
						ui,
						CaptionTexts.editSomeMessage,
						CaptionTexts.text,
						ButtonTexts.cancel,
						ButtonTexts.send,
						MessageTexts.preparedMessage,
						MessageTexts.preparedMessage.length()+512,
						0.5,
						0.6);
		elt.setOnDoneOk(() -> flashOneMessage(wrapper, elt.getText()));
		elt.setEnabled(true);
		functionality.addWindow(elt, "Skicka skriven text");
	}

	private void setWrittenWarningMessage() {
		functionality.updateUidname();
		final EnterLongText elt =
				new EnterLongText(
						ui,
						CaptionTexts.editSomeMessage,
						CaptionTexts.text,
						ButtonTexts.cancel,
						ButtonTexts.save,
						SessionRegistry.getWarningText(),
						MessageTexts.preparedMessage.length()+512, // Den duger även om något ologisk här
						0.5,
						0.6);
		elt.setOnDoneOk(() -> {
			SessionRegistry.setWarningTime(null);
			SessionRegistry.setWarningText(elt.getText());
		});
		elt.setEnabled(true);
		functionality.addWindow(elt, "Redigera varningsmeddelande");
	}

	private void sendWrittenMessageToAll() {
		functionality.updateUidname();
		final EnterLongText elt =
				new EnterLongText(
						ui,
						CaptionTexts.editSomeMessage,
						CaptionTexts.text,
						ButtonTexts.cancel,
						ButtonTexts.send,
						MessageTexts.preparedMessage,
						MessageTexts.preparedMessage.length()+512,
						0.5,
						0.6);
		elt.setOnDoneOk(() -> flashMessageToAll(elt.getText()));
		elt.setEnabled(true);
		functionality.addWindow(elt, "Skicka skriven text alla");
	}


	public static void manageGlobalSettings(BaseFunctionalityProvider functionality) {
		functionality.updateUidname();
		WindowV8ish win = new WindowV8ish();
		VerticalLayoutV8ish vl = LayoutFactory.createVertical(functionality.getHaveMargins());

		CheckBoxV8ish[] states = SessionRegistry.getCheckboxSettings(functionality.getUsername());
		for(CheckBoxV8ish state : states) {
			vl.addComponent(state);
		}

		ButtonV8ish closeBtn = ButtonFactory.create(ButtonTexts.close);
		closeBtn.addClickListener(e -> win.close());
		vl.addComponent(closeBtn);
		//vl.setSizeFull();
		win.setContent(vl);
		win.setWidth("20%");
		win.setHeight("20%");
		functionality.addWindow(win, "Hantera globala inställningar");
	}

	private HandleRowListItem previousHandler = null;

	private void executeChoice(ComboBoxV8ish<HandleRowListItem> handlingChoser) {
		functionality.updateUidname();
		HandleRowListItem handler = HandleRowListItem.getCurrentHandleRowListItem(handlingChoser);
		if(handler != previousHandler) {
			previousHandler = handler;
			handler.run();
		}
	}

}

