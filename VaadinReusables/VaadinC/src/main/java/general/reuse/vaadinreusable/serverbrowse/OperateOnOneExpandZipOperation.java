package general.reuse.vaadinreusable.serverbrowse;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class OperateOnOneExpandZipOperation implements OperateOnOneBrowsed {

    private static Logger logg = Logger.getLogger(OperateOnOneExpandZipOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnOneExpandZipOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Zipexpandering";
    }

    @Override
    public String getDescription() {
        return "Expandera en zipfil till samma plats";
    }

    @Override
    public Runnable getOperate(String filepath) {
        return () -> {
            operate(filepath);
        };
    }

    @Override
    public void operate(String filepath) {
        logg.info("filepath="+filepath);

        File f = new File(filepath);
        if(!f.exists()) {
            String msg = "Filen finns inte";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        String folder = null;
        try {
            folder = f.getParentFile().getCanonicalPath();
        }
        catch(Exception e) {
            String msg = "Internt fel. Kan ej bestämma kanonisk sökväg";
            logg.error(msg+". filepath="+filepath, e);
            functionality.showError(msg);
            return;
        }

        System.out.println("filepath="+filepath+" -> innuti katalogen: folder="+folder);
        File folderF = new File(folder);
        if(!folderF.exists()) {
            String msg = "Nivån ovanför den existerade filen finns inte. Konstigt med lik förbannat!";
            logg.info(msg+". filepath="+filepath+", folder="+folder);
            functionality.showError(msg);
            return;
        }
        try {
            unzip(filepath, folder);
        }
        catch(Exception e) {
            String msg = "Vid uppzippningen uppstod fel. Ledtext\n"+e.getMessage();
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
    }

    /**
     * Size of the buffer to read/write data
     */
    private static final int BUFFER_SIZE = 4096;
    /**
     * Extracts a zip file specified by the zipFilePath to a directory specified by
     * destDirectory (will be created if does not exists)
     * @param zipFilePath
     * @param destDirectory
     * @throws IOException
     */
    public static void unzip(String zipFilePath, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        unzip(new File(zipFilePath), destDir);
    }

    public static void unzip(File zipFile, File destDir) throws IOException {
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        String destDirectory = destDir.getCanonicalPath();
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFile));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdirs();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }
    /**
     * Extracts a zip entry (file entry)
     * @param zipIn
     * @param filePath
     * @throws IOException
     */
    private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }

}
