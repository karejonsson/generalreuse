package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.combobox.ComboBox;

import java.util.Optional;

public class ComboBoxV8ish<T extends Object> extends ComboBox<T> {

    public ComboBoxV8ish() {
        super();
    }

    public ComboBoxV8ish(String caption) {
        super(caption);
    }

    public void setSelectedItem(T cbobject) {
        super.setValue(cbobject);
    }

    public void setTextInputAllowed(boolean b) {
        super.setAllowCustomValue(b);
    }

    public void setEmptySelectionAllowed(boolean b) {
        super.setRequired(b);
    }

    public Optional<T> getSelectedItem() {
        return getOptionalValue();
    }

    public void setPrimaryStyleName(String cssname) {
        setClassName(cssname);
    }

    public interface CaptionFilter extends ItemFilter<String> {
        boolean test(String item, String filterText);
    }

    public void addSelectionListener(ComponentEventListener<SelectedItemChangeEvent<ComboBox<T>>> listener) {
        addSelectedItemChangeListener(listener);
    }

}