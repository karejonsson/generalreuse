package general.reuse.vaadinreusable.managedapp.factories;

import general.reuse.vaadinreusable.vaadin8ish.TextAreaV8ish;

public class TextAreaFactory {

	public final static String cssname = "cmdlwebtextarea";
	private static final boolean isStyled = false;
	
	public static TextAreaV8ish styles(TextAreaV8ish in) {
		if(isStyled) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static TextAreaV8ish create(String caption) {
		return styles(new TextAreaV8ish(caption));
	}

	public static TextAreaV8ish create() {
		return styles(new TextAreaV8ish());
	}

}
