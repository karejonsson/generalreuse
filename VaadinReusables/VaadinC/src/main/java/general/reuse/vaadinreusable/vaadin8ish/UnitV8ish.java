package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.splitlayout.SplitLayout;

public class UnitV8ish {

    public static final UnitV8ish PERCENTAGE = new UnitV8ish("%");

    private String unit = null;

    public UnitV8ish(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public void set(SplitLayout sl, int i) {
        sl.setPrimaryStyle("max-width", ""+i+""+unit);
        sl.setSecondaryStyle("width", ""+(100-i)+""+unit);
    }

}
