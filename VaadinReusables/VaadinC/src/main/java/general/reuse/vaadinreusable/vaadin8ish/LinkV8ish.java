package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.server.AbstractStreamResource;
import com.vaadin.flow.server.InputStreamFactory;
import com.vaadin.flow.server.StreamResource;

import java.io.*;

public class LinkV8ish extends Anchor {

    public LinkV8ish() {
        super();
    }

    public LinkV8ish(String caption, AbstractStreamResource resource) {
        super(resource, caption);
    }

    public LinkV8ish(AbstractStreamResource resource, String caption) {
        super(resource, caption);
    }

    public void setTargetName(String targetName) {
        this.setTarget(targetName);
    }

    public LinkV8ish(String caption, String filename, final InputStream resource) {
        super(new StreamResource(filename, new InputStreamFactory() {
            @Override
            public InputStream createInputStream() {
                try {
                    return resource;
                } catch (Exception e) {
                    return null;
                }
            }
        }), caption);
    }

    public static final String defaultName = "result.html";

    public LinkV8ish(final File file) throws FileNotFoundException {
        this(file.getName(), file.getName(), new FileInputStream(file));
    }

    public LinkV8ish(String caption, final File file) throws FileNotFoundException {
        this(caption, file.getName(), new FileInputStream(file));
    }

    public LinkV8ish(String caption, String filename, final File file) throws FileNotFoundException {
        this(caption, filename, new FileInputStream(file));
    }

    public LinkV8ish(String caption, final InputStream resource) {
        this(caption, defaultName, resource);
    }

    public LinkV8ish(String caption, final byte[] bytes) {
        this(caption, defaultName, new ByteArrayInputStream(bytes));
    }

    public LinkV8ish(String caption, String filename, final byte[] bytes) {
        this(caption, filename, new ByteArrayInputStream(bytes));
    }

}
