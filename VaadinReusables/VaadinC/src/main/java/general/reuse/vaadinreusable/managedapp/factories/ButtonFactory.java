package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;
import general.reuse.vaadinreusable.vaadin8ish.ButtonV8ish;

public class ButtonFactory {
	
	public final static String cssname = "managedappbutton";
	private static final boolean isStyled = false;
	
	public static ButtonV8ish styles(ButtonV8ish in) {
		if(isStyled) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static void addIcon(String caption, ButtonV8ish button) {
		if(caption == ButtonTexts.userData) {
			button.setIcon(new Icon(VaadinIcon.USER_CARD));
			return;
		}
		if(caption == ButtonTexts.exportContentColumns) {
			button.setIcon(new Icon(VaadinIcon.TABLE));
			return;
		}
		if(caption == ButtonTexts.fileHandling) {
			button.setIcon(new Icon(VaadinIcon.FILE_TREE));
			return;
		}
		if(caption == ButtonTexts.userOpenings) {
			button.setIcon(new Icon(VaadinIcon.RECORDS));
			return;
		}
		if(caption == ButtonTexts.diagnosePlatform) {
			button.setIcon(new Icon(VaadinIcon.DOCTOR));
			return;
		}
		if(caption == ButtonTexts.revert) {
			button.setIcon(new Icon(VaadinIcon.STEP_BACKWARD));
			return;
		}
		if(caption == ButtonTexts.save) {
			button.setIcon(new Icon(VaadinIcon.DISC));
			return;
		}
		if(caption == ButtonTexts.remove) {
			button.setIcon(new Icon(VaadinIcon.TRASH));
			return;
		}
		if(caption == ButtonTexts.edit) {
			button.setIcon(new Icon(VaadinIcon.EDIT));
			return;
		}
		if(caption == ButtonTexts.enableAccess) {
			button.setIcon(new Icon(VaadinIcon.EDIT));
			return;
		}
		if(caption == ButtonTexts.activeUsers) {
			button.setIcon(new Icon(VaadinIcon.USER));
			return;
		}
		if(caption == ButtonTexts.users) {
			button.setIcon(new Icon(VaadinIcon.USERS));
			return;
		}
		if(caption == ButtonTexts.erase) {
			button.setIcon(new Icon(VaadinIcon.ERASER));
			return;
		}
		if(caption == ButtonTexts.further) {
			button.setIcon(new Icon(VaadinIcon.FORWARD));
			return;
		}
		if(caption == ButtonTexts.copy) {
			button.setIcon(new Icon(VaadinIcon.COPY));
			return;
		}
		if(caption == ButtonTexts.groups) {
			button.setIcon(new Icon(VaadinIcon.GROUP));
			return;
		}
		if(caption == ButtonTexts.close) {
			button.setIcon(new Icon(VaadinIcon.CLOSE));
			return;
		}
		if(caption == ButtonTexts.closeFolder) {
			button.setIcon(new Icon(VaadinIcon.CLOSE));
			return;
		}
		if(caption == ButtonTexts.closeMessage_handleRelated) {
			button.setIcon(new Icon(VaadinIcon.CLOSE));
			return;
		}
		if(caption == ButtonTexts.disableAccess) {
			button.setIcon(new Icon(VaadinIcon.LOCK));
			return;
		}
		if(caption == ButtonTexts.downloadFile) {
			button.setIcon(new Icon(VaadinIcon.DOWNLOAD));
			return;
		}
		if(caption == ButtonTexts.userSettings) {
			button.setIcon(new Icon(VaadinIcon.ELLIPSIS_DOTS_V));
			return;
		}
		if(caption == ButtonTexts.settings) {
			button.setIcon(new Icon(VaadinIcon.ELLIPSIS_DOTS_V));
			return;
		}
		if(caption == ButtonTexts.personalSettings) {
			button.setIcon(new Icon(VaadinIcon.ELLIPSIS_DOTS_V));
			return;
		}
		if(caption == ButtonTexts.search) {
			button.setIcon(new Icon(VaadinIcon.SEARCH));
			return;
		}
		if(caption == ButtonTexts.documentSearch) {
			button.setIcon(new Icon(VaadinIcon.SEARCH));
			return;
		}
		if(caption == ButtonTexts.larger) {
			button.setIcon(new Icon(VaadinIcon.SEARCH_PLUS));
			return;
		}
		if(caption == ButtonTexts.smaller) {
			button.setIcon(new Icon(VaadinIcon.SEARCH_MINUS));
			return;
		}
		if(caption == ButtonTexts.start) {
			button.setIcon(new Icon(VaadinIcon.HOME_O));
			return;
		}
		if(caption == ButtonTexts.updatePage) {
			button.setIcon(new Icon(VaadinIcon.REFRESH));
			return;
		}
		if(caption == ButtonTexts.send) {
			button.setIcon(new Icon(VaadinIcon.OUTBOX));
			return;
		}
		if(caption == ButtonTexts.read) {
			button.setIcon(new Icon(VaadinIcon.EYE));
			return;
		}
		if(caption == ButtonTexts.openDossier) {
			button.setIcon(new Icon(VaadinIcon.FOLDER_ADD));
			return;
		}
		if(caption == ButtonTexts.throwOut) {
			button.setIcon(new Icon(VaadinIcon.SIGN_OUT));
			return;
		}
		if(caption == ButtonTexts.throwOutAllOthers) {
			button.setIcon(new Icon(VaadinIcon.SIGN_OUT));
			return;
		}
		if(caption == ButtonTexts.cancel) {
			button.setIcon(new Icon(VaadinIcon.STOP));
			return;
		}
	}

	public static ButtonV8ish create(String caption) {
		ButtonV8ish outBtn = new ButtonV8ish(caption);
		addIcon(caption, outBtn);
		return styles(outBtn);
	}

	public static ButtonV8ish create(VaadinIcon icon) {
		return styles(new ButtonV8ish(icon));
	}

	public static ButtonV8ish create(String caption, VaadinIcon icon) {
		return styles(new ButtonV8ish(caption, icon));
	}
	public static ButtonV8ish create(VaadinIcon icon, String caption) {
		return styles(new ButtonV8ish(caption, icon));
	}

}
