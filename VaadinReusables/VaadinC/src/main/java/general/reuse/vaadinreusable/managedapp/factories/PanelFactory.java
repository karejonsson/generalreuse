package general.reuse.vaadinreusable.managedapp.factories;

import general.reuse.vaadinreusable.vaadin8ish.HorizontalSplitPanelV8ish;
import general.reuse.vaadinreusable.vaadin8ish.PanelV8ish;
import general.reuse.vaadinreusable.vaadin8ish.VerticalSplitPanelV8ish;

public class PanelFactory {

	public final static String cssname = "cmdlwebpanel";
	public final static String csshorizontalname = "cmdlwebhorizontalsplitpanel";
	public final static String cssverticalname = "cmdlwebverticalsplitpanel";

	private static final boolean isStyled = false;
	private static final boolean isHorizontalStyled = false;
	private static final boolean isVerticalStyled = false;

	static {
	}

	public static PanelV8ish styles(PanelV8ish in) {
		if(isStyled) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static HorizontalSplitPanelV8ish styles(HorizontalSplitPanelV8ish in) {
		if(isHorizontalStyled) {
			in.setPrimaryStyleName(csshorizontalname);
		}
		return in;
	}

	public static VerticalSplitPanelV8ish styles(VerticalSplitPanelV8ish in) {
		if(isVerticalStyled) {
			in.setPrimaryStyleName(cssverticalname);
		}
		return in;
	}

	public static PanelV8ish create() {
		return styles(new PanelV8ish());
	}

	public static HorizontalSplitPanelV8ish createHorizontalSplit() {
		return styles(new HorizontalSplitPanelV8ish());
	}

	public static VerticalSplitPanelV8ish createVerticalSplit() {
		return styles(new VerticalSplitPanelV8ish());
	}

}
