package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;

public class PanelV8ish extends Div {

    public PanelV8ish() {}

    public PanelV8ish(String title) {
        setTitle(title);
        setSizeFull();
    }

    public void setContent(Component comp) {
        super.removeAll();
        super.add(comp);
    }

    public void setHeight(int val, UnitV8ish unit) {
        setHeight(""+val+unit.getUnit());
    }

    public void setPrimaryStyleName(String cssname) {
        setClassName(cssname);
    }

    public void setCaption( String name) {
        setTitle(name);
    }

}
