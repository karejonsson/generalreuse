package general.reuse.vaadinreusable.serverbrowse;

import org.apache.log4j.Logger;

import java.io.File;

public class OperateOnOneDeleteFileOperation implements OperateOnOneBrowsed {

    private static Logger logg = Logger.getLogger(OperateOnOneDeleteFileOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnOneDeleteFileOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Radera fil";
    }

    @Override
    public String getDescription() {
        return "Raderar en fil";
    }

    @Override
    public Runnable getOperate(String filepath) {
        return () -> {
            operate(filepath);
        };
    }

    @Override
    public void operate(String filepath) {
        OperationsReuse.okForHardAction(functionality, "Verifiera att filen skall raderas",
                () -> deleteFile_actionIsOK(filepath),
                () -> functionality.showAssistive("Operationen avbruten")
       );
    }

    private void deleteFile_actionIsOK(String filepath) {
        logg.info("filepath="+filepath);

        File f = new File(filepath);
        if(!f.exists()) {
            String msg = "Filen finns inte";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        if(f.isDirectory()) {
            String msg = "Det är en katalog. Radera som katalog.";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        if(f.delete()) {
            String msg = "Filen är raderad";
            logg.info(msg+". filepath="+filepath);
            functionality.showAssistive(msg);
            return;
        }
        else {
            String msg = "Filen kunde inte raderas";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
    }

}
