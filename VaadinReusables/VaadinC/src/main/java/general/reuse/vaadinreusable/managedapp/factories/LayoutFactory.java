package general.reuse.vaadinreusable.managedapp.factories;

import general.reuse.vaadinreusable.vaadin8ish.HorizontalLayoutV8ish;
import general.reuse.vaadinreusable.vaadin8ish.VerticalLayoutV8ish;

public class LayoutFactory {
	
	public final static String csshorizontalname = "cmdlwebhorizontallayout";
	public final static String cssverticalname = "cmdlwebverticallayout";
	public final static String cssabsolutename = "cmdlwebabsolutelayout";

	private static final boolean isHorizontalStyled = false;
	private static final boolean isVerticalStyled = false;
	private static final boolean isAbsoluteStyled = false;

	static {
	}

	public static HorizontalLayoutV8ish styles(HorizontalLayoutV8ish in, boolean haveMargin) {
		if(isHorizontalStyled) {
			in.setPrimaryStyleName(csshorizontalname);
		}
		if(!haveMargin) {
			in.setMargin(haveMargin);
		}
		return in;
	}

	public static VerticalLayoutV8ish styles(VerticalLayoutV8ish in, boolean haveMargin) {
		if(isVerticalStyled) {
			in.setPrimaryStyleName(cssverticalname);
		}
		if(!haveMargin) {
			in.setMargin(haveMargin);
		}
		return in;
	}

	public static HorizontalLayoutV8ish createHorizontal(boolean haveMargin) {
		return styles(new HorizontalLayoutV8ish(), haveMargin);
	}

	public static HorizontalLayoutV8ish createHorizontal(String caption, boolean haveMargin) {
		HorizontalLayoutV8ish out = new HorizontalLayoutV8ish();
		//out.setCaption(caption);
		return styles(out, haveMargin);
	}

	public static VerticalLayoutV8ish createVertical(boolean haveMargin) {
		return styles(new VerticalLayoutV8ish(), haveMargin);
	}

	public static VerticalLayoutV8ish createVertical(String caption, boolean haveMargin) {
		VerticalLayoutV8ish out = new VerticalLayoutV8ish();
		//out.setCaption(caption);
		return styles(out, haveMargin);
	}

}
