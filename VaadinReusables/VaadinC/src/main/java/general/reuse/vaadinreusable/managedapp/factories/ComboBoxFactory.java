package general.reuse.vaadinreusable.managedapp.factories;

import general.reuse.vaadinreusable.vaadin8ish.ComboBoxV8ish;

public class ComboBoxFactory {

	public final static String cssname = "cmdlwebcombobox";
	private static final boolean isStyled = false;
	
	public static <T> ComboBoxV8ish<T> styles(ComboBoxV8ish<T> in) {
		if(isStyled) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static <T> ComboBoxV8ish<T> create(String caption) {
		return styles(new ComboBoxV8ish<T>(caption));
	}

	public static <T> ComboBoxV8ish<T> create() {
		return styles(new ComboBoxV8ish<T>());
	}

}
