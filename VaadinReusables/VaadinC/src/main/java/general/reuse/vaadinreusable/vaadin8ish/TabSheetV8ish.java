package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;

public class TabSheetV8ish extends Tabs {

    public void setPrimaryStyleName(String cssname) {
        setClassName(cssname);
    }
    public void addTab(Component c, String name) {
        Tab t = new Tab(c);
        t.setLabel(name);
        this.add(t);
    }

}
