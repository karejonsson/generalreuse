package general.reuse.vaadinreusable.serverbrowse;

import org.apache.log4j.Logger;

import java.io.File;

public class OperateOnTwoChangeNameOperation implements OperateOnTwoBrowsed_source_target {

    private static Logger logg = Logger.getLogger(OperateOnTwoChangeNameOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnTwoChangeNameOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Namnbyte";
    }

    @Override
    public String getDescription() {
        return "Byt namn på en fil";
    }

    @Override
    public Runnable getOperate(File first, String second) {
        return () -> {
            operate(first, second);
        };
    }

    @Override
    public void operate(File first, String second) {
        if(!first.exists()) {
            String msg = "Filen att byta namn på finns inte";
            logg.info(msg+". filepath="+first.getAbsolutePath());
            functionality.showError(msg);
            return;
        }
        if(second.contains("/") || second.contains("\\")) {
            String msg = "Målnamnet får inte innehålla slashar";
            logg.info(msg+". filepath="+first.getAbsolutePath());
            functionality.showError(msg);
            return;
        }
        File parent = first.getParentFile();
        File target = new File(parent, second);
        if(target.exists()) {
            String msg = "Målfilen finns redan";
            logg.info(msg+". filepath="+first.getAbsolutePath());
            functionality.showError(msg);
            return;
        }
        try {
            first.renameTo(target);
        }
        catch(Exception e) {
            String msg = "Fel vid byte av filnamn. Underliggande felmeddelande\n"+e.getMessage();
            logg.error(msg+". filepath="+first.getAbsolutePath(), e);
            functionality.showError(msg);
            return;
        }
    }

}
