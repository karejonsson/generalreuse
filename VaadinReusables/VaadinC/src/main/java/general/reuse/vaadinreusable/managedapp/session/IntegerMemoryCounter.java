package general.reuse.vaadinreusable.managedapp.session;

import org.apache.log4j.Logger;

import java.util.concurrent.atomic.AtomicInteger;

public class IntegerMemoryCounter {

    private static Logger logg = Logger.getLogger(IntegerMemoryCounter.class);

    private String counterName = null;
    private AtomicInteger value = null;

    public IntegerMemoryCounter(Integer startValue, String counterName) {
        value = startValue == null ? null : new AtomicInteger(startValue);
        this.counterName = counterName;
    }

    public Integer getValue() {
        if(value == null) {
            return null;
        }
        return value.intValue();
    }

    public void setValue(Integer value) {
        if(this.value != null) {
            if(value != null) {
                this.value.set(value);
            }
            else {
                this.value = null;
            }
        }
        else {
            if(value != null) {
                this.value = new AtomicInteger(value);
            }
            else {
                // interna value är redan null och sätts till null, inget görs
            }
        }
    }

    public Integer increment() {
        if(value == null) {
            this.value = new AtomicInteger(1);
            return 1;
        }
        return value.addAndGet(1);
    }

    public String getCounterName() {
        return counterName;
    }

}
