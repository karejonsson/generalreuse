package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.checkbox.Checkbox;

public class CheckBoxV8ish extends Checkbox {

    public CheckBoxV8ish() {
        super();
    }

    public CheckBoxV8ish(String caption) {
        super(caption);
    }

    public void setPrimaryStyleName(String cssname) {
        setClassName(cssname);
    }

}
