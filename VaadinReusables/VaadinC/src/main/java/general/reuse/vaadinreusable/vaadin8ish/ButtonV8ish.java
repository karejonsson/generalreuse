package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;

public class ButtonV8ish extends Button {

    public ButtonV8ish() {
        super();
    }
    public ButtonV8ish(VaadinIcon icon) {
        setIcon(new Icon(icon));
    }
    public ButtonV8ish(String caption) {
        super(caption);
    }
    public ButtonV8ish(String caption, VaadinIcon icon) {
        this(icon, caption);
    }
    public ButtonV8ish(VaadinIcon icon, String caption) {
        super(caption);
        setIcon(new Icon(icon));
    }

    public void setPrimaryStyleName(String cssname) {
        super.setClassName(cssname);
    }

    public void setCaption(String s) {
        super.setText(s);
    }

    public void setDescription(String description) {
    }
}
