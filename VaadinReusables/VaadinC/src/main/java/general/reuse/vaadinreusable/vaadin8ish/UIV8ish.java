package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.server.VaadinSession;
import general.reuse.vaadinreusable.managedapp.install.SessionSymbols;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

public abstract class UIV8ish extends Div {

    private UI ui;
    private VaadinSession session;
    private Page page;
    private int width = 0;
    private int height = 0;

    public UIV8ish() {
        this(UI.getCurrent());
    }

    public UIV8ish(UI ui) {
        this.ui = ui;
        init();
    }

    private void init() {
        if(ui == null) {
            ui = UI.getCurrent();
        }

        if(session == null) {
            session = VaadinSession.getCurrent();
            if(session != null) {
                for(String key : attribts.keySet()) {
                    session.setAttribute(key, attribts.get(key));
                }
            }
        }

        if(ui != null) {
            page = ui.getPage();
            if (page != null) {
                page.addBrowserWindowResizeListener(e -> {
                    width = e.getWidth();
                    height = e.getHeight();
                });
                page.retrieveExtendedClientDetails(e -> {
                    width = e.getWindowInnerWidth();
                    height = e.getWindowInnerHeight();
                });
            }
        }
    }

    public int getBrowserWindowWidth() {
        init();
        return width;
    }

    public int getBrowserWindowHeight() {
        init();
        return height;
    }

    public void setTitle(String title) {
        init();
        page.setTitle(title);
    }

    public void addWindow(WindowV8ish win) {
        init();
        ui.add(win);
    }

    public void setContent(Component comp) {
        init();
        removeAll();
        addComponentAsFirst(comp);
    }

    public Component getContent() {
        init();
        return getComponentAt(0);
    }

    public void setLocation(String uri) {
        init();
        page.setLocation(uri);
    }

    private Map<String, Object> attribts = new HashMap<>();

    public void setAttribute(String key, Object o) {
        init();
        attribts.put(key, o);
        if(session != null) {
            session.setAttribute(key, o);
        }
    }

    public Object getAttribute(String key) {
        init();
        if(attribts.containsKey(key)) {
            return attribts.get(key);
        }
        if(session != null) {
            return session.getAttribute(key);
        }
        return null;
    }

    public Future<Void> access(Runnable runnable) {
        init();
        return ui.access(() -> runnable.run());
    }

}
