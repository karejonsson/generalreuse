package general.reuse.vaadinreusable.login;

public interface CredentialsAndGroupVerifyer {

	String[] possibleGroups();
	boolean accepted(String username, String password);
	boolean partOfGroup(String group, String username);
	void setAcceptedGroup(String acceptedGroup);
	String getAcceptedActualGroup();
	
}
