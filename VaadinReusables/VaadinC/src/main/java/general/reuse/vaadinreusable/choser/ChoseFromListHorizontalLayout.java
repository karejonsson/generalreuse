package general.reuse.vaadinreusable.choser;

import com.vaadin.flow.component.notification.Notification;
import general.reuse.vaadinreusable.choser.ChoseFromListVerticalLayout.ChoiceMadeNotifier;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.vaadin8ish.ButtonV8ish;
import general.reuse.vaadinreusable.vaadin8ish.ComboBoxV8ish;
import general.reuse.vaadinreusable.vaadin8ish.HorizontalLayoutV8ish;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ChoseFromListHorizontalLayout<T> extends HorizontalLayoutV8ish {
	
	private static final long serialVersionUID = -2594061578616805386L;

	public interface ToString<T> {
		String render(T t);
	}
	
	public static abstract class Searcher<T> {
		protected ChoiceMadeNotifier<T> searchersChoiceMadeNotifier = null;
		abstract public void search(List<T> list);
		final public void setChoiceMadeNotifier(ChoiceMadeNotifier<T> searchersChoiceMadeNotifier) {
			this.searchersChoiceMadeNotifier = searchersChoiceMadeNotifier;
		}
	}
	
	private List<T> list = null;
	private ToString<T> renderer = null;
	private Searcher<T> searcher = null;
	private List<ComboBoxListItem<T>> cbobjects = null;
	private String title = null;
	private Runnable onCancel = null;
	private Runnable onDone = null;
	private ChoiceMadeNotifier<T> choiceMadeNotifier = null;
	
	private ComboBoxV8ish<ComboBoxListItem<T>> userChoice = null;

	public ChoseFromListHorizontalLayout(List<T> list, ToString<T> renderer, String title) {
		this(list, renderer, null, title, true);
	}

	public ChoseFromListHorizontalLayout(List<T> list, ToString<T> renderer, String title, boolean haveButtons) {
		this(list, renderer, null, title, haveButtons);
	}

	public ChoseFromListHorizontalLayout(List<T> list, ToString<T> renderer, Searcher<T> searcher, String title) {
		this(list, renderer, searcher, title, true);
	}

	public ChoseFromListHorizontalLayout(List<T> list, ToString<T> renderer, Searcher<T> searcher, String title, boolean haveButtons) {
		this.list = list;
		this.renderer = renderer;
		this.searcher = searcher;
		this.title = title;
		
		userChoice = new ComboBoxV8ish<ComboBoxListItem<T>>(title);
		//userChoice.setSizeFull();

		cbobjects = new ArrayList<>();
		list.forEach(it -> cbobjects.add(new ComboBoxListItem<T>(it, renderer)));
		userChoice.setItems(cbobjects);
		
		addComponent(userChoice);

		if(haveButtons) {
			HorizontalLayoutV8ish buttons = new HorizontalLayoutV8ish();
			ButtonV8ish cancelBtn = new ButtonV8ish("Avbryt");
			cancelBtn.addClickListener(e -> { out = null; if(onCancel != null) { onCancel.run(); } });
			buttons.addComponent(cancelBtn);
			ButtonV8ish doneBtn = new ButtonV8ish("Klar");
			doneBtn.addClickListener(e -> { out = getCurrentUserOption(); if(onDone != null) { onDone.run(); } });
			buttons.addComponent(doneBtn);
			if(searcher != null) {
				ButtonV8ish searchBtn = new ButtonV8ish("Sök");
				searchBtn.addClickListener(e -> search());
				buttons.addComponent(searchBtn);
			}
			addComponent(buttons);
		}
		else {
			if(searcher != null) {
				ButtonV8ish searchBtn = new ButtonV8ish("Sök");
				searchBtn.addClickListener(e -> search());
				addComponent(searchBtn);
			}
		}
		if(searcher != null) {
			searcher.setChoiceMadeNotifier(it -> {
				if(it == null) {
					CommonNotification.plain("Inget val gjordes");
					return;
				}
				for(ComboBoxListItem<T> cbobject : cbobjects) {
					if(cbobject.getItem() == it) {
						userChoice.setSelectedItem(cbobject);
						return;
					}
				}
				CommonNotification.plain("Valet finns inte bland valmöjligheterna");
			});
		}
		//this.setMargin(false);//new MarginInfo(false, false, false, false));
	}

	public void setEmptySelectionAllowed(boolean emptySelectionAllowed) {
		userChoice.setEmptySelectionAllowed(emptySelectionAllowed);
	}

	public void setInternalSizeFull() {
		userChoice.setSizeFull();
	}

	public void setInternalTextInputAllowed(boolean editable) {
		userChoice.setTextInputAllowed(editable);
	}

	public void setInternalWidth(String width) {
		userChoice.setWidth(width);
	}

	public void setInternalHeight(String height) {
		userChoice.setHeight(height);
	}

	final public void setChoiceMadeNotifier(ChoiceMadeNotifier<T> choiceMadeNotifier) {
		this.choiceMadeNotifier = choiceMadeNotifier;
		if(choiceMadeNotifier != null) {
/*
			userChoice.addClickListener(e -> {
				setChosenObject(getCurrentUserOption());
				choiceMadeNotifier.choiceMade(getCurrentUserOption());
			});
 */
			userChoice.addValueChangeListener(e -> {
				setChosenObject(getCurrentUserOption());
				choiceMadeNotifier.choiceMade(getCurrentUserOption());
			});
		}
	}
	
	private void search() {
		searcher.search(list);
	}
	
	public void setOnCancel(Runnable onCancel) {
		this.onCancel = onCancel;
	}

	public void setOnDone(Runnable onDone) {
		this.onDone = onDone;
	}

	private T out = null;
	
	public T getChosenObject() {
		if(out == null) {
			T some = getCurrentUserOption();
			if(some != null) {
				out = some;
			}
		}
		return out;
	}

	public void setChosenObject() {
		out = getCurrentUserOption();
		setChosenObject(out);
	}

	public void setChosenObject(T obj) {
		for(ComboBoxListItem<T> cbobject : cbobjects) {
			if(cbobject.getItem().equals(obj)) {
				userChoice.setSelectedItem(cbobject);
				//System.out.println("ChoseFromListHorizontalLayout.setChosenObject(...) träff. obj="+obj);
				return;
			}
		}
		//System.out.println("ChoseFromListHorizontalLayout.setChosenObject(...) fick ingen träff. obj="+obj);
	}
	
	private T getCurrentUserOption() {
		ComboBoxListItem<T> out = null;
		try {
			Optional<ComboBoxListItem<T>> op = userChoice.getSelectedItem();
			out = op.get();
		}
		catch(Exception e) {
			//e.printStackTrace();
		}
		if(out == null) {
			return null;
		}
		return out.getItem();
	}

	private class ComboBoxListItem<T> {
		private T t = null;
		private ToString<T> ts = null;
		public ComboBoxListItem(T t, ToString<T> ts) {
			this.t = t;
			this.ts = ts;
		}
		public String toString() {
			return ts.render(t);
		}
		public T getItem() {
			return t;
		}
	}

}
