package general.reuse.vaadinreusable.managedapp.functionality;

import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;

public interface BaseAdminFunctionalityProvider {
	
	BaseFunctionalityProvider getFunctionality();
	void updateUidname();

	void start();
	void openAdmin(CommonRunnable toHere);


	void addWindow(WindowV8ish win, String winType);
	void setFunctionality(BaseFunctionalityProvider baseFunctionalityProvider);
	void openUserManagement(CommonRunnable onBack);
	void openActiveManagement(CommonRunnable onBack);
	void openFileHandling(CommonRunnable onBack);

}
