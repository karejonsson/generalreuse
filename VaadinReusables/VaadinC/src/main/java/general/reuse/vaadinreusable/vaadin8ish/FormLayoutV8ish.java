package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.formlayout.FormLayout;

public class FormLayoutV8ish extends FormLayout {

    public void addComponent(Component c) {
        add(c);
    }

    public void addStyleName(String classname) {
        addClassName(classname);
    }

    public void setMargin(boolean haveMargin) {

    }

}
