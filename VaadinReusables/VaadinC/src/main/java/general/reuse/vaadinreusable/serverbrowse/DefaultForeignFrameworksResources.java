package general.reuse.vaadinreusable.serverbrowse;

import com.vaadin.flow.component.Component;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;

public class DefaultForeignFrameworksResources implements ForeignFrameworksResources {

    @Override
    public boolean getHaveMargins() {
        return false;
    }

    @Override
    public void showWarning(String s) {
        CommonNotification.showWarning(s);
    }

    @Override
    public void showAssistive(String s) {
        CommonNotification.showAssistive(s);
    }

    @Override
    public void showError(String s) {
        CommonNotification.showError(s);
    }

    @Override
    public void addWindow(WindowV8ish win) {
    }

    @Override
    public void addWindow(WindowV8ish win, String winType) {
    }

    @Override
    public void styles(Component components) {
    }

    @Override
    public String getParseProgramAsGroovyText() {
        return null;
    }

    @Override
    public String getExecuteProgramAsGroovyText() {
        return "Exekvera groovy";
    }

    @Override
    public String getBrowseText() {
        return "Bläddra";
    }

    @Override
    public String getEditText() {
        return "Redigera";
    }

    @Override
    public String getCancelText() {
        return "Avbryt";
    }

    @Override
    public String getSaveText() {
        return "Spara";
    }

    @Override
    public String getOneParameterText() {
        return "En parameter";
    }

    @Override
    public String getFileText() {
        return "Fil";
    }

    @Override
    public String getRevertText() {
        return "Åter";
    }

    @Override
    public String getOnCurrentText() {
        return "På nuvarande";
    }

    @Override
    public String getTwoParametersText() {
        return "Två parametrar";
    }
}
