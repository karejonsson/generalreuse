package general.reuse.vaadinreusable.managedapp.functionality;

import general.reuse.vaadinreusable.vaadin8ish.ButtonV8ish;
import general.reuse.vaadinreusable.vaadin8ish.UIV8ish;
import general.reuse.vaadinreusable.vaadin8ish.VerticalLayoutV8ish;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;
import org.apache.log4j.Logger;

import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;
import general.reuse.vaadinreusable.managedapp.texts.ActivityTexts;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;
import general.reuse.vaadinreusable.managedapp.factories.LabelFactory;

public class DefaultBaseAdminFunctionalityProvider implements BaseAdminFunctionalityProvider {

	private static Logger logg = Logger.getLogger(DefaultBaseAdminFunctionalityProvider.class);

	protected UIV8ish ui = null;
	private BaseFunctionalityProvider functionality = null;
	
	public String toString() {
		StringBuffer out = new StringBuffer();
		out.append("DefaultAdminFunctionality: UI "+(ui != null));
		out.append(", functionality "+(functionality != null));
		return out.toString();
	}

	public DefaultBaseAdminFunctionalityProvider(UIV8ish ui) {
		this.ui = ui;
	}

	public void updateUidname() {
		if(functionality != null) {
			functionality.updateUidname();
		}
	}

	@Override
	public void openAdmin(CommonRunnable onBack) {
		updateUidname();
		functionality.setActivity(ActivityTexts.openAdmin, false);
		VerticalLayoutV8ish vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		vl.addComponent(LabelFactory.create("Navigationsfunktionalitet för administration ej tillhanda"));
		ButtonV8ish backBtn = ButtonFactory.create(ButtonTexts.revert);
		backBtn.addClickListener(e -> onBack.run());
		vl.addComponent(backBtn);
		ui.setContent(vl);
	}

	@Override
	public void start() {
		functionality.start();
	}

	@Override
	public void addWindow(WindowV8ish win, String winType) {
		functionality.addWindow(win, true, winType);
	}

	@Override
	public BaseFunctionalityProvider getFunctionality() {
		return functionality;
	}

	@Override
	public void setFunctionality(BaseFunctionalityProvider functionality) {
		this.functionality = functionality;
	}

	@Override
	public void openUserManagement(CommonRunnable onBack) {
		updateUidname();
		functionality.setActivity(ActivityTexts.openUserManagement, false);
		VerticalLayoutV8ish vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		vl.addComponent(LabelFactory.create("Funktionalitet för att admin skall hantera användare ej tillhanda"));
		ButtonV8ish backBtn = ButtonFactory.create(ButtonTexts.revert);
		backBtn.addClickListener(e -> onBack.run());
		vl.addComponent(backBtn);
		ui.setContent(vl);
	}

	@Override
	public void openActiveManagement(CommonRunnable onBack) {
		updateUidname();
		functionality.setActivity(ActivityTexts.openActiveManagement, false);
		VerticalLayoutV8ish vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		vl.addComponent(LabelFactory.create("Funktionalitet för att admin skall hantera alla aktiva användare ej tillhanda"));
		ButtonV8ish backBtn = ButtonFactory.create(ButtonTexts.revert);
		backBtn.addClickListener(e -> onBack.run());
		vl.addComponent(backBtn);
		ui.setContent(vl);
	}

	@Override
	public void openFileHandling(CommonRunnable onBack) {
		updateUidname();
		functionality.setActivity(ActivityTexts.openFileHandling, false);
		VerticalLayoutV8ish vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		vl.addComponent(LabelFactory.create("Funktionalitet för admins filhantering ej tillhanda"));
		ButtonV8ish backBtn = ButtonFactory.create(ButtonTexts.revert);
		backBtn.addClickListener(e -> onBack.run());
		vl.addComponent(backBtn);
		ui.setContent(vl);
	}

}
