package general.reuse.vaadinreusable.serverbrowse;

import java.io.File;

public interface FilesystemCallback {
    String describeFile(File file);
    String describeDirectory(File file);
    void onClickFile(File file);
    void onClickDirectory(File file);
}

