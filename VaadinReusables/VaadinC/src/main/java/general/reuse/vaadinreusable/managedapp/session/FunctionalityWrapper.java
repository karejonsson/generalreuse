package general.reuse.vaadinreusable.managedapp.session;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.server.VaadinServlet;
import general.reuse.vaadinreusable.choser.QuestionAndButtonsWindow;
import org.apache.log4j.Logger;
import general.reuse.vaadinreusable.managedapp.functionality.CommonRunnable;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.functionality.UserInteractionEvent;
import general.reuse.vaadinreusable.managedapp.install.InstallationProperties;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;

public class FunctionalityWrapper {

	private final static Logger log = Logger.getLogger(FunctionalityWrapper.class);

	public final static String dateFormat = "yyyy-MM-dd HH:mm:ss.SSS";
	private final BaseFunctionalityProvider functionality;
	private final SimpleDateFormat formatter;
	private final Date start;
	
	public FunctionalityWrapper(BaseFunctionalityProvider functionality) {
		this.functionality = functionality;
		start = new Date();
		formatter = new SimpleDateFormat(dateFormat);
	}

	public boolean contains(BaseFunctionalityProvider functionality) {
		return this.functionality == functionality;
	}
	
	public String getStart() {
		return formatter.format(start);
	}
	
	public String getLast() {
		return formatter.format(functionality.getLast());
	}
	
	public String getUsername() {
		return functionality.getUsername();
	}

	public int getSecondsIdle() {
		if(functionality == null) {
			return 0;
		}
		Date last = functionality.getLast();
		if(last == null) {
			return 0;
		}
		return (int) ((System.currentTimeMillis() - last.getTime()) / 1000);
	}
	
	public String toString() {
		return "FunctionalityWrapper{username="+getUsername()+", sekunder overksam "+getSecondsIdle()+"}";
	}

	public String getPrivilege() {
		return functionality.getUserRolename();
	}

	public void destroy() {
		functionality.destroyAllInternals();
	}

	public void throwOutMechanically() {
		try {
			functionality.destroyAllInternals();
			SessionRegistry.remove(functionality);
		}
		catch(Exception e) {
			log.error("Fel vid intern destruering", e);
		}
	}

	public void setLocationLogout(final Runnable onDone) {
		Runnable r = () -> {
			try {
				String location = InstallationProperties.getString(InstallationProperties.prv_managedapp_logoutredirect, null);
				if (location == null || location.trim().length() == 0) {
					log.info("Loggar ut och visar utloggningstext");
					functionality.setLocation(VaadinServlet.getCurrent().getServletContext().getContextPath() + "/utloggad");
				} else {
					log.info("Loggar ut och pekar om läsaren till " + location);
					functionality.setLocation(location);
				}
			}
			catch(Exception e) {
				log.error("Fel vid omflyttningen av utkastade användarens webbläsare", e);
			}
			if(onDone != null) {
				try {
					onDone.run();
					log.info("Avslutningsrutinen körd utan problem");
				}
				catch(Exception e) {
					log.error("Avslutningsrutinen körd med problem", e);
				}
			}
			else {
				log.info("Ingen avslutningsrutin att köra");
			}
		};

		functionality.access(r);
	}

	public void flashMessage(String message) {
		functionality.access(() -> CommonNotification.showAssistive(functionality, message));
    }

	public void forceToChat() {
		final CommonRunnable back = new CommonRunnable(functionality.getRunnableForReturnToCurrentContent());
		functionality.access(() -> functionality.openChat(back));
	}

	public void askToChat(String question) {
		functionality.access(() -> {
			log.info("Ber om chatt: "+functionality.getUsername());
			QuestionAndButtonsWindow confirm = new QuestionAndButtonsWindow("Systemskötsel", question);
			confirm.addButtonWithClose(VaadinIcon.THUMBS_UP, ButtonTexts.yes, () -> {
				log.info("Beviljar chatt: "+functionality.getUsername());
				final CommonRunnable back = new CommonRunnable(functionality.getRunnableForReturnToCurrentContent());
				functionality.access(() -> functionality.openChat(back));
			});
			confirm.addButtonWithClose(VaadinIcon.THUMBS_DOWN, ButtonTexts.no, () -> {
				log.info("Avböjer chatt: "+functionality.getUsername());
			});
			confirm.setWidth("15%");
			functionality.addWindow(confirm, "Ber om chatt");
		});
	}

	public String getStateDescription() {
		return functionality.getStateDescription();
    }

    public String getActivity() {
		return functionality.getActivity();
    }

	public UserInteractionEvent[] getActivityEvents() {
		return functionality.getActivityEvents();
	}

	public boolean isStateful() {
		return functionality.getStateful();
	}

	public boolean possiblyVulnerable() {
		return functionality.possiblyWulnerable();
	}

	public boolean isFunctional() {
		return functionality.isFunctional();
	}

}
