package general.reuse.vaadinreusable.login;

//import com.vaadin.navigator.View;
//import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.vaadin8ish.*;
//import org.springframework.web.servlet.View;

public class CredentialsLoginPage extends VerticalLayoutV8ish {
	
	private static final long serialVersionUID = 3484919610407063509L;

	public CredentialsLoginPage(CredentialsVerifyer verifyer, Runnable onSuccess, Runnable onFailure) {
		this(verifyer, onSuccess, onFailure, "Behörighetskontroll", "Användarnamn", "Lösenord", "Vidare", "Felaktiga behörigheter");
	}
	
	public CredentialsLoginPage(CredentialsVerifyer verifyer, Runnable onSuccess, Runnable onFailure, String frameTitle, String frameUsername, String framePassword, String frameEnter, String frameWrongCredentials) {
		if(verifyer.isDevmode()) {
			//System.out.println("<ctor> utvecklingsläge");
			//postponedEnter(verifyer, onSuccess);
		}
		else {
			//System.out.println("<ctor> inte utvecklingsläge");
		}
		PanelV8ish panel = new PanelV8ish(frameTitle);
		panel.setSizeUndefined();
		addComponent(panel);
		
		FormLayoutV8ish content = new FormLayoutV8ish();
		TextFieldV8ish username = new TextFieldV8ish(frameUsername);
		content.addComponent(username);
		PasswordFieldV8ish password = new PasswordFieldV8ish(framePassword);
		content.addComponent(password);

		ButtonV8ish send = new ButtonV8ish(frameEnter);
		send.addClickListener(e -> {
			if(verifyer.accepted(username.getValue().toLowerCase(), password.getValue())) {
				onSuccess.run();
			}
			else {
				if(onFailure == null) {
					CommonNotification.showError(frameWrongCredentials);
				}
				else {
					onFailure.run();
				}
			}
		});
		
		content.addComponent(send);
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		setComponentAlignment(panel, AlignmentV8ish.MIDDLE_CENTER);
	}

	/*
	@Override
	public void enter(ViewChangeEvent event) {
	}
	*/
	
	private void postponedEnter(final CredentialsVerifyer verifyer, final Runnable onSuccess) { //, final UI current) {
		System.out.println("postponedEnter");
		Runnable doLater = new Runnable() {
			@Override
			public void run() {
				while(true) {
					//System.out.println("THREAD UI "+UI.getCurrent()+", PAGE "+Page.getCurrent()+", VAADINSESSION "+VaadinSession.getCurrent());
					//System.out.println("postponedEnter väntar");
					try {
						Thread.sleep(5500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					try {
						verifyer.prepareDevmode();
						break;
					}
					catch(Exception e) { e.printStackTrace(); }
				}
				System.out.println("postponedEnter klart");
				onSuccess.run();		
			}
		};
		Thread t = new Thread(doLater);
		t.start();
	}
	
}
