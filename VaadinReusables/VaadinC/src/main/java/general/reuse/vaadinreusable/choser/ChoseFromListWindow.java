package general.reuse.vaadinreusable.choser;

import general.reuse.vaadinreusable.choser.ChoseFromListVerticalLayout.Searcher;
import general.reuse.vaadinreusable.choser.ChoseFromListVerticalLayout.ToString;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;

import java.util.List;

public class ChoseFromListWindow<T> extends WindowV8ish {
	
	private static final long serialVersionUID = -2594061578616805386L;

	private ChoseFromListVerticalLayout<T> layout = null;
	
	public ChoseFromListWindow(List<T> list, ToString<T> renderer, String title) {
		this(list, renderer, null, title, true);
	}
	
	public ChoseFromListWindow(List<T> list, ToString<T> renderer, String title, boolean haveButtons) {
		this(list, renderer, null, title, haveButtons);
	}
	
	public ChoseFromListWindow(List<T> list, ToString<T> renderer, Searcher<T> searcher, String title) {
		this(list, renderer, searcher, title, true);
	}
	
	public ChoseFromListWindow(List<T> list, ToString<T> renderer, Searcher<T> searcher, String title, boolean haveButtons) {
		layout = new ChoseFromListVerticalLayout<T>(list, renderer, searcher, title, haveButtons);
		setContent(layout);
	}
	
	public void setOnCancel(Runnable onCancel) {
		layout.setOnCancel(onCancel);
	}

	public void setOnDone(Runnable onDone) {
		layout.setOnDone(onDone);
	}
	
	public T getChosenObject() {
		return layout.getChosenObject();
	}
	
	public void sortList() {
		layout.sortList();
	}

}
