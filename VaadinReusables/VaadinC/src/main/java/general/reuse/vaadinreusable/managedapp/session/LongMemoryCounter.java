package general.reuse.vaadinreusable.managedapp.session;

import org.apache.log4j.Logger;

import java.util.concurrent.atomic.AtomicLong;

public class LongMemoryCounter {

    private static Logger logg = Logger.getLogger(IntegerMemoryCounter.class);

    private String counterName = null;
    private AtomicLong value = null;

    public LongMemoryCounter(Long startValue, String counterName) {
        value = startValue == null ? null : new AtomicLong(startValue);
        this.counterName = counterName;
    }

    public Long getValue() {
        if(value == null) {
            return null;
        }
        return value.longValue();
    }

    public void setValue(Long value) {
        if(this.value != null) {
            if(value != null) {
                this.value.set(value);
            }
            else {
                this.value = null;
            }
        }
        else {
            if(value != null) {
                this.value = new AtomicLong(value);
            }
            else {
                // interna value är redan null och sätts till null, inget görs
            }
        }
    }

    public Long increment() {
        if(value == null) {
            this.value = new AtomicLong(1);
            return 1l;
        }
        return value.addAndGet(1);
    }

    public String getCounterName() {
        return counterName;
    }

}
