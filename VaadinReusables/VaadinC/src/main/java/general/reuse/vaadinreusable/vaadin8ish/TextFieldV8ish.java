package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.textfield.TextField;

public class TextFieldV8ish extends TextField {
    public TextFieldV8ish() {
        super();
    }
    public TextFieldV8ish(String caption) {
        super(caption);
    }

    public void setPrimaryStyleName(String cssname) {
        setClassName(cssname);
    }

}
