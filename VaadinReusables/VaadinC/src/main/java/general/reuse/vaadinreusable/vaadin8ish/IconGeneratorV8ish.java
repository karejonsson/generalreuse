package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.icon.VaadinIcon;

public interface IconGeneratorV8ish<T> {

    VaadinIcon get(T f);

}