package general.reuse.vaadinreusable.managedapp.install;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MetaAnalysis {
	
	public static List<String> getFields(Class clazz) {
		Field[] fields = clazz.getDeclaredFields();
		List<String> out = new ArrayList<>();
		for(Field field : fields) {
			if(field.getType() == String.class) {
				if(Modifier.isStatic(field.getModifiers())) {
					try {
						Object obj = field.get(null);
						if(obj != null) {
							out.add(obj.toString());
						}
					}
					catch(Exception e) {}
				}
			}
		}
		Collections.sort(out);
		return out;
	}

}
