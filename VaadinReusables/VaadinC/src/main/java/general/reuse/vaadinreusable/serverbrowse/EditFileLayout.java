package general.reuse.vaadinreusable.serverbrowse;

import general.reuse.vaadinreusable.vaadin8ish.*;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.nio.charset.Charset;

public class EditFileLayout extends VerticalSplitPanelV8ish {

    private static Logger logg = Logger.getLogger(ServerFilesystemManipulationPanel.class);

    private ForeignFrameworksResources functionality = null;

    private TextFieldV8ish tf = null;
    private PanelV8ish workArea = null;
    private HorizontalLayoutV8ish lowerRight = null;

    public EditFileLayout(ForeignFrameworksResources functionality) {
        functionality.styles(this);
        this.functionality = functionality;
        workArea = new PanelV8ish();
        functionality.styles(workArea);
        workArea.setSizeFull();
        setFirstComponent(workArea);
        setSplitPosition(92, UnitV8ish.PERCENTAGE);
        setupLower();
        setSecondComponent(lowerRight);
        editLowerRight();
    }

    private ButtonV8ish browseBtn = null;
    private ButtonV8ish groovyExecBtn = null;
    private ButtonV8ish groovyParseBtn = null;
    private ButtonV8ish editBtn = null;
    private ButtonV8ish cancelBtn = null;
    private ButtonV8ish saveBtn = null;

    private void setupLower() {
        lowerRight = new HorizontalLayoutV8ish();
        functionality.styles(lowerRight);
        lowerRight.setWidth("100%");
        lowerRight.setHeight("100%");

        tf = new TextFieldV8ish("Aktuell fil");
        functionality.styles(tf);
        tf.setWidth("100%");
        tf.setHeight("40px");//.setSizeFull();//Width("900px");
        lowerRight.addComponent(tf);

        HorizontalLayoutV8ish lowerRightBtns = new HorizontalLayoutV8ish();
        functionality.styles(lowerRightBtns);

        groovyParseBtn = new ButtonV8ish(functionality.getParseProgramAsGroovyText());
                //ButtonFactory.create(ButtonTexts.parseProgramAsGroovy);
        groovyParseBtn.addClickListener(e -> executeAsGroovy());
        lowerRightBtns.addComponent(groovyParseBtn);
        lowerRightBtns.setComponentAlignment(groovyParseBtn, AlignmentV8ish.BOTTOM_RIGHT);

        groovyExecBtn = new ButtonV8ish(functionality.getExecuteProgramAsGroovyText());
                //ButtonFactory.create(ButtonTexts.executeProgramAsGroovy);
        groovyExecBtn.addClickListener(e -> executeAsGroovy());
        lowerRightBtns.addComponent(groovyExecBtn);
        lowerRightBtns.setComponentAlignment(groovyExecBtn, AlignmentV8ish.BOTTOM_RIGHT);

        browseBtn = new ButtonV8ish(functionality.getBrowseText());
                //ButtonFactory.create(ButtonTexts.browse);
        browseBtn.addClickListener(e -> browse());
        lowerRightBtns.addComponent(browseBtn);
        lowerRightBtns.setComponentAlignment(browseBtn, AlignmentV8ish.BOTTOM_RIGHT);

        editBtn = new ButtonV8ish(functionality.getEditText());
                //ButtonFactory.create(ButtonTexts.edit);
        editBtn.addClickListener(e -> editLowerRight());
        lowerRightBtns.addComponent(editBtn);
        lowerRightBtns.setComponentAlignment(editBtn, AlignmentV8ish.BOTTOM_RIGHT);

        cancelBtn = new ButtonV8ish(functionality.getCancelText());
                //ButtonFactory.create(ButtonTexts.cancel);
        cancelBtn.addClickListener(e -> cancelLowerRight());
        lowerRightBtns.addComponent(cancelBtn);
        lowerRightBtns.setComponentAlignment(cancelBtn, AlignmentV8ish.BOTTOM_RIGHT);

        saveBtn = new ButtonV8ish(functionality.getSaveText());
                //ButtonFactory.create(ButtonTexts.save);
        saveBtn.addClickListener(e -> saveLowerRight());
        lowerRightBtns.addComponent(saveBtn);
        lowerRightBtns.setComponentAlignment(saveBtn, AlignmentV8ish.BOTTOM_RIGHT);

        lowerRight.addComponent(lowerRightBtns);
        lowerRight.setComponentAlignment(lowerRightBtns, AlignmentV8ish.BOTTOM_RIGHT);
        //lowerRight.setMargin(false);
        //lowerRight.setSpacing(false);
    }

    private void parseAsGroovy() {
        if(editTA == null) {
            functionality.showWarning("Det finns inget program att parsa!");
            return;
        }
        String groovyProgram = editTA.getValue();
        try {
            Object out = EvaluateScript.parse(groovyProgram);
            if(out != null) {
                functionality.showAssistive("OK!");
            }
            else {
                functionality.showError("Fel, ej kompilerbart");
            }
        }
        catch(Exception e) {
            functionality.showError("Fel: "+e.getMessage());
        }
    }

    private void executeAsGroovy() {
        if(editTA == null) {
            functionality.showWarning("Det finns inget program att exekvera!");
        return;
        }
        String groovyProgram = editTA.getValue();
        //System.out.println("Program: "+groovyProgram);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Object out = EvaluateScript.eval(groovyProgram, baos);
            String output = baos.toString(Charset.defaultCharset());
            int len = output.length();
            String report = null;
            if(len > 4000) {
                report = output.substring(0, 4000)+"\n\n+ ytterligare "+(len-4000)+" tecken";
            }
            else {
                report = output;
            }
            if(out == null) {
                if(report == null) {
                    functionality.showAssistive("Färdigt.\nInget producerat resultat!\nIngen utskrift");
                }
                else {
                    functionality.showAssistive("Färdigt.\nInget producerat resultat!\nUtskrift\n"+report);
                }
            }
            else {
                if(report == null) {
                    functionality.showAssistive("Färdigt.\nProducerade "+out+"\nIngen utskrift");
                }
                else {
                    functionality.showAssistive("Färdigt.\nProducerade "+out+"\nUtskrift\n"+report);
                }
            }
        }
        catch(Exception e) {
            //e.printStackTrace();
            functionality.showError("Fel: "+e.getMessage());
        }
    }

    private void browse() {
        TreeV8ish<File> tree = OperationsReuse.getBrowser(tf);
        WindowV8ish win = new WindowV8ish();
        win.setWidth("80%");
        win.setHeight("80%");
        VerticalLayoutV8ish l = new VerticalLayoutV8ish();
        l.addComponent(tree);
        win.setContent(l);
        functionality.addWindow(win, "Bläddra redigering");
    }

    private TextAreaV8ish editTA = null;

    private void editLowerRight() {
        String filepath = tf.getValue();
        logg.info("filepath="+filepath);
        setEditMode(true);

        File f = new File(filepath);
        if(!f.exists()) {
            String msg = "Filen finns inte";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        if(f.isDirectory()) {
            String msg = "Filen är en katalog. För att redigera måste det vara en fil.";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        setEditMode(true);
        if(editTA == null) {
            editTA = new TextAreaV8ish();
        }
        editTA.setSizeFull();
        workArea.setContent(editTA);
        editTA.setValue(readWholeFile(f));
    }

    private void cancelLowerRight() {
        setEditMode(false);
    }

    //private VerticalLayout editVL = null;
    private static String readWholeFile(File f) {
        try {
            return new String((new FileInputStream(f)).readAllBytes(), "UTF-8");
        }
        catch(Exception e) {

        }
        return null;
    }

    private void setEditMode(boolean editmode) {
        editBtn.setEnabled(!editmode);
        cancelBtn.setEnabled(editmode);
        saveBtn.setEnabled(editmode);
        if(editTA != null) {
            editTA.setEnabled(editmode);
        }
    }

    private void saveLowerRight() {
        String filepath = tf.getValue();
        logg.info("filepath="+filepath);
        if(editTA == null) {
            String msg = "Redigeringsfönstret saknas. Inget att skriva ner";
            logg.info(msg+". filepath="+filepath);
            functionality.showWarning(msg);
            return;
        }
        String contents = editTA.getValue();
        FileWriter fw = null;
        try {
            File f = new File(filepath);
            fw = new FileWriter(f);
            fw.write(contents);
            fw.close();
            fw = null;
        } catch (Exception e) {
            String msg = "Fel vid skrivning av filen";
            logg.error(msg+". filepath="+filepath, e);
            functionality.showError(msg);
            return;
        }
        try {
            if(fw != null) {
                fw.close();
            }
        }
        catch(Exception e) {
            String msg = "Fel vid stängning av filen";
            logg.error(msg+". filepath="+filepath, e);
            functionality.showError(msg);
            return;
        }
        setEditMode(false);
    }

}