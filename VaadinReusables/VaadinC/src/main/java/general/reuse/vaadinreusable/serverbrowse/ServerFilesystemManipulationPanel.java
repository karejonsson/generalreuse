package general.reuse.vaadinreusable.serverbrowse;

import general.reuse.vaadinreusable.vaadin8ish.*;
import org.apache.log4j.Logger;

public class ServerFilesystemManipulationPanel extends HorizontalSplitPanelV8ish {

    private static Logger logg = Logger.getLogger(ServerFilesystemManipulationPanel.class);

    private ForeignFrameworksResources functionality = null;
    private Runnable onBack = null;

    private VerticalLayoutV8ish left = null;
    private VerticalSplitPanelV8ish right = null;
    private PanelV8ish workArea = null;
    private HorizontalLayoutV8ish lowerRight = null;

    public ServerFilesystemManipulationPanel() {
        this(new DefaultForeignFrameworksResources(), null);
    }

    public ServerFilesystemManipulationPanel(ForeignFrameworksResources functionality, Runnable onBack) {
        functionality.styles(this);
        this.functionality = functionality;
        this.onBack = onBack;
        setSizeFull();
        setupLeft();
        setFirstComponent(left);
        right = new VerticalSplitPanelV8ish();
        setSecondComponent(right);
        right.setSizeFull();
        workArea = new PanelV8ish();
        workArea.setHeight("100%");
        setSecondComponent(workArea);
        setSplitPosition(16, UnitV8ish.PERCENTAGE);
    }

    private void setupLeft() {
        left = new VerticalLayoutV8ish();
        left.setWidth("260px");

        if(onBack != null) {
            ButtonV8ish backBtn = new ButtonV8ish(functionality.getRevertText());
            backBtn.addClickListener(e -> onBack.run());
            left.addComponent(backBtn);
        }

        VerticalLayoutV8ish onCurrent = new VerticalLayoutV8ish();
        onCurrent.setCaption(functionality.getOnCurrentText());

        editBtn = new ButtonV8ish(functionality.getEditText());
        editBtn.addClickListener(e -> editOnRight());
        onCurrent.addComponent(editBtn);

        VerticalLayoutV8ish onServerside = new VerticalLayoutV8ish();
        onServerside.setCaption("Serversidan");

        ButtonV8ish oneOp = new ButtonV8ish(functionality.getOneParameterText());
        oneOp.addClickListener(e -> operationsForOneServersideParameter());
        onServerside.addComponent(oneOp);

        ButtonV8ish twoOps = new ButtonV8ish(functionality.getTwoParametersText());
        twoOps.addClickListener(e -> operationsForTwoServersideParameters());
        onServerside.addComponent(twoOps);

        onCurrent.addComponent(onServerside);

        VerticalLayoutV8ish onMixedServerAndNetside = new VerticalLayoutV8ish();
        onMixedServerAndNetside.setCaption("Serversidan och nätverket");

        onCurrent.addComponent(onMixedServerAndNetside);
        ButtonV8ish netAndServerOps = new ButtonV8ish("Nät resp serversida");
        netAndServerOps.addClickListener(e -> operationsForNetAndServersideParameters());
        onServerside.addComponent(netAndServerOps);

        left.addComponent(onCurrent);
    }

    private ButtonV8ish editBtn = null;

    private void editOnRight() {
        setSecondComponent(new EditFileLayout(functionality));
    }

    private void operationsForOneServersideParameter() {
        setSecondComponent(new OperateOnOneBrowsedLayout(
                functionality,
                new OperateOnOneBrowsed[] {
                        new OperateOnOneExpandZipOperation(functionality),
                        new OperateOnOneFolderZipOperation(functionality),
                        new OperateOnOneUploadOperation(functionality),
                        new OperateOnOneDownloadOperation(functionality),
                        new OperateOnOneCreateFolderOperation(functionality),
                        new OperateOnOneDeleteFolderOperation(functionality),
                        new OperateOnOneDeleteFileOperation(functionality),
                        new OperateOnOneCreateFileOperation(functionality),
                        new OperateOnOneDescribeOperation(functionality)
                }));

    }

    private void operationsForTwoServersideParameters() {
        setSecondComponent(new OperateOnTwoBrowsedLayout_source_target(
                functionality,
                new OperateOnTwoBrowsed_source_target[] {
                        new OperateOnTwoChangeNameOperation(functionality),
                        new OperateOnTwoMoveFileOperation(functionality),
                        new OperateOnTwoMoveFolderOperation(functionality),
                        new OperateOnTwoExpandZipOperation(functionality),
                        new OperateOnTwoFolderZipOperation(functionality)
                }));
    }

    private void operationsForNetAndServersideParameters() {
        setSecondComponent(new OperateOnWrittenBrowsedLayout_source_target(
                functionality,
                new OperateOnWrittenBrowsed_source_target[] {
                        new OperateOnNetworkServerUploadOperation(functionality)
                }));
    }

}
