package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.flow.component.icon.VaadinIcon;
import general.reuse.vaadinreusable.vaadin8ish.CheckBoxV8ish;

public class CheckBoxFactory {

	public final static String cssname = "cmdlwebcheckbox";
	private static final boolean isStyled = false;
	
	public static CheckBoxV8ish styles(CheckBoxV8ish in) {
		if(isStyled) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static CheckBoxV8ish create(String caption) {
		return styles(new CheckBoxV8ish(caption));
	}

	public static CheckBoxV8ish create() {
		return styles(new CheckBoxV8ish());
	}

}
