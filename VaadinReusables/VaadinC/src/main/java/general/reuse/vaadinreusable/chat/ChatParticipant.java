package general.reuse.vaadinreusable.chat;

import general.reuse.vaadinreusable.vaadin8ish.TextFieldV8ish;

public interface ChatParticipant {
    boolean hasChatAnyway();
    String getUsername();
    boolean getHaveMargins();
    boolean isFunctional();
    void setChatSince(long since);
    void access(Runnable onAccess);
    void addReturnKeyListener(TextFieldV8ish tf, Runnable onSend);
}
