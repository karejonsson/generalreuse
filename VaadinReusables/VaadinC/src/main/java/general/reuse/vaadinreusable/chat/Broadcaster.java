package general.reuse.vaadinreusable.chat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Broadcaster implements Serializable {

	private static List<ChatHolder> replay = new ArrayList<>();
	
    static ExecutorService executorService = Executors.newSingleThreadExecutor();

    private static LinkedList<BroadcastListener> listeners = new LinkedList<BroadcastListener>();

	private static void updateListeners() {
		LinkedList<BroadcastListener> activeListeners = new LinkedList<BroadcastListener>();
		List<String> usernames = new ArrayList<>();
		listeners.forEach(it -> {
			if(it.isFunctional()) {
				if(!usernames.contains(it.getUsername())) {
					activeListeners.add(it);
					usernames.add(it.getUsername());
				}
			}
		});
		listeners = activeListeners;
	}

	private static void _updateListeners() {
		LinkedList<BroadcastListener> activeListeners = new LinkedList<BroadcastListener>();
		listeners.forEach(it -> {
			if(it.isFunctional()) {
				activeListeners.add(it);
			}
		});
		listeners = activeListeners;
		listeners.forEach(it -> System.out.println("OK: "+it.getUsername()));
	}

	public static synchronized List<String> users() {
		//updateListeners();
    	List<String> out = new ArrayList<>();
    	for(BroadcastListener bl : listeners) {
    		out.add(bl.getUsername());
    	}
    	return out;
    }

    public static synchronized void register(BroadcastListener listener) {
		if(!listener.isFunctional()) {
			return;
		}
        join(listener.getUsername());
        listeners.add(0, listener);
		updateListeners();
    }

    public static synchronized void unregister(BroadcastListener listener) {
        listeners.remove(listener);
        leave(listener.getUsername());
		updateListeners();
    }
    
    public static final int storeAmount = 50;

    public static synchronized void message(final String username, final String message) {
    	ChatHolder ch = new ChatHolder();
    	ch.username = username;
    	ch.message = message;
    	ch.time = System.currentTimeMillis();
    	replay.add(ch);
    	while(replay.size() != 0 && replay.size() > storeAmount) {
    		replay.remove(0);
    	}
		updateListeners();
        for (final BroadcastListener listener: listeners) {
            executorService.execute(() -> listener.message(ch));
        }
    }
    
    public static synchronized void join(final String username) {
		updateListeners();
        for (final BroadcastListener listener: listeners) {
        	if(listener.isFunctional()) {
				executorService.execute(() -> listener.join(username));
			}
        }
    }
    
    public static synchronized void leave(final String username) {
		updateListeners();
        for (final BroadcastListener listener: listeners) {
        	if(listener.isFunctional()) {
				executorService.execute(() -> listener.leave(username));
			}
		}
    }
    
    public static synchronized void replayAmount(final String username, int amount) {
    	BroadcastListener target = null;
		updateListeners();
    	for(BroadcastListener listener : listeners) {
    		if(listener.getUsername().equals(username)) {
    			target = listener;
    			System.out.println("Fann användaren: "+listener.getUsername());
    		}
    		else {
    			System.out.println("Inte "+listener.getUsername());
    		}
    	}
    	if(target == null) {
    		return;
    	}
    	final BroadcastListener targetF = target;
		System.out.println("replay.size()="+replay.size()+", amount="+amount+", Math.max(0, replay.size()-amount)="+Math.max(0, replay.size()-amount));
    	for(int i = Math.max(0, replay.size()-amount) ; i < replay.size() ; i++) {
    		final int iF = i;
			System.out.println("iF ="+iF);
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                	System.out.println("##### Försöker");
                	targetF.message(replay.get(iF));
                }
            });
    	}
    }
    
    public static synchronized void replaySince(final String username, long since) {
    	BroadcastListener target = null;
		updateListeners();
    	for(BroadcastListener listener : listeners) {
			if(listener.isFunctional()) {
				if (listener.getUsername().equals(username)) {
					target = listener;
				}
			}
    	}
    	if(target == null) {
    		return;
    	}
    	final BroadcastListener targetF = target;
    	for(int i = 0 ; i < replay.size() ; i++) {
    		final ChatHolder ch = replay.get(i);
    		if(ch.time > since) {
                executorService.execute(() -> targetF.message(ch));
    		}
    	}
    }
    
    public static synchronized int countSince(long since) {
    	int count = 0;
    	for(int i = 0 ; i < replay.size() ; i++) {
    		final ChatHolder ch = replay.get(i);
    		if(ch.time > since) {
    			count++;
    		}
    	}
    	return count;
    }
    
    public static synchronized void clear() {
    	replay.clear();
    }

	public static int countMessages() {
		return replay.size();
	}
    
}