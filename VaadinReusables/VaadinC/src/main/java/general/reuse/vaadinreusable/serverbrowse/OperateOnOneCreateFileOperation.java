package general.reuse.vaadinreusable.serverbrowse;

import org.apache.log4j.Logger;

import java.io.File;

public class OperateOnOneCreateFileOperation implements OperateOnOneBrowsed {

    private static Logger logg = Logger.getLogger(OperateOnOneCreateFileOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnOneCreateFileOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Skapa fil";
    }

    @Override
    public String getDescription() {
        return "Skapar en tom fil";
    }

    @Override
    public Runnable getOperate(String filepath) {
        return () -> {
            operate(filepath);
        };
    }

    @Override
    public void operate(String filepath) {
        logg.info("filepath="+filepath);

        File f = new File(filepath);
        if(f.exists()) {
            String msg = "Filen finns redan";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        if(f.isDirectory()) {
            String msg = "Det finns redan en katalog på den platsen";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        File p = f.getParentFile();
        if(!p.exists()) {
            String msg = "Nivån ett steg upp finns inte";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        try {
            if(f.createNewFile()) {
                String msg = "Filen är skapad";
                logg.info(msg+". filepath="+filepath);
                functionality.showAssistive(msg);
                return;
            }
            else {
                String msg = "Filen kunde inte skapas";
                logg.info(msg+". filepath="+filepath);
                functionality.showError(msg);
                return;
            }
        }
        catch(Exception e) {
            String msg = "Filen kunde inte skapas på grund av internt fel";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
    }

}
