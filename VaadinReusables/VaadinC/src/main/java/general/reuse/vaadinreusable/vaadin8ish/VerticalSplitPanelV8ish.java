package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.splitlayout.SplitLayout;

public class VerticalSplitPanelV8ish extends SplitLayout {

    public VerticalSplitPanelV8ish() {
        setOrientation(Orientation.VERTICAL);
    }

    public void setCaption(String caption) {
    }

    public void setSplitPosition(int i, UnitV8ish unit) {
        unit.set(this, i);
    }

    public void setFirstComponent(Component first) {
        addToPrimary(first);
    }

    public void setSecondComponent(Component second) {
        addToSecondary(second);
    }

    public void setPrimaryStyleName(String cssverticalname) {
        setClassName(cssverticalname);
    }

}
