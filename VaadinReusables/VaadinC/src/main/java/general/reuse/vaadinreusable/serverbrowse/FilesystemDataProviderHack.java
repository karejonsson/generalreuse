package general.reuse.vaadinreusable.serverbrowse;

import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import general.reuse.vaadinreusable.vaadin8ish.IconGeneratorV8ish;
import general.reuse.vaadinreusable.vaadin8ish.TreeV8ish;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Optional;
import java.util.stream.Stream;

public class FilesystemDataProviderHack extends AbstractBackEndHierarchicalDataProvider<File, FilenameFilter> {

	private final File[] roots;

	public FilesystemDataProviderHack() {
		this(getPreferred());
	}

	public FilesystemDataProviderHack(File[] roots) {
		this.roots = roots;
	}

	@Override
	public boolean hasChildren(File item) {
		return item.list() != null && item.list().length > 0;
	}

	@Override
	public int getChildCount(HierarchicalQuery<File, FilenameFilter> query) {
		return (int) fetchChildren(query).count();
	}

	@Override
	protected Stream<File> fetchChildrenFromBackEnd(
			HierarchicalQuery<File, FilenameFilter> query) {
		final Optional<File> parentOpt = query.getParentOptional();//.orElse(roots);
		if(!parentOpt.isPresent()) {
			//System.out.println("Inget sedan tidigare");
			return Stream.of(roots);
		}
		File parent = parentOpt.get();
		//System.out.println("Sedan tidigare: "+parent.getAbsolutePath());
		return Stream.of(parent.listFiles());
		/*
		return query.getFilter()
				.map(filter -> Stream.of(parent.listFiles(filter)))
				.orElse(Stream.of(roots))
				.skip(query.getOffset()).limit(query.getLimit())
				.sorted((f1, f2) -> {
					return f1.getName().compareTo(f2.getName());
				});

		 */
	}

	public static final IconGeneratorV8ish<File> defaultIconGenerator = file -> {
		if (file.isDirectory()) {
			return VaadinIcon.FOLDER;
		}
		return VaadinIcon.FILE;
	};

	/*
	public static File getBaseFileViaSystemDrive() {
		String sysdrive = System.getenv("SystemDrive");
		if (sysdrive == null || sysdrive.trim().length() == 0) {
			sysdrive = "/";
		}
		else {
			if(sysdrive.endsWith(":")) {
				sysdrive = sysdrive+"\\";
			}
		}
		File rootFile = new File(sysdrive);
		return rootFile;
	}
	*/

	public static File getFilesystemTop() {
		File f = new File(".");
		try {
			String cfp = f.getCanonicalPath();
			f = new File(cfp);
			while(f.getParentFile() != null) {
				f = f.getParentFile();
			}
			return f;
		}
		catch(Exception e) {
		}
		return null;
	}

	public static File[] getPreferred() {
		return File.listRoots();
		/*
		File f = null;
		try {
			f = getBaseFilesystemTop();
			//System.out.println("FS topp "+f);
		}
		catch(Exception e) {
		}
		if(f == null) {
			f = getBaseFileViaSystemDrive();
			//System.out.println("SystemDrive "+f);
		}
		 */
		//return paths;
	}

	public static TreeV8ish<File> browseFromPreferred(final FilesystemCallback fc) {
		return browse(fc, getPreferred());
	}

	public static TreeV8ish<File> browse(final FilesystemCallback fc, File[] rootFile) {
		final TreeV8ish<File> tree = new TreeV8ish<>();

		FilesystemDataProviderHack fileSystem = new FilesystemDataProviderHack(rootFile);
		tree.setDataProvider(fileSystem);
		tree.setItemIconGenerator(defaultIconGenerator);

		tree.setItemDescriptionGenerator(file -> {
			if(!file.isDirectory()) {
				return fc.describeFile(file);
			}
			else {
				return fc.describeDirectory(file);
			}
		});

		tree.addItemClickListener(event -> {
			File file = event.getItem();
			if(!file.isDirectory()) {
				fc.onClickFile(file);
			}
			else {
				fc.onClickDirectory(file);
			}
		});

		return tree;
	}

}
