package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import general.reuse.vaadinreusable.vaadin8ish.LabelV8ish;
import org.apache.log4j.Logger;

public class LabelFactory {

	private static Logger logg = Logger.getLogger(LabelFactory.class);

	public final static String cssnameplain = "cmdlweblabelplain";
	public final static String cssnameblue = "cmdlweblabelblue";
	public final static String cssnamegreen = "cmdlweblabelgreen";

	private static final boolean isStyledPlain = false;
	private static final boolean isStyledBlue = false;
	private static final boolean isStyledGreen = false;

	static {
	}

	public static LabelV8ish styles(LabelV8ish in) {
		if(isStyledPlain) {
			in.setPrimaryStyleName(cssnameplain);
		}
		return in;
	}

	public static LabelV8ish create(String text) {
		return styles(new LabelV8ish(text));
	}

	public static LabelV8ish create() {
		return styles(new LabelV8ish());
	}

	private static LabelV8ish stylesByFlagOptionalColumn(LabelV8ish in, int flag) {
		if(flag == GREEN) {
			if(isStyledGreen) {
				in.setPrimaryStyleName(cssnamegreen);
			}
			return in;
		}
		if(flag == BLUE) {
			if(isStyledBlue) {
				in.setPrimaryStyleName(cssnameblue);
			}
			return in;
		}
		if(isStyledPlain) {
			in.setPrimaryStyleName(cssnameplain);
		}
		return in;
	}

	public static LabelV8ish createByFlagOptionalColumn(String text, int flag) {
		return stylesByFlagOptionalColumn(new LabelV8ish(text), flag);
	}

	public static final int PLAIN = 674234;
	public static final int BLUE = 294564;
	public static final int GREEN = 873243;

	private static String mangleCSSClassnameForMessageStatus(int messageIndex) {
		return "messstat"+messageIndex;
	}

	private static String getCss(String color) {
		return "{ color:"+color+"; }";
	}

	private static void addCSSClassForMessageStatus(StringBuffer s, String color, int classnr) {
		String css = getCss(color);
		if(css != null && css.trim().length() > 2) {
			css = css.replaceAll(";", ";\n\t").replaceAll("\\{", "{\n\t");
			s.append("."+ mangleCSSClassnameForMessageStatus(classnr)+" "+css+"\n");
		}
	}

	public static LabelV8ish createByMessageIndex(VaadinIcon icon, String text, int messageIndex) {
		LabelV8ish out = new LabelV8ish(text);
		//out.setIcon(icon);
		return stylesByMessageIndex(out, messageIndex);
	}

	public static LabelV8ish createByMessageIndex(VaadinIcon icon, int messageIndex) {
		return createByMessageIndex(icon, null, messageIndex);
	}

	public static LabelV8ish createByMessageIndex(String text, int messageIndex) {
		return stylesByMessageIndex(new LabelV8ish(text), messageIndex);
	}

	private static LabelV8ish stylesByMessageIndex(LabelV8ish in, int messageIndex) {
		in.setPrimaryStyleName(mangleCSSClassnameForMessageStatus(messageIndex));
		return in;
	}

}

