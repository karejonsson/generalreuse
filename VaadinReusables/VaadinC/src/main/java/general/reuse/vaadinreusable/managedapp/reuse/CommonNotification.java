package general.reuse.vaadinreusable.managedapp.reuse;

import com.vaadin.flow.component.notification.Notification;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.vaadin8ish.*;
import org.apache.log4j.Logger;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;

public class CommonNotification {

    private static Logger logg = Logger.getLogger(CommonNotification.class);
    private static final int duration = 1000;

    private static void showInternal(BaseFunctionalityProvider functionality, String message, String title) {
        QuestionAndButtonsWindow confirm = new QuestionAndButtonsWindow(title, message);
        confirm.addButtonWithClose(ButtonTexts.confirmingOK, () -> {});
        functionality.addWindow(confirm, "Notif, intern");
    }

    public static void plain(String message) {
        Notification.show(message);
    }

    public static void showError(String message) {
        plain(message);
    }

    public static void showAssistive(String message) {
        plain(message);
    }

    public static void showHumanized(String message) {
        plain(message);
    }

    public static void showWarning(String message) {
        plain(message);
    }

    public static void showError(BaseFunctionalityProvider functionality, String message) {
        logg.info(message);
        if(functionality == null) {
            Notification.show(message, duration, Notification.Position.MIDDLE);
            return;
        }
        showInternal(functionality, message, "Allvarligt fel");
    }

    public static void showWarning(BaseFunctionalityProvider functionality, String message) {
        logg.info(message);
        if(functionality == null) {
            Notification.show(message, duration, Notification.Position.MIDDLE);
            return;
        }
        showInternal(functionality, message, "Varning");
    }

    public static void showAssistive(BaseFunctionalityProvider functionality, String message) {
        logg.info(message);
        if(functionality == null) {
            Notification.show(message, duration, Notification.Position.MIDDLE);
            return;
        }
        showInternal(functionality, message, "Upplysning");
    }

    public static void showHumanized(BaseFunctionalityProvider functionality, String message) {
        logg.info(message);
        if(functionality == null) {
            Notification.show(message, duration, Notification.Position.MIDDLE);
            return;
        }
        showInternal(functionality, message, "Information");
    }

    public static class QuestionAndButtonsWindow extends WindowV8ish {

        private static final long serialVersionUID = 1845565644184742832L;

        private String title;
        private String question;
        private HorizontalLayoutV8ish hl = new HorizontalLayoutV8ish();

        public QuestionAndButtonsWindow(String title, String question) {
            this.title = title;
            this.question = question;
            //setCaption(title);
            VerticalLayoutV8ish vl = new VerticalLayoutV8ish();
            LabelV8ish l = new LabelV8ish(question);
            vl.addComponent(l);
            vl.addComponent(hl);
            setContent(vl);
        }

        public void addButtonWithClose(String text, Runnable onPress) {
            ButtonV8ish b = new ButtonV8ish(text);
            b.addClickListener(e -> { b.setEnabled(false); close() ; onPress.run(); } );
            hl.addComponent(b);
        }

        public void addButtonWithoutClose(String text, Runnable onPress) {
            ButtonV8ish b = new ButtonV8ish(text);
            b.addClickListener(e -> { b.setEnabled(false); onPress.run(); } );
            hl.addComponent(b);
        }

    }

}
