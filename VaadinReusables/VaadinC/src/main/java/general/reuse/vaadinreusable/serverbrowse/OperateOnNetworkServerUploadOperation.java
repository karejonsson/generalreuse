package general.reuse.vaadinreusable.serverbrowse;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class OperateOnNetworkServerUploadOperation implements OperateOnWrittenBrowsed_source_target {

    private ForeignFrameworksResources functionality = null;

    public OperateOnNetworkServerUploadOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Hämtning till servern";
    }

    @Override
    public String getDescription() {
        return "På serverns sida aktiveras hämtning av fil från nätet";
    }

    @Override
    public Runnable getOperate(String written, File folder) {
        return () -> {
            operate(written, folder);
        };
    }

    @Override
    public void operate(String written, File folder) {
        if (folder.exists()) {
            try {
                download(written, folder);
            } catch (Exception e) {
                functionality.showError("Fel vid nedladdning\n" + e.getMessage());
                return;
            }
        } else {
            functionality.showError("Katalogen finns inte");
            return;
        }
    }

    // https://stackoverflow.com/questions/3411480/how-to-resume-an-interrupted-download
    public static boolean download(String strUrl, File folder) throws Exception {
        BufferedInputStream in = null;
        FileOutputStream fos = null;
        BufferedOutputStream bout = null;
        URLConnection connection = null;

        int downloaded = 0;

        try {
            System.out.println("mark ... download start");
            URL url = new URL(strUrl);

            connection = url.openConnection();

            String fieldValue = connection.getHeaderField("Content-Disposition");
            if (fieldValue == null || !fieldValue.contains("filename=\"")) {
                int idx = Math.max(strUrl.lastIndexOf("/"), strUrl.lastIndexOf("\\"));
                fieldValue = "filename=\""+strUrl.substring(idx+1).trim()+"\"";
                // no file name there -> throw exception ...
            }

            // parse the file name from the header field
            String filename = fieldValue.substring(fieldValue.indexOf("filename=\"") + 10, fieldValue.length() - 1);

            File file = new File(folder, filename);
            if(file.exists()){
                downloaded = (int) file.length();
            }
            if (downloaded == 0) {
                connection.connect();
            }
            else {
                connection.setRequestProperty("Range", "bytes=" + downloaded + "-");
                connection.connect();
            }

            try {
                in = new BufferedInputStream(connection.getInputStream());
            } catch (IOException e) {
                int responseCode = 0;
                try {
                    responseCode = ((HttpURLConnection)connection).getResponseCode();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                if (responseCode == 416) {
                    return true;
                } else {
                    e.printStackTrace();
                    return false;
                }
            }

            fos=(downloaded==0)? new FileOutputStream(file): new FileOutputStream(file,true);
            bout = new BufferedOutputStream(fos, 1024);

            byte[] data = new byte[1024];
            int x = 0;
            while ((x = in.read(data, 0, 1024)) >= 0) {
                bout.write(data, 0, x);
            }

            in.close();
            bout.flush();
            bout.close();
            return false;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                }
            }
            if (bout != null) {
                try {
                    bout.close();
                } catch (IOException e) {
                }
            }

            if (connection != null) {
                ((HttpURLConnection)connection).disconnect();
            }
        }
    }

}
