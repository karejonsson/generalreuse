package general.reuse.vaadinreusable.managedapp.factories;

import org.apache.log4j.Logger;

public class CompleteStyling {
	
	private static Logger logg = Logger.getLogger(CompleteStyling.class); 

	private static String styles = null;
	
	static {
		styles = "";
	}
	
	public static String getStyles() {
		return styles;
	}

}
