package general.reuse.vaadinreusable.managedapp.reuse;

import com.vaadin.flow.component.Component;
import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.factories.LabelFactory;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;
import general.reuse.vaadinreusable.vaadin8ish.ButtonV8ish;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PresentDate {
	
	private static Logger logg = Logger.getLogger(PresentDate.class); 

	public static String getDateString(Date inputI, String dateformat) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(dateformat);
			return formatter.format(inputI);
		}
		catch(Exception e) {
			return inputI.toString();
		}
	}
	
	public static String getDateString(Date inputI, BaseFunctionalityProvider functionality) {
		try {
			return getDateString(inputI, functionality.getDateformat());
		}
		catch(Exception e) {
			return inputI.toString();
		}
	}

	public static Component getDate(Date inputI, BaseFunctionalityProvider functionality, Object row) {
		try {
			String formattedDate = getDateString(inputI, functionality);
			return LabelFactory.create(formattedDate);
		}
		catch(Exception e) {
			String msg = e.getMessage()+"\n"+row.toString()+"\n";
			logg.error("row="+row+", msg="+msg, e);
			ButtonV8ish dateBtn = ButtonFactory.create(ButtonTexts.dateNotAvailable);
			dateBtn.addClickListener(ev -> { CommonNotification.showError(functionality, msg); });
			return dateBtn;
		}
	}

	public static Component getDate(Date inputI, BaseFunctionalityProvider functionality, Object row, int messageIndex) {
		try {
			String formattedDate = getDateString(inputI, functionality);
			return LabelFactory.createByMessageIndex(formattedDate, messageIndex);
		}
		catch(Exception e) {
			String msg = e.getMessage()+"\n"+row.toString()+"\n";
			logg.error("row="+row+", msg="+msg, e);
			ButtonV8ish dateBtn = ButtonFactory.create(ButtonTexts.dateNotAvailable);
			dateBtn.addClickListener(ev -> { CommonNotification.showError(functionality, msg); });
			return dateBtn;
		}
	}

}
