package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.treegrid.TreeGrid;

import java.io.File;

public class TreeV8ish<T> extends TreeGrid<T> {

    public void setItemIconGenerator(IconGeneratorV8ish<T> defaultIconGenerator) {
        //super.set
    }

    public interface ProduceDescription {
        public String get(File f);
    }

    public void setItemDescriptionGenerator(ProduceDescription item) {

    }

}