package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class HorizontalLayoutV8ish extends HorizontalLayout {

    public Component getComponent(int index) {
        return super.getComponentAt(index);
    }

    public void addComponent(Component c) {
        add(c);
    }

    public void removeComponent(Component c) {
        remove(c);
    }

    public void setPrimaryStyleName(String csshorizontalname) {
        setClassName(csshorizontalname);
    }

    public void setComponentAlignment(Component groovyExecBtn, AlignmentV8ish alignment) {
    }

    public void setCaption(String caption) {

    }

}
