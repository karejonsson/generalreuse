package general.reuse.vaadinreusable.login;

//import com.vaadin.navigator.View;
//import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.vaadin8ish.*;
//import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Optional;

public class CredentialsAndGroupLoginPage extends VerticalLayoutV8ish {
	
	private static final long serialVersionUID = 3484919610407063509L;

	private ComboBoxV8ish<String> groupChoice = null;

	public CredentialsAndGroupLoginPage(CredentialsAndGroupVerifyer verifyer, Runnable onSuccess, Runnable onFailure) {
		this(verifyer, onSuccess, onFailure, "Behörighetskontroll", "Användarnamn", "Lösenord", "Vidare", "Ogiltig grupptillhörighet", "Felaktiga behörigheter");
	}

	public CredentialsAndGroupLoginPage(CredentialsAndGroupVerifyer verifyer, Runnable onSuccess, Runnable onFailure, String frameTitle, String frameUsername, String framePassword, String frameEnter, String frameWrongGroup, String frameWrongCredentials) {
		PanelV8ish panel = new PanelV8ish(frameTitle);
		panel.setSizeUndefined();
		addComponent(panel);
		
		FormLayoutV8ish content = new FormLayoutV8ish();
		TextFieldV8ish usernameField = new TextFieldV8ish(frameUsername);
		content.addComponent(usernameField);
		
		PasswordFieldV8ish password = new PasswordFieldV8ish(framePassword);
		content.addComponent(password);
		
		setupGroupComponent(verifyer);
		content.addComponent(groupChoice);

		ButtonV8ish send = new ButtonV8ish(frameEnter);
		send.addClickListener(e -> {
			String username = usernameField.getValue().toLowerCase();
			if(verifyer.accepted(username, password.getValue())) {
				String chosenGroup = getCurrentGroupOption();
				if(verifyer.partOfGroup(chosenGroup, username)) {
					verifyer.setAcceptedGroup(chosenGroup);
					onSuccess.run();
				}
				else {
					CommonNotification.showError(frameWrongGroup);
					onFailure.run();
				}
			}
			else {
				if(onFailure == null) {
					CommonNotification.showError(frameWrongCredentials);
				}
				else {
					onFailure.run();
				}
			}
		});
		
		content.addComponent(send);
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		setComponentAlignment(panel, AlignmentV8ish.MIDDLE_CENTER);
	}

	public void setupGroupComponent(CredentialsAndGroupVerifyer verifyer) {
		groupChoice = new ComboBoxV8ish<String>("Grupp");
		String[] groups = verifyer.possibleGroups();
		groupChoice.setSizeFull();
		groupChoice.setEmptySelectionAllowed(false);
		groupChoice.setTextInputAllowed(false);
		//groupChoice.setWidthUndefined();
		groupChoice.setItems(groups);
		groupChoice.setSelectedItem(groups[0]);
		groupChoice.setValue(groups[0]);
	}
	
	public String getCurrentGroupOption() {
		String out = null;
		try {
			Optional<String> op = groupChoice.getSelectedItem();
			out = op.get();
		}
		catch(Exception e) {
		}
		if(out == null) {
			return null;
		}
		return out;
	}

	/*
	@Override
	public String getContentType() {
		return null;
	}

	@Override
	public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception {

	}
	 */

}
