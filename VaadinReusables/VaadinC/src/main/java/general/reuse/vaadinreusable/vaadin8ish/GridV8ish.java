package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.*;
import com.vaadin.flow.data.provider.QuerySortOrder;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.function.SerializableComparator;
import com.vaadin.flow.function.SerializableFunction;
import com.vaadin.flow.function.ValueProvider;

import java.util.Comparator;
import java.util.stream.Stream;

public class GridV8ish<T> extends Grid<T> {

    public GridV8ish() {
        super();
    }

    public GridV8ish(String caption) {
    }

    public <V extends Component> GridV8ish.Column<T> addComponentColumn(ValueProvider<T, V> componentProvider) {
        // public <V extends Component> Grid.Column<T> addComponentColumn(ValueProvider<T, V> componentProvider) {
        Grid.Column<T> col = super.addComponentColumn(componentProvider);
        return new Column<T>(col);
    }

    public void setPrimaryStyleName(String cssname) {
        setClassName(cssname);
    }

    public static class Column<T> extends Grid.Column<T> {
        private Grid.Column<T> col = null;
        public Column(Grid.Column<T> col) {
            super((Grid<T>) col.getGrid(), null, col.getRenderer());
        }
        public Grid.Column<T> setWidth(String width) {
            return col.setWidth(width);
        }
        public String getWidth() {
            return col.getWidth();
        }
        public Grid.Column<T> setFlexGrow(int flexGrow) {
            return col.setFlexGrow(flexGrow);
        }
        public int getFlexGrow() {
            return col.getFlexGrow();
        }
        public Grid.Column<T> setAutoWidth(boolean autoWidth) {
            return this;//col.setAutoWidth(autoWidth);
        }
        public boolean isAutoWidth() {
            return false;//col.isAutoWidth();
        }
        public Grid.Column<T> setKey(String key) {
            return setKey(key);
        }
        public String getKey() {
            return col.getKey();
        }

        public Element getElement() {
            return super.getElement();
        }

        public GridV8ish.Column<T> setComparator(Comparator<T> comparator) {
            Grid.Column<T> col = super.setComparator(comparator);
            return new Column<T>(col);
        }

        public <V extends Comparable<? super V>> Grid.Column<T> setComparator(ValueProvider<T, V> keyExtractor) {
            return col.setComparator(keyExtractor);
        }

        public SerializableComparator<T> getComparator(SortDirection sortDirection) {
            return col.getComparator(sortDirection);
        }

        public Grid.Column<T> setSortProperty(String... properties) {
            return col.setSortProperty(properties);
        }

        public Grid.Column<T> setSortOrderProvider(SortOrderProvider provider) {
            return col.setSortOrderProvider(provider);
        }

        public Stream<QuerySortOrder> getSortOrder(SortDirection direction) {
            return col.getSortOrder(direction);
        }

        public Grid.Column<T> setSortable(boolean sortable) {
            return col.setSortable(sortable);
        }

        public boolean isSortable() {
            return col.isSortable();
        }

        public Grid.Column<T> setHeader(String labelText) {
            return setHeader(labelText);
        }

        public Grid.Column<T> setFooter(String labelText) {
            return col.setFooter(labelText);
        }

        public Grid.Column<T> setHeader(Component headerComponent) {
            return col.setHeader(headerComponent);
        }

        public Grid.Column<T> setFooter(Component footerComponent) {
            return col.setFooter(footerComponent);
        }

        public Grid.Column<T> setEditorComponent(Component editorComponent) {
            return col.setEditorComponent(editorComponent);
        }

        public Grid.Column<T> setEditorComponent(SerializableFunction<T, ? extends Component> componentCallback) {
            return col.setEditorComponent(componentCallback);
        }

        public Component getEditorComponent() {
            return col.getEditorComponent();
        }

        public Grid.Column<T> setClassNameGenerator(SerializableFunction<T, String> classNameGenerator) {
            return col.setClassNameGenerator(classNameGenerator);
        }

        public SerializableFunction<T, String> getClassNameGenerator() {
            return col.getClassNameGenerator();
        }

        public void setCaption(String caption) {
            setHeaderText(caption);
        }

    }

    //public <V extends Component> GridV8ish.Column<T> addComponentColumn(ValueProvider<T, V> componentProvider) {
    //public <V extends Component> Grid.Column<T> addComponentColumn(ValueProvider<T, V> componentProvider) {

     public GridV8ish.Column<T> addColumn(ValueProvider<T, ?> valueProvider) {
        //public Grid.Column<T> addColumn(ValueProvider<T, ?> valueProvider) {
        Grid.Column<T> col = super.addColumn(valueProvider);
        return new Column<T>(col);
    }

}
