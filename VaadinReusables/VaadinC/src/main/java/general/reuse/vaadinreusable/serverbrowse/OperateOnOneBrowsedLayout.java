package general.reuse.vaadinreusable.serverbrowse;

import general.reuse.vaadinreusable.vaadin8ish.*;
import org.apache.log4j.Logger;

import java.io.File;

public class OperateOnOneBrowsedLayout extends VerticalLayoutV8ish {

    private static Logger logg = Logger.getLogger(OperateOnTwoBrowsedLayout_source_target.class);

    private ForeignFrameworksResources functionality = null;
    private OperateOnOneBrowsed[] oots = null;

    private TextFieldV8ish source = null;

    public OperateOnOneBrowsedLayout(ForeignFrameworksResources functionality, OperateOnOneBrowsed[] oots) {
        functionality.styles(this);
        //LayoutFactory.styles(this, functionality.getHaveMargins());
        this.functionality = functionality;
        this.oots = oots;
        setupLayout();
    }

    private void setupLayout() {
        addComponent(new LabelV8ish(functionality.getOneParameterText()));
        HorizontalLayoutV8ish sourceLayout = new HorizontalLayoutV8ish();
        functionality.styles(sourceLayout);
        sourceLayout.setWidthFull();
        source = new TextFieldV8ish(functionality.getFileText());
        source.setWidthFull();
        source.setHeight("40px");
        sourceLayout.addComponent(source);
        ButtonV8ish bBtn = new ButtonV8ish(functionality.getBrowseText());
        bBtn.addClickListener(e -> browse(source));
        sourceLayout.addComponent(bBtn);
        addComponent(sourceLayout);

        for(OperateOnOneBrowsed oot : oots) {
            ButtonV8ish actionBtn = new ButtonV8ish(oot.getName());
            actionBtn.addClickListener(e -> execute(oot));
            actionBtn.setDescription(oot.getDescription());
            addComponent(actionBtn);
        }
    }

    private void browse(TextFieldV8ish tf) {
        TreeV8ish<File> tree = OperationsReuse.getBrowser(tf);
        WindowV8ish win = new WindowV8ish();
        win.setWidth("80%");
        win.setHeight("80%");
        VerticalLayoutV8ish l = new VerticalLayoutV8ish();
        l.addComponent(tree);
        win.setContent(l);
        functionality.addWindow(win, "Bläddra en parameter");
    }

    private void execute(OperateOnOneBrowsed oot) {
        try {
            oot.operate(source.getValue());
        }
        catch(Exception e) {
            logg.error("Fel vid operationen", e);
            functionality.showError(e.getMessage());
            return;
        }
    }

}
