package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.textfield.TextArea;

public class TextAreaV8ish extends TextArea {
    public TextAreaV8ish() {
        super();
    }
    public TextAreaV8ish(String caption) {
        super(caption);
    }

    public void setPrimaryStyleName(String cssname) {
        setClassName(cssname);
    }

}
