package general.reuse.vaadinreusable.background;

import general.reuse.timeouthandling.background.PoolWorkerProtector;
import general.reuse.timeouthandling.background.WorkQueue;
import general.reuse.timeouthandling.background.WorkQueueRepresentation;
import general.reuse.vaadinreusable.vaadin8ish.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class BackgroundWorkView extends PanelV8ish {

	private static final long serialVersionUID = 8316506320969809948L;
	public static final String NAME_COLUMN_TEXT = "Namn";
	public static final String JOBNUMBER_COLUMN_TEXT = "Jobbnummer";
	public static final String ENQUEUEDATE_COLUMN_TEXT = "Inköad";
	public static final String REENQUEUEDATE_COLUMN_TEXT = "Återinköad";
	public static final String STARTDATE_COLUMN_TEXT = "Start";
	public static final String STOPDATE_COLUMN_TEXT = "Slut";
	public static final String PROGRESS_COLUMN_TEXT = "Progress";
	public static final String ACTION_COLUMN_TEXT = "Hantering";

	private VerticalLayoutV8ish layout = null;

	private GridV8ish<WorkQueueRepresentation> waitingTable = null;
	private PanelV8ish waitingTableArea = null;

	private GridV8ish<PoolWorkerProtector> executingTable = null;
	private PanelV8ish executingTableArea = null;

	private GridV8ish<WorkQueueRepresentation> doneWithTable = null;
	private PanelV8ish doneWithArea = null;

	private String dateformat = null;
	private Runnable onBack = null;

	public BackgroundWorkView() {
		this("yyyyMMdd hh:mm:ss", null);
	}

	public BackgroundWorkView(Runnable onBack) {
		this("yyyyMMdd hh:mm:ss", onBack);
	}

	public BackgroundWorkView(String dateformat, Runnable onBack) {
		this.dateformat = dateformat;
		this.onBack = onBack;

		layout = new VerticalLayoutV8ish();
		setWidth("100%");
		setHeight("100%");
		if(onBack != null) {
			ButtonV8ish backBtn = new ButtonV8ish("Åter");
			backBtn.addClickListener(e -> {
				onBack.run();
			});
			layout.addComponent(backBtn);
		}

		waitingTableArea = new PanelV8ish();
		setupWaitingTable();
		layout.addComponent(waitingTableArea);

		executingTableArea = new PanelV8ish();
		setupExecutingTable();
		layout.addComponent(executingTableArea);

		doneWithArea = new PanelV8ish();
		setupDoneWithTable();
		layout.addComponent(doneWithArea);

		ButtonV8ish renew = new ButtonV8ish("Förnya allt");
		renew.addClickListener(e -> {
			setupWaitingTable();
			setupExecutingTable();
			setupDoneWithTable();
		});

		layout.addComponent(renew);
		setContent(layout);
	}
	
	private String presentDate(Date date) {
		if(date == null) {
			return "";
		}
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(dateformat);
			return formatter.format(date);
		}
		catch(Exception e) {}
		return date.toString();
	}

	private void setupWaitingTable() {
		waitingTable = new GridV8ish<WorkQueueRepresentation>();
		waitingTable.addComponentColumn(row -> {
			final LabelV8ish tf = new LabelV8ish(row.getName());
			tf.setWidth("100%");
			return tf;
		}).setCaption(NAME_COLUMN_TEXT);

		waitingTable.addComponentColumn(row -> {
			String visible = "";
			try {
				visible = presentDate(row.getEnqueueDate());
			}
			catch(Exception e) {
			}
			final LabelV8ish tf = new LabelV8ish(visible);
			tf.setWidth("100%");
			return tf;
		}).setCaption(ENQUEUEDATE_COLUMN_TEXT);

		waitingTable.addComponentColumn(row -> {
			String visible = "";
			try {
				visible = presentDate(row.getInQueueUpdateDate());
			}
			catch(Exception e) {
			}
			final LabelV8ish tf = new LabelV8ish(visible);
			tf.setWidth("100%");
			return tf;
		}).setCaption(REENQUEUEDATE_COLUMN_TEXT);

		waitingTable.addComponentColumn(row -> {
			ButtonV8ish navigateToFeedBtn = new ButtonV8ish("Ta bort");
			navigateToFeedBtn.addClickListener(e -> {
				WorkQueue.remove(row.getName());
				setupWaitingTable();
			});

			HorizontalLayoutV8ish out = new HorizontalLayoutV8ish();
			out.addComponent(navigateToFeedBtn);

			return out;
		}).setCaption(ACTION_COLUMN_TEXT);

		List<WorkQueueRepresentation> waiters = WorkQueue.getQueue();
		waitingTable.setItems(waiters);
		waitingTable.setWidth("100%");
		waitingTableArea.setContent(waitingTable);

		VerticalLayoutV8ish vl = new VerticalLayoutV8ish();
		HorizontalLayoutV8ish buttons = new HorizontalLayoutV8ish();
		ButtonV8ish b = new ButtonV8ish("Förnya");
		b.addClickListener(e -> { setupWaitingTable(); }); 
		buttons.addComponent(b);

		b = new ButtonV8ish("Rensa");
		b.addClickListener(e -> {
			WorkQueue.clearWaiting();
			setupWaitingTable();
		}); 
		buttons.addComponent(b);

		vl.addComponent(buttons);		
		vl.addComponent(waitingTable);
		waitingTableArea.setContent(vl);
	}

	private void setupExecutingTable() {
		executingTable = new GridV8ish<PoolWorkerProtector>();
		executingTable.addComponentColumn(row -> {
			final LabelV8ish tf = new LabelV8ish(row.getName());
			tf.setWidth("100%");
			return tf;
		}).setCaption(NAME_COLUMN_TEXT);

		executingTable.addComponentColumn(row -> {
			String visual = "";
			try {
				Integer jobnr = row.getJobNr();
				if(jobnr != null) {
					visual = ""+jobnr;
				}
			}
			catch(Exception e) {}
			final LabelV8ish tf = new LabelV8ish(visual);
			tf.setWidth("100%");
			return tf;
		}).setCaption(JOBNUMBER_COLUMN_TEXT);

		executingTable.addComponentColumn(row -> {
			String visible = "";
			try {
				visible = presentDate(row.getEnqueueDate());
			}
			catch(Exception e) {
			}
			final LabelV8ish tf = new LabelV8ish(visible);
			tf.setWidth("100%");
			return tf;
		}).setCaption(ENQUEUEDATE_COLUMN_TEXT);

		executingTable.addComponentColumn(row -> {
			String visible = "";
			try {
				visible = presentDate(row.getInQueueUpdateDate());
			}
			catch(Exception e) {
			}
			final LabelV8ish tf = new LabelV8ish(visible);
			tf.setWidth("100%");
			return tf;
		}).setCaption(REENQUEUEDATE_COLUMN_TEXT);

		executingTable.addComponentColumn(row -> {
			String visible = "";
			try {
				visible = presentDate(row.getStartDate());
			}
			catch(Exception e) {
			}
			final LabelV8ish tf = new LabelV8ish(visible);
			tf.setWidth("100%");
			return tf;
		}).setCaption(STARTDATE_COLUMN_TEXT);

		executingTable.addComponentColumn(row -> {
			final LabelV8ish tf = new LabelV8ish(row.get10CharProgress());
			tf.setWidth("100%");
			return tf;
		}).setCaption(PROGRESS_COLUMN_TEXT);

		executingTable.addComponentColumn(row -> {
			final ButtonV8ish stopWorkBtn = new ButtonV8ish("Avbryt");
			stopWorkBtn.addClickListener(e -> {
				row.requestStop();
				stopWorkBtn.setEnabled(false);
				try { Thread.sleep(1000); } catch (InterruptedException ie) { }
				setupWaitingTable();
				setupExecutingTable();
				setupDoneWithTable();
			});
			stopWorkBtn.setEnabled(row.isExecuting());

			HorizontalLayoutV8ish out = new HorizontalLayoutV8ish();
			out.addComponent(stopWorkBtn);

			return out;
		}).setCaption(ACTION_COLUMN_TEXT);

		PoolWorkerProtector[] executors = WorkQueue.getPoolWorkers();
		executingTable.setItems(executors);
		executingTable.setWidth("100%");
		executingTableArea.setContent(executingTable);

		VerticalLayoutV8ish vl = new VerticalLayoutV8ish();
		ButtonV8ish b = new ButtonV8ish("Förnya");
		b.addClickListener(e -> { setupExecutingTable(); } );
		vl.addComponent(b);
		vl.addComponent(executingTable);
		executingTableArea.setContent(vl);
	}

	private void setupDoneWithTable() {
		doneWithTable = new GridV8ish<WorkQueueRepresentation>();
		List<WorkQueueRepresentation> waiters = WorkQueue.getRecentlyFinished();

		doneWithTable.addComponentColumn(row -> {
			return new LabelV8ish(row.getName());
		}).setCaption(NAME_COLUMN_TEXT);;

		doneWithTable.addComponentColumn(row -> {
			String visual = "";
			try {
				Integer jobnr = row.getJobNr();
				if(jobnr != null) {
					visual = ""+jobnr;
				}
			}
			catch(Exception e) {}
			final LabelV8ish tf = new LabelV8ish(visual);
			tf.setWidth("100%");
			return tf;
		}).setCaption(JOBNUMBER_COLUMN_TEXT);;

		doneWithTable.addComponentColumn(row -> {
			String visible = "";
			try {
				visible = presentDate(row.getEnqueueDate());
			}
			catch(Exception e) {
			}
			final LabelV8ish tf = new LabelV8ish(visible);
			tf.setWidth("100%");
			return tf;
		}).setCaption(ENQUEUEDATE_COLUMN_TEXT);;

		doneWithTable.addComponentColumn(row -> {
			String visible = "";
			try {
				visible = presentDate(row.getInQueueUpdateDate());
			}
			catch(Exception e) {
			}
			final LabelV8ish tf = new LabelV8ish(visible);
			tf.setWidth("100%");
			return tf;
		}).setCaption(REENQUEUEDATE_COLUMN_TEXT);;

		doneWithTable.addComponentColumn(row -> {
			String visible = "";
			try {
				visible = presentDate(row.getStartDate());
			}
			catch(Exception e) {
			}
			final LabelV8ish tf = new LabelV8ish(visible);
			tf.setWidth("100%");
			return tf;
		}).setCaption(STARTDATE_COLUMN_TEXT);;

		doneWithTable.addComponentColumn(row -> {
			String visible = "";
			try {
				visible = presentDate(row.getEndDate());
			}
			catch(Exception e) {
			}
			final LabelV8ish tf = new LabelV8ish(visible);
			tf.setWidth("100%");
			return tf;
		}).setCaption(STOPDATE_COLUMN_TEXT);;

		doneWithTable.setItems(waiters);
		doneWithTable.setWidth("100%");

		VerticalLayoutV8ish vl = new VerticalLayoutV8ish();
		HorizontalLayoutV8ish buttons = new HorizontalLayoutV8ish();
		ButtonV8ish b = new ButtonV8ish("Förnya");
		b.addClickListener(e -> { setupDoneWithTable();	});
		buttons.addComponent(b);

		b = new ButtonV8ish("Rensa");
		b.addClickListener(e -> {
			WorkQueue.clearFinished();
			setupDoneWithTable();
		});
		buttons.addComponent(b);

		vl.addComponent(buttons);
		vl.addComponent(doneWithTable);
		doneWithArea.setContent(vl);
	}

}
