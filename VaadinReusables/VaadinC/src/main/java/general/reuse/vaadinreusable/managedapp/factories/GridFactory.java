package general.reuse.vaadinreusable.managedapp.factories;

import general.reuse.vaadinreusable.vaadin8ish.GridV8ish;

public class GridFactory {
	
	public final static String cssname = "cmdlwebgrid";
	private static final boolean isStyled = false;
	
	public static <T> GridV8ish<T> styles(GridV8ish<T> in) {
		if(isStyled) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static <T> GridV8ish<T> create(String caption) {
		return styles(new GridV8ish<T>(caption));
	}

	public static <T> GridV8ish<T> create() {
		return styles(new GridV8ish<T>());
	}

}
