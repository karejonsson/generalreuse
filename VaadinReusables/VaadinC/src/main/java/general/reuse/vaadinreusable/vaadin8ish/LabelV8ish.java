package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class LabelV8ish extends HorizontalLayout {

    private Label label = null;

    public LabelV8ish() {
        this("");
    }

    public LabelV8ish(String contents) {
        super();
        setMargin(false);
        label = new Label(contents);
        label.setSizeFull();
        add(label);
    }

    public String getValue() {
        return label.getText();
    }

    public void setPrimaryStyleName(String cssname) {
        super.setClassName(cssname);
    }

    public void setValue(String s) {
        label.setText(s);
    }

}
/*
public class LabelV8ish extends Label {

    public LabelV8ish() {
        super();
    }

    public LabelV8ish(String contents) {
        super(contents);
    }

    public String getValue() {
        return getText();
    }

    public void setPrimaryStyleName(String cssname) {
        setClassName(cssname);
    }

    public void setValue(String s) {
        setText(s);
    }

}
*/