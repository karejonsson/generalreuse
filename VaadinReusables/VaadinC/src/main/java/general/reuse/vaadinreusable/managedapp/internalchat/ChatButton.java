package general.reuse.vaadinreusable.managedapp.internalchat;

import com.vaadin.flow.component.icon.VaadinIcon;
import general.reuse.vaadinreusable.chat.Broadcaster;
import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.session.SessionRegistry;
import general.reuse.vaadinreusable.managedapp.texts.DescriptionTexts;
import general.reuse.vaadinreusable.vaadin8ish.ButtonV8ish;
import org.apache.log4j.Logger;

public class ChatButton {
	
	private static Logger logg = Logger.getLogger(ChatButton.class);

	public static ButtonV8ish createWhenActiveStateConsidered(BaseFunctionalityProvider functionality) {
		if(!SessionRegistry.chatActive.getValue()) {
			return null;
		}
		long since = functionality.getChatSince();
		int noSince = Broadcaster.countSince(since);
		if(noSince == 0 && functionality.getAdminFunctionality() == null) {
			return null;
		}
		ButtonV8ish chatBtn = ButtonFactory.create(VaadinIcon.CHAT);
		if(noSince != 0) {
			chatBtn.setCaption("("+noSince+")");
		}
		chatBtn.setDescription(DescriptionTexts.internalChat);
		chatBtn.addClickListener(e -> {
			functionality.openChat(functionality.getRunnableForReturnToCurrentContent());
		});
		return chatBtn;
	}

	public static ButtonV8ish createAlwaysStateConsidered(BaseFunctionalityProvider functionality) {
		if(!SessionRegistry.chatActive.getValue()) {
			return null;
		}
		long since = functionality.getChatSince();
		int noSince = Broadcaster.countSince(since);
		ButtonV8ish chatBtn = ButtonFactory.create(VaadinIcon.CHAT);
		if(noSince != 0) {
			chatBtn.setCaption("("+noSince+")");
		}
		chatBtn.setDescription(DescriptionTexts.internalChat);
		chatBtn.addClickListener(e -> {
			functionality.openChat(functionality.getRunnableForReturnToCurrentContent());
		});
		return chatBtn;
	}

	public static ButtonV8ish createAlwaysStateNotConsidered(BaseFunctionalityProvider functionality) {
		long since = functionality.getChatSince();
		int noSince = Broadcaster.countSince(since);
		ButtonV8ish chatBtn = ButtonFactory.create(VaadinIcon.CHAT);
		if(noSince != 0) {
			chatBtn.setCaption("("+noSince+")");
		}
		chatBtn.setDescription(DescriptionTexts.internalChat);
		chatBtn.addClickListener(e -> {
			functionality.openChat(functionality.getRunnableForReturnToCurrentContent());
		});
		return chatBtn;
	}

}
