package general.reuse.vaadinreusable.managedapp.texts;

public class MessageTexts {

    public static final String preparedMessage =
            "Vi kommer om en stund stänga ner "+DescriptionTexts.appname+" för underhåll.\n\n"+
                    "Det rör sig om cirka ? minuter./Det är planerat till klockan ??:??.\n"+
                    "Den förväntade nertiden är cirka ? minuter.\n\n"+
                    "När stunden är inne kommer alla kvarvarande loggas ut med automatik.\n"+
                    "Automatiken innebär att akterna kommer stängas och de meddelanden Ni\n"+
                    "öppnat med kommer sättas i sparat läge. Det går bra att arbeta vidare\n"+
                    "men det minsta Ni måste uppfylla är att vara i ett läge där inget går\n"+
                    "förlorat av detta förfarande.\n\n"+
                    "När det bara är lite tid kvar kommer vi försöka varna igen. Vi kommer\n"+
                    "också hålla oss tillgängliga på den inbyggda chatten om vi misstänker\n"+
                    "att någon inte gjort sig beredd. I slutminutrarna kommer inloggning\n"+
                    "vara blockerat. Försök att logga in stör inte driftens process.\n\n"+
                    "Det är uppskattat om Ni kan gå ur i god tid. Nedstängningen kommer\n"+
                    "utföras även utan kontakt när tiden är inne.\n\n"+
                    "Vänligen,\n"+
                    "driften";

    public final static String askToGoToChat = "Kan du gå till "+DescriptionTexts.appname+"-chatten?";
    
}
