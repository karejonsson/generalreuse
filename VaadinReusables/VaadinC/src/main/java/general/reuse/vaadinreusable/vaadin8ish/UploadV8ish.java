package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.flow.component.upload.Receiver;
import com.vaadin.flow.component.upload.Upload;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import org.apache.log4j.Logger;

import java.io.*;

public class UploadV8ish extends Upload {

    private static Logger log = Logger.getLogger(UploadV8ish.class);

    private String filename = null;
    //private FilepathUploader receiver = null;

    public UploadV8ish(String filename, final FilepathUploader receiver) {
        super(receiver);
        //this.receiver = receiver;
        this.filename = filename;
        addSucceededListener(event -> {
            receiver.uploadSucceeded(receiver.getInputStream());
        });
    }

    public void setImmediateMode(boolean b) {
    }

    public static class FilepathUploader implements Receiver {//}, SucceededListener {

        private String filepath = null;
        private String filename = null;
        private byte[] fileBytes = null;
        private ByteArrayOutputStream bais = null;

        public FilepathUploader(String filepath) {
            this.filepath = filepath;
        }

        public OutputStream receiveUpload(String filename, String mimeType) {
            log.info("filename="+filename+", mimeType="+mimeType);
            this.filename = filename;
            bais = new ByteArrayOutputStream();
            return bais;
        }

        public InputStream getInputStream() {
            File f = new File(filepath);
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(f);
            }
            catch (FileNotFoundException e) {
                log.error("Kunde inte skapa ström till "+filepath, e);
                return null;
            }
            return fis;
            //return this.file != null ? new ByteArrayInputStream(((ByteArrayOutputStream)this.file.getOutputBuffer()).toByteArray()) : new ByteArrayInputStream(new byte[0]);
        }

        public void uploadSucceeded(InputStream is) {
            try {
                fileBytes = is.readAllBytes();
                FileOutputStream fos = new FileOutputStream(new File(new File(filepath), filename));
                fos.write(fileBytes);
                fos.flush();
                fos.close();
            }
            catch(Exception e) {
                String msg = "Uppladdningen misslyckades. Underliggande felmeddelande\n"+e.getMessage();
                log.error(msg + ". filepath=" + filepath, e);
                CommonNotification.showError(msg);
                return;
            }
            CommonNotification.showAssistive("Uppladdningen lyckades");
        }
    };

}
