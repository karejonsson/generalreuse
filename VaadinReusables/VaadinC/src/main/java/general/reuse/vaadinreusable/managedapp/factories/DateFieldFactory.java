package general.reuse.vaadinreusable.managedapp.factories;

import general.reuse.vaadinreusable.vaadin8ish.DateFieldV8ish;

public class DateFieldFactory {

	public final static String cssname = "cmdlwebdatefield";
	private static final boolean isStyled = false;
	
	public static DateFieldV8ish styles(DateFieldV8ish in) {
		if(isStyled) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static DateFieldV8ish create(String caption) {
		return styles(new DateFieldV8ish(caption));
	}
	
}
