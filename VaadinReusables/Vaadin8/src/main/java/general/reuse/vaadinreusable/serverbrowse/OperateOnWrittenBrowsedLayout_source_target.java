package general.reuse.vaadinreusable.serverbrowse;

import com.vaadin.ui.Tree;
import com.vaadin.ui.Window;
import general.reuse.vaadinreusable.vaadin8ish.*;
import org.apache.log4j.Logger;

import java.io.File;

public class OperateOnWrittenBrowsedLayout_source_target extends VerticalLayoutV8ish {

    private static Logger logg = Logger.getLogger(OperateOnTwoBrowsedLayout_source_target.class);

    private ForeignFrameworksResources functionality = null;
    private OperateOnWrittenBrowsed_source_target[] oots = null;

    private TextFieldV8ish written = null;
    private TextFieldV8ish folder = null;

    public OperateOnWrittenBrowsedLayout_source_target(ForeignFrameworksResources functionality, OperateOnWrittenBrowsed_source_target[] oots) {
        functionality.styles(this);
        this.functionality = functionality;
        this.oots = oots;
        setupLayout();
    }

    private void setupLayout() {
        addComponent(new LabelV8ish(functionality.getTwoParametersText()));
        HorizontalLayoutV8ish sourceLayout = new HorizontalLayoutV8ish();
        sourceLayout.setWidthFull();
        written = new TextFieldV8ish("Skriven parameter");
        written.setWidthFull();
        written.setHeight("40px");
        sourceLayout.addComponent(written);
        ButtonV8ish b1Btn = new ButtonV8ish(functionality.getBrowseText());
        b1Btn.addClickListener(e -> browse(written));
        sourceLayout.addComponent(b1Btn);
        addComponent(sourceLayout);

        HorizontalLayoutV8ish targetLayout = new HorizontalLayoutV8ish();
        targetLayout.setWidthFull();
        folder = new TextFieldV8ish("Målkatalog");
        folder.setWidthFull();
        folder.setHeight("40px");
        targetLayout.addComponent(folder);
        ButtonV8ish b2Btn = new ButtonV8ish(functionality.getBrowseText());
        b2Btn.addClickListener(e -> browse(folder));
        targetLayout.addComponent(b2Btn);
        addComponent(targetLayout);

        for(OperateOnWrittenBrowsed_source_target oot : oots) {
            ButtonV8ish actionBtn = new ButtonV8ish(oot.getName());
            actionBtn.addClickListener(e -> execute(oot));
            actionBtn.setDescription(oot.getDescription());
            addComponent(actionBtn);
        }
    }

    private void browse(TextFieldV8ish tf) {
        Tree<File> tree = OperationsReuse.getBrowser(tf);
        Window win = new Window();
        win.setWidth("80%");
        win.setHeight("80%");
        VerticalLayoutV8ish l = new VerticalLayoutV8ish();
        l.addComponent(tree);
        win.setContent(l);
        functionality.addWindow(win, "Bläddra");
    }

    private void execute(OperateOnWrittenBrowsed_source_target oot) {
        try {
            oot.operate(written.getValue(), new File(folder.getValue()));
        }
        catch(Exception e) {
            String msg = "Oväntat fel vid operationen. Underliggande felmeddelande\n"+e.getMessage();
            logg.error(msg+". written.getValue()="+written.getValue()+", folder.getValue()="+folder.getValue(), e);
            functionality.showError(msg);
            return;
        }
    }

}
