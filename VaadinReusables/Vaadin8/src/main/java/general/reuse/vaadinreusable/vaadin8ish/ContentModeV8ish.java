package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.shared.ui.ContentMode;

public class ContentModeV8ish {
    //public static ContentMode TEXT = ContentMode.TEXT;
    //public static ContentMode PREFORMATTED = ContentMode.PREFORMATTED;
    //public static ContentMode HTML = ContentMode.HTML;

    public static ContentModeV8ish TEXT = new ContentModeV8ish(ContentMode.TEXT);
    public static ContentModeV8ish PREFORMATTED = new ContentModeV8ish(ContentMode.PREFORMATTED);
    public static ContentModeV8ish HTML = new ContentModeV8ish(ContentMode.HTML);
    //ContentModeV8ish(ContentMode.PREFORMATTED),
    //ContentModeV8ish(ContentMode.HTML);

    private ContentMode cm;
    private ContentModeV8ish(ContentMode cm) {
        this.cm = cm;
    }
    public ContentMode get() { return cm; }
}

/*
public enum ContentMode {
    TEXT,
    PREFORMATTED,
    HTML;

    private ContentMode() {
    }
}
*/