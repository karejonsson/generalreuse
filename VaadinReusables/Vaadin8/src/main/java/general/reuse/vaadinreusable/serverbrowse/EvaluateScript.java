package general.reuse.vaadinreusable.serverbrowse;

import general.reuse.codeloading.DynamicClassLoader;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class EvaluateScript {

    public static Object eval(String script) {
        return eval(script, new Binding(), null);
    }

    public static Object eval(String script, OutputStream os) {
        return eval(script, new Binding(), os);
    }

    public static Object eval(String script, Binding bind, OutputStream os) {
        DynamicClassLoader reusedClassLoader = new DynamicClassLoader(EvaluateScript.class.getClassLoader());
        PrintStream ps = null;
        if(os != null) {
            ps = new PrintStream(os);
            bind.setProperty("out", ps);
        }
        GroovyShell shell = new GroovyShell(reusedClassLoader, bind);
        try {
            return shell.evaluate(script, "name", "");
        }
        catch(Exception e) {
            throw e;
        }
        finally {
            if(ps != null) {
                ps.flush();
                ps.close();
            }
        }
    }

    public static Script parse(String scriptText) {
        DynamicClassLoader reusedClassLoader = new DynamicClassLoader(EvaluateScript.class.getClassLoader());
        Binding bind = new Binding();
        OutputStream output = new ByteArrayOutputStream();
        bind.setProperty("out", new PrintStream(output));
        GroovyShell shell = new GroovyShell(reusedClassLoader, bind);
        Script script = shell.parse(scriptText);
        return script;
    }

}
