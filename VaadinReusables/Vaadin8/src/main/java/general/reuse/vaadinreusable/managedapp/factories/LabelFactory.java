package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.icons.VaadinIcons;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import org.apache.log4j.Logger;

public class LabelFactory {

	private static Logger logg = Logger.getLogger(LabelFactory.class);

	private static String cssnameplain = null;
	private static String cssnameblue = null;
	private static String cssnamegreen = null;

	public static void setCssnamePlain(String cssname) {
		LabelFactory.cssnameplain = cssname;
	}

	public static void setCssnameBlue(String cssname) {
		LabelFactory.cssnameblue = cssname;
	}

	public static void setCssnameGreen(String cssname) {
		LabelFactory.cssnamegreen = cssname;
	}

	public static Label styles(Label in) {
		if(cssnameplain != null) {
			in.setPrimaryStyleName(cssnameplain);
		}
		return in;
	}

	public static Label create(String text) {
		return styles(new Label(text));
	}

	public static Label create(VaadinIcons vi) {
		Label out = new Label("");
		out.setIcon(vi);
		return styles(out);
	}

	public static Label create(VaadinIcons vi, String text) {
		return create(text, vi);
	}

	public static Label create(String text, VaadinIcons vi) {
		Label out = new Label(text);
		out.setIcon(vi);
		return styles(out);
	}

	public static Label create(String text, ContentMode cm) {
		return styles(new Label(text, cm));
	}

	public static Label create() {
		return styles(new Label());
	}

	public static Label stylesByFlagOptionalColumn(Label in, int flag) {
		if(flag == GREEN) {
			if(cssnamegreen != null) {
				in.setPrimaryStyleName(cssnamegreen);
			}
			return in;
		}
		if(flag == BLUE) {
			if(cssnameblue != null) {
				in.setPrimaryStyleName(cssnameblue);
			}
			return in;
		}
		if(cssnameplain != null) {
			in.setPrimaryStyleName(cssnameplain);
		}
		return in;
	}

	public static Label createByFlagOptionalColumn(String text, int flag) {
		return stylesByFlagOptionalColumn(new Label(text), flag);
	}

	/*
	public static Label createByFlagOptionalColumn(String text, ContentMode cm, int flag) {
		return stylesByFlagOptionalColumn(new Label(text, cm), flag);
	}

	public static Label createByFlagOptionalColumn(int flag) {
		return stylesByFlagOptionalColumn(new Label(), flag);
	}
	*/

	public static final int PLAIN = 674234;
	public static final int BLUE = 294564;
	public static final int GREEN = 873243;


	private static String mangleCSSClassnameForMessageStatus(int messageIndex) {
		return "messstat"+messageIndex;
	}

	private static String getCss(String color) {
		return "{ color:"+color+"; }";
	}

	private static void addCSSClassForMessageStatus(StringBuffer s, String color, int classnr) {
		String css = getCss(color);
		if(css != null && css.trim().length() > 2) {
			css = css.replaceAll(";", ";\n\t").replaceAll("\\{", "{\n\t");
			s.append("."+ mangleCSSClassnameForMessageStatus(classnr)+" "+css+"\n");
		}
	}

	/*
	public static String getUserColorCSS(BaseFunctionalityProvider functionality) {
		return getUserMessageStatusColorCSS(functionality.getMessageStatusColors());
	}

	public static String getUserMessageStatusColorCSS(String[] colors) {
		if(colors == null) {
			logg.warn("Färgerna är en tom vektor");
			return getUserColorCSSBuiltinDefault();
		}
		if(colors.length != MessagesStatuses.nrOfMessageStatuses) {
			logg.warn("Färgerna har inte rätt längd. colors.length="+colors.length);
			return getUserColorCSSBuiltinDefault();
		}
		return getUserMessageStatusColorCSSWhenItIsCorrect(colors);
	}

	private static String getUserColorCSSBuiltinDefault() {
		return getUserMessageStatusColorCSSWhenItIsCorrect(DefaultUserSettings.defaultMessageStatusColors);
	}

	private static String getUserMessageStatusColorCSSWhenItIsCorrect(String[] colors) {
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < MessagesStatuses.nrOfMessageStatuses ; i++) {
			try {
				addCSSClassForMessageStatus(sb, colors[i], i);
			}
			catch(Exception e) {
				logg.error("Fel när i="+i+". colors.length="+colors.length, e);
				return "";
			}
		}
		return sb.toString();
	}
	*/

	/*
	// Klasses MessageStatusTexts finns i en annan modul som jag inte vill lägga till beroende mot
	public static Label createByMessageIndex(String text, double msgindstatus) {
		return stylesByMessageIndex(new Label(text), MessageStatusTexts.getIndexForStatus(msgindstatus));
	}
	*/

	public static Label createByMessageIndex(VaadinIcons icon, String text, int messageIndex) {
		Label out = new Label(text);
		out.setIcon(icon);
		return stylesByMessageIndex(out, messageIndex);
	}

	public static Label createByMessageIndex(VaadinIcons icon, int messageIndex) {
		return createByMessageIndex(icon, null, messageIndex);
	}

	public static Label createByMessageIndex(String text, int messageIndex) {
		return stylesByMessageIndex(new Label(text), messageIndex);
	}

	private static Label stylesByMessageIndex(Label in, int messageIndex) {
		in.setPrimaryStyleName(mangleCSSClassnameForMessageStatus(messageIndex));
		return in;
	}

}

