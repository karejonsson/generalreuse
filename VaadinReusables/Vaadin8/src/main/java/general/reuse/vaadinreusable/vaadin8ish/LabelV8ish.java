package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.Label;

public class LabelV8ish extends Label implements ComponentV8ish {

    public LabelV8ish() {
        super();
    }

    public LabelV8ish(String contents) {
        super(contents);
    }

    public LabelV8ish(String text, ContentModeV8ish cm) {
        super(text, cm.get());
    }

}
