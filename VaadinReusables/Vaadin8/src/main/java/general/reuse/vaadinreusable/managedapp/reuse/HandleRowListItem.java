package general.reuse.vaadinreusable.managedapp.reuse;

import com.vaadin.ui.ComboBox;

import java.util.Optional;

public class HandleRowListItem implements Runnable {
	
	private Runnable runnable;
	private String tostring;
	
	public HandleRowListItem(Runnable runnable, String tostring) {
		this.runnable = runnable;
		this.tostring = tostring;
	}
	
	public String toString() {
		return tostring;
	}
	
	public void run() {
		runnable.run();
	}
	
	public static HandleRowListItem getCurrentHandleRowListItem(ComboBox<HandleRowListItem> handlingChoser) {
		HandleRowListItem out = null;
		try {
			Optional<HandleRowListItem> op = handlingChoser.getSelectedItem();
			out = op.get();
		}
		catch(Exception e) {
		}
		return out;
	}

}

