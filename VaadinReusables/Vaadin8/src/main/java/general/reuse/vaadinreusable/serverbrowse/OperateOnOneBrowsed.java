package general.reuse.vaadinreusable.serverbrowse;

public interface OperateOnOneBrowsed {

    String getName();
    String getDescription();
    void operate(String filepath) throws Exception;
    Runnable getOperate(String filepath);

}
