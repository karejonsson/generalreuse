package general.reuse.vaadinreusable.serverbrowse;

import general.reuse.vaadinreusable.vaadin8ish.UploadV8ish;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;
import org.apache.log4j.Logger;

import java.io.File;

public class OperateOnOneUploadOperation implements OperateOnOneBrowsed {

    private static Logger log = Logger.getLogger(OperateOnOneUploadOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnOneUploadOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Uppladdning";
    }

    @Override
    public String getDescription() {
        return "Ladda upp fil";
    }

    @Override
    public Runnable getOperate(String filepath) {
        return () -> {
            operate(filepath);
        };
    }

    @Override
    public void operate(String filepath) {
        log.info("filepath="+filepath);

        File f = new File(filepath);
        if(!f.exists()) {
            String msg = "Katalogen finns inte";
            log.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }
        if(!f.isDirectory()) {
            String msg = "Det är inte en katalog";
            log.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }

        UploadV8ish.FilepathUploader receiver = new UploadV8ish.FilepathUploader(filepath);
        UploadV8ish upload = new UploadV8ish("Fil att ladda upp", receiver);
        upload.addSucceededListener(receiver);
        upload.setImmediateMode(false);
        WindowV8ish win = new WindowV8ish();
        win.setHeight("60px");
        win.setWidth("40%");
        win.setContent(upload);
        functionality.addWindow(win, "Fil att ladda upp");
    }

}
