package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.GridRowDragger;

public class GridFactory {

	private static String cssname = null;

	public static void setCssname(String cssname) {
		GridFactory.cssname = cssname;
	}

	public static <T> Grid<T> styles(Grid<T> in) {
		if(cssname != null) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static <T> Grid<T> create(String caption) {
		return styles(new Grid<T>(caption));
	}

	public static <T> Grid<T> create() {
		return styles(new Grid<T>());
	}

	public static <T> GridRowDragger<T> createRowDragger(Grid<T> rightGrid) {
		return new GridRowDragger<T>(rightGrid);
	}

}
