package general.reuse.vaadinreusable.parameters;

import java.util.List;

import com.vaadin.ui.Button;
import general.reuse.database.parameters.domain.PersistentValue;
import general.reuse.vaadinreusable.vaadin8ish.*;

public class PersistentValueView extends VerticalLayoutV8ish {
	
	private static final long serialVersionUID = 4708526137793702022L;
	
	private List<PersistentValue> rows = null;
	private GridV8ish<PersistentValue> grid = null;
	private PersistentValuePersister pvp = null;
	
	public PersistentValueView(PersistentValuePersister pvp) {
		this(pvp, null, true);
	}

	public PersistentValueView(PersistentValuePersister pvp, Button[] additionals, boolean addSave) {
		this.pvp = pvp;
		try {
			rows = pvp.getAllPersistentValues();
		} 
		catch(Exception e) {
			e.printStackTrace();
		}

		grid = new GridV8ish<>();

        grid.addColumn(PersistentValue::getKey).setCaption("Nyckel").setEditorComponent(new TextFieldV8ish(), (bean, fieldvalue) -> {
			try {
				bean.setKey(fieldvalue);
			} 
			catch(Exception e) {
				NotificationV8ish.show(e.getMessage());
			}
		}).setEditable(true);
        grid.addColumn(PersistentValue::getValue).setCaption("Värde").setEditorComponent(new TextFieldV8ish(), (bean, fieldvalue) -> {
			try {
				bean.setValue(fieldvalue);
			} 
			catch(Exception e) {
				NotificationV8ish.show(e.getMessage());
			}
		}).setEditable(true);

        grid.getEditor().setEnabled(true);

		grid.addComponentColumn(row -> {
			HorizontalLayoutV8ish out = new HorizontalLayoutV8ish();
		      ButtonV8ish deleteBtn = new ButtonV8ish("Radera");
		      deleteBtn.setEnabled(true);
		      deleteBtn.addClickListener(click -> delete(row));
		      out.addComponent(deleteBtn);
		      return out;
		}).setCaption("Hantering");
		
		grid.setItems(rows);

		grid.setWidth("100%");
		grid.setHeight("100%");

		addComponent(grid);
		setExpandRatio(grid, 1.0f);

		HorizontalLayoutV8ish buttons = new HorizontalLayoutV8ish();
		if(addSave) {
			ButtonV8ish saveBtn = new ButtonV8ish("Spara");
			saveBtn.addListener(e -> pvp.saveAll());		
			buttons.addComponent(saveBtn);		
		}
		ButtonV8ish newBtn = new ButtonV8ish("Ny");
		newBtn.addListener(e -> newKeyValuePair());		
		buttons.addComponent(newBtn);
		if(additionals != null) {
			for(Button additional : additionals) {
				buttons.addComponent(additional);
			}
		}
		//buttons.setHeight("15%");

		addComponent(buttons);
		setSizeFull();
	}

	private void delete(PersistentValue pv) {
		try {
			
			System.out.println("Är med 1 "+(rows.contains(pv))+", typ "+pv.getClass().getName());
			pvp.removeValue(pv.getKey());
			System.out.println("Är med 2 "+(rows.contains(pv)));
			rows.remove(pv);
		}
		catch(Exception e) {
			NotificationV8ish.show("Kunde inte radera "+pv.getKey());
			return;
		}
		//rows.remove(pv);
		grid.getDataProvider().refreshAll();
	}
	
	private void newKeyValuePair() {
		rows.add(pvp.getNewPersistentValue());
		grid.getDataProvider().refreshAll();
	}
	
}
