package general.reuse.vaadinreusable.managedapp.texts;

public class ButtonTexts {

	public static final String revert = "Åter";
	public static final String admin = "Admin";
	public static final String cancel = "Avbryt";
	public static final String logout = "Logga ut";
	
	public static final String users = "Användare";
	public static final String activeUsers = "Aktiva användare";
	public static final String close = "Stäng";
	public static final String personalSettings = "Personliga inställningar";//"Mina sidor";
	public static final String confirmingOK = "OK";
	
	public static final String start = "Start";
	public static final String create = "Skapa";
	public static final String save = "Spara";
	public static final String erase = "Radera";
	public static final String edit = "Redigera";
	public static final String openDossier = "Öppna akt";
	public static final String updatePage = "Uppdatera sida";//"Läs om";
	public static final String dateNotAvailable = "Datum ej tillgängligt";
	public static final String add = "Lägg till";
	public static final String search = "Sök";

	public static final String larger = "Större";
	public static final String smaller = "Mindre";

	public static final String remove = "Ta bort";
	public static final String read = "Läs";
	public static final String done_t = "Klart";
	public static final String settings = "Inställningar";
	public static final String documentSearch = "Dokumentsök";
	public static final String show = "Visa";

	public static final String further = "Skicka vidare";//"Vidare";
	public static final String send = "Skicka";

	public static final String copy = "Kopiera";

	public static final String groups = "Grupper";

	public static final String message = "Meddelande";

	public static final String exportContentColumns = "Kolumner vid innehållsexport";
	
	public static final String closeMessage_handleRelated = "Stäng meddelande, hantera relaterade";

	public static final String askAllToEnterChat = "Be alla gå in i chatten";
	public static final String writtenMessageToAll = "Skrivet meddelande till alla";
	public static final String forceAllToEnterChat = "Tvinga alla till chatten";

	public static final String clearChat = "Rensa chatt";
	public static final String diagnosePlatform = "Diagnos av plattform";

    public static final String userData = "Användarinformation" ;
	public static final String downloadFile = "Ladda ner fil";
    public static final String fileHandling = "Generell filhantering";
    public static final String closeFolder = "Stäng akt";
    public static final String throwOut = "Kasta ut";
	public static final String throwOutAllOthers = "Kasta ut alla andra";
	public static final String throwOutNotWulnerable = "Kasta ut okänsliga";


	public static final String enableAccess = "Redigera";
	public static final String disableAccess = "Lås";
    public static final String userSettings = "Användarinställningar";
	public static final String userOpenings = "Användares öppningar";
    public static final String image = "Bild";
    public static final String states = "Tillstånd";
	public static final String yes = "Ja";
	public static final String no = "Nej";
    public static final String export = "Exportera";
    public static final String warningMessage = "Varningsmeddelande";

	public static final String SQL_show_responstable = "PreparedStatement.executeQuery() -> ResultSet";
	public static final String SQL_answer_generated = "PreparedStatement(RETURN_GENERATED_KEYS).getGeneratedKeys() -> ResultSet";
	public static final String SQL_answer_boolean = "PreparedStatement.execute() -> boolean";
	public static final String SQL_answer_int = "Statement.executeUpdate(sql) -> int";
	public static final String SQL_linewise_boolean = "Radvis: "+SQL_answer_boolean;
	public static final String SQL_linewise_generating = "Radvis: "+SQL_answer_generated;

	public static final String take = "Ta";
	public static final String back = "Ge";
	public static final String delete = "Bort";
	public static final String clear = "Rensa";

    public static final String chose = "Välj";
    public static final String reset = "Nollställ";
}

/*

sed -i 's/new\ Button("Exportera\ till\ SQL")/new\ Button(ButtonTexts.exportToSql)/g' `find . -name '*.java' -print`

grep 'ButtonFactory.create ("B' `find . -name '*.java' -print` | wc -l
grep 'ButtonFactory.create ("B' `find . -name '*.java' -print`
sed -i 's/new\ Button("Brevlåda")/new\ Button(ButtonTexts.messageBox)/g' `find . -name '*.java' -print`
sed -i 's/new\ Button("Beskriv")/new\ Button(ButtonTexts.describe)/g' `find . -name '*.java' -print`
sed -i 's/new\ Button("Behåll\ meddelande")/new\ Button(ButtonTexts.keepMessage)/g' `find . -name '*.java' -print`
grep 'ButtonFactory.create ("A' `find . -name '*.java' -print`
sed -i 's/new\ Button("Överför")/new\ Button(ButtonTexts.transfer)/g' `find . -name '*.java' -print`
 
sed -i 's/new\ Button("Radera")/new\ Button(ButtonTexts.erase)/g' `find . -name '*.java' -print`
sed -i 's/new\ Button("Större")/new\ Button(ButtonTexts.larger)/g' `find . -name '*.java' -print`
 */