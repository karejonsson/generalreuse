package general.reuse.vaadinreusable.serverbrowse;

import com.vaadin.ui.Component;
import com.vaadin.ui.Window;
import general.reuse.vaadinreusable.vaadin8ish.ComponentV8ish;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;

public interface ForeignFrameworksResources {

    boolean getHaveMargins();
    void showWarning(String s);
    void showAssistive(String s);
    void showError(String s);
    void addWindow(Window win);
    void addWindow(Window win, String winType);
    void styles(Component components);

    String getParseProgramAsGroovyText();
    String getExecuteProgramAsGroovyText();
    String getBrowseText();
    String getEditText();
    String getCancelText();
    String getSaveText();
    String getOneParameterText();
    String getFileText();
    String getRevertText();
    String getOnCurrentText();
    String getTwoParametersText();
}
