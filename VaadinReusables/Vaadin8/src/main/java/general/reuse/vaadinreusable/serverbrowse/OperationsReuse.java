package general.reuse.vaadinreusable.serverbrowse;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree;
import general.reuse.vaadinreusable.choser.QuestionAndButtonsWindow;
import general.reuse.vaadinreusable.vaadin8ish.TextFieldV8ish;
import general.reuse.vaadinreusable.vaadin8ish.TreeV8ish;

import java.io.File;

public class OperationsReuse {

    public static void okForHardAction(ForeignFrameworksResources functionality, String question, Runnable onOK, Runnable onNok) {
        if(onOK == null && onNok == null) {
            // Konstigt men för all del. Programmet skall vara robust.
            return;
        }
        if(question == null || question.trim().length() == 0) {
            // Konstigt. Ingen fråga tillhandahållen. Gör inget då ingen verifiering kan göras.
            return;
        }
        QuestionAndButtonsWindow confirm = new QuestionAndButtonsWindow("Bekräfta hård åtgärd", question);
        confirm.addButtonWithClose(VaadinIcons.THUMBS_UP, "Ja", onOK != null ? onOK : () -> {});
        confirm.addButtonWithClose(VaadinIcons.STOP, "Avbryt", onNok != null ? onNok : () -> {});
        functionality.addWindow(confirm, "OK hård åtgärd");
    }

    public static Tree<File> getBrowser(TextField tf) {
        FilesystemCallback fc = new FilesystemCallbackDefault(tf);
        return FilesystemDataProviderHack.browseFromPreferred(fc);
    }

}
