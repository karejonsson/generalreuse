package general.reuse.vaadinreusable.login;

import java.util.Optional;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.*;

public class CredentialsAndGroupLoginPage extends VerticalLayout implements View {
	
	private static final long serialVersionUID = 3484919610407063509L;

	private ComboBox<String> groupChoice = null;

	public CredentialsAndGroupLoginPage(CredentialsAndGroupVerifyer verifyer, Runnable onSuccess, Runnable onFailure) {
		this(verifyer, onSuccess, onFailure, "Behörighetskontroll", "Användarnamn", "Lösenord", "Vidare", "Ogiltig grupptillhörighet", "Felaktiga behörigheter");
	}

	public CredentialsAndGroupLoginPage(CredentialsAndGroupVerifyer verifyer, Runnable onSuccess, Runnable onFailure, String frameTitle, String frameUsername, String framePassword, String frameEnter, String frameWrongGroup, String frameWrongCredentials) {
		Panel panel = new Panel(frameTitle);
		panel.setSizeUndefined();
		addComponent(panel);
		
		FormLayout content = new FormLayout();
		TextField usernameField = new TextField(frameUsername);
		content.addComponent(usernameField);
		
		PasswordField password = new PasswordField(framePassword);
		content.addComponent(password);
		
		setupGroupComponent(verifyer);
		content.addComponent(groupChoice);

		Button send = new Button(frameEnter);
		send.addListener(e -> {
			String username = usernameField.getValue().toLowerCase();
			if(verifyer.accepted(username, password.getValue())) {
				String chosenGroup = getCurrentGroupOption();
				if(verifyer.partOfGroup(chosenGroup, username)) {
					verifyer.setAcceptedGroup(chosenGroup);
					onSuccess.run();
				}
				else {
					Notification.show(frameWrongGroup);
					onFailure.run();
				}
			}
			else {
				if(onFailure == null) {
					Notification.show(frameWrongCredentials);
				}
				else {
					onFailure.run();
				}
			}
		});
		
		content.addComponent(send);
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	public void setupGroupComponent(CredentialsAndGroupVerifyer verifyer) {
		groupChoice = new ComboBox<String>("Grupp");
		String[] groups = verifyer.possibleGroups();
		groupChoice.setSizeFull();
		groupChoice.setEmptySelectionAllowed(false);
		groupChoice.setTextInputAllowed(false);
		groupChoice.setWidthUndefined();
		groupChoice.setItems(groups);
		groupChoice.setSelectedItem(groups[0]);
		groupChoice.setValue(groups[0]);
	}
	
	public String getCurrentGroupOption() {
		String out = null;
		try {
			Optional<String> op = groupChoice.getSelectedItem();
			out = op.get();
		}
		catch(Exception e) {
		}
		if(out == null) {
			return null;
		}
		return out;
	}
	


}
