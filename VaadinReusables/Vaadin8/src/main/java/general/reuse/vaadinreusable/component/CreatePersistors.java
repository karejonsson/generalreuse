package general.reuse.vaadinreusable.component;

import general.reuse.interfaces.RiskyInjector;
import general.reuse.interfaces.SafeInjector;
import general.reuse.interfaces.Treater;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import org.apache.log4j.Logger;

import java.util.Date;

public class CreatePersistors {

    private static Logger log = Logger.getLogger(EditOneRowForm.class);

    public static <T> SafeInjector<T, String> getStringPersister(BaseFunctionalityProvider functionality, Treater<T, Boolean> persister, RiskyInjector<T, String> inj) {
        return (r, s) -> {
            try {
                inj.set(r, s);
            }
            catch(Exception e) {
                CommonNotification.showError(functionality, "Det nuya värdet är förbjudet");
                return;
            }
            try {
                if(!persister.treat(r)) {
                    final String msg = "Kunde inte skriva ner förändringen i databasen";
                    log.error(msg);
                    CommonNotification.showError(functionality, msg);
                    return;
                }
            }
            catch(Exception e) {
                final String msg = "Kunde inte skriva ner förändringen i databasen";
                log.error("Problem: "+msg, e);
                CommonNotification.showError(functionality, msg);
                return;
            }
        };
    }

    public static <T> SafeInjector<T, Date> getDatePersister(BaseFunctionalityProvider functionality, Treater<T, Boolean> persister, RiskyInjector<T, Date> inj) {
        return (r, s) -> {
            try {
                inj.set(r, s);
            }
            catch(Exception e) {
                CommonNotification.showError(functionality, "Det nuya värdet är förbjudet");
                return;
            }
            try {
                if(!persister.treat(r)) {
                    final String msg = "Kunde inte skriva ner förändringen i databasen";
                    log.error(msg);
                    CommonNotification.showError(functionality, msg);
                    return;
                }
            }
            catch(Exception e) {
                final String msg = "Kunde inte skriva ner förändringen i databasen";
                log.error("Problem: "+msg, e);
                CommonNotification.showError(functionality, msg);
                return;
            }
        };
    }

}
