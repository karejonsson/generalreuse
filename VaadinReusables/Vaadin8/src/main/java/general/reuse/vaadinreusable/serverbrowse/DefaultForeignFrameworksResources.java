package general.reuse.vaadinreusable.serverbrowse;

import com.vaadin.ui.Component;
import com.vaadin.ui.Window;
import general.reuse.vaadinreusable.vaadin8ish.ComponentV8ish;
import general.reuse.vaadinreusable.vaadin8ish.NotificationV8ish;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;

public class DefaultForeignFrameworksResources implements ForeignFrameworksResources {

    @Override
    public boolean getHaveMargins() {
        return false;
    }

    @Override
    public void showWarning(String s) {
        NotificationV8ish.show(s, NotificationV8ish.Type.WARNING_MESSAGE);
    }

    @Override
    public void showAssistive(String s) {
        NotificationV8ish.show(s, NotificationV8ish.Type.ASSISTIVE_NOTIFICATION);
    }

    @Override
    public void showError(String s) {
        NotificationV8ish.show(s, NotificationV8ish.Type.ERROR_MESSAGE);
    }

    @Override
    public void addWindow(Window win) {
    }

    @Override
    public void addWindow(Window win, String winType) {
    }

    @Override
    public void styles(Component components) {
    }

    @Override
    public String getParseProgramAsGroovyText() {
        return null;
    }

    @Override
    public String getExecuteProgramAsGroovyText() {
        return "Exekvera groovy";
    }

    @Override
    public String getBrowseText() {
        return "Bläddra";
    }

    @Override
    public String getEditText() {
        return "Redigera";
    }

    @Override
    public String getCancelText() {
        return "Avbryt";
    }

    @Override
    public String getSaveText() {
        return "Spara";
    }

    @Override
    public String getOneParameterText() {
        return "En parameter";
    }

    @Override
    public String getFileText() {
        return "Fil";
    }

    @Override
    public String getRevertText() {
        return "Åter";
    }

    @Override
    public String getOnCurrentText() {
        return "På nuvarande";
    }

    @Override
    public String getTwoParametersText() {
        return "Två parametrar";
    }
}
