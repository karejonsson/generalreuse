package general.reuse.vaadinreusable.login;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;

public class CredentialsLoginPage extends VerticalLayout implements View {
	
	private static final long serialVersionUID = 3484919610407063509L;

	public CredentialsLoginPage(CredentialsVerifyer verifyer, Runnable onSuccess, Runnable onFailure) {
		this(verifyer, onSuccess, onFailure, "Behörighetskontroll", "Användarnamn", "Lösenord", "Vidare", "Felaktiga behörigheter");
	}
	
	public CredentialsLoginPage(CredentialsVerifyer verifyer, Runnable onSuccess, Runnable onFailure, String frameTitle, String frameUsername, String framePassword, String frameEnter, String frameWrongCredentials) {
		if(verifyer.isDevmode()) {
			//System.out.println("<ctor> utvecklingsläge");
			//postponedEnter(verifyer, onSuccess);
		}
		else {
			//System.out.println("<ctor> inte utvecklingsläge");
		}
		Panel panel = new Panel(frameTitle);
		panel.setSizeUndefined();
		addComponent(panel);
		
		FormLayout content = new FormLayout();
		TextField username = new TextField(frameUsername);
		content.addComponent(username);
		PasswordField password = new PasswordField(framePassword);
		content.addComponent(password);

		Button send = new Button(frameEnter);
		send.addListener(e -> {
			if(verifyer.accepted(username.getValue().toLowerCase(), password.getValue())) {
				onSuccess.run();
			}
			else {
				if(onFailure == null) {
					Notification.show(frameWrongCredentials);
				}
				else {
					onFailure.run();
				}
			}
		});
		
		content.addComponent(send);
		content.setSizeUndefined();
		content.setMargin(true);
		panel.setContent(content);
		setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	private void postponedEnter(final CredentialsVerifyer verifyer, final Runnable onSuccess) { //, final UI current) {
		System.out.println("postponedEnter");
		Runnable doLater = new Runnable() {
			@Override
			public void run() {
				while(true) {
					//System.out.println("THREAD UI "+UI.getCurrent()+", PAGE "+Page.getCurrent()+", VAADINSESSION "+VaadinSession.getCurrent());
					//System.out.println("postponedEnter väntar");
					try {
						Thread.sleep(5500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					try {
						verifyer.prepareDevmode();
						break;
					}
					catch(Exception e) { e.printStackTrace(); }
				}
				System.out.println("postponedEnter klart");
				onSuccess.run();		
			}
		};
		Thread t = new Thread(doLater);
		t.start();
	}
	
}
