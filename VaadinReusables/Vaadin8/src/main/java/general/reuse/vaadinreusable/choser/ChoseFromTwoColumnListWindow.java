package general.reuse.vaadinreusable.choser;

import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.*;
import general.reuse.interfaces.Extractor;
import general.reuse.interfaces.Setter;
import general.reuse.vaadinreusable.choser.ChoseFromListHorizontalLayout.ToString;
import general.reuse.vaadinreusable.managedapp.factories.*;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;
import general.reuse.vaadinreusable.managedapp.texts.CaptionTexts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ChoseFromTwoColumnListWindow<T> extends Window {

	private static final long serialVersionUID = -2878545232969602030L;
	
	public static final String contains = "innehåller";
	public static final String startsWith = "börjar med";
	public static final String filteringValue = "Filtrerande värde";
	public static final String filtersLogic = "Filtrets logik";
	public static final String someField = "Något fält";
	public static final String windowWidth = "70%";
	public static final String gridInteriorWidth = "100%";

	private BaseFunctionalityProvider functionality = null;
	private Setter<T> set = null;
	private List<T> list = null;
	private Grid<T> grid = GridFactory.create();
	private TextField filter = TextFieldFactory.create(filteringValue);
	private ComboBox<String> filterLogic = ComboBoxFactory.create(filtersLogic);
	private String nameCol1 = null;
	private Extractor<T, String>  renderCol1 = null;
	private String nameCol2 = null;
	private Extractor<T, String>  renderCol2 = null;
	
	private String containsFirst = null;
	private String containsSecond = null;
	private String startsWithFirst = null;
	private String startsWithSecond = null;
	private String containsAny = null;
	private String startsWithAny = null;
	private String chosenFilterLogic = null;

	public ChoseFromTwoColumnListWindow(BaseFunctionalityProvider functionality, Setter<T> set, List<T> list, String nameCol1, Extractor<T, String> renderCol1, String nameCol2, Extractor<T, String>  renderCol2) {
		WindowFactory.styles(this);
		this.functionality = functionality;
		this.set = set;
		this.list = list;
		this.nameCol1 = nameCol1;
		this.renderCol1 = renderCol1;
		this.nameCol2 = nameCol2;
		this.renderCol2 = renderCol2;
		
		containsFirst = nameCol1+" "+contains;
		containsSecond = nameCol2+" "+contains;
		startsWithFirst = nameCol1+" "+startsWith;
		startsWithSecond = nameCol2+" "+startsWith;
		containsAny = someField+" "+contains;
		startsWithAny = someField+" "+startsWith;
		chosenFilterLogic = containsFirst;
		
		setup();

		setWidth(windowWidth);
	}

	private void setupGrid() {
		grid.addComponentColumn(row -> {
			return LabelFactory.create(renderCol1.get(row));
		}).setComparator((r1, r2) -> {
			try {
				return renderCol1.get(r1).compareTo(renderCol1.get(r2));
			}
			catch(Exception e) {
				return 0;
			}
		}).setCaption(nameCol1);
		grid.addComponentColumn(row -> {
			Button choseBtn = ButtonFactory.create(ButtonTexts.chose);
			choseBtn.addClickListener(e -> choiceMade(row));
			return choseBtn;
		}).setComparator((r1, r2) -> { return 0; }).setCaption(CaptionTexts.handling);
		grid.addComponentColumn(row -> {
			return LabelFactory.create(renderCol2.get(row));
		}).setComparator((r1, r2) -> {
			try {
				return renderCol2.get(r1).compareTo(renderCol2.get(r1));
			}
			catch(Exception e) {
				return 0;
			}
		}).setCaption(nameCol2);
		grid.setWidth(gridInteriorWidth);
		grid.setItems(list);
	}
		
	private void setup() {	
		setupGrid();
        ListDataProvider<T> dataProvider = (ListDataProvider<T>) grid.getDataProvider();
        dataProvider.setFilter(it -> caseInsensitiveContains(it, chosenFilterLogic));
        performFiltering("");

		VerticalLayout vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		
		HorizontalLayout hl = LayoutFactory.createHorizontal(functionality.getHaveMargins());
		hl.addComponent(filter);
		filter.addValueChangeListener(e -> performFiltering(filter.getValue()));
				
		hl.addComponent(filterLogic);
		List<String> filterMethods = new ArrayList<>();
		
		filterMethods.add(containsFirst);
		filterMethods.add(startsWithFirst);
		filterMethods.add(containsSecond);
		filterMethods.add(startsWithSecond);
		filterMethods.add(containsAny);
		filterMethods.add(startsWithAny);
		filterLogic.setItems(filterMethods);
		filterLogic.setSelectedItem(containsFirst);
		filterLogic.addValueChangeListener(e -> changedFilterLogic(e));

		vl.addComponent(hl);
		vl.addComponent(grid);
		vl.setExpandRatio(grid, 1.0f);

		Button closeBtn = ButtonFactory.create(ButtonTexts.close);
		closeBtn.addClickListener(e -> close());
		vl.addComponent(closeBtn);
		setContent(vl);
	}
		
	private void changedFilterLogic(HasValue.ValueChangeEvent<String> e) {
		Optional<String> op = filterLogic.getSelectedItem();
		chosenFilterLogic = op.get();
		performFiltering(filter.getValue());
	}

	private void choiceMade(T row) {
		try {
			set.set(row);
		}
		catch(Exception e) {
			String msg = "Kunde inte sätta in valet";
			System.out.println(msg);
			CommonNotification.showError(functionality, msg);
			return;
		}
		close();
	}

	private void performFiltering(HasValue.ValueChangeEvent<String> event) {
		performFiltering(event.getValue());
	}
	
	private void performFiltering(String value) {
        ListDataProvider<T> dataProvider = (ListDataProvider<T>) grid.getDataProvider();
        dataProvider.setFilter(it -> caseInsensitiveContains(it, value));
	}
	
    private Boolean caseInsensitiveContains(T code, String what) {
    	if(chosenFilterLogic == null) {
            return true;
    	}
    	if(chosenFilterLogic.equals(containsFirst)) {
            return renderCol1.get(code).toLowerCase().contains(what.toLowerCase());
    	}
    	if(chosenFilterLogic.equals(containsSecond)) {
            return renderCol2.get(code).toLowerCase().contains(what.toLowerCase());
    	}
    	if(chosenFilterLogic.equals(startsWithFirst)) {
            return renderCol1.get(code).toLowerCase().startsWith(what.toLowerCase());
    	}
    	if(chosenFilterLogic.equals(startsWithSecond)) {
            return renderCol2.get(code).toLowerCase().startsWith(what.toLowerCase());
    	}
    	if(chosenFilterLogic.equals(containsAny)) {
            return renderCol1.get(code).toLowerCase().contains(what.toLowerCase()) || renderCol2.get(code).toLowerCase().contains(what.toLowerCase());
    	}
    	if(chosenFilterLogic.equals(startsWithAny)) {
            return renderCol1.get(code).toLowerCase().startsWith(what.toLowerCase()) || renderCol2.get(code).toLowerCase().startsWith(what.toLowerCase());
    	}
    	return true;
    }

	public void sortList() {
		Collections.sort(list, (g1, g2) -> {
			String s1 = renderCol1.get(g1);
			String s2 = renderCol1.get(g2);
			if(s1 == null && s2 == null) {
				return 0;
			}
			if(s1 == null && s2 != null) {
				return 1;
			}
			if(s1 != null && s2 == null) {
				return -1;
			}
			int out = s1.compareTo(s2);
			if(out != 0) {
				return out;
			}
			s1 = renderCol2.get(g1);
			s2 = renderCol2.get(g2);
			return s1.compareTo(s2);
		});
	}

}
