package general.reuse.vaadinreusable.component;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import general.reuse.interfaces.Extractor;
import general.reuse.interfaces.Getter;
import general.reuse.interfaces.Setter;
import general.reuse.interfaces.Wrapper;
import general.reuse.vaadinreusable.choser.ChoseFromListHorizontalLayout;
import general.reuse.vaadinreusable.choser.ChoseFromListVerticalLayout;
import general.reuse.vaadinreusable.choser.ChoseFromTwoColumnListWindow;
import general.reuse.vaadinreusable.managedapp.factories.*;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.Callable;

public class AddToForm {

    private static Logger log = Logger.getLogger(AddToForm.class);

    public static CheckBox createBooleanField(List<Callable> inserters, List<Callable> clearers, final Getter<Boolean> get, final Setter<Boolean> set, final String caption, final Boolean boldCaption) {
        final CheckBox fieldCB = (boldCaption != null && boldCaption) ?
            CheckBoxFactory.create("<b>" + caption + "</b>") :
            CheckBoxFactory.create(caption);
        fieldCB.setCaptionAsHtml(boldCaption != null && boldCaption);
        Boolean initailValue = null;
        //log.info("1: initailValue="+initailValue);
        //log.info("1: get="+get);
        if(get != null) {
            initailValue = get.get();
            //log.info("2: initailValue="+initailValue);
        }
        if(initailValue != null) {
            fieldCB.setValue(initailValue);
            //log.info("3: initailValue="+initailValue);
        }
        //log.info("4: initailValue="+initailValue);
        //final CheckBox fieldTF = CheckBoxFactory.create(caption);
        if (clearers != null && get != null) {
            clearers.add(() -> {
                Boolean b = get.get();
                if(b != null) {
                    fieldCB.setValue(b);
                }
                else {
                    fieldCB.clear();
                }
                return null;
            });
        }
        fieldCB.setEnabled(set != null);
        try {
            //log.info("5: initailValue="+initailValue);
            if (get != null && get.get() != null) {
                log.info("6: initailValue="+initailValue);
                fieldCB.setValue(get.get());
            }
        } catch (Exception e) {
            log.error(e);
            // OK. Någon struktur kan ha null.
        }
        //log.info("5: fieldCB.getValue()="+fieldCB.getValue());
        setSize(fieldCB);
        if (inserters != null && set != null) {
            inserters.add(() -> {
                set.set(fieldCB.getValue());
                return null;
            });
        }
        return fieldCB;
    }

    public static void addBooleanField(Layout fields, List<Callable> inserters, List<Callable> clearers, final Getter<Boolean> get, final Setter<Boolean> set, final String caption, final Boolean boldCaption) {
        fields.addComponent(createBooleanField(inserters, clearers, get, set, caption, boldCaption));
    }

    public static <T> ChoseFromListHorizontalLayout<T> createSearchableChoiceField(
        BaseFunctionalityProvider functionality,
        List<Callable> inserters,
        List<Callable> clearers,
        final Getter<T> get,
        final Setter<T> set,
        List<T> alternatives,
        Extractor<T, String> renderSort,
        String nameCol1,
        Extractor<T, String> renderCol1,
        String nameCol2,
        Extractor<T, String> renderCol2,
        final String caption,
        final Boolean boldCaption) {

        List<T> alts = new ArrayList<T>(alternatives);
        final Wrapper<ChoseFromListHorizontalLayout<T>> w = new Wrapper<>();
        ChoseFromListHorizontalLayout.Searcher<T> search =
            new ChoseFromListHorizontalLayout.Searcher<T>() {
                @Override
                public void search(List<T> list) {
                    ChoseFromTwoColumnListWindow<T> choser = new ChoseFromTwoColumnListWindow<T>(
                        functionality,
                        t -> w.ptr.setChosenObject(t),
                        alts,
                        nameCol1,
                        renderCol1,
                        nameCol2,
                        renderCol2);
                    choser.sortList();
                    functionality.addWindow(choser, caption);
                }
            };

        Collections.sort(
            alts,
            (oe1, oe2) -> renderSort.get(oe1).compareTo(renderSort.get(oe2)));
        ChoseFromListHorizontalLayout<T> choice = new ChoseFromListHorizontalLayout<T>(
            alts,
            renderSort,
            search,
            caption,
            false);
        choice.setCaptionAsHtml(boldCaption != null && boldCaption);
        w.ptr = choice;
        if (clearers != null) {
            final T value = get.get();
            clearers.add(() -> {
                try {
                    choice.setChosenObject(value);
                } catch (Exception ee) {
                    log.error(caption + "-fältet 1", ee);
                }
                ;
                return null;
            });
        }
        choice.setEnabled(set != null);
        choice.setEmptySelectionAllowed(alternatives.size() == 0);
        choice.setInternalTextInputAllowed(false);

        try {
            if (get != null && get.get() != null) {
                T r = get.get();
                choice.setChosenObject(r);
                //choice.setSelectedItem(r);
            } else {
                try {
                    T r = alternatives.iterator().next();
                    choice.setChosenObject(r);
                    //choice.setSelectedItem(r);
                } catch (Exception ee) {
                    log.error(caption + "-fältet 2. Tabellen ordersources är tom!", ee);
                }
            }
        } catch (Exception e) {
            // OK. Någon struktur kan ha null.
            if (set != null) {
                try {
                    choice.setChosenObject(alternatives.iterator().next());
                } catch (Exception ee) {
                    log.error(caption + "-fältet 3", ee);
                }
            }
        }
        setSize(choice);
        if (set != null) {
            if (inserters != null) {
                inserters.add(() -> {
                    try {
                        set.set(choice.getChosenObject());
                    } catch (Exception e) {
                        log.error("Fel när " + caption + "-fältet skulle få sitt värde");
                    }
                    return null;
                });
            }
        }
        return choice;
    }

    public static <T> ComboBox<T> createChoiceField(List<Callable> inserters, List<Callable> clearers, final Getter<T> get, final Setter<T> set, Collection<T> alternatives, final String caption, final Boolean boldCaption) {
        final ComboBox<T> fieldCB = (caption != null) ?
            ((boldCaption != null && boldCaption) ?
                ComboBoxFactory.create("<b>" + caption + "</b>") :
                ComboBoxFactory.create(caption)) :
            ComboBoxFactory.create();
        fieldCB.setCaptionAsHtml(boldCaption != null && boldCaption);

        if (clearers != null) {
            clearers.add(() -> {
                try {
                    fieldCB.setValue(alternatives.iterator().next());
                } catch (Exception ee) {
                    log.error(caption + "-fältet 1", ee);
                }
                ;
                return null;
            });
        }
        fieldCB.setEnabled(set != null);
        fieldCB.setEmptySelectionAllowed(alternatives.size() == 0);
        fieldCB.setTextInputAllowed(false);
        fieldCB.setItems(alternatives);
        try {
            if (get != null && get.get() != null) {
                T r = get.get();
                fieldCB.setValue(r);
                fieldCB.setSelectedItem(r);
            } else {
                try {
                    T r = alternatives.iterator().next();
                    fieldCB.setValue(r);
                    fieldCB.setSelectedItem(r);
                } catch (Exception ee) {
                    log.error(caption + "-fältet 2. Tabellen ordersources är tom!", ee);
                }
            }
        } catch (Exception e) {
            // OK. Någon struktur kan ha null.
            if (set != null) {
                try {
                    fieldCB.setValue(alternatives.iterator().next());
                } catch (Exception ee) {
                    log.error(caption + "-fältet 3", ee);
                }
            }
        }
        setSize(fieldCB);
        if (set != null) {
            if (inserters != null) {
                inserters.add(() -> {
                    try {
                        set.set(fieldCB.getValue());
                    } catch (Exception e) {
                        log.error("Fel när " + caption + "-fältet skulle få sitt värde");
                    }
                    return null;
                });
            }
        }
        return fieldCB;
    }

    public static void addChoiceField(Layout fields, List<Callable> inserters, List<Callable> clearers, final Getter<Object> get, final Setter<Object> set, Collection<Object> alternatives, final String caption, final Boolean boldCaption) {
        fields.addComponent(createChoiceField(inserters, clearers, get, set, alternatives, caption, boldCaption));
    }

    public static <T> void addSearchableChoiceField(
        BaseFunctionalityProvider functionality,
        Layout fields,
        List<Callable> inserters,
        List<Callable> clearers,
        final Getter<T> get,
        final Setter<T> set,
        List<T> alternatives,
        Extractor<T, String> renderSort,
        String nameCol1,
        Extractor<T, String> renderCol1,
        String nameCol2,
        Extractor<T, String> renderCol2,
        final String caption,
        final Boolean boldCaption) {
        fields.addComponent(createSearchableChoiceField(
            functionality,
            inserters,
            clearers,
            get,
            set,
            alternatives,
            renderSort,
            nameCol1,
            renderCol1,
            nameCol2,
            renderCol2,
            caption,
            boldCaption));
    }



    public static void setSize(Component c) {
        c.setWidthFull();
        c.setHeightUndefined();
    }


    public static TextArea createTexteditorField(List<Callable> inserters, List<Callable> clearers, final Getter<String> get, final Setter<String> set, final String caption, final Boolean boldCaption) {
        boolean boldness = boldCaption != null && boldCaption;
        final TextArea fieldTA = (boldness) ?
            TextAreaFactory.create("<b>" + caption + "</b>") :
            TextAreaFactory.create(caption);
        fieldTA.setCaptionAsHtml(boldness);
        if (clearers != null) {
            final String initialValue = fieldTA.getValue();
            clearers.add(() -> {
                fieldTA.setValue(initialValue);
                return null;
            });
        }
        fieldTA.setEnabled(set != null);
        try {
            if (get != null && get.get() != null) {
                fieldTA.setValue(get.get() == null ? "" : get.get());
            }
        } catch (Exception e) {
            // OK. Någon struktur kan ha null.
        }
        fieldTA.setSizeFull();
        if (set != null) {
            if (inserters != null) {
                inserters.add(() -> {
                    set.set(fieldTA.getValue());
                    return null;
                });
            }
        }
        return fieldTA;
    }

    public static void addTexteditorField(VerticalLayout fields, List<Callable> inserters, List<Callable> clearers, final Getter<String> get, final Setter<String> set, final String caption, final Boolean boldCaption, final Float expandRatio) {
        TextArea fieldTA = createTexteditorField(inserters, clearers, get, set, caption, boldCaption);
        //fields.addComponent(fieldTA);
        fields.setHeight("100%");
        fields.addComponent(fieldTA);
        fields.setExpandRatio(fieldTA, 1.0f);
        //fieldTA.setHeightFull();
        if (expandRatio != null) {
            fields.setExpandRatio(fieldTA, expandRatio);
        }
    }

    public static Label createLabel(String text, Boolean boldCaption) {
        boolean boldness = boldCaption != null && boldCaption;
        Label out = boldness ?
            LabelFactory.create("<b>" + text + "</b>") :
            LabelFactory.create(text);
        //out.setCaptionAsHtml(boldness);
        if (boldness) {
            out.setContentMode(ContentMode.HTML);
        }
        return out;
    }

    public static void addLabel(Layout fields, String text, Boolean boldCaption) {
        fields.addComponent(createLabel(text, boldCaption));
    }

    public static TextField createStringField(List<Callable> inserters, List<Callable> clearers, final Getter<String> get, final Setter<String> set, final String caption, Boolean boldCaption) {
        final TextField fieldTF = (boldCaption != null && boldCaption) ?
            TextFieldFactory.create("<b>" + caption + "</b>") :
            TextFieldFactory.create(caption);
        fieldTF.setCaptionAsHtml(boldCaption != null && boldCaption);
        if (clearers != null) {
            final String initialValue = fieldTF.getValue();
            clearers.add(() -> {
                fieldTF.setValue(initialValue);
                return null;
            });
        }
        fieldTF.setEnabled(set != null);
        try {
            if (get != null && get.get() != null) {
                fieldTF.setValue(get.get() == null ? "" : get.get());
            }
        } catch (Exception e) {
            // OK. Någon struktur kan ha null.
        }
        setSize(fieldTF);
        if (set != null) {
            if (inserters != null) {
                inserters.add(() -> {
                    set.set(fieldTF.getValue());
                    return null;
                });
            }
        }
        return fieldTF;
    }

    public static void addStringField(Layout fields, List<Callable> inserters, List<Callable> clearers, final Getter<String> get, final Setter<String> set, final String caption, Boolean boldCaption) {
        fields.addComponent(createStringField(inserters, clearers, get, set, caption, boldCaption));
    }

    public static void addDateField(Layout fields, List<Callable> inserters, List<Callable> clearers, final Getter<Date> get, final Setter<Date> set, String dateFormat, final String caption, Boolean boldCaption) {
        fields.addComponent(createDateField(inserters, clearers, get, set, dateFormat, caption, boldCaption));
    }

    public static DateField createDateField(List<Callable> inserters, List<Callable> clearers, final Getter<Date> get, final Setter<Date> set, final String dateFormat, final String caption, Boolean boldCaption) {
        final DateField fieldDF = (boldCaption != null && boldCaption) ?
            DateFieldFactory.create("<b>" + caption + "</b>") :
            DateFieldFactory.create(caption);
        if(dateFormat != null) {
            fieldDF.setDateFormat(dateFormat);
        }
        else {
            fieldDF.setDateFormat("yyyy-MM-dd");
        }
        final Date date = get.get();
        final LocalDate dateLD = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        fieldDF.setValue(dateLD);
        fieldDF.setCaptionAsHtml(boldCaption != null && boldCaption);
        if (clearers != null) {
            clearers.add(() -> {
                fieldDF.setValue(dateLD);
                return null;
            });
        }
        fieldDF.setEnabled(set != null);
        try {
            if (get != null && get.get() != null) {
                Date res = get.get() == null ? new Date() : get.get();
                LocalDate resLD = res.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                fieldDF.setValue(resLD);
            }
        } catch (Exception e) {
            // OK. Någon struktur kan ha null.
        }
        setSize(fieldDF);
        if (set != null) {
            if (inserters != null) {
                inserters.add(() -> {
                    Date newDate = Date.from(fieldDF.getValue().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
                    set.set(newDate);
                    return null;
                });
            }
        }
        return fieldDF;
    }

}
