package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.Image;

public class ImageV8ish extends Image {

    public ImageV8ish() {
        super();
    }

    public ImageV8ish(String caption) {
        super(caption);
    }

    public ImageV8ish(String caption, ResourceV8ish source) {
        super(caption, source);
    }

}
