package general.reuse.vaadinreusable.serverbrowse;

import general.reuse.vaadinreusable.vaadin8ish.LabelV8ish;
import general.reuse.vaadinreusable.vaadin8ish.VerticalLayoutV8ish;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OperateOnOneDescribeOperation implements OperateOnOneBrowsed {

    private static Logger logg = Logger.getLogger(OperateOnOneDescribeOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnOneDescribeOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Beskriv";
    }

    @Override
    public String getDescription() {
        return "Beskriver en fil";
    }

    @Override
    public Runnable getOperate(String filepath) {
        return () -> {
            operate(filepath);
        };
    }

    @Override
    public void operate(String filepath) {
        List<String> description = new ArrayList<>();
        if(filepath == null || filepath.trim().length() == 0) {
            description.add("Fältet för filnamn är tomt\n");
            showDescription(description);
            return;
        }

        logg.info("filepath="+filepath);
        description.add("Beskrivning av sökvägen "+filepath+"\n");

        File f = new File(filepath);
        if(f.exists()) {
            description.add("Filen finns på filsystemet\n");
        }
        else {
            description.add("Filen finns inte på filsystemet\n");
            showDescription(description);
            return;
        }
        if(f.isDirectory()) {
            description.add("Sökvägen pekar på en katalog\n");
        }
        else {
            description.add("Sökvägen pekar inte på en katalog\n");
        }
        if(f.canRead()) {
            description.add("Den är läsbar\n");
        }
        else {
            description.add("Den är inte läsbar\n");
        }
        if(f.canWrite()) {
            description.add("Den är skrivbar\n");
        }
        else {
            description.add("Den är inte skrivbar\n");
        }
        if(f.canExecute()) {
            description.add("Den är exekverbar\n");
        }
        else {
            description.add("Den är inte exekverbar\n");
        }
        if(f.isHidden()) {
            description.add("Den är gömd\n");
        }
        else {
            description.add("Den är inte gömd\n");
        }
        description.add("Längd är "+f.length()+"\n");
        description.add("URI är "+f.toURI()+"\n");
        description.add("Filsystem "+f.toPath().getFileSystem().toString()+"\n");
        description.add("Modifierades senast  "+new Date(f.lastModified())+"\n");
        showDescription(description);
    }

    private void showDescription(List<String> description) {
        VerticalLayoutV8ish vl = new VerticalLayoutV8ish();
        for(String line : description) {
            vl.addComponent(new LabelV8ish(line));
        }
        WindowV8ish win = new WindowV8ish();
        win.setContent(vl);
        win.setWidth("40%");
        win.setHeight("50%");
        functionality.addWindow(win, "Visa beskrivning");
    }

}
