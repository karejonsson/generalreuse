package general.reuse.vaadinreusable.parameters;

import java.util.List;

import general.reuse.database.parameters.domain.PersistentValue;

public interface PersistentValuePersister {

	List<PersistentValue> getAllPersistentValues() throws Exception;
	PersistentValue getNewPersistentValue();
	void removeValue(String key) throws Exception;
	void saveAll();

}
