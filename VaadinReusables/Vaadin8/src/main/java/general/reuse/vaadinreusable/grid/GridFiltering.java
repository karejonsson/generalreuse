package general.reuse.vaadinreusable.grid;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Link;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.components.grid.HeaderCell;
import com.vaadin.ui.components.grid.HeaderRow;
import com.vaadin.ui.themes.ValoTheme;

public class GridFiltering {

    public static String getValue(Object obj) {
    	if(obj instanceof Label) {
    		return ((Label) obj).getValue();
    	}
    	if(obj instanceof AbstractTextField) {
    		return ((AbstractTextField) obj).getValue();
    	}
    	if(obj instanceof CheckBox) {
    		Boolean out = ((CheckBox) obj).getValue();
    		if(out == null) {
    			return "";
    		}
    		return out.toString();
    	}
    	if(obj instanceof Link) {
    		String out = ((Link) obj).getCaption();
    		if(out == null) {
    			return "";
    		}
    		return out.toString();
    	}
    	if(obj instanceof ComboBox) {
			Optional<Object> op = ((ComboBox) obj).getSelectedItem();
			if(op == null) {
				return "";
			}
			Object out = op.get();
    		if(out == null) {
    			return "";
    		}
    		return out.toString();
    	}
    	if(obj instanceof Layout) {
			return "";
    	}
    	if(obj instanceof DateField) {
        	//System.out.println("DateField Typ "+obj.getClass().getName()+", value "+obj.toString());
    		DateField df = (DateField) obj;
    		String format = df.getDateFormat();
    		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
    		LocalDate ld = df.getValue();
    		return ld.format(formatter);
    	}
    	/*
    	System.out.println("Typ "+obj.getClass().getName());
    	if(obj instanceof Component) {
    		Component comp = (Component) obj;
    		return comp.toString();
    	}
    	*/
    	if(obj instanceof HorizontalLayout) {
    		HorizontalLayout objHL = (HorizontalLayout) obj;
    		try {
    			return getValue(objHL.getComponent(0));
    		}
    		catch(Exception e) {}
    	}
    	return obj.toString();
    }
    
    public static Boolean caseInsensitiveContains(String where, String what) {
        return where.toLowerCase().contains(what.toLowerCase());
    }
    
    public static void main(String[] args) {
    	ValueProvider vp = new ValueProvider() {
			@Override
			public Object apply(Object source) {
				return null;
			}
    	};
    	ValueProvider<String, Runnable> vpt = new ValueProvider<String, Runnable>() {
			@Override
			public Runnable apply(String source) {
				return null;
			}
    	};
    }
    
	public static boolean addColumnFilters(Grid grid) {
		final HeaderRow filterRow = grid.appendHeaderRow();
		DataProvider dp = grid.getDataProvider();
		if(!(dp instanceof ListDataProvider)) {
			System.err.println("Grids columns are asked to be arranged for filtering");
			System.err.println("but the data provider is not of the type managed by");
			System.err.println("this implementation. Following stacktrace allows to");
			System.err.println("find the call");
			(new Throwable()).printStackTrace();
			return false;
		}
		ListDataProvider dataProvider = (ListDataProvider) dp;
		for (final Column<Object, ?> column : (List<Column<Object, ?>>) grid.getColumns()) {
	        final TextField filter = new TextField();
	        filter.addStyleName(ValoTheme.TEXTFIELD_TINY);
	        filter.setWidth(100, Unit.PERCENTAGE);
	        ValueProvider vp = (ValueProvider) column.getValueProvider();
	        filter.addValueChangeListener(event -> {
	            dataProvider.setFilter(vp, s -> caseInsensitiveContains(getValue(s), event.getValue()));
	        });
	        
	        CssLayout filtering = new CssLayout();
	        Button clear = new Button(FontAwesome.TIMES);
	        clear.setDescription("Clear filter");
	        clear.addClickListener(e -> filter.clear());
	        clear.addStyleName(ValoTheme.TEXTFIELD_TINY);
	        filtering.addComponents(filter, clear);
	        filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

			final HeaderCell headerCell = filterRow.getCell(column);
			headerCell.setComponent(filtering);
		}	
		return true;
	}

	public static boolean addColumnSorting(Grid grid) {
		final HeaderRow filterRow = grid.appendHeaderRow();
		DataProvider dp = grid.getDataProvider();
		if(!(dp instanceof ListDataProvider)) {
			System.err.println("Grids columns are asked to be arranged for filtering");
			System.err.println("but the data provider is not of the type managed by");
			System.err.println("this implementation. Following stacktrace allows to");
			System.err.println("find the call");
			(new Throwable()).printStackTrace();
			return false;
		}
		ListDataProvider dataProvider = (ListDataProvider) dp;
		for (final Column<Object, ?> column : (List<Column<Object, ?>>) grid.getColumns()) {
			column.setSortable(true);
		}	
		return true;
	}

}
