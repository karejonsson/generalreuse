package general.reuse.vaadinreusable.managedapp.internalchat;

import com.vaadin.ui.Button;
import general.reuse.vaadinreusable.chat.Broadcaster;
import org.apache.log4j.Logger;

import com.vaadin.icons.VaadinIcons;

import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.session.SessionRegistry;
import general.reuse.vaadinreusable.managedapp.texts.DescriptionTexts;

public class ChatButton {
	
	private static Logger logg = Logger.getLogger(ChatButton.class);

	public static Button createWhenActiveStateConsidered(BaseFunctionalityProvider functionality) {
		if(!SessionRegistry.chatActive.getValue()) {
			return null;
		}
		long since = functionality.getChatSince();
		int noSince = Broadcaster.countSince(since);
		if(noSince == 0 && functionality.getAdminFunctionality() == null) {
			return null;
		}
		Button chatBtn = ButtonFactory.create(VaadinIcons.CHAT);
		if(noSince != 0) {
			chatBtn.setCaption("("+noSince+")");
		}
		chatBtn.setDescription(DescriptionTexts.internalChat);
		chatBtn.addClickListener(e -> {
			functionality.openChat(functionality.getRunnableForReturnToCurrentContent());
		});
		return chatBtn;
	}

	public static Button createAlwaysStateConsidered(BaseFunctionalityProvider functionality) {
		if(!SessionRegistry.chatActive.getValue()) {
			return null;
		}
		long since = functionality.getChatSince();
		int noSince = Broadcaster.countSince(since);
		Button chatBtn = ButtonFactory.create(VaadinIcons.CHAT);
		if(noSince != 0) {
			chatBtn.setCaption("("+noSince+")");
		}
		chatBtn.setDescription(DescriptionTexts.internalChat);
		chatBtn.addClickListener(e -> {
			functionality.openChat(functionality.getRunnableForReturnToCurrentContent());
		});
		return chatBtn;
	}

	public static Button createAlwaysStateNotConsidered(BaseFunctionalityProvider functionality) {
		long since = functionality.getChatSince();
		int noSince = Broadcaster.countSince(since);
		Button chatBtn = ButtonFactory.create(VaadinIcons.CHAT);
		if(noSince != 0) {
			chatBtn.setCaption("("+noSince+")");
		}
		chatBtn.setDescription(DescriptionTexts.internalChat);
		chatBtn.addClickListener(e -> {
			functionality.openChat(functionality.getRunnableForReturnToCurrentContent());
		});
		return chatBtn;
	}

}
