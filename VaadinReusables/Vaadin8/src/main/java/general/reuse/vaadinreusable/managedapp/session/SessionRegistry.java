package general.reuse.vaadinreusable.managedapp.session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vaadin.ui.CheckBox;
import general.reuse.vaadinreusable.vaadin8ish.CheckBoxV8ish;
import org.apache.log4j.Logger;

import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.install.DatabaseProperties;
import general.reuse.vaadinreusable.managedapp.install.InstallationProperties;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.managedapp.texts.CaptionTexts;
import general.reuse.vaadinreusable.managedapp.texts.DescriptionTexts;

public class SessionRegistry {

	private static Logger logg = Logger.getLogger(SessionRegistry.class);	

	private static Map<String, List<FunctionalityWrapper>> m = new HashMap<>();

	public static void add(BaseFunctionalityProvider functionality) {
		handleWarning(functionality);
		Boolean opened = openedForLogin.getValue();
		if(opened != null && !opened) {
			logg.info("Avvisar inloggning. functionality="+functionality);
			functionality.destroyAllInternals();
			return;
		}
		List<FunctionalityWrapper> l = m.get(functionality.getUsername());
		if(l == null) {
			l = new ArrayList<>();
			m.put(functionality.getUsername(), l);
		}
		while(l.size() > 0) {
			FunctionalityWrapper fw = l.remove(0);
			fw.destroy();
		}
		l.add(new FunctionalityWrapper(functionality));
		logg.info("Noterar användare "+functionality.getUsername());
		if(l.size() > 1) {
			logg.error("Noterar felaktighet. Användare "+functionality.getUsername()+" har dubbel närvaro.");
		}
	}

	public static void remove(BaseFunctionalityProvider functionality) {
		List<FunctionalityWrapper> l = m.get(functionality.getUsername());
		if(l == null) {
			logg.error("Noterar felaktighet. Användare "+functionality.getUsername()+" tas bort utan att ha varit tillagd.");
			return;
		}
		for(FunctionalityWrapper wrapper : l) {
			if(wrapper.contains(functionality)) {
				l.remove(wrapper); // Ok då vi alltid gör return nedan.
				if(l.size() == 0) {
					m.remove(functionality.getUsername());
					logg.info("Avnoterar användare "+functionality.getUsername());
				}
				else {
					logg.info("Avnoterar en förekomst av användare "+functionality.getUsername());
				}
				return;
			}
		}
		logg.error("Noterar felaktighet. Användare "+functionality.getUsername()+" tas bort utan att ha varit tillagd.");
	}
	
	public static List<FunctionalityWrapper> getCurrentUserlist() {
		List<FunctionalityWrapper> out = new ArrayList<>();
		for(String username : m.keySet()) {
			List<FunctionalityWrapper> functionalities = m.get(username);
			for(FunctionalityWrapper wrapper : functionalities) {
				out.add(wrapper);
			}
		}
		return out;
	}

	public static final BooleanMemoryAnonymousSetting openedForLogin = new BooleanMemoryAnonymousSetting(DatabaseProperties.login_onlyfor, null, CaptionTexts.allowLoginRightNow);
	public static final BooleanMemoryAnonymousSetting warnShutdown = new BooleanMemoryAnonymousSetting(InstallationProperties.prv_managedapp_warnshutdown, false, CaptionTexts.warnShutdown);
	public static final BooleanMemoryAnonymousSetting chatActive = new BooleanMemoryAnonymousSetting(InstallationProperties.prv_managedapp_chatactive, false, CaptionTexts.activateChat);

	public static CheckBoxV8ish[] getCheckboxSettings(String privileged) {
		return new CheckBoxV8ish[] {
				openedForLogin.getCheckbox(),
				warnShutdown.getCheckbox(),
				chatActive.getCheckbox()
		};
	}

	public static LongMemoryCounter clicks = new LongMemoryCounter(0L, "Klick");
	public static LongMemoryCounter views = new LongMemoryCounter(0L, "Vyer");

	private static final Map<String, Long> since = new HashMap<>();

	public static void clearWarnings() {
		since.clear();
	}

	public static Long warnedSince(BaseFunctionalityProvider functionality) {
		return since.get(functionality.getUsername());
	}

	public static void setWarned(BaseFunctionalityProvider functionality) {
		since.put(functionality.getUsername(), System.currentTimeMillis()/1000);
	}

	public static boolean isWarned(BaseFunctionalityProvider functionality) {
		return since.keySet().contains(functionality.getUsername());
	}

	public static boolean isWarned(String username) {
		return since.keySet().contains(username);
	}

	private static String warningText =
			"Det är inplanerat att stänga ner "+ DescriptionTexts.appname+" i närtid.\n"+
					"Lämna inte "+DescriptionTexts.appname+" i ett känsligt läge, dvs så att\n"+
					"något tar skada av en plötslig nedstängning.\n"+
					"Den vanliga proceduren för stängning kommer tillämpas, inklusive\n"+
					"varningsmeddelanden, när det närmar sig.\n"+
					"Vänligen,\n"+
					"Driften";

	public static void setWarningText(String text) {
		warningText = text;
	}

	public static String getWarningText() {
		return warningText;
	}

	private static Long warningTime = null;

	public static void setWarningTime(Long time) {
		warningTime = time;
	}

	public static long getWarningTime() {
		if(warningTime == null) {
			return Long.MAX_VALUE;
		}
		if(warningTime == null) {
			return 3*3600l;
		}
		return warningTime;
	}

	public static void handleWarning(BaseFunctionalityProvider functionality) {
		if(SessionRegistry.warnShutdown.getValue()) {
			Long since = warnedSince(functionality);
			long now = System.currentTimeMillis()/1000;
			long secsSince = now - (since != null ? since : 0);
			long warnTime = getWarningTime();
			logg.info("Varning: "+functionality.getUsername()+" -> "+since+", förfluten tid "+secsSince+", tidsgräns "+warnTime);
			if(since == null || secsSince > warnTime) {
				logg.info("Varnar: "+functionality.getUsername());
				CommonNotification.showAssistive(functionality, warningText);
				setWarned(functionality);
			}
			else {
				logg.info("Varnar inte: "+functionality.getUsername());
			}
		}
		else {
			clearWarnings();
		}
	}

	public static boolean closeMechanicallyAndLogout(final BaseFunctionalityProvider functionality) {
		functionality.updateUidname();
		logg.info("Loggar ut");
		boolean out = true;
		try {
			functionality.logout(null);
		}
		catch(Exception e) {
			out = false;
		}
		return out;
	}

}
