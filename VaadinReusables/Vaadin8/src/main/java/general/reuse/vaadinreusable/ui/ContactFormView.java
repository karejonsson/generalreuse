package general.reuse.vaadinreusable.ui;

import java.awt.Font;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import general.reuse.vaadinreusable.vaadin8ish.*;
import se.prv.mailing.ConfiguredMailer;
import general.reuse.vaadinreusable.careful.CaptchaFunctions;

public class ContactFormView extends PanelV8ish {
	
	private static final long serialVersionUID = 6897526523727343534L;
	
	private static final int captchaImageWidth = 250;
	private static final int captchaImageHeight = 70;
	
	private TextFieldV8ish email = null;
	private TextFieldV8ish name = null;
	private TextFieldV8ish title = null;
	private TextAreaV8ish message = null;
	private TextFieldV8ish captcha = null;
	
	private String captchaId = null;
	
	private ImageV8ish captchaImage = null;
	private ConfiguredMailer mailer = null;
	private Runnable onCancel;
	private Runnable onSuccess;
	
	public ContactFormView(ConfiguredMailer mailer, Runnable onCancel, Runnable onSuccess) { 
		this.mailer = mailer;
		this.onCancel = onCancel;
		this.onSuccess = onSuccess;
		
		//String fragment = getPage().getUriFragment();
		//System.out.println("Gragment <"+fragment+">");
		FormLayoutV8ish layout = new FormLayoutV8ish();
		layout.addStyleName("contactform");
        setWidth("100%");
        setHeight("100%");

        email = new TextFieldV8ish("E-postadress");
        email.setWidth("70%");
        layout.addComponent(email);
        //layout.setComponentAlignment(email, Alignment.MIDDLE_CENTER);

        name = new TextFieldV8ish("Namn");
        name.setWidth("70%");
        layout.addComponent(name);
        //layout.setComponentAlignment(name, Alignment.MIDDLE_CENTER);

        title = new TextFieldV8ish("Titel");
        title.setWidth("70%");
        layout.addComponent(title);
        //layout.setComponentAlignment(title, Alignment.MIDDLE_CENTER);

        message = new TextAreaV8ish("Meddelande");
        message.setWidth("70%");
        layout.addComponent(message);
        //layout.setComponentAlignment(message, Alignment.MIDDLE_CENTER);
        
        try {
			captchaId = CaptchaFunctions.generateCaptcha();
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		}

        captchaImage = new ImageV8ish("");

        CaptchaFunctions.setCaptchaImage(captchaImage, captchaId, captchaImageWidth, captchaImageHeight);
        
        layout.addComponent(captchaImage);
        
        captcha = new TextFieldV8ish("Skriv tecknen.");
        captcha.setWidth("20%");
        layout.addComponent(captcha);

		HorizontalLayoutV8ish horizontalLayout = new HorizontalLayoutV8ish();

        final ButtonV8ish send = new ButtonV8ish("Skicka");
        send.addListener(e -> {
        		try {
            		send();
        		}
        		catch(Exception ee) {
        			NotificationV8ish.show("Kontaktfunktionen kan ej användas", NotificationV8ish.TypeV8ish.ERROR_MESSAGE);
        			return;
        		}
        	});
        horizontalLayout.addComponent(send);

        if(onCancel != null) {
            final ButtonV8ish cancel = new ButtonV8ish("Avbryt");
            cancel.addListener(e -> onCancel.run());
            horizontalLayout.addComponent(cancel);
        }

        final ButtonV8ish explainCaptcha = new ButtonV8ish("Förklara bilden");
        explainCaptcha.addListener(e -> {
        		NotificationV8ish.show("Syftet är att webb-robotar inte skall kunna skicka mejl.\nSkriv tecknen i bilden i fältet nedanför. 8 tecken, siffor och bokstäver.");
        	});
        horizontalLayout.addComponent(explainCaptcha);

        final ButtonV8ish otherCaptcha = new ButtonV8ish("Ny bild");
        otherCaptcha.addListener(e -> {
        		NotificationV8ish.show(messageWhenDelayingRobots);
        		try {
					Thread.sleep(timeToDelayRobots);
				} catch (InterruptedException ee) {
					ee.printStackTrace();
				}
        		CaptchaFunctions.setCaptchaImage(captchaImage, captchaId, captchaImageWidth, captchaImageHeight);
        		captcha.setValue("");
        	});
        horizontalLayout.addComponent(otherCaptcha);

        layout.addComponent(horizontalLayout);
        //layout.setComponentAlignment(horizontalLayout, Alignment.MIDDLE_CENTER);
        setContent(layout);
	}
	
	private static final int timeToDelayRobots = 3000;
	private static final String messageWhenDelayingRobots = "Väntade i 3 sekunder!";

	private void send() throws Exception {
		try {
			Thread.sleep(timeToDelayRobots);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		String response = captcha.getValue();
		boolean isResponseCorrect = response.equals(captchaId);
		
		System.out.println("Resultat "+isResponseCorrect+", skrivet "+response+", rätt svar "+captchaId);

		if(!isResponseCorrect) {
			CaptchaFunctions.setCaptchaImage(captchaImage, captchaId, captchaImageWidth, captchaImageHeight);
			captcha.setValue("");
			NotificationV8ish.show(
					messageWhenDelayingRobots+"\n"+
					"Koden stämde inte.\n"+
					"Försök igen med den nygenererade koden.",
					NotificationV8ish.TypeV8ish.WARNING_MESSAGE
					);
			return;
		} 
		
		NotificationV8ish.show(messageWhenDelayingRobots);
		
		mailer.send(email.getValue(), title.getValue(), message.getValue()+"\n\nAvsändarens namn: "+name.getValue());
		captcha.clear();
		
		if(onSuccess != null) {
			onSuccess.run();
		}
	}
	
	private static List<Font> usedFonts = null;
	
	public static List<Font> getAllFonts() {
		if(usedFonts != null) {
			return usedFonts;
		}
		usedFonts = CaptchaFunctions.getAllFonts();
		return usedFonts;
	}

}
