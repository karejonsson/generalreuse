package general.reuse.vaadinreusable.chat;

public interface BroadcastListener {

    void message(ChatHolder ch);
    void join(String username);
    void leave(String username);
    String getUsername();
    boolean isFunctional();

}
