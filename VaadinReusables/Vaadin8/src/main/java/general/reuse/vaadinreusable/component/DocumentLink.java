package general.reuse.vaadinreusable.component;

import com.vaadin.server.StreamResource;
import com.vaadin.ui.Link;
import general.reuse.interfaces.Extractor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class DocumentLink<T> extends Link {

    private static final long serialVersionUID = -861816214400624227L;

    private Extractor<T, byte[]> getFile = null;
    private Extractor<T, String> getFilename = null;
    private Extractor<T, String> getMimetype = null;
    private Runnable externalRefresh = null;
    private T obj = null;

    public DocumentLink(Extractor<T, byte[]> getFile, Extractor<T, String> getFilename, Extractor<T, String> getMimetype, Runnable externalRefresh, T obj) {
        super();
        this.getFile = getFile;
        this.getFilename = getFilename;
        this.getMimetype = getMimetype;
        this.externalRefresh = externalRefresh;
        this.obj = obj;
        refresh();
        setDescription("Hämta fil till din dator");
        setTargetName("_blank");
    }

    public void refresh() {
        setCaption(getFilename.get(obj));
        if(resource != null) {
            resource.setMIMEType(getMimetype.get(obj));
        }
        setEnabled(getFilename.get(obj) != null && getFilename.get(obj).trim().length() > 0);
        if(externalRefresh != null) {
            externalRefresh.run();
        }
    }

    private StreamResource resource = null;

    @Override
    public void attach() {
        super.attach(); // Must call.
        String filename = getFilename.get(obj);

        StreamResource.StreamSource source = new StreamResource.StreamSource() {
            public InputStream getStream() {
                if(filename == null) {
                    return null;
                }
                return new ByteArrayInputStream(getFile.get(obj));
            }
        };
        resource = new StreamResource(source, filename);
        resource.setMIMEType(getMimetype.get(obj));
        resource.setCacheTime(0);
        setResource(resource);
    }

}