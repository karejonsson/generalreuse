package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.UI;

import java.net.URI;

public abstract class UIV8ish extends UI {

    public int getBrowserWindowWidth() {
        return getPage().getBrowserWindowWidth();
    }

    public int getBrowserWindowHeight() {
        return getPage().getBrowserWindowHeight();
    }

    public ComponentV8ish getContent() {
        return (ComponentV8ish) super.getContent();
    }

    public URI getLocation() {
        return getPage().getLocation();
    }

}
