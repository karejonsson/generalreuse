package general.reuse.vaadinreusable.managedapp.session;

import general.reuse.vaadinreusable.vaadin8ish.CheckBoxV8ish;
import org.apache.log4j.Logger;
import general.reuse.vaadinreusable.managedapp.install.InstallationProperties;

public class BooleanMemoryAnonymousSetting {

    private String propertyName = null;
    private Boolean defaultState = null;
    private String askingText = null;
    private Boolean state = false;

    public BooleanMemoryAnonymousSetting(String propertyName, Boolean defaultState, String askingText) {
        this.propertyName = propertyName;
        this.defaultState = defaultState;
        this.askingText = askingText;
        state = InstallationProperties.getBoolean(propertyName ,defaultState);
    }

    public Boolean getValue() {
        return state;
    }

    public void setValue(Boolean state) {
        this.state = state;
    }

    public String getAskingText() {
        return askingText;
    }

    private static Logger logg = Logger.getLogger(BooleanMemoryAnonymousSetting.class);

    public CheckBoxV8ish getCheckbox() {
        CheckBoxV8ish outCB = new CheckBoxV8ish(getAskingText());
        Boolean initValue = getValue();
        if(initValue == null) {
            initValue = false;
        }
        outCB.setValue(initValue);
        outCB.addValueChangeListener(e -> {
            logg.info("Ändrar värdet för '"+askingText+"'. Innan är värdet "+ getValue()+", efter "+outCB.getValue());
            setValue(outCB.getValue());
        });
        return outCB;
    }

}
