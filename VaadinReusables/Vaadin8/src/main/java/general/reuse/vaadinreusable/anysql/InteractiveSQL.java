package general.reuse.vaadinreusable.anysql;

import com.vaadin.server.Sizeable;
import com.vaadin.ui.VerticalSplitPanel;
import general.reuse.database.pool.ManagedConnection;
import general.reuse.vaadinreusable.managedapp.factories.PanelFactory;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import org.apache.log4j.Logger;

public class InteractiveSQL extends VerticalSplitPanel {
	
	private static final long serialVersionUID = 4524555622942192051L;
	private static Logger logg = Logger.getLogger(InteractiveSQL.class);

	private BaseFunctionalityProvider functionality = null;
	private Runnable onBack = null;
	
	public InteractiveSQL(BaseFunctionalityProvider functionality, ManagedConnection mc, Runnable onBack) {
		PanelFactory.styles(this);
		this.functionality = functionality;
		this.onBack = onBack;
		setSplitPosition(35, Sizeable.UNITS_PERCENTAGE);
		ShowResultSet srs = new ShowResultSet(functionality);
		ManageWrittenSQL mwsql = new ManageWrittenSQL(functionality, onBack, mc, (rs) -> srs.show(rs));
		mwsql.setSizeFull();
		srs.setSizeFull();
		setFirstComponent(mwsql);
		setSecondComponent(srs);
	}

}
