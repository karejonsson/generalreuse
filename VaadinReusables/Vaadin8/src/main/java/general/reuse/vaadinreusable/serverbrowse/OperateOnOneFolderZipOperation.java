package general.reuse.vaadinreusable.serverbrowse;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class OperateOnOneFolderZipOperation implements OperateOnOneBrowsed {

    private static Logger logg = Logger.getLogger(OperateOnOneFolderZipOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnOneFolderZipOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Zippning";
    }

    @Override
    public String getDescription() {
        return "Zippar något till samma plats, lägger till '.zip'";
    }

    @Override
    public Runnable getOperate(String filepath) {
        return () -> {
            operate(filepath);
        };
    }

    @Override
    public void operate(String filepath) {
        logg.info("filepath=" + filepath);

        File f = new File(filepath);
        if(!f.exists()) {
            String msg = "Filen finns inte";
            logg.info(msg + ". filepath=" + filepath);
            functionality.showError(msg);
            return;
        }
        if(!f.isDirectory()) {
            String msg = "Filen är inte en katalog";
            logg.info(msg + ". filepath=" + filepath);
            functionality.showError(msg);
            return;
        }
        String filepath_zip = filepath+".zip";
        File fz = new File(filepath_zip);
        if(fz.exists()) {
            String msg = "Det beräknade filen och platsen "+filepath_zip+" är upptagen.";
            logg.info(msg + ". filepath=" + filepath);
            functionality.showError(msg);
            return;
        }

        List<File> files = new ArrayList<>();
        files.add(f);

        try {
            zip(files, filepath_zip);
        }
        catch(Exception e) {
            String msg = "Fel vid uppzippningen";
            logg.error(msg + ". filepath=" + filepath, e);
            functionality.showError(msg);
            return;
        }
    }

    /**
     * A constants for buffer size used to read/write data
     */
    private static final int BUFFER_SIZE = 4096;
    /**
     * Compresses a list of files to a destination zip file
     * @param listFiles A collection of files and directories
     * @param destZipFile The path of the destination zip file
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void zip(List<File> listFiles, String destZipFile) throws FileNotFoundException,
            IOException {
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(destZipFile));
        for (File file : listFiles) {
            if (file.isDirectory()) {
                zipDirectory(file, file.getName(), zos);
            } else {
                zipFile(file, zos);
            }
        }
        zos.flush();
        zos.close();
    }
    /**
     * Compresses files represented in an array of paths
     * @param files a String array containing file paths
     * @param destZipFile The path of the destination zip file
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void zip(String[] files, String destZipFile) throws FileNotFoundException, IOException {
        List<File> listFiles = new ArrayList<File>();
        for (int i = 0; i < files.length; i++) {
            listFiles.add(new File(files[i]));
        }
        zip(listFiles, destZipFile);
    }
    /**
     * Adds a directory to the current zip output stream
     * @param folder the directory to be  added
     * @param parentFolder the path of parent directory
     * @param zos the current zip output stream
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static void zipDirectory(File folder, String parentFolder,
                              ZipOutputStream zos) throws FileNotFoundException, IOException {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                zipDirectory(file, parentFolder + "/" + file.getName(), zos);
                continue;
            }
            zos.putNextEntry(new ZipEntry(parentFolder + "/" + file.getName()));
            BufferedInputStream bis = new BufferedInputStream(
                    new FileInputStream(file));
            long bytesRead = 0;
            byte[] bytesIn = new byte[BUFFER_SIZE];
            int read = 0;
            while ((read = bis.read(bytesIn)) != -1) {
                zos.write(bytesIn, 0, read);
                bytesRead += read;
            }
            zos.closeEntry();
        }
    }
    /**
     * Adds a file to the current zip output stream
     * @param file the file to be added
     * @param zos the current zip output stream
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static void zipFile(File file, ZipOutputStream zos)
            throws FileNotFoundException, IOException {
        zos.putNextEntry(new ZipEntry(file.getName()));
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(
                file));
        long bytesRead = 0;
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = bis.read(bytesIn)) != -1) {
            zos.write(bytesIn, 0, read);
            bytesRead += read;
        }
        zos.closeEntry();
    }

}