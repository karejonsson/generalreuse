package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.GridRowDragger;

public class GridRowDraggerV8ish<T> extends GridRowDragger<T> {
    public GridRowDraggerV8ish(Grid grid) {
        super(grid);
    }
}
