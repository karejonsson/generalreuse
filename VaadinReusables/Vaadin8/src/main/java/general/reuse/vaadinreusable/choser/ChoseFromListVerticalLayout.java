package general.reuse.vaadinreusable.choser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import general.reuse.vaadinreusable.vaadin8ish.*;

public class ChoseFromListVerticalLayout<T> extends VerticalLayoutV8ish {
	
	private static final long serialVersionUID = -2594061578616805386L;

	public interface ToString<T> {
		String render(T t);
	}
	
	public static abstract class Searcher<T> {
		protected ChoiceMadeNotifier<T> choiceMadeNotifier = null;
		abstract public void search(List<T> list);
		final public void setChoiceMadeNotifier(ChoiceMadeNotifier<T> choiceMadeNotifier) {
			this.choiceMadeNotifier = choiceMadeNotifier;
		}
	}
	
	public interface ChoiceMadeNotifier<T> {
		void choiceMade(T choice);
	}

	private List<T> list = null;
	private ToString<T> renderer = null;
	private Searcher<T> searcher = null;
	private List<ComboBoxListItem<T>> cbobjects = null;
	private String title = null;
	
	private FilteringWrapper filterw = null;
	
	private Runnable onCancel = null;
	private Runnable onDone = null;
	
	private ComboBoxV8ish<ComboBoxListItem<T>> userChoice = null;

	public ChoseFromListVerticalLayout(List<T> list, ToString<T> renderer, String title) {
		this(list, renderer, null, title, true);
	}

	public ChoseFromListVerticalLayout(List<T> list, ToString<T> renderer, String title, boolean haveButtons) {
		this(list, renderer, null, title, haveButtons);
	}

	public ChoseFromListVerticalLayout(List<T> list, ToString<T> renderer, Searcher<T> searcher, String title) {
		this(list, renderer, searcher, title, true);
	}

	public ChoseFromListVerticalLayout(List<T> list, ToString<T> renderer, Searcher<T> searcher, String title, boolean haveButtons) {
		this.list = list;
		this.renderer = renderer;
		this.searcher = searcher;
		this.title = title;
		filterw = new FilteringWrapper();
		
		userChoice = new ComboBoxV8ish<ComboBoxListItem<T>>(title);
		userChoice.setSizeFull();

		cbobjects = new ArrayList<>();
		list.forEach(it -> cbobjects.add(new ComboBoxListItem<T>(it, renderer)));
		userChoice.setItems(filterw, cbobjects);
		
		addComponent(userChoice);
		
		HorizontalLayoutV8ish buttons = new HorizontalLayoutV8ish();
		if(haveButtons) {
			ButtonV8ish cancelBtn = new ButtonV8ish("Avbryt");
			cancelBtn.addListener(e -> { /*out = null;*/ if(onCancel != null) { onCancel.run(); } });
			buttons.addComponent(cancelBtn);
			ButtonV8ish doneBtn = new ButtonV8ish("Klar");
			doneBtn.addListener(e -> { /*out = getCurrentUserOption();*/ if(onDone != null) { onDone.run(); } });
			buttons.addComponent(doneBtn);
		}
		if(searcher != null) {
			ButtonV8ish searchBtn = new ButtonV8ish("Sök");
			searchBtn.addListener(e -> search());
			buttons.addComponent(searchBtn);
			// Lägg till ComboBox-valet för innehåller/börjar med här.
			buttons.addComponent(getCBForFilterChoice());
		}
		addComponent(buttons);
		if(searcher != null) {
			searcher.setChoiceMadeNotifier(it -> {
				if(it == null) {
					NotificationV8ish.show("Inget val gjordes", NotificationV8ish.TypeV8ish.ERROR_MESSAGE);
					return;
				}
				for(ComboBoxListItem<T> cbobject : cbobjects) {
					if(cbobject.getItem() == it) {
						userChoice.setSelectedItem(cbobject);
						return;
					}
				}
				NotificationV8ish.show("Valet finns inte bland valmöjligheterna", NotificationV8ish.TypeV8ish.ERROR_MESSAGE);
			});
		}
	}
	
	public void setEmptySelectionAllowed(boolean emptySelectionAllowed) {
		if(!emptySelectionAllowed) {
			userChoice.setSelectedItem(cbobjects.get(0));
			setChosenObject(list.get(0));
		}
		userChoice.setEmptySelectionAllowed(emptySelectionAllowed);
	}
	
	public void setTextInputAllowed(boolean textInputAllowed) {
		userChoice.setTextInputAllowed(textInputAllowed);
	}
	
	private void search() {
		searcher.search(list);
	}
	
	public void setOnCancel(Runnable onCancel) {
		this.onCancel = onCancel;
	}

	public void setOnDone(Runnable onDone) {
		this.onDone = onDone;
	}

	public T getChosenObject() {
		return getCurrentUserOption();
	}
	
	public void setChosenObject(T obj) {
		for(ComboBoxListItem<T> cbobject : cbobjects) {
			if(cbobject.getItem().equals(obj)) {
				userChoice.setSelectedItem(cbobject);
				return;
			}
		}
	}
	
	private T getCurrentUserOption() {
		ComboBoxListItem<T> out = null;
		try {
			Optional<ComboBoxListItem<T>> op = userChoice.getSelectedItem();
			out = op.get();
		}
		catch(Exception e) {
		}
		if(out == null) {
			return null;
		}
		return out.getItem();
	}

	private class ComboBoxListItem<T> {
		private T t = null;
		private ToString<T> ts = null;
		public ComboBoxListItem(T t, ToString<T> ts) {
			this.t = t;
			this.ts = ts;
		}
		public String toString() {
			return ts.render(t);
		}
		public T getItem() {
			return t;
		}
	}
	
	public void addSelectionListener(SingleSelectionListenerV8ish<ComboBoxListItem<T>> listener) {
		userChoice.addSelectionListener(listener);
	}
	
	private ComboBoxV8ish<ComboBoxListItem<ComboBoxV8ish.CaptionFilter>> getCBForFilterChoice() {//ComboBox<ComboBoxListItem<T>> userChoice) {
		ComboBoxV8ish<ComboBoxListItem<ComboBoxV8ish.CaptionFilter>> out = new ComboBoxV8ish<>();
		ComboBoxListItem<ComboBoxV8ish.CaptionFilter> startsWith = new ComboBoxListItem<>(
				(itCap, filTex) -> { return itCap.toLowerCase().startsWith(filTex.toLowerCase()); }, 
				(it) -> { return "Börjar med"; }
		);
		List<ComboBoxListItem<ComboBoxV8ish.CaptionFilter>> filters = new ArrayList<>();
		filters.add(startsWith);
		ComboBoxListItem<ComboBoxV8ish.CaptionFilter> contains = new ComboBoxListItem<>(
				(itCap, filTex) -> { return itCap.toLowerCase().contains(filTex.toLowerCase()); }, 
				(it) -> { return "Innehåller"; }
		);
		filters.add(contains);
		out.setItems(filters);
		out.setSelectedItem(startsWith);
		out.setTextInputAllowed(false);
		filterw.setFilter((itCap, filTex) -> { return itCap.toLowerCase().startsWith(filTex.toLowerCase()); });

		out.setEmptySelectionAllowed(false);
		out.addSelectionListener(e -> {
			try {
				Optional<ChoseFromListVerticalLayout<T>.ComboBoxListItem<ComboBoxV8ish.CaptionFilter>> op = e.getSelectedItem();
				ComboBoxListItem<ComboBoxV8ish.CaptionFilter> li = op.get();
				ComboBoxV8ish.CaptionFilter filter = li.getItem();
				filterw.setFilter(filter);
				// Här skall man programmatiskt göra så att det nyvalda filtret används.
				userChoice.getDataProvider().refreshAll();
			}
			catch(Exception ee) {}
		});
		return out;
	}
	
	public static class FilteringWrapper implements ComboBoxV8ish.CaptionFilter {
		
		private static final long serialVersionUID = 5334041488669173945L;
		private ComboBoxV8ish.CaptionFilter filter = null;
		
		public FilteringWrapper() {
			this(null);
		}
		
		public FilteringWrapper(ComboBoxV8ish.CaptionFilter filter) {
			this.filter = filter;
		}
		
		public void setFilter(ComboBoxV8ish.CaptionFilter filter) {
			this.filter = filter;
		}

		@Override
		public boolean test(String itemCaption, String filterText) {
			if(filter == null) {
				return true;
			}
			return filter.test(itemCaption, filterText);
		}
		
	}

	public void setSelectedItem(T item) {
		for(ComboBoxListItem<T> cbobject : cbobjects) {
			T t = cbobject.getItem();
			if(t.equals(item)) {
				userChoice.setSelectedItem(cbobject);
				return;
			}
		}
	}

	public void sortList() {
		Collections.sort(list, (g1, g2) -> {
			return eval(renderer.render(g1), renderer.render(g2));
		});
		Collections.sort(cbobjects, (g1, g2) -> {
			return eval(renderer.render(g1.getItem()), renderer.render(g2.getItem()));
		});
		userChoice.getDataProvider().refreshAll();
	}
	
	private static int eval(String s1, String s2) {
		if(s1 == null && s2 == null) {
			return 0;
		}
		if(s1 == null && s2 != null) {
			return 1;
		}
		if(s1 != null && s2 == null) {
			return -1;
		}
		return s1.compareTo(s2);
	}

	public void setInternalWidth(String width) {
		userChoice.setWidth(width);
	}

	public void setInternalHeight(String height) {
		userChoice.setHeight(height);
	}



}
