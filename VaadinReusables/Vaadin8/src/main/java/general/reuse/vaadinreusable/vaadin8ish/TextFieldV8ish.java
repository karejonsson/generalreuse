package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.TextField;

public class TextFieldV8ish extends TextField implements ComponentV8ish {
    public TextFieldV8ish() {
        super();
    }
    public TextFieldV8ish(String caption) {
        super(caption);
    }
}
