package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalSplitPanel;

public class PanelFactory {

	private static String cssname = null;
	private static String csshorizontalname = null;
	private static String cssverticalname = null;

	public static void setCssname(String cssname) {
		PanelFactory.cssname = cssname;
	}

	public static void setCssnameHorizontal(String cssname) {
		PanelFactory.csshorizontalname = cssname;
	}

	public static void setCssnameVerticalname(String cssname) {
		PanelFactory.cssverticalname = cssname;
	}


	public static Panel styles(Panel in) {
		if(cssname != null) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static HorizontalSplitPanel styles(HorizontalSplitPanel in) {
		if(csshorizontalname != null) {
			in.setPrimaryStyleName(csshorizontalname);
		}
		return in;
	}

	public static VerticalSplitPanel styles(VerticalSplitPanel in) {
		if(cssverticalname != null) {
			in.setPrimaryStyleName(cssverticalname);
		}
		return in;
	}

	public static Panel create() {
		return styles(new Panel());
	}

	public static Panel create(String caption) {
		return styles(new Panel(caption));
	}

	public static HorizontalSplitPanel createHorizontalSplit() {
		return styles(new HorizontalSplitPanel());
	}

	public static VerticalSplitPanel createVerticalSplit() {
		return styles(new VerticalSplitPanel());
	}

}
