package general.reuse.vaadinreusable.component;

import com.vaadin.ui.ComboBox;
import general.reuse.vaadinreusable.managedapp.factories.ComboBoxFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MultiActionComboBox implements Runnable {

    private Runnable runnable;
    private String caption;

    public MultiActionComboBox(Runnable runnable, String caption) {
        this.runnable = runnable;
        this.caption = caption;
    }

    public String toString() {
        return caption;
    }

    public void run() {
        runnable.run();
    }

    public static MultiActionComboBox getCurrentHandleRowListItem(ComboBox<MultiActionComboBox> handlingChoser) {
        MultiActionComboBox out = null;
        try {
            Optional<MultiActionComboBox> op = handlingChoser.getSelectedItem();
            out = op.get();
        }
        catch(Exception e) {
        }
        return out;
    }

    public static ComboBox<MultiActionComboBox> setup(String text, List<MultiActionComboBox> options) {
        final ComboBox<MultiActionComboBox> out = ComboBoxFactory.create();
        MultiActionComboBox defaultAtSelection = new MultiActionComboBox(() -> {}, text);
        options.add(defaultAtSelection);
        out.setItems(options);
        out.addSelectionListener(e -> {
            MultiActionComboBox handler = MultiActionComboBox.getCurrentHandleRowListItem(out);
            if(handler != null) {
                handler.run();
            }
            out.setSelectedItem(defaultAtSelection);
        });
        out.setEmptySelectionAllowed(false);
        out.setTextInputAllowed(false);
        out.setSelectedItem(defaultAtSelection);
        return out;
    }

}