package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.ui.DateField;

public class DateFieldFactory {

	private static String cssname = null;

	public static void setCssname(String cssname) {
		DateFieldFactory.cssname = cssname;
	}

	public static DateField styles(DateField in) {
		if(cssname != null) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static DateField create() {
		return styles(new DateField());
	}

	public static DateField create(String caption) {
		return styles(new DateField(caption));
	}

}
