package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.server.Resource;
import com.vaadin.ui.Image;

public class ImageFactory {

    private static String cssname = null;

    public static void setCssname(String cssname) {
        ImageFactory.cssname = cssname;
    }

    public static Image styles(Image in) {
        if(cssname != null) {
            in.setPrimaryStyleName(cssname);
        }
        return in;
    }

    public static Image create(Resource source) {
        return create(null, source);
    }

    public static Image create(String caption, Resource source) {
        return styles(new Image(caption, source));
    }

    public static Image create(String caption) {
        return styles(new Image(caption));
    }

}
