package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.ui.TabSheet;

public class TabSheetFactory {

	private static String cssname = null;

	public static void setCssname(String cssname) {
		TabSheetFactory.cssname = cssname;
	}

	public static TabSheet styles(TabSheet in) {
		if(cssname != null) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}
	
	public static TabSheet create() {
		return styles(new TabSheet());
	}

}
