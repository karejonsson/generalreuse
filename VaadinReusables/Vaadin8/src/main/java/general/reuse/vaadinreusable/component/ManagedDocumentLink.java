package general.reuse.vaadinreusable.component;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Upload;
import general.reuse.interfaces.Extractor;
import general.reuse.interfaces.RiskyInjector;
import general.reuse.interfaces.Treater;
import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.managedapp.texts.CaptionTexts;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ManagedDocumentLink<T> extends HorizontalLayout implements Upload.Receiver {

    private static Logger log = Logger.getLogger(ManagedDocumentLink.class);

    private BaseFunctionalityProvider functionality = null;
    private Extractor<T, byte[]> getFile = null;
    private Extractor<T, String> getFilename = null;
    private Extractor<T, String> getMimetype = null;
    private RiskyInjector<T, byte[]> setFile = null;
    private RiskyInjector<T, String> setFilename = null;
    private Treater<T, Boolean> saver = null;
    private Runnable externalRefresh = null;
    private RiskyInjector<T, String> setMimetype = null;
    private T obj = null;

    private DocumentLink dl = null;
    private Button clearBtn = null;

    private String filename = null;
    private String mimetype = null;
    private byte[] fileBytes = null;
    private ByteArrayOutputStream baos = null;

    public ManagedDocumentLink(
        BaseFunctionalityProvider functionality,
        Extractor<T, byte[]> getFile,
        Extractor<T, String> getFilename,
        Extractor<T, String> getMimetype,
        RiskyInjector<T, byte[]> setFile,
        RiskyInjector<T, String> setFilename,
        RiskyInjector<T, String> setMimetype,
        Treater<T, Boolean> saver,
        Runnable externalRefresh,
        T obj) {
        LayoutFactory.styles(this, false);
        this.functionality = functionality;
        this.getFile = getFile;
        this.getFilename = getFilename;
        this.getMimetype = getMimetype;
        this.setFile = setFile;
        this.setFilename = setFilename;
        this.saver = saver;
        this.externalRefresh = externalRefresh;
        this.setMimetype = setMimetype;
        this.obj = obj;

        if(getFilename.get(obj) != null) {
            dl = new DocumentLink(getFile, getFilename, getMimetype, externalRefresh, obj);
            addComponent(dl, 0);
            setComponentAlignment(dl, Alignment.MIDDLE_LEFT);
        }

        Upload ul = new Upload();
        //ul.setCaption("Vart tar det här vägen?");
        ul.setReceiver(this);
        ul.setButtonCaption(CaptionTexts.upload);
        //ul.setIcon(VaadinIcons.EDIT);
        //ul.setIcon(VaadinIcons.ARROW_UP);
        addComponent(ul);
        ul.setWidthUndefined();
        clearBtn = ButtonFactory.create(VaadinIcons.DEL);
        clearBtn.addClickListener(e -> clear());
        addComponent(clearBtn);
        setComponentAlignment(ul, Alignment.MIDDLE_RIGHT);
        //setWidth("200px");
        setWidthFull();
    }

    private void clear() {
        updateRow(null, null, null);
    }

    @Override
    public OutputStream receiveUpload(String filename, String mimetype) {
        log.info("filename="+filename+", mimetype="+mimetype);
        this.filename = filename;
        this.mimetype = mimetype;
        baos = new ByteArrayOutputStream() {
            public void close() throws IOException {
            super.close();
            log.info("KLOVSE");
            uploadSucceeded();
            }
        };
        return baos;
    }

    public void uploadSucceeded() {
        fileBytes = baos.toByteArray();
        updateRow(filename, mimetype, fileBytes);
    }

    public void updateRow(String filename, String mimetype, byte[] fileBytes) {
        log.info("fileBytes.length="+(fileBytes != null ? fileBytes.length : 0));
        try {
            setFilename.set(obj, filename);
        }
        catch(Exception e) {
            final String msg =
                "Kunde inte sätta filnamnet "+filename+"\n"+
                    "Signalerat fel: "+e.getMessage();
            log.error(msg, e);
            CommonNotification.showAssistive(functionality, msg);
            return;
        }
        try {
            setMimetype.set(obj, mimetype);
        }
        catch(Exception e) {
            final String msg =
                "Kunde inte sätta mimetypen "+mimetype+"\n"+
                    "Signalerat fel: "+e.getMessage();
            log.error(msg, e);
            CommonNotification.showAssistive(functionality, msg);
            return;
        }
        try {
            saver.treat(obj);
        }
        catch(Exception e) {
            final String msg = "Kunde inte spara uppladdad fil";
            log.error(msg, e);
            CommonNotification.showAssistive(functionality, msg);
            return;
        }
        try {
            setFile.set(obj, fileBytes);
        }
        catch(Exception e) {
            final String msg =
                "Kunde inte lagra filens innehåll om "+fileBytes.length+" oktetter.\n"+
                    "Signalerat fel: "+e.getMessage();
            log.error(msg, e);
            CommonNotification.showAssistive(functionality, msg);
            return;
        }
        if(getFilename.get(obj) != null) {
            if(dl == null) {
                dl = new DocumentLink(getFile, getFilename, getMimetype, externalRefresh, obj);
                addComponent(dl, 0);
            }
            else {
                dl.refresh();
            }
        }
        else {
            if(dl != null) {
                removeComponent(dl);
                dl = null;
            }
        }
        functionality.access(() -> {
            if(dl != null) {
                dl.refresh();
            }
            if(fileBytes != null) {
                CommonNotification.showAssistive(functionality, "Uppladdningen lyckades");
            }
            else {
                CommonNotification.showAssistive(functionality, "Rensningen lyckades");
            }
        });
    }

}