package general.reuse.vaadinreusable.anysql;

import com.vaadin.ui.*;
import general.reuse.database.pool.ManagedConnection;
import general.reuse.vaadinreusable.component.SimpleObjectCache;
import general.reuse.vaadinreusable.managedapp.factories.*;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

public class ManageWrittenSQL extends Panel {

	private static final long serialVersionUID = -1207753500145054017L;
	private static Logger logg = Logger.getLogger(ManageWrittenSQL.class);

	public interface VisualizeTable {
		void show(ResultSet rs);
	}

	private BaseFunctionalityProvider functionality = null;
	private String poolname = null;
	private VisualizeTable visualizer = null;
	private ManagedConnection connectionManager = null;
	private Runnable onBack = null;
	private CheckBox commitCB = null;

	private TextArea ta = null;

	public ManageWrittenSQL(BaseFunctionalityProvider functionality, Runnable onBack, ManagedConnection connectionManager, VisualizeTable visualizer) {
		PanelFactory.styles(this);
		this.functionality = functionality;
		this.visualizer = visualizer;
		this.connectionManager = connectionManager;
		this.onBack = onBack;
		poolname = functionality.getPoolname();
		setup();
	}

	private static final String flag = "skldsvme4m9ouasev4t890mq23v0589vmfnv";

	private void setup() {
		HorizontalSplitPanel hsp = PanelFactory.createHorizontalSplit();
		hsp.setSizeFull();
		hsp.setSplitPosition(60, Unit.PERCENTAGE);
		ta = TextAreaFactory.create("Skriv SQL här");
		ta.setSizeFull();
		hsp.setFirstComponent(ta);

		VerticalLayout buttons = LayoutFactory.createVertical(functionality.getHaveMargins());
		HorizontalLayout hl = LayoutFactory.createHorizontal(false);
		if(onBack != null) {
			Button backBtn = ButtonFactory.create(ButtonTexts.revert);
			backBtn.addClickListener(e -> onBack.run());
			hl.addComponent(backBtn);
		}
		final Map<Integer, String> em = (Map<Integer, String>) functionality.get(flag);
		hl.addComponent(new SimpleObjectCache<Integer, String>(
			() -> ta.getValue(),
			(s) -> ta.setValue(s),
			(em == null) ? null : () -> em,
			(m) -> functionality.put(flag, (Map<Integer, String>)m),
			new SimpleObjectCache.IntegerNexterCounting()
		));
		buttons.addComponent(hl);

		Button runSqlBtn = ButtonFactory.create(ButtonTexts.SQL_show_responstable);
		runSqlBtn.addClickListener(e -> runSQLShowTable());
		buttons.addComponent(runSqlBtn);

		Button runSqlGebneratingBtn = ButtonFactory.create(ButtonTexts.SQL_answer_generated);
		runSqlGebneratingBtn.addClickListener(e -> runSQLGenerating());
		buttons.addComponent(runSqlGebneratingBtn);

		Button runSqlSilentBtn = ButtonFactory.create(ButtonTexts.SQL_answer_boolean);
		runSqlSilentBtn.addClickListener(e -> runSQLSilent());
		buttons.addComponent(runSqlSilentBtn);

		Button runSQLExecuteUpdateBtn = ButtonFactory.create(ButtonTexts.SQL_answer_int);
		runSQLExecuteUpdateBtn.addClickListener(e -> runSQLExecuteUpdate());
		buttons.addComponent(runSQLExecuteUpdateBtn);

		Button runRowByRowSqlSilentBooleanBtn = ButtonFactory.create(ButtonTexts.SQL_linewise_boolean);
		runRowByRowSqlSilentBooleanBtn.addClickListener(e -> runSQLSilentBooleanRowByRow());
		buttons.addComponent(runRowByRowSqlSilentBooleanBtn);

		Button runRowByRowSqlSilentGeneratingBtn = ButtonFactory.create(ButtonTexts.SQL_linewise_generating);
		runRowByRowSqlSilentGeneratingBtn.addClickListener(e -> runSQLSilentGeneratingRowByRow());
		buttons.addComponent(runRowByRowSqlSilentGeneratingBtn);

		hl = LayoutFactory.createHorizontal(false);
		commitCB = CheckBoxFactory.create("Commit");
		hl.addComponent(commitCB);

		Button clearBtn = ButtonFactory.create(ButtonTexts.clear);
		clearBtn.addClickListener(e -> ta.setValue(""));
		hl.addComponent(clearBtn);

		Button resetBtn = ButtonFactory.create(ButtonTexts.reset);
		resetBtn.addClickListener(e -> connectionManager.reset(functionality.getPoolname()));
		hl.addComponent(resetBtn);

		buttons.addComponent(hl);

		hsp.setSecondComponent(buttons);
		setContent(hsp);
	}

	private boolean getCommitStatus() {
		boolean commit = commitCB.getValue();
		//commitCB.setValue(false);
		return commit;
	}

	private void runSQLGenerating() {
		String sql = ta.getValue();
		boolean commit = getCommitStatus() ;
		runSQLGenerating(sql, commit);
	}

	private void runSQLGenerating(String sql, final boolean commit) {
		functionality.updateUidname();
		//commitCB.setValue(false);
		PreparedStatement stmt = null;
		ResultSet rs = null;
		logg.info("SQL="+sql);
		Connection conn = connectionManager.getConnection(poolname);
		try {
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.executeUpdate();
			rs = stmt.getGeneratedKeys();
			if(commit) {
				conn.commit();
			}
			visualizer.show(rs);
		}
		catch(Exception e) {
			String msg = getMessage(e);//"FEL: "+e.getMessage();
			logg.error(msg, e);
			CommonNotification.showError(functionality, msg);
			return;
		}
		finally {
			if(stmt != null) {
				try { stmt.close(); } catch(Exception e) {}
			}
			if(rs != null) {
				try { rs.close(); } catch(Exception e) {}
			}
			if(conn != null) {
				connectionManager.returnConnection(conn, poolname);
			}
		}
	}

	private void runSQLExecuteUpdate() {
		String sql = ta.getValue();
		boolean commit = getCommitStatus() ;
		runSQLExecuteUpdate(sql, commit);
	}

	private void runSQLExecuteUpdate(String sql, final boolean commit) {
		functionality.updateUidname();
		//commitCB.setValue(false);
		Statement stmt = null;
		logg.info("SQL="+sql);
		Integer out = null;
		Connection conn = connectionManager.getConnection(poolname);
		try {
			stmt = conn.createStatement();
			out = stmt.executeUpdate(sql);
			if(commit) {
				conn.commit();
			}
			visualize(out);
		}
		catch(Exception e) {
			String msg = getMessage(e);//"FEL: "+e.getMessage();
			logg.error(msg, e);
			CommonNotification.showError(functionality, msg);
			return;
		}
		finally {
			if(stmt != null) {
				try { stmt.close(); } catch(Exception e) {}
			}
			if(conn != null) {
				connectionManager.returnConnection(conn, poolname);
			}
		}
	}

	private void visualize(Object out) {
		if(out instanceof ResultSet) {
			visualizer.show((ResultSet) out);
			return;
		}
		CommonNotification.showAssistive(functionality, "Resultat: "+out);
	}

	private void runSQLShowTable() {
		functionality.updateUidname();
		boolean commit = getCommitStatus() ;
		//commitCB.setValue(false);
		String sql = ta.getValue();
		PreparedStatement stmt = null;
		ResultSet rs = null;
        logg.info("SQL="+sql);
        Connection conn = connectionManager.getConnection(poolname);
        try {
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            if(commit) {
            	conn.commit();
            }
            visualizer.show(rs);
        }
        catch(Exception e) {
        	String msg = getMessage(e);//"FEL: "+e.getMessage();
        	logg.error(msg, e);
        	CommonNotification.showError(functionality, msg);
        	return;
        }
        finally {
            if(stmt != null) {
            	try { stmt.close(); } catch(Exception e) {}
            }
            if(rs != null) {
            	try { rs.close(); } catch(Exception e) {}
            }
        	if(conn != null) {
        		connectionManager.returnConnection(conn, poolname);
        	}
        }
	}

	private void runSQLSilent() {
		functionality.updateUidname();
		String sql = ta.getValue();
		boolean commit = getCommitStatus() ;
		try {
			boolean outcome = runOneSQLSilent(sql, commit) ;
	        CommonNotification.showAssistive(functionality, "Svaret blev "+outcome);
		}
		catch(Exception e) {
			logg.error("Fel" ,e);
	        CommonNotification.showError(functionality, e.getMessage());
		}
        visualizer.show(null);
	}

	private boolean runOneSQLSilent(String sql, boolean commit) throws Exception {
		functionality.updateUidname();
		PreparedStatement stmt = null;
		boolean outcome = false;
        logg.info("SQL="+sql);
        Connection conn = connectionManager.getConnection(poolname);
        try {
            stmt = conn.prepareStatement(sql);
            outcome = stmt.execute();
            if(commit) {
            	conn.commit();
            }
        }
        catch(Exception e) {
			String msg = getMessage(e);//"FEL: "+e.getMessage();
        	logg.error(msg, e);
        	throw new Exception(msg, e);
        }
        finally {
            if(stmt != null) {
            	try { stmt.close(); } catch(Exception e) {}
            }
        	if(conn != null) {
        		connectionManager.returnConnection(conn, poolname);
        	}
        }
        visualize(outcome);
        return outcome;
	}

	private String getMessage(Exception e) {
		return e.getMessage();
	}

	private void runSQLSilentBooleanRowByRow() {
		functionality.updateUidname();
		final boolean commit = getCommitStatus() ;
		//commitCB.setValue(false);
		String sql = ta.getValue();
		String[] lines = sql.split("\n");
		int ctr = 0;
		int ctr_true = 0;
		int ctr_false = 0;
		for(String sqlline : lines) {
			if(sqlline == null) {
				continue;
			}
			if(sqlline.trim().length() == 0) {
				continue;
			}
			ctr++;
			sqlline = sqlline.replaceAll("\r", "");
			try {
				boolean outcome = runOneSQLSilent(sqlline, commit);
				if(outcome) {
					ctr_true++;
				}
				if(!outcome) {
					ctr_false++;
				}
			}
			catch(Exception e) {
				ctr_false++;
			}
		}
        CommonNotification.showAssistive(functionality, ""+ctr+" sql kördes. "+ctr_true+" gick bra och "+ctr_false+" misslyckades");
	}

	private void runSQLSilentGeneratingRowByRow() {
		functionality.updateUidname();
		final boolean commit = getCommitStatus() ;
		//commitCB.setValue(false);
		String sql = ta.getValue();
		String[] lines = sql.split("\n");
		int ctr = 0;
		int ctr_true = 0;
		int ctr_false = 0;
		for(String sqlline : lines) {
			if(sqlline == null) {
				continue;
			}
			if(sqlline.trim().length() == 0) {
				continue;
			}
			ctr++;
			sqlline = sqlline.replaceAll("\r", "");
			try {
				runSQLGenerating(sqlline, commit);
				ctr_true++;
			}
			catch(Exception e) {
				ctr_false++;
			}
		}
        CommonNotification.showAssistive(functionality, ""+ctr+" sql kördes. "+ctr_true+" gick bra och "+ctr_false+" misslyckades");
	}

}
