package general.reuse.vaadinreusable.serverbrowse;

import com.vaadin.ui.TextField;
import general.reuse.vaadinreusable.vaadin8ish.NotificationV8ish;
import general.reuse.vaadinreusable.vaadin8ish.TextFieldV8ish;

import java.io.File;
import java.util.Date;

public class FilesystemCallbackDefault implements FilesystemCallback {

    private TextField tf = null;

    public FilesystemCallbackDefault(TextField tf) {
        this.tf = tf;
    }

    public String describeFile(File file) {
        Date date = new Date(file.lastModified());
        long size = file.length();
        String unit = "";
        if (size > 1024*1024*1024) {
            size = size / (1024*1024*1024);
            unit = "GB";
        }
        else if (size > 1024*1024) {
            size = size / (1024*1024);
            unit = "MB";
        }
        else if (size > 1024) {
            size = size / (1024);
            unit = "KB";
        } else {
            unit = "B";
        }
        return file.getName()+", "+date+", "+size+ " "+unit;
    }

    public String describeDirectory(File file) {
        if(file == null) {
            return "<NULL>";
        }
        String[] children = file.list();
        if(children == null) {
            return "Inga underliggande";
        }
        if(children.length == 0) {
            return "Inga underliggande filer";
        }
        return ""+children.length+" files";
    }

    public void onClickFile(File file) {
        onClickDirectory(file);
        Date date = new Date(file.lastModified());
        NotificationV8ish.show(file.getPath()+", "+date+", "+file.length());
    }

    public void onClickDirectory(File file) {
        try {
            if(tf != null) {
                tf.setValue(file.getCanonicalPath());
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

}
