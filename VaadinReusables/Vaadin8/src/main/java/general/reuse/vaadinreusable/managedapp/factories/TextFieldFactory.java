package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.ui.TextField;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

public class TextFieldFactory {

	private static String cssname = null;

	public static void setCssname(String cssname) {
		TextFieldFactory.cssname = cssname;
	}
	
	public static TextField styles(TextField in) {
		if(cssname != null) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public interface Sanitizer {
		String sanitize(String in);
	}

	private static Sanitizer current = s -> {
		if(s == null) {
			return null;
		}
		Document doc = Jsoup.parse(s);
		String text = doc.text();
		if(text.toLowerCase().contains("jndi:")) {
			text = null;
		}
		return text;
	};

	public static void setSanitizer(Sanitizer sanitizer) {
		current = sanitizer;
	}

	public static String sanitize(String in) {
		if(current == null) {
			return in;
		}
		return current.sanitize(in);
	}

	public static TextField create(String caption) {
		TextField out = new TextField(caption) {
			public String getValue() {
				return sanitize(super.getValue());
			}
		};
		return styles(out);
	}

	public static TextField create(String caption, final Sanitizer sanitizer) {
		TextField out = new TextField(caption) {
			public String getValue() {
				String value = super.getValue();
				if(sanitizer == null) {
					return value;
				}
				return sanitizer.sanitize(value);
			}
		};
		return styles(out);
	}

	public static TextField create() {
		TextField out = new TextField() {
			public String getValue() {
				return sanitize(super.getValue());
			}
		};
		return styles(out);
	}

	public static TextField create(final Sanitizer sanitizer) {
		TextField out = new TextField() {
			public String getValue() {
				String value = super.getValue();
				if(sanitizer == null) {
					return value;
				}
				return sanitizer.sanitize(value);
			}
		};
		return styles(out);
	}

	public static void main(String[] args) {
		System.out.println(sanitize("1 < 2"));
		System.out.println(sanitize("<bold>Hej</bold>"));
		System.out.println(sanitize("${jndi:ldap://hax.local:1389/123}"));
		System.out.println(sanitize("<Nisse>"));
		System.out.println(sanitize("1 < 2 < 3 <= 4"));
		System.out.println(sanitize("1 < 3 > 2"));
		System.out.println(sanitize("1 <3> 2"));
		System.out.println(sanitize("1 <X> 2"));
		System.out.println(sanitize("1 <val> 2"));
		System.out.println(sanitize("1234567-8"));
		System.out.println(sanitize("ab <3a> cd"));
	}

}
