package general.reuse.vaadinreusable.component;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import general.reuse.interfaces.Getter;
import general.reuse.interfaces.Setter;
import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.factories.ComboBoxFactory;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class SimpleObjectCache<I, T> extends HorizontalLayout {

    private Getter<T> getter = null;
    private Setter<T> setter = null;
    private Getter<Map<I, T>> retriever = null;
    private Setter<Map<I, T>> storer = null;
    private IndexGenerator nexter = null;

    public interface IndexGenerator<I, T> {
        I next(T t);
        void consider(Collection<I> c);
    }

    public SimpleObjectCache(Getter<T> getter, Setter<T> setter, IndexGenerator<I, T> next) {
        this(getter, setter, null, null, next);
    }

    public SimpleObjectCache(Getter<T> getter, Setter<T> setter, Getter<Map<I, T>> retriever, Setter<Map<I, T>> storer, IndexGenerator<I, T> next) {
        LayoutFactory.styles(this, false);
        this.getter = getter;
        this.setter = setter;
        this.retriever = retriever;
        this.storer = storer;
        this.nexter = next;
        setup();
    }

    private ComboBox<I> cb = null;

    private void setup() {
        if(getter != null) {
            Button getBtn = ButtonFactory.create(VaadinIcons.ARROW_RIGHT);
            getBtn.addClickListener(e -> take());
            addComponent(getBtn);
        }

        if(setter != null) {
            Button setBtn = ButtonFactory.create(VaadinIcons.ARROW_LEFT);
            setBtn.addClickListener(e -> back());
            addComponent(setBtn);
        }

        cb = ComboBoxFactory.create();
        addComponent(cb);

        if(retriever != null) {
            Button retrieveBtn = ButtonFactory.create(VaadinIcons.ARROW_DOWN);
            retrieveBtn.addClickListener(e -> retrieve());
            addComponent(retrieveBtn);
        }

        if(storer != null) {
            Button storeBtn = ButtonFactory.create(VaadinIcons.ARROW_UP);
            storeBtn.addClickListener(ev -> { try { storer.set(m); } catch(Exception e) {}});
            addComponent(storeBtn);
        }

        Button deleteBtn = ButtonFactory.create(VaadinIcons.TRASH);
        deleteBtn.addClickListener(e -> delete());
        addComponent(deleteBtn);
    }

    private void retrieve() {
        Map<I, T> mtmp = retriever.get();
        m.clear();
        for(I k : mtmp.keySet()) {
            m.put(k, mtmp.get(k));
        }
        cb.setItems(m.keySet());
        nexter.consider(mtmp.keySet());
    }

    private void delete() {
        m.remove(cb.getValue());
        cb.setItems(m.keySet());
        cb.setValue(null);
    }

    private void back() {
        I idx = cb.getValue();
        if(idx != null) {
            try { setter.set(m.get(idx)); } catch(Exception e) {}
        }
    }

    private Map<I, T> m = new HashMap<>();

    private void take() {
        T t = getter.get();
        m.put((I) nexter.next(t), t);
        cb.setItems(m.keySet());
    }

    public static class IntegerNexterCounting<Void> implements IndexGenerator<Integer, Object> {

        private int ctr = 0;

        @Override
        public Integer next(Object t) {
            return ++ctr;
        }

        @Override
        public void consider(Collection<Integer> c) {
            if(c == null) {
                ctr = 0;
                return;
            }
            if(c.size() == 0) {
                ctr = 0;
                return;
            }
            Integer candidate = null;
            for(Integer i : c) {
                if(candidate == null) {
                    candidate = i;
                    continue;
                }
                if(i > candidate) {
                    candidate = i;
                }
            }
            ctr = candidate;
        }

    };

    /*
    public static class StringNexterStarting implements IndexGenerator<String, String> {

        private int len = 0;

        public StringNexterStarting(int len) {
            this.len = len;
        }

        @Override
        public String next(String t) {
            String trimmed = t.trim();
            return trimmed.substring(0, Math.min(len, trimmed.length()));
        }

        @Override
        public void consider(Collection<String> c) {
        }

    };
     */

}
