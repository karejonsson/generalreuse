package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.CheckBox;

public class CheckBoxV8ish extends CheckBox {

    public CheckBoxV8ish() {
        super();
    }

    public CheckBoxV8ish(String caption) {
        super(caption);
    }

}
