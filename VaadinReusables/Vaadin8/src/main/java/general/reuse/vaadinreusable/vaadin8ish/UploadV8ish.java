package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.Upload;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import org.apache.log4j.Logger;

import java.io.*;

public class UploadV8ish extends Upload {

    private static Logger log = Logger.getLogger(UploadV8ish.class);

    public UploadV8ish(String fil_att_ladda_upp, FilepathUploader receiver) {
        super(fil_att_ladda_upp, receiver);
    }

    public static class FilepathUploader implements UploadV8ish.Receiver, UploadV8ish.SucceededListener {

        private String filepath = null;
        private String filename = null;
        private byte[] fileBytes = null;
        private ByteArrayOutputStream baos = null;

        public FilepathUploader(String filepath) {
            this.filepath = filepath;
        }

        @Override
        public OutputStream receiveUpload(String filename, String mimeType) {
            log.info("filename="+filename+", mimeType="+mimeType);
            this.filename = filename;
            baos = new ByteArrayOutputStream() {
                public void close() throws IOException {
                    super.close();
                    log.info("KLOVSE");
                }
             };
            return baos;
        }

        @Override
        public void uploadSucceeded(UploadV8ish.SucceededEvent event) {
            fileBytes = baos.toByteArray();
            try {
                FileOutputStream fos = new FileOutputStream(new File(new File(filepath), filename));
                fos.write(fileBytes);
                fos.flush();
                fos.close();
            }
            catch(Exception e) {
                String msg = "Uppladdningen misslyckades. Underliggande felmeddelande\n"+e.getMessage();
                log.error(msg + ". filepath=" + filepath, e);
                CommonNotification.showError(msg);
                return;
            }
            CommonNotification.showAssistive("Uppladdningen lyckades");
        }
    };

}
