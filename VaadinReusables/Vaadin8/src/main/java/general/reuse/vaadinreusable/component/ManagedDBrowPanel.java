package general.reuse.vaadinreusable.component;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import general.reuse.interfaces.*;
import general.reuse.vaadinreusable.managedapp.factories.*;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.functionality.CommonRunnable;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;
import general.reuse.vaadinreusable.managedapp.texts.CaptionTexts;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.Callable;

abstract public class ManagedDBrowPanel<T> extends Panel {

    private static Logger log = Logger.getLogger(ManagedDBrowPanel.class);

    protected BaseFunctionalityProvider functionality = null;
    protected CommonRunnable onBack = null;
    protected CommonRunnable onStart = null;
    private boolean canCreate = true;
    private Comparator<T> comparator = null;
    protected Grid<T> grid = null;
    private List<T> items = null;
    //private Treater<T, Boolean> canEdit = (t) -> true;
    //private Treater<T, Boolean> canDelete = (t) -> true;

    private String dateFormat = null;
    private String handling = "Hantera";

    public ManagedDBrowPanel(BaseFunctionalityProvider functionality, CommonRunnable onBack, CommonRunnable onStart, boolean canCreate, Comparator<T> comparator) {
        this.functionality = functionality;
        this.onBack = onBack;
        this.onStart = onStart;
        this.canCreate = canCreate;
        this.comparator = comparator;
        if(onBack == null) {
            log.info("Ingen återknapp");
        }
    }

    public void setHandlingCaption(String handling) {
        this.handling = handling;
    }

    public static class GeneralRowHandler<T> {
        public String caption;
        Extractor<T, Boolean> allow;
        Setter<T> handler;
        public GeneralRowHandler(String caption, Setter<T> handler) {
            this(caption, m -> true, handler);
        }
        public GeneralRowHandler(String caption, Extractor<T, Boolean> allow, Setter<T> handler) {
            this.caption = caption;
            this.allow = allow;
            this.handler = handler;
        }
    }

    public static class GeneralTableHandler {
        public String caption;
        Runnable handler;
        public GeneralTableHandler(String caption, Runnable handler) {
            this.caption = caption;
            this.handler = handler;
        }
    }

    private ArrayList<GeneralRowHandler<T>> rowHandlers = new ArrayList<>();
    private ArrayList<GeneralTableHandler> tableHandlers = new ArrayList<>();

    public void addRowHandler(String caption, Setter<T> handler) {
        rowHandlers.add(new GeneralRowHandler(caption, handler));
    }

    public void addRowHandler(String caption, Extractor<T, Boolean> allow, Setter<T> handler) {
        rowHandlers.add(new GeneralRowHandler(caption, allow, handler));
    }

    public void addTableHandler(String caption, Runnable handler) {
        tableHandlers.add(new GeneralTableHandler(caption, handler));
    }

    protected void addStringColumn(Extractor<T, String> get, String caption) {
        grid.addComponentColumn(row -> {
            return LabelFactory.create(get.get(row));
        }).setComparator((r1, r2) -> {
            String n1 = get.get(r1);
            String n2 = get.get(r2);
            if(n1 == null && n2 == null) {
                return 0;
            }
            if(n1 == null) {
                return -1;
            }
            if(n2 == null) {
                return 1;
            }
            return n1.compareTo(n2);
        }).setCaption(caption);
    }

    protected void addEditableStringColumn(Extractor<T, String> get, SafeInjector<T, String> set, int maxlen, String caption) {
        grid.addComponentColumn(row -> {
            HorizontalLayout out = LayoutFactory.createHorizontal(functionality.getHaveMargins());
            out.setWidthFull();
            Label text = LabelFactory.create(get.get(row));
            out.addComponent(text);
            out.setComponentAlignment(text, Alignment.MIDDLE_LEFT);
            Button editBtn = ButtonFactory.create(VaadinIcons.EDIT);
            out.addComponent(editBtn);
            out.setComponentAlignment(editBtn, Alignment.MIDDLE_RIGHT);
            editBtn.addClickListener(e -> {
                final EnterLongText elt = new EnterLongText(
                    "Redigera "+ caption,
                    null,
                    CaptionTexts.cancel,
                    CaptionTexts.save,
                    get.get(row),
                    maxlen,
                    EnterLongText.inner,
                    EnterLongText.outer);
                elt.setOnDoneOk(() -> {
                    set.set(row, elt.getText());
                    grid.getDataProvider().refreshAll();
                });
                functionality.addWindow(elt, "Redigerad användares notering");
            });

            return out;
        }).setComparator((r1, r2) -> {
            String n1 = get.get(r1);
            String n2 = get.get(r2);
            if(n1 == null && n2 == null) {
                return 0;
            }
            if(n1 == null) {
                return -1;
            }
            if(n2 == null) {
                return 1;
            }
            return n1.compareTo(n2);
        }).setCaption(caption);
    }

    protected void addPersistingChoiceColumn(Extractor<T, String> ext, RiskyInjector<T, String> inj, Collection<String> alternatives, String caption, Treater<T, Boolean> onPersist) {
        grid.addComponentColumn(row -> {
            final List<Callable> inserters = new ArrayList<>();
            Getter<String> getter = () -> ext.get(row);
            Setter<String> setter = s -> inj.set(row, s);
            ComboBox<String> out = AddToForm.createChoiceField(inserters, null, getter, setter, alternatives, null, null);
            out.addValueChangeListener(ev -> {
                try {
                    for(Callable c : inserters) {
                        c.call();
                    }
                    if(!onPersist.treat(row)) {
                        log.error("Kunde inte persistera. row="+row);
                        CommonNotification.showError(functionality, "Kunde inte skriva ner i DB");
                        return;
                    }
                }
                catch(Exception e) {
                    log.error("Problem: Kunde inte persistera. row="+row);
                    CommonNotification.showError(functionality, "Kunde inte skriva ner i DB");
                    return;
                }
            });
            HorizontalLayout hl = LayoutFactory.createHorizontal(functionality.getHaveMargins());
            hl.addComponent(out);
            return hl;
        }).setComparator((r1, r2) -> {
            String n1 = ext.get(r1);
            String n2 = ext.get(r2);
            if(n1 == null && n2 == null) {
                return 0;
            }
            if(n1 == null) {
                return -1;
            }
            if(n2 == null) {
                return 1;
            }
            return n1.compareTo(n2);
        }).setCaption(caption);
    }

    protected void addDateColumn(Extractor<T, Date> get, SimpleDateFormat sdf, String caption) {
        grid.addComponentColumn(row -> {
            Date d = get.get(row);
            if(d == null) {
                return LabelFactory.create(CaptionTexts.missing);
            }
            return LabelFactory.create(sdf.format(d));
        }).setComparator((r1, r2) -> {
            Date d1 = get.get(r1);
            Date d2 = get.get(r2);
            if(d1 == null && d2 == null) {
                return 0;
            }
            if(d1 == null) {
                return -1;
            }
            if(d2 == null) {
                return 1;
            }
            return (int) (d1.getTime() - d2.getTime());
        }).setCaption(caption);
    }

    protected void addEditableDateColumn(Extractor<T, Date> get, SafeInjector<T, Date> set, String dateFormat, String caption) {
        grid.addComponentColumn(row -> {
            DateField out = DateFieldFactory.create();
            out.setDateFormat(dateFormat);
            Date initialDate = get.get(row);
            LocalDate date = initialDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            out.setValue(date);

            out.addValueChangeListener(e -> {
                Date newDate = Date.from(out.getValue().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
                set.set(row, newDate);
            });
            return out;
        }).setComparator((r1, r2) -> {
            long n1 = get.get(r1).getTime();
            long n2 = get.get(r2).getTime();
            if(n1 > n2) {
                return 1;
            }
            if(n1 < n2) {
                return -1;
            }
            return 0;
        }).setCaption(caption);
    }

    protected void addDownloadColumn(Extractor<T, byte[]> getFile, Extractor<T, String> getFilename, Extractor<T, String> getMimetype, String caption) {
        grid.addComponentColumn(row -> {
            return new DocumentLink(getFile, getFilename, getMimetype, () -> grid.getDataProvider().refreshAll(), row);
        }).setCaption(caption);
    }

    protected void addUpAndDownloadColumn(Extractor<T, byte[]> getFile, Extractor<T, String> getFilename, Extractor<T, String> getMimetype, RiskyInjector<T, byte[]> setFile, RiskyInjector<T, String> setFilename, RiskyInjector<T, String> setMimetype, Treater<T, Boolean> saver, String caption) {
        grid.addComponentColumn(row -> {
            return new ManagedDocumentLink<T>(functionality, getFile, getFilename, getMimetype, setFile, setFilename, setMimetype, saver, () -> grid.getDataProvider().refreshAll(), row);
        }).setCaption(caption);
    }

    protected void addBooleanColumn(Extractor<T, Boolean> get, String caption) {
        grid.addComponentColumn(row -> {
            CheckBox cb = CheckBoxFactory.create(caption);
            Boolean b = get.get(row);
            return LabelFactory.create(b == null ? "Saknas" : b.toString());
        }).setCaption(caption);
    }

    abstract protected EditDynamicForm setupForm(T row);
    abstract protected void setupColumns();
    abstract protected List<T> getDbRows();
    abstract protected void editRow(T row);
    abstract protected boolean deleteRow(T row) throws Exception;
    abstract protected void newObject();

    protected boolean deleteRowWithCommunication(T row) {
        try {
            if(!deleteRow(row)) {
                final String msg = "DB-raden kunde inte raderas";
                log.error(msg+", row="+row);
                CommonNotification.showError(functionality, msg);
                return false;
            }
        }
        catch(Exception ex) {
            final String msg = "DB-raden kunde inte raderas";
            log.error("Problem: "+msg+", row="+row, ex);
            CommonNotification.showError(functionality, msg);
            return false;
        }
        items.remove(row);
        grid.getDataProvider().refreshAll();
        return true;
    }

    protected void setupComponents() {
        VerticalLayout vl = LayoutFactory.createVertical(functionality.getHaveMargins());
        vl.setSizeFull();
        grid = GridFactory.create();

        setupColumns();

        grid.addComponentColumn(row -> {
            if (rowHandlers.size() > 2) {
                List<MultiActionComboBox> options = new ArrayList<>();
                boolean optionsAdded = false;
                final ComboBox<MultiActionComboBox> handlingChoser = MultiActionComboBox.setup(CaptionTexts.chose, options);
                for (GeneralRowHandler<T> handler : rowHandlers) {
                    if(handler.allow.get(row)) {
                        MultiActionComboBox selection = new MultiActionComboBox(
                            () -> {
                                try {
                                    handler.handler.set(row);
                                } catch (Exception e) {
                                    //e.printStackTrace();
                                    log.error("handler=" + handler, e);
                                }
                            },
                            handler.caption);
                        options.add(selection);
                        optionsAdded = true;
                    }
                }
                if (optionsAdded) {
                    return handlingChoser;
                }
            }
            else {
                boolean optionsAdded = false;
                HorizontalLayout handlingChoser = LayoutFactory.createHorizontal(functionality.getHaveMargins());
                for (GeneralRowHandler<T> handler : rowHandlers) {
                    if(handler.allow.get(row)) {
                        Button handlerBtn = ButtonFactory.create(handler.caption);
                        handlerBtn.addClickListener(
                            e -> {
                                try {
                                    handler.handler.set(row);
                                } catch (Exception ee) {
                                    //e.printStackTrace();
                                    log.error("handler=" + handler, ee);
                                }
                            }
                        );
                        handlingChoser.addComponent(handlerBtn);
                        optionsAdded = true;
                    }
                }
                if (optionsAdded) {
                    return handlingChoser;
                }
            }
            return LabelFactory.create("");
        }).setCaption(handling);
        grid.setSizeFull();
        vl.addComponent(grid);
        vl.setExpandRatio(grid, 1.0f);
        HorizontalLayout hl = LayoutFactory.createHorizontal(functionality.getHaveMargins());

        if(onBack != null) {
            Button backBtn = ButtonFactory.create(ButtonTexts.revert);
            backBtn.addClickListener(e -> onBack.run());
            hl.addComponent(backBtn);
        }

        if(onStart != null) {
            Button startBtn = ButtonFactory.create(ButtonTexts.start);
            startBtn.addClickListener(e -> onStart.run());
            hl.addComponent(startBtn);

        }

        if(functionality.getAdminFunctionality() != null) {
            Button adminBtn = ButtonFactory.create(ButtonTexts.admin);
            adminBtn.addClickListener(e -> functionality.getAdminFunctionality().openAdmin(onBack));
            hl.addComponent(adminBtn);
        }

        if(canCreate) {
            Button newManagedOrderBtn = ButtonFactory.create(CaptionTexts.create);
            newManagedOrderBtn.addClickListener(e -> newObject());
            hl.addComponent(newManagedOrderBtn);
        }

        for(GeneralTableHandler th : tableHandlers) {
            Button handlerBtn = ButtonFactory.create(th.caption);
            handlerBtn.addClickListener(e -> th.handler.run());
            hl.addComponent(handlerBtn);
        }

        if(hl.getComponentCount() > 0) {
            vl.addComponent(hl);
        }

        setContent(vl);
    }

    public void refreshRowsFromDatabase() {
        items = getDbRows();
        if(comparator != null) {
            Collections.sort(items, comparator);
        }
        grid.setItems(items);
    }

    protected void addRow(T r) {
        items.add(r);
        if(comparator != null) {
            Collections.sort(items, comparator);
        }
    }

    protected boolean removeRow(T r) {
        for(T item : items) {
            if(item.equals(r)) {
                return items.remove(item);
            }
        }
        return false;
    }

    protected void refreshGrid() {
        //Sort.orders(items);
        grid.getDataProvider().refreshAll();
    }

}
