package general.reuse.vaadinreusable.anysql;

import com.vaadin.ui.Grid;
import com.vaadin.ui.Panel;
import general.reuse.vaadinreusable.managedapp.factories.GridFactory;
import general.reuse.vaadinreusable.managedapp.factories.LabelFactory;
import general.reuse.vaadinreusable.managedapp.factories.PanelFactory;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShowResultSet extends Panel {
	
	private static final long serialVersionUID = 6845747350676606029L;
	private static Logger logg = Logger.getLogger(ShowResultSet.class);
	
	private BaseFunctionalityProvider functionality = null;
	
	public ShowResultSet(BaseFunctionalityProvider functionality) {
		PanelFactory.styles(this);
		this.functionality = functionality;
	}

	public void show(ResultSet rs) {
		functionality.updateUidname();
		if(rs == null) {
			setContent(LabelFactory.create(""));
			return;
		}
		Grid<MyRow> grid = GridFactory.create();
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			int colcount = rsmd.getColumnCount();
			List<String> names = new ArrayList<>();
			for(int i = 1 ; i <= colcount ; i++) {
				final String colname = rsmd.getColumnName(i);
				names.add(colname);
				grid.addComponentColumn(row -> {
					String s = row.getObject(colname);
					if(s == null) {
						s = "<NULL>";
					}
					return LabelFactory.create(s);
				}).setCaption(colname);
			}
			List<MyRow> rows = new ArrayList<>();
			while(rs.next()) {
				MyRow row = new MyRow();
				for(String colname : names) {
					row.add(colname, rs.getObject(colname));
				}
				rows.add(row);
			}
			grid.setItems(rows);
		} catch (SQLException e) {
			logg.error("Fel", e);
		}
		grid.setSizeFull();
		setContent(grid);
	}
	
	public class MyRow {
		
		private Map<String, Object> m = new HashMap<>();
		
		public String getObject(String colname) {
			Object o = m.get(colname);
			if(o == null) {
				return null;
			}
			return o.toString();
		}

		public void add(String colname, Object object) {
			m.put(colname,  object);
		}
	}
	
}
