package general.reuse.vaadinreusable.component;

import com.vaadin.ui.*;

public class EnterLongText extends Window {

	private static final long serialVersionUID = -5028474051361059691L;
	public final static double outer = 0.8;
	//public final static double outer_W = outer;
	//public final static double outer_H = outer;
	public final static double inner = 0.68;
	//public final static double inner_W = inner;
	//public final static double inner_H = inner;

	private UI ui = null;
	private TextArea ta = null;
	private Runnable onDoneCancel = null;
	private Runnable onDoneOk = null;
	
	private double internal_inner;
	private double internal_outer;

	public EnterLongText(String text, Runnable onDoneOk) {
		this((UI)null, text, onDoneOk);
	}

	public EnterLongText(UI ui, String text, Runnable onDoneOk) {
		this(ui, "", "Specifikation", "Avbryt", "Spara", text, null, null, onDoneOk, inner, outer);
	}

	public EnterLongText(String frame, String text, Runnable onDoneOk) {
		this(null, frame, text, onDoneOk);
	}

	public EnterLongText(UI ui, String frame, String text, Runnable onDoneOk) {
		this(ui, frame, "Specifikation", "Avbryt", "Spara", text, null, null, onDoneOk, inner, outer);
	}

	public EnterLongText(String frame, String componentText, String cancelButtonText, String okButtonText, String text) {
		this((UI)null, frame, componentText, cancelButtonText, okButtonText, text, null, null, null, inner, outer);
	}

	public EnterLongText(UI ui, String frame, String componentText, String cancelButtonText, String okButtonText, String text, Integer maxLength) {
		this(ui, frame, componentText, cancelButtonText, okButtonText, text, maxLength, null, null, inner, outer);
	}

	public EnterLongText(String frame, String componentText, String cancelButtonText, String okButtonText, String text, Integer maxLength, double internal_inner, double internal_outer) {
		this((UI)null, frame, componentText, cancelButtonText, okButtonText, text, maxLength, null, null, internal_inner, internal_outer);
	}

	public EnterLongText(UI ui, String frame, String componentText, String cancelButtonText, String okButtonText, String text, Integer maxLength, double internal_inner, double internal_outer) {
		this(ui, frame, componentText, cancelButtonText, okButtonText, text, maxLength, null, null, internal_inner, internal_outer);
	}

	public EnterLongText(String frame, String componentText, String cancelButtonText, String okButtonText, String text, Integer maxLength, Runnable onDoneCancel, Runnable onDoneOk, double internal_inner, double internal_outer) {
		this(null, frame, componentText, cancelButtonText, okButtonText, text, maxLength, onDoneCancel, onDoneOk, internal_inner, internal_outer);
	}

	public EnterLongText(UI ui, String frame, String componentText, String cancelButtonText, String okButtonText, String text, Integer maxLength, Runnable onDoneCancel, Runnable onDoneOk, double internal_inner, double internal_outer) {
		super(frame);
		this.ui = ui != null ? ui : UI.getCurrent();
		setModal(true);
		this.onDoneCancel = onDoneCancel;
		this.onDoneOk = onDoneOk;
		this.internal_inner = internal_inner;
		this.internal_outer = internal_outer;

		VerticalLayout content = new VerticalLayout();
		content.setSizeFull();

		ta = new TextArea(componentText);
		if(maxLength != null) {
			ta.setMaxLength(maxLength);
		}
		if(text != null) {
			ta.setValue(text);
		}
		ta.setSizeFull();
		content.addComponent(ta);
		content.setExpandRatio(ta, 1.0f);

		HorizontalLayout buttons = new HorizontalLayout();

		Button doneBtn = new Button(okButtonText);
		doneBtn.addClickListener(e -> {
			close();
			if(this.onDoneOk != null) {
				this.onDoneOk.run();
			}
		});

		Button cancelBtn = new Button(cancelButtonText);
		cancelBtn.addClickListener(e -> {
			ta = null;
			close();
			if(this.onDoneCancel != null) {
				this.onDoneCancel.run();
			}
		});

		buttons.addComponent(doneBtn);
		buttons.addComponent(cancelBtn);

		content.addComponent(buttons);
		setContent(content);
		setWidth(""+((int) 100*outer)+"%");
		setHeight(""+((int) 100*outer)+"%");
		ta.focus();
	}
	
	public void setOnDone(Runnable onDone) {
		this.onDoneOk = onDone;
	}

	public void setOnDoneCancel(Runnable onDoneCancel) {
		this.onDoneCancel = onDoneCancel;
	}

	public void setOnDoneOk(Runnable onDoneOk) {
		this.onDoneOk = onDoneOk;
	}

	public String getText() {
		if(ta == null) {
			return null;
		}
		String out = ta.getValue();
		return out;
	}

	public void setEnabled(boolean enabled) {
		ta.setEnabled(enabled);
	}

	public void setMaxLength(int maxlen) {
		ta.setMaxLength(maxlen);
	}

}

