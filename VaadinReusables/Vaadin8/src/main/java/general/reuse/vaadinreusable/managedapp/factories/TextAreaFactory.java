package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

public class TextAreaFactory {

	private static String cssname = null;

	public static void setCssname(String cssname) {
		TextAreaFactory.cssname = cssname;
	}

	private static TextFieldFactory.Sanitizer current = s -> {
		if(s == null) {
			return null;
		}
		Document doc = Jsoup.parse(s);
		String text = doc.text();
		if(text.toLowerCase().contains("jndi:")) {
			text = null;
		}
		return text;
	};

	public static void setSanitizer(TextFieldFactory.Sanitizer sanitizer) {
		current = sanitizer;
	}

	public static String sanitize(String in) {
		return current.sanitize(in);
	}

	public static TextArea styles(TextArea in) {
		if(cssname != null) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static TextArea create(String caption) {
		TextArea out = new TextArea(caption) {
			public String getValue() {
				return sanitize(super.getValue());
			}
		};
		return styles(out);
	}

	public static TextArea create(String caption, final TextFieldFactory.Sanitizer sanitizer) {
		TextArea out = new TextArea(caption) {
			public String getValue() {
				return sanitizer.sanitize(super.getValue());
			}
		};
		return styles(out);
	}

	public static TextArea create() {
		TextArea out = new TextArea() {
			public String getValue() {
				return sanitize(super.getValue());
			}
		};
		return styles(out);
	}

	public static TextArea create(final TextFieldFactory.Sanitizer sanitizer) {
		TextArea out = new TextArea() {
			public String getValue() {
				return sanitizer.sanitize(super.getValue());
			}
		};
		return styles(out);
	}

}
