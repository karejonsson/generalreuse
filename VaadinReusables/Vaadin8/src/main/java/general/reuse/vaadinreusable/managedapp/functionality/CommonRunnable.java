package general.reuse.vaadinreusable.managedapp.functionality;

import java.io.Serializable;

import org.apache.log4j.Logger;

public class CommonRunnable implements Serializable, Runnable {
	
	private static final long serialVersionUID = 4826536551714571886L;
	private static Logger log = Logger.getLogger(CommonRunnable.class);
	private Runnable onCall = null;
	private String msg = null;
	
	public CommonRunnable(Runnable onCall) {
		this.onCall = onCall;
		final String filename = Thread.currentThread().getStackTrace()[2].getFileName();
		final int linenumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
		msg = "Fil "+filename+", rad "+linenumber;
	}
	
	public CommonRunnable(Runnable onCall, String msg) {
		this.onCall = onCall;
		this.msg = msg;
	}

	// Den här finns för att eftersteget skall kunna ändras när man loggar ut.
	// Den runnable som finns skickar användaren tillbaka till meddelandeöppningen troligtvis
	// men vid utloggning skall det ändras till att gå tillbaka till utloggningen.
	public void setRunnable(Runnable onCall) {
		this.onCall = onCall;
	}
	
	public void run() {
		final String filename = Thread.currentThread().getStackTrace()[2].getFileName();
		final int linenumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
		String exemsg = "Fil "+filename+", rad "+linenumber;
		log.info("------------ Kör CommonRunnable: Skapad: "+msg+", exekverad "+exemsg);
		onCall.run();
	}
	
	public String toString() {
		return msg;
	}
	
}
