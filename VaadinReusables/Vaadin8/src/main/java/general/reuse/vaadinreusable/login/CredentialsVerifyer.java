package general.reuse.vaadinreusable.login;

public interface CredentialsVerifyer {
	
	boolean accepted(String username, String password);
	boolean isDevmode();
	void prepareDevmode();

}
