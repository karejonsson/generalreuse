package general.reuse.vaadinreusable.managedapp.texts;

public class CaptionTexts {
	
	public static final String handling = "Hantering";
	public static final String text = "Text";
	public static final String name = "Namn";
	public static final String message = "Meddelande";
	public static final String date = "Datum";

	public static final String allowLoginRightNow = "Tillåt inloggning just nu";

	public static final String choseContactMethod = "Välj sätt att kontakta";
	public static final String askForChat = "Be om chatt";
	public static final String forceToChat = "Tvinga till chatt";
	public static final String writtenMessage = "Skrivet meddelande";
	public static final String yes = "Ja";
	public static final String no = "Nej";

    public static final String activateChat = "Chatten aktiverad";
	public static final String warnShutdown = "Varna för nedstängning";
    public static final String editSomeMessage = "Redigera ett meddelande";
	public static final String memoryState = "Minnestillstånd";
	public static final String activity = "Aktivitet";
	public static final String userType = "Användartyp";
	public static final String username = "Användarnamn";
	public static final String engagement = "Engagemang";
	public static final String sensitive = "Känslig";
	public static final String timeIdle = "Tid overksam";
    public static final String cancel = "Avbryt";
	public static final String clear = "Rensa";
	public static final String save = "Spara";
	public static final String edit = "Redigera";
	public static final String upload = "Ladda upp";
	public static final String delete = "Radera";
	public static final String missing = "Saknas";
	public static final String chose = "Välj";
	public static final String choseOperation = "Välj operation";
	public static final String create = "Skapa";
}
/*
sed -i 's/Caption(CaptionTexts.date)/Caption(CaptionTexts.date)/g' `find . -name '*.java' -print`
sed -i 's/Caption(CaptionTexts.dateTime)/Caption(CaptionTexts.dateTime)/g' `find . -name '*.java' -print`
sed -i 's/Caption("Mottagare")/Caption(CaptionTexts.recievers)/g' `find . -name '*.java' -print`
sed -i 's/Caption("Avsändare")/Caption(CaptionTexts.senders)/g' `find . -name '*.java' -print`
sed -i 's/Caption("Markerad")/Caption(CaptionTexts.marked)/g' `find . -name '*.java' -print`
sed -i 's/Caption("Aktstatus")/Caption(CaptionTexts.folderStatus)/g' `find . -name '*.java' -print`
sed -i 's/Caption("Prioritet")/Caption(CaptionTexts.priority)/g' `find . -name '*.java' -print`
sed -i 's/Caption("Meddelandestatus")/Caption(CaptionTexts.messageStatus)/g' `find . -name '*.java' -print`

./MessageFunctionality/src/main/java/se/prv/phoenix/messages/misc/GrouplessMessagesPanel.java:199:		}).setCaption("Status");
./MessageFunctionality/src/main/java/se/prv/phoenix/messages/misc/GrouplessMessagesPanel.java:217:		}).setCaption("Grupp");
./MessageFunctionality/src/main/java/se/prv/phoenix/messages/misc/GrouplessMessagesPanel.java:230:		}).setCaption("Mottagare");
./MessageFunctionality/src/main/java/se/prv/phoenix/messages/misc/GrouplessMessagesPanel.java:243:		}).setCaption("Avsändare");
./MessageFunctionality/src/main/java/se/prv/phoenix/messages/group/ForPersonalInGroupPanel.java:130:		}).setCaption("Markerad");
./MessageFunctionality/src/main/java/se/prv/phoenix/messages/group/ForPersonalInGroupPanel.java:133:		}).setCaption("Aktstatus");
./MessageFunctionality/src/main/java/se/prv/phoenix/messages/group/ForPersonalInGroupPanel.java:136:		}).setComparator(Comparators.filingDateComparator).setCaption(CaptionTexts.date);
./MessageFunctionality/src/main/java/se/prv/phoenix/messages/group/ForPersonalInGroupPanel.java:158:		}).setCaption("Prioritet");
./MessageFunctionality/src/main/java/se/prv/phoenix/messages/group/ForPersonalInGroupPanel.java:169:		}).setCaption("Meddelandestatus");
./*/