package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.Notification;

public class NotificationV8ish extends Notification {
    public NotificationV8ish(String caption) {
        super(caption);
    }

    public static class TypeV8ish {
        public static final Type ERROR_MESSAGE = Type.ERROR_MESSAGE;
        public static final Type WARNING_MESSAGE = Type.WARNING_MESSAGE;
    }
}
