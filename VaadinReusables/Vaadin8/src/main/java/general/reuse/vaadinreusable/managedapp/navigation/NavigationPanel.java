package general.reuse.vaadinreusable.managedapp.navigation;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import general.reuse.vaadinreusable.component.ShowLongText;
import general.reuse.vaadinreusable.vaadin8ish.*;
import org.apache.log4j.Logger;
import general.reuse.vaadinreusable.managedapp.functionality.BaseAdminFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.functionality.CommonRunnable;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.internalchat.ChatButton;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;
import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;
import general.reuse.vaadinreusable.managedapp.factories.PanelFactory;

import java.util.Map;

public class NavigationPanel extends Panel {

	private static Logger logg = Logger.getLogger(NavigationPanel.class);
	private static final long serialVersionUID = -1335626300083283997L;
	private BaseAdminFunctionalityProvider adminFunctionality = null;

	private BaseFunctionalityProvider functionality = null;
	private CommonRunnable onBack = null;
	//private UIV8ish ui = null;

	public NavigationPanel(BaseAdminFunctionalityProvider adminFunctionality/*, UIV8ish ui*/, CommonRunnable onBack) {
		PanelFactory.styles(this);
		//this.ui = ui;
		this.adminFunctionality = adminFunctionality;
		functionality = adminFunctionality.getFunctionality();
		adminFunctionality.updateUidname();
		
		this.onBack = onBack;
		VerticalLayout vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		//vl.addComponent(LabelFactory.create("AdminPanel"));
		
		HorizontalLayout collegues = LayoutFactory.createHorizontal(functionality.getHaveMargins());
		collegues.setCaption("Medarbetare");
		
		Button activeUsersBtn = ButtonFactory.create(ButtonTexts.activeUsers);
		activeUsersBtn.addClickListener(e -> adminFunctionality.openActiveManagement(new CommonRunnable(() -> adminFunctionality.openAdmin(onBack))));
		collegues.addComponent(activeUsersBtn);

		vl.addComponent(collegues);

		HorizontalLayout infrastructure = LayoutFactory.createHorizontal(functionality.getHaveMargins());
		infrastructure.setCaption("Infrastruktur");

		Button chatBtn = ChatButton.createAlwaysStateConsidered(adminFunctionality.getFunctionality());
		if(chatBtn != null) {
			infrastructure.addComponent(chatBtn);
		}

		Button fileHandlingBtn = ButtonFactory.create(ButtonTexts.fileHandling);
		fileHandlingBtn.addClickListener(e -> adminFunctionality.openFileHandling(new CommonRunnable(() -> adminFunctionality.openAdmin(onBack))));
		infrastructure.addComponent(fileHandlingBtn);

		Button statesBtn = ButtonFactory.create(ButtonTexts.states);
		statesBtn.addClickListener(e -> UsersActivePanel.manageGlobalSettings(functionality));
		infrastructure.addComponent(statesBtn);

		vl.addComponent(infrastructure);

		HorizontalLayout flow = LayoutFactory.createHorizontal(functionality.getHaveMargins());

		if(onBack != null) {
			Button backBtn = ButtonFactory.create(ButtonTexts.revert);
			backBtn.addClickListener(e -> onBack.run());
			flow.addComponent(backBtn);
		}

		Button startBtn = ButtonFactory.create(ButtonTexts.start);
		startBtn.addClickListener(e -> adminFunctionality.start());
		flow.addComponent(startBtn);
		
		Button logoutBtn = ButtonFactory.create(ButtonTexts.logout, VaadinIcons.POWER_OFF);
		logoutBtn.addClickListener(e -> adminFunctionality.getFunctionality().logout(adminFunctionality.getFunctionality().getRunnableForReturnToCurrentContent()));
		flow.addComponent(logoutBtn);

		vl.addComponent(flow);
		
		setContent(vl);
	}

	private interface CountersGetter {
		Map<String, Long> get();
	}

	private Component getCounter(String name, final CountersGetter getter) {
		Button outBtn = ButtonFactory.create(name);
		Map<String, Long> m = getter.get();
		StringBuffer sb = new StringBuffer();
		sb.append("Alla tabeller: "+m.get("all")+"\n");
		for(String key : m.keySet()) {
			if(key.equals("all")) {
				continue;
			}
			sb.append(key+": "+m.get(key)+"\n");
		}
		String text = sb.toString();
		outBtn.addClickListener(e -> {
			ShowLongText slt = new ShowLongText("Räknare", "DB: "+name, ButtonTexts.close, text, true);
			functionality.addWindow(slt, "Räknare: "+name);
		});
		return outBtn;
	}

}
