package general.reuse.vaadinreusable.component;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import general.reuse.interfaces.Getter;
import general.reuse.interfaces.Setter;
import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;
import general.reuse.vaadinreusable.managedapp.factories.WindowFactory;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.managedapp.texts.CaptionTexts;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

public class EditOneRowForm extends EditDynamicForm {

    private static Logger log = Logger.getLogger(EditOneRowForm.class);
    public static final double outer = 0.8;

    private BaseFunctionalityProvider functionality = null;
    private HorizontalLayout buttonsHL = null;

    private VerticalLayout fieldsVL = null;

    public EditOneRowForm(BaseFunctionalityProvider functionality) {
        WindowFactory.styles(this);
        this.functionality = functionality;
        VerticalLayout mainVL = LayoutFactory.createVertical(functionality.getHaveMargins());
        fieldsVL = LayoutFactory.createVertical(functionality.getHaveMargins());
        fieldsVL.setSizeFull();
        mainVL.addComponent(fieldsVL);
        mainVL.setExpandRatio(fieldsVL, 1.0f);
        buttonsHL = LayoutFactory.createHorizontal(functionality.getHaveMargins());
        mainVL.addComponent(buttonsHL);
        Button cancelBtn = ButtonFactory.create(CaptionTexts.cancel);
        cancelBtn.addClickListener(e -> close());
        buttonsHL.addComponent(cancelBtn);
        Button clearBtn = ButtonFactory.create(CaptionTexts.clear);
        clearBtn.addClickListener(e -> clearForm());
        buttonsHL.addComponent(clearBtn);

        mainVL.setSizeFull();
        setWidth(""+((int) 100*outer)+"%");
        setHeight(""+((int) 100*outer)+"%");
        setModal(true);
        setContent(mainVL);
    }

    private List<Callable> inserters = new ArrayList<>();
    private List<Callable> clearers = new ArrayList<>();

    public void addLabel(String text) {
        addLabel(text, true);
    }

    public void addLabel(String text, boolean boldness) {
        AddToForm.addLabel(fieldsVL, text, boldness);
    }

    public void addStringField(final Getter<String> get, final String caption) {
        AddToForm.addStringField(fieldsVL, inserters, clearers, get, null, caption, true);
    }

    public void addStringField(final Getter<String> get, final Setter<String> set, final String caption) {
        AddToForm.addStringField(fieldsVL, inserters, clearers, get, set, caption, true);
    }

    public void addChoiceField(final Getter<Object> get, final Setter<Object> set, Collection<Object> alternatives, final String caption) {
        AddToForm.addChoiceField(fieldsVL, inserters, clearers, get, set, alternatives, caption, true);
    }

    public void addBooleanField(final Getter<Boolean> get, final String caption) {
        AddToForm.addBooleanField(fieldsVL, inserters, clearers, get, null, caption, true);
    }

    public void addBooleanField(final Getter<Boolean> get, final Setter<Boolean> set, final String caption) {
        AddToForm.addBooleanField(fieldsVL, inserters, clearers, get, set, caption, true);
    }

    public void addTexteditorField(final Getter<String> get, final Setter<String> set, final String caption) {
        AddToForm.addTexteditorField(
            fieldsVL,
            inserters,
            clearers,
            get,
            set,
            caption,
            true,
            1.0f);
    }


    public void addAction(String caption, Runnable onClick) {
        Button actionBtn = ButtonFactory.create(caption);
        actionBtn.addClickListener(e -> onClick.run());
        buttonsHL.addComponent(actionBtn);
    }

    public void updateValues() throws Exception {
        for(Callable inserter : inserters) {
            inserter.call();
        }
    }

    public void clearForm() {
        try {
            for(Callable clearer : clearers) {
                clearer.call();
            }
        }
        catch(Exception e) {
            final String msg = "Fel uppstod när formuläret skulle rensas";
            log.error(msg, e);
            CommonNotification.showError(functionality, msg);
        }
    }

    public void persist(Persister p, BaseFunctionalityProvider functionality, String toString, Runnable onPositive) {
        try {
            updateValues();
        }
        catch(Exception e) {
            final String msg = "Värdena kan inte läggas in i DB-raden";
            log.error("Problem: "+msg+", row="+toString, e);
            CommonNotification.showError(functionality, msg);
            return;
        }
        try {
            if(!p.persist()) {
                final String msg = "DB-raden kunde inte skapas";
                log.error(msg+", row="+toString);
                CommonNotification.showError(functionality, msg);
                return;
            }
        }
        catch(Exception e) {
            final String msg = "DB-raden kunde inte skapas";
            log.error("Problem: "+msg+", row="+toString, e);
            CommonNotification.showError(functionality, msg);
            return;
        }
        close();
        onPositive.run();
        CommonNotification.showAssistive(functionality, "Sparat!");
    }

}
