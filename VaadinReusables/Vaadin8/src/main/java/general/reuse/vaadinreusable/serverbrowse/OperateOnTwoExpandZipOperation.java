package general.reuse.vaadinreusable.serverbrowse;

import org.apache.log4j.Logger;

import java.io.File;

public class OperateOnTwoExpandZipOperation implements OperateOnTwoBrowsed_source_target {

    private static Logger logg = Logger.getLogger(OperateOnTwoExpandZipOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnTwoExpandZipOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Zipexpandering";
    }

    @Override
    public String getDescription() {
        return "Expandera en zipfil till en annan plats";
    }

    @Override
    public Runnable getOperate(File first, String second) {
        return () -> {
            operate(first, second);
        };
    }

    @Override
    public void operate(File source, String filepath) {
        logg.info("source="+source.getAbsolutePath()+", filepath="+filepath);

        if(!source.exists()) {
            String msg = "Källfilen finns inte";
            logg.info(msg+". source="+source.getAbsolutePath());
            functionality.showError(msg);
            return;
        }

        if(!source.isDirectory()) {
            String msg = "Källfilen är inte en katalog";
            logg.info(msg+". source="+source.getAbsolutePath());
            functionality.showError(msg);
            return;
        }

        File f = new File(filepath);
        if(f.exists()) {
            String msg = "Målkatalogen finns inte";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }

        if(!f.isDirectory()) {
            String msg = "Målkatalogen är inte en katalog";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }

        try {
            OperateOnOneExpandZipOperation.unzip(source, f);
        }
        catch(Exception e) {
            String msg = "Fel vid uppzippningen. Underliggande felmeddelande\n"+e.getMessage();
            logg.error(msg+". filepath="+filepath, e);
            functionality.showError(msg);
            return;
        }
    }

}
