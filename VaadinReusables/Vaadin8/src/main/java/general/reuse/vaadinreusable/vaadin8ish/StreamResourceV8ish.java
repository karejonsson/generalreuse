package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.server.StreamResource;

public class StreamResourceV8ish extends StreamResource implements ResourceV8ish {
    public StreamResourceV8ish(StreamSource streamSource, String filename) {
        super(streamSource, filename);
    }
}
