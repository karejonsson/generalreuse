package general.reuse.vaadinreusable.managedapp.functionality;

import com.vaadin.ui.Window;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.Future;

public interface BaseFunctionalityProvider {

	BaseAdminFunctionalityProvider getAdminFunctionality();
	CommonRunnable getRunnableForReturnToCurrentContent();

	void start();

	void addWindow(Window win, String winType);
	void addWindow(Window win, boolean modal, String winType);

	void setUsername(String username);
	String getUsername();

	void updateUidname();
	String getDateformat();
	void setTextualName(String username);
	String getTextualName();
	void setEmail(String email);
	String getEmail();

	String getPoolname();
	String getUserRolename();

	void setChatSince();
	long getChatSince();

	void openChat(CommonRunnable onBack);

	String getActivity();
	boolean getStateful();
	void setActivity(String activity, boolean stateful);
	UserInteractionEvent[] getActivityEvents();

	boolean isFunctional();
	Future<Void> access(final Runnable runnable);

	Boolean getHaveMargins();

	String getStateDescription();
	boolean possiblyWulnerable();
	void setLocation(String s);
	void destroyAllInternals();
	Date getLast();

	void addReturnKeyListener(Object o, CommonRunnable r);
	void clearReturnKeyListener(Object o);
	void clearReturnKeyListeners();

	void setParameters(Map<String, String> parameters);
	Map<String, String> getParameters();

	void logout(CommonRunnable onBack);
	Object get(String k);
	void put(String k, Object o);

}
