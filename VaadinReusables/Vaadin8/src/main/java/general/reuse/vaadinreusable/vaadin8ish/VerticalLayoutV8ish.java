package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.VerticalLayout;

public class VerticalLayoutV8ish extends VerticalLayout implements ComponentV8ish, LayoutV8ish {
    public ComponentV8ish getComponent(int index) {
        return (ComponentV8ish) super.getComponent(index);
    }
}
