package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

public class LayoutFactory {

	private static String csshorizontalname = null;
	private static String cssverticalname = null;
	private static String cssabsolutename = null;

	public static void setCssnameHorizontal(String cssname) {
		LayoutFactory.csshorizontalname = cssname;
	}

	public static void setCssnameVerticalname(String cssname) {
		LayoutFactory.cssverticalname = cssname;
	}

	public static void setCssnameAbsolutename(String cssname) {
		LayoutFactory.cssabsolutename = cssname;
	}

	public static HorizontalLayout styles(HorizontalLayout in, boolean haveMargin) {
		if(csshorizontalname != null) {
			in.setPrimaryStyleName(csshorizontalname);
		}
		if(!haveMargin) {
			in.setMargin(haveMargin);
		}
		return in;
	}

	public static VerticalLayout styles(VerticalLayout in, boolean haveMargin) {
		if(cssverticalname != null) {
			in.setPrimaryStyleName(cssverticalname);
		}
		if(!haveMargin) {
			in.setMargin(haveMargin);
		}
		return in;
	}

	public static AbsoluteLayout styles(AbsoluteLayout in) {
		if(cssabsolutename != null) {
			in.setPrimaryStyleName(cssabsolutename);
		}
		return in;
	}

	public static HorizontalLayout createHorizontal(boolean haveMargin) {
		return styles(new HorizontalLayout(), haveMargin);
	}

	public static HorizontalLayout createHorizontal(String caption, boolean haveMargin) {
		HorizontalLayout out = new HorizontalLayout();
		out.setCaption(caption);
		return styles(out, haveMargin);
	}

	public static VerticalLayout createVertical(boolean haveMargin) {
		return styles(new VerticalLayout(), haveMargin);
	}

	public static VerticalLayout createVertical(String caption, boolean haveMargin) {
		VerticalLayout out = new VerticalLayout();
		out.setCaption(caption);
		return styles(out, haveMargin);
	}

	public static AbsoluteLayout createAbsolute() {
		return styles(new AbsoluteLayout());
	}

}
