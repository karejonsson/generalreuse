package general.reuse.vaadinreusable.ui;

import general.reuse.vaadinreusable.vaadin8ish.*;

public abstract class SimplifiedContentUI extends UIV8ish {

	private static final long serialVersionUID = -8886139178227173770L;
	protected MenuBarV8ish menubar = new MenuBarV8ish();
	protected VerticalLayoutV8ish mainPanel = new VerticalLayoutV8ish();
	protected final VerticalLayoutV8ish layout = new VerticalLayoutV8ish();
	protected final HorizontalLayoutV8ish menubarlayout = new HorizontalLayoutV8ish();
	
	protected PanelV8ish barLevelRightmost = new PanelV8ish();
	private int barLevelRightmostPixels = 100;

	protected PanelV8ish barLevelMiddle = new PanelV8ish();
	private int barLevelMiddlePixels = 300;

	public SimplifiedContentUI() {	
		//setHeight(""+getScreenHeight()+"px");
		mainPanel.setSizeFull();
		setSizeFull();
	}
	
	/*
	public SimplifiedContentUI(int orientationPixels) {	
		this.orientationPixels = orientationPixels;
	}
	*/
	
	public void setup(ComponentV8ish c) {
		setupGeneralMenubar();
		setupParticularMenubar();
		
		if(menubar.getItems().size() > 0) {
			menubarlayout.addComponent(menubar);
			menubarlayout.setComponentAlignment(menubar, AlignmentV8ish.MIDDLE_LEFT);
			
			barLevelMiddle.setWidth(""+barLevelMiddlePixels+"px");
			barLevelMiddle.setHeight("100%");
			barLevelMiddle.setVisible(false);
			menubarlayout.addComponent(barLevelMiddle);
			menubarlayout.setComponentAlignment(barLevelMiddle, AlignmentV8ish.MIDDLE_CENTER);
			
			barLevelRightmost.setWidth(""+barLevelRightmostPixels+"px");
			barLevelRightmost.setHeight("100%");
			barLevelRightmost.setVisible(false);
			menubarlayout.addComponent(barLevelRightmost);
			menubarlayout.setComponentAlignment(barLevelRightmost, AlignmentV8ish.MIDDLE_RIGHT);
			
			menubarlayout.setWidth("100%");
			layout.addComponent(menubarlayout);
			//layout.setExpandRatio(menubarlayout, 1);
		}
		//mainPanel.setHeight(""+((int) (0.9*getScreenHeight()))+"px");
		layout.addComponent(mainPanel);

		layout.setExpandRatio(mainPanel, 1.0f);
		//layout.setComponentAlignment(mainPanel, Alignment.MIDDLE_CENTER);
		layout.setSizeFull();
		layout.setMargin(false);
		setContent(layout);

		setWorkContent(c);
	}
	
	public void setBarLevelRightmostVisible(boolean visible) {
		barLevelRightmost.setVisible(visible);
	}
	
	public void setBarLevelMiddleVisible(boolean visible) {
		barLevelMiddle.setVisible(visible);
	}
	
	public void setBarLevelRightmost(String s) {
		setBarLevelRightmost(new LabelV8ish(s));
	}

	public void setBarLevelRightmost(String s, int widthPixels) {
		setBarLevelRightmost(new LabelV8ish(s), widthPixels);
	}

	public void setBarLevelRightmost(ComponentV8ish c) {
		if(!barLevelRightmost.isVisible()) {
			setBarLevelRightmostVisible(true);
		}
		barLevelRightmost.setContent(c);
	}
	
	public void setBarLevelRightmost(ComponentV8ish c, int widthPixels) {
		if(!barLevelRightmost.isVisible()) {
			setBarLevelRightmostVisible(true);
		}
		barLevelRightmost.setWidth(""+widthPixels+"px");
		barLevelRightmostPixels = widthPixels;
		barLevelRightmost.setContent(c);
	}
	
	public void setBarLevelMiddle(String s) {
		setBarLevelMiddle(new LabelV8ish(s));
	}

	public void setBarLevelMiddle(String s, int widthPixels) {
		setBarLevelMiddle(new LabelV8ish(s), widthPixels);
	}

	public void setBarLevelMiddle(ComponentV8ish c) {
		if(!barLevelMiddle.isVisible()) {
			setBarLevelMiddleVisible(true);
		}
		barLevelMiddle.setContent(c);
	}
	
	public void setBarLevelMiddle(ComponentV8ish c, int widthPixels) {
		if(!barLevelMiddle.isVisible()) {
			setBarLevelMiddleVisible(true);
		}
		barLevelMiddle.setWidth(""+widthPixels+"px");
		barLevelMiddlePixels = widthPixels;
		barLevelMiddle.setContent(c);
	}
	
	public void setWorkContent(ComponentV8ish c) {
		mainPanel.removeAllComponents();
		if(c instanceof VerticalLayoutV8ish) {
			((VerticalLayoutV8ish) c).setMargin(false);
		}
		mainPanel.addComponent(c);
		mainPanel.setExpandRatio(c, 1.0f);
	}
	
	public ComponentV8ish getWorkContent() {
		if(mainPanel.getComponentCount() == 0) {
			return null;
		}
		return mainPanel.getComponent(0);
	}
	
	public void openSubwindow(WindowV8ish window) {
		window.center();
		window.setModal(true);
        addWindow(window);
	}
	
	public abstract void setupGeneralMenubar();
	public abstract void setupParticularMenubar();

	/*
	public static int getScreenHeight() {
		WebBrowser browser = (Page.getCurrent() != null) ? Page.getCurrent().getWebBrowser() : null;
		if(browser != null) {
			return browser.getScreenHeight();
		}
		return -1;
	}
	*(
	 */

}
