package general.reuse.vaadinreusable.component;

import com.vaadin.ui.Window;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;

public abstract class EditDynamicForm extends Window {

    public abstract void updateValues() throws Exception;
    public abstract void addAction(String caption, Runnable onClick);

    public interface Persister {
        boolean persist() throws Exception;
    }

    public abstract void persist(Persister p, BaseFunctionalityProvider functionality, String toString, Runnable onSuccess);

}
