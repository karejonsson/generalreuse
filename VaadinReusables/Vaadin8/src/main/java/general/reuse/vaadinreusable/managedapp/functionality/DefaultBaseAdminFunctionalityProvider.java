package general.reuse.vaadinreusable.managedapp.functionality;

import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import org.apache.log4j.Logger;

import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;
import general.reuse.vaadinreusable.managedapp.texts.ActivityTexts;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;
import general.reuse.vaadinreusable.managedapp.factories.LabelFactory;

public class DefaultBaseAdminFunctionalityProvider implements BaseAdminFunctionalityProvider {

	private static Logger logg = Logger.getLogger(DefaultBaseAdminFunctionalityProvider.class);

	protected UI ui = null;
	private BaseFunctionalityProvider functionality = null;
	
	public String toString() {
		StringBuffer out = new StringBuffer();
		out.append("DefaultAdminFunctionality: UI "+(ui != null));
		out.append(", functionality "+(functionality != null));
		return out.toString();
	}

	public DefaultBaseAdminFunctionalityProvider(UI ui) {
		this.ui = ui;
	}

	public void updateUidname() {
		if(functionality != null) {
			functionality.updateUidname();
		}
	}

	@Override
	public void openAdmin(CommonRunnable onBack) {
		updateUidname();
		functionality.setActivity(ActivityTexts.openAdmin, false);
		VerticalLayout vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		vl.addComponent(LabelFactory.create("Navigationsfunktionalitet för administration ej tillhanda"));
		Button backBtn = ButtonFactory.create(ButtonTexts.revert);
		backBtn.addClickListener(e -> onBack.run());
		vl.addComponent(backBtn);
		ui.setContent(vl);
	}

	@Override
	public void start() {
		functionality.start();
	}

	@Override
	public void addWindow(Window win, String winType) {
		functionality.addWindow(win, true, winType);
	}

	@Override
	public BaseFunctionalityProvider getFunctionality() {
		return functionality;
	}

	@Override
	public void setFunctionality(BaseFunctionalityProvider functionality) {
		this.functionality = functionality;
	}

	@Override
	public void openUserManagement(CommonRunnable onBack) {
		updateUidname();
		functionality.setActivity(ActivityTexts.openUserManagement, false);
		VerticalLayout vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		vl.addComponent(LabelFactory.create("Funktionalitet för att admin skall hantera användare ej tillhanda"));
		Button backBtn = ButtonFactory.create(ButtonTexts.revert);
		backBtn.addClickListener(e -> onBack.run());
		vl.addComponent(backBtn);
		ui.setContent(vl);
	}

	@Override
	public void openActiveManagement(CommonRunnable onBack) {
		updateUidname();
		functionality.setActivity(ActivityTexts.openActiveManagement, false);
		VerticalLayout vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		vl.addComponent(LabelFactory.create("Funktionalitet för att admin skall hantera alla aktiva användare ej tillhanda"));
		Button backBtn = ButtonFactory.create(ButtonTexts.revert);
		backBtn.addClickListener(e -> onBack.run());
		vl.addComponent(backBtn);
		ui.setContent(vl);
	}

	@Override
	public void openFileHandling(CommonRunnable onBack) {
		updateUidname();
		functionality.setActivity(ActivityTexts.openFileHandling, false);
		VerticalLayout vl = LayoutFactory.createVertical(functionality.getHaveMargins());
		vl.addComponent(LabelFactory.create("Funktionalitet för admins filhantering ej tillhanda"));
		Button backBtn = ButtonFactory.create(ButtonTexts.revert);
		backBtn.addClickListener(e -> onBack.run());
		vl.addComponent(backBtn);
		ui.setContent(vl);
	}

}
