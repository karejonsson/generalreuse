package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import general.reuse.vaadinreusable.choser.ChoseFromListVerticalLayout;

import java.util.List;

public class ComboBoxV8ish<T> extends ComboBox<T> {
    public ComboBoxV8ish() {
        super();
    }
    public ComboBoxV8ish(String caption) {
        super(caption);
    }

    public void setItems(ChoseFromListVerticalLayout.FilteringWrapper filterw, List<T> cbobjects) {
        super.setItems(filterw, cbobjects);
    }
}
