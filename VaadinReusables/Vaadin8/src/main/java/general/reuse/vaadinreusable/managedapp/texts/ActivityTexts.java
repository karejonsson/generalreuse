package general.reuse.vaadinreusable.managedapp.texts;

public class ActivityTexts {

    public static final String start = "Startsidan";
    public static final String logout = "Utloggning";
    public static final String openChat = "Intern chatt";

    // Admin
    public static final String openAdmin = "Admins funktionsnavigering";
    public static final String openUserManagement = "Användarhantering";
    public static final String openActiveManagement = "Aktiva användare";
    public static final String openFileHandling = "Filhantering";
    public static final String operationCancelled = "Operationen avbruten";
    public static final String pleaseGoToChat = "Skulle du kunna gå in i "+DescriptionTexts.appname+"-chatten?";
    public static final String openInteractiveSQL = "Interaktiv SQL";
}
