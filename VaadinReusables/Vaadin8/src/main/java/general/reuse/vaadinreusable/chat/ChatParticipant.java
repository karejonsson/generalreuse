package general.reuse.vaadinreusable.chat;

import com.vaadin.ui.TextField;

public interface ChatParticipant {
    boolean hasChatAnyway();
    String getUsername();
    boolean getHaveMargins();
    boolean isFunctional();
    void setChatSince(long since);
    void access(Runnable onAccess);
    void addReturnKeyListener(TextField tf, Runnable onSend);
}
