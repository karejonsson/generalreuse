package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;

public class ButtonFactory {

	private static String cssname = null;

	public static void setCssname(String cssname) {
		ButtonFactory.cssname = cssname;
	}
	
	public static Button styles(Button in) {
		if(cssname != null) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static void addIcon(String caption, Button button) {
		if(caption == null) {
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.userData)) {
			button.setIcon(VaadinIcons.USER_CARD);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.exportContentColumns)) {
			button.setIcon(VaadinIcons.TABLE);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.fileHandling)) {
			button.setIcon(VaadinIcons.FILE_TREE);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.userOpenings)) {
			button.setIcon(VaadinIcons.RECORDS);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.diagnosePlatform)) {
			button.setIcon(VaadinIcons.DOCTOR);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.revert)) {
			button.setIcon(VaadinIcons.STEP_BACKWARD);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.save)) {
			button.setIcon(VaadinIcons.DISC);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.remove)) {
			button.setIcon(VaadinIcons.TRASH);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.edit)) {
			button.setIcon(VaadinIcons.EDIT);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.enableAccess)) {
			button.setIcon(VaadinIcons.EDIT);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.activeUsers)) {
			button.setIcon(VaadinIcons.USER);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.users)) {
			button.setIcon(VaadinIcons.USERS);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.erase)) {
			button.setIcon(VaadinIcons.ERASER);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.further)) {
			button.setIcon(VaadinIcons.FORWARD);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.copy)) {
			button.setIcon(VaadinIcons.COPY);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.groups)) {
			button.setIcon(VaadinIcons.GROUP);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.close)) {
			button.setIcon(VaadinIcons.CLOSE);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.closeFolder)) {
			button.setIcon(VaadinIcons.CLOSE);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.closeMessage_handleRelated)) {
			button.setIcon(VaadinIcons.CLOSE);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.disableAccess)) {
			button.setIcon(VaadinIcons.LOCK);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.downloadFile)) {
			button.setIcon(VaadinIcons.DOWNLOAD);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.userSettings)) {
			button.setIcon(VaadinIcons.ELLIPSIS_DOTS_V);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.settings)) {
			button.setIcon(VaadinIcons.ELLIPSIS_DOTS_V);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.personalSettings)) {
			button.setIcon(VaadinIcons.ELLIPSIS_DOTS_V);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.search)) {
			button.setIcon(VaadinIcons.SEARCH);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.documentSearch)) {
			button.setIcon(VaadinIcons.SEARCH);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.larger)) {
			button.setIcon(VaadinIcons.SEARCH_PLUS);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.smaller)) {
			button.setIcon(VaadinIcons.SEARCH_MINUS);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.start)) {
			button.setIcon(VaadinIcons.HOME_O);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.updatePage)) {
			button.setIcon(VaadinIcons.REFRESH);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.send)) {
			button.setIcon(VaadinIcons.OUTBOX);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.read)) {
			button.setIcon(VaadinIcons.EYE);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.openDossier)) {
			button.setIcon(VaadinIcons.FOLDER_ADD);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.throwOut)) {
			button.setIcon(VaadinIcons.SIGN_OUT);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.throwOutAllOthers)) {
			button.setIcon(VaadinIcons.SIGN_OUT);
			return;
		}
		if(caption.equalsIgnoreCase(ButtonTexts.cancel)) {
			button.setIcon(VaadinIcons.STOP);
			return;
		}
	}

	public static Button create(String caption) {
		Button outBtn = new Button(caption);
		addIcon(caption, outBtn);
		return styles(outBtn);
	}

	public static Button create(VaadinIcons icon) {
		return styles(new Button(icon));
	}

	public static Button create(String caption, VaadinIcons icon) {
		return styles(new Button(caption, icon));
	}

	public static Button create(VaadinIcons icon, String caption) {
		return styles(new Button(caption, icon));
	}

}
