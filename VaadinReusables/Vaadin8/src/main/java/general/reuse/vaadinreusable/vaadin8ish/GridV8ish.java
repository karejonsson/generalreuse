package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.Grid;

public class GridV8ish<T> extends Grid<T> {

    public GridV8ish() {
        super();
    }

    public GridV8ish(String caption) {
        super(caption);
    }

}
