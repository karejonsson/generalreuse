package general.reuse.vaadinreusable.managedapp.functionality;

import java.io.Serializable;
import java.util.Date;

public class UserInteractionEvent implements Serializable {

    private Date when = null;
    private String what = null;
    private boolean stateful = false;
    public UserInteractionEvent(Date when, String what, boolean stateful) {
        this.when = when;
        this.what = what;
        this.stateful = stateful;
    }
    public String toString() {
        return "När "+when+", minnestillstånd "+(stateful ? "JA " : "NEJ")+", vad "+what;
    }
    public Date getWhen() {
        return when;
    }
    public String getWhat() {
        return what;
    }

    public boolean isStateful() {
        return stateful;
    }

}
