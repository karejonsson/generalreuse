package general.reuse.vaadinreusable.component;

import com.vaadin.ui.*;
import general.reuse.interfaces.Extractor;
import general.reuse.interfaces.Getter;
import general.reuse.interfaces.Setter;
import general.reuse.vaadinreusable.choser.ChoseFromListHorizontalLayout;
import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;
import general.reuse.vaadinreusable.managedapp.factories.PanelFactory;
import general.reuse.vaadinreusable.managedapp.factories.WindowFactory;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;
import general.reuse.vaadinreusable.managedapp.texts.CaptionTexts;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

public class EditSplitForm extends EditDynamicForm {

    private static Logger log = Logger.getLogger(EditOneRowForm.class);
    public static final double outer = 0.8;

    private BaseFunctionalityProvider functionality = null;
    private HorizontalLayout buttonsHL = null;

    private Panel leftP = null;
    private VerticalLayout leftVL = null;
    private Panel rightP = null;
    private VerticalLayout rightVL = null;

    public EditSplitForm(BaseFunctionalityProvider functionality) {
        WindowFactory.styles(this);
        this.functionality = functionality;
        VerticalLayout main = LayoutFactory.createVertical(functionality.getHaveMargins());
        HorizontalSplitPanel hsp = PanelFactory.createHorizontalSplit();
        main.addComponent(hsp);
        main.setExpandRatio(hsp, 1.0f);
        hsp.setSizeFull();
        leftVL = LayoutFactory.createVertical(functionality.getHaveMargins());
        leftVL.setWidthFull();
        leftVL.setHeightUndefined();
        leftP = PanelFactory.create();
        leftP.setContent(leftVL);
        leftP.setSizeFull();
        hsp.setFirstComponent(leftP);
        rightVL = LayoutFactory.createVertical(functionality.getHaveMargins());
        rightVL.setWidthFull();
        rightVL.setHeightUndefined();
        rightP = PanelFactory.create();
        rightP.setContent(rightVL);
        rightP.setSizeFull();
        hsp.setSecondComponent(rightP);
        buttonsHL = LayoutFactory.createHorizontal(functionality.getHaveMargins());
        main.addComponent(buttonsHL);
        Button cancelBtn = ButtonFactory.create(CaptionTexts.cancel);
        cancelBtn.addClickListener(e -> close());
        buttonsHL.addComponent(cancelBtn);
        Button clearBtn = ButtonFactory.create(CaptionTexts.clear);
        clearBtn.addClickListener(e -> clearForm());
        buttonsHL.addComponent(clearBtn);

        main.setSizeFull();
        setWidth(""+((int) 100*outer)+"%");
        setHeight(""+((int) 100*outer)+"%");
        setModal(true);
        setContent(main);
    }

    private List<Callable> inserters = new ArrayList<>();
    private List<Callable> clearers = new ArrayList<>();

    public void addStringFieldLeft(final Getter<String> get, final String caption) {
        AddToForm.addStringField(leftVL, inserters, clearers, get, null, caption, true);
    }

    public void addStringFieldLeft(final Getter<String> get, final Setter<String> set, final String caption) {
        AddToForm.addStringField(leftVL, inserters, clearers, get, set, caption, true);
    }

    public void addDateFieldLeft(final Getter<Date> get, final Setter<Date> set, String dateFormat, final String caption) {
        AddToForm.addDateField(leftVL, inserters, clearers, get, set, dateFormat, caption, true);
    }

    public void addChoiceFieldLeft(final Getter<Object> get, final Setter<Object> set, Collection<Object> alternatives, final String caption) {
        AddToForm.addChoiceField(leftVL, inserters, clearers, get, set, alternatives, caption, true);
    }

    public void addSearchableChoiceFieldLeft(final Getter<Object> get, final Setter<Object> set, List<Object> alternatives, Extractor<Object, String> renderSort, String nameCol1, Extractor<Object, String> renderCol1, String nameCol2, Extractor<Object, String> renderCol2, final String caption) {
        AddToForm.addSearchableChoiceField(
            functionality,
            leftVL,
            inserters,
            clearers,
            get,
            set,
            alternatives,
            renderSort,
            nameCol1,
            renderCol1,
            nameCol2,
            renderCol2,
            caption,
            true);
    }

    public void addSearchableChoiceFieldRight(final Getter<Object> get, final Setter<Object> set, List<Object> alternatives, Extractor<Object, String> renderSort, String nameCol1, Extractor<Object, String> renderCol1, String nameCol2, Extractor<Object, String> renderCol2, final String caption) {
        AddToForm.addSearchableChoiceField(
            functionality,
            rightVL,
            inserters,
            clearers,
            get,
            set,
            alternatives,
            renderSort,
            nameCol1,
            renderCol1,
            nameCol2,
            renderCol2,
            caption,
            true);
    }

    public void addBooleanFieldLeft(final Getter<Boolean> get, final String caption) {
        AddToForm.addBooleanField(leftVL, inserters, clearers, get, null, caption, true);
    }

    public void addBooleanFieldLeft(final Getter<Boolean> get, final Setter<Boolean> set, final String caption) {
        AddToForm.addBooleanField(leftVL, inserters, clearers, get, set, caption, true);
    }

    public void addTexteditorFieldLeft(final Getter<String> get, final Setter<String> set, final String caption) {
        AddToForm.addTexteditorField(leftVL, inserters, clearers, get, set, caption,true,1.0f);
    }

    public void addLabelLeft(String text) {
        AddToForm.addLabel(leftVL, text, true);
    }

    public void addStringFieldRight(final Getter<String> get, final String caption) {
        AddToForm.addStringField(rightVL, inserters, clearers, get, null, caption, true);
    }

    public void addStringFieldRight(final Getter<String> get, final Setter<String> set, final String caption) {
        AddToForm.addStringField(rightVL, inserters, clearers, get, set, caption, true);
    }

    public void addChoiceFieldRight(final Getter<Object> get, final Setter<Object> set, Collection<Object> alternatives, final String caption) {
        AddToForm.addChoiceField(rightVL, inserters, clearers, get, set, alternatives, caption, true);
    }

    public void addBooleanFieldRight(final Getter<Boolean> get, final String caption) {
        AddToForm.addBooleanField(rightVL, inserters, clearers, get, null, caption, true);
    }

    public void addBooleanFieldRight(final Getter<Boolean> get, final Setter<Boolean> set, final String caption) {
        AddToForm.addBooleanField(rightVL, inserters, clearers, get, set, caption, true);
    }

    public void addTexteditorFieldRight(final Getter<String> get, final Setter<String> set, final String caption) {
        AddToForm.addTexteditorField(rightVL, inserters, clearers, get, set, caption,true,1.0f);
    }

    public void addLabelRight(String text) {
        AddToForm.addLabel(rightVL, text, true);
    }

    public void addAction(String caption, Runnable onClick) {
        Button actionBtn = ButtonFactory.create(caption);
        actionBtn.addClickListener(e -> onClick.run());
        buttonsHL.addComponent(actionBtn);
    }

    public void updateValues() throws Exception {
        for(Callable inserter : inserters) {
            inserter.call();
        }
    }

    public void clearForm() {
        try {
            for(Callable clearer : clearers) {
                clearer.call();
            }
        }
        catch(Exception e) {
            final String msg = "Fel uppstod när formuläret skulle rensas";
            log.error(msg, e);
            CommonNotification.showError(functionality, msg);
        }
    }

    public void persist(Persister p, BaseFunctionalityProvider functionality, String toString, Runnable onPositive) {
        try {
            updateValues();
        }
        catch(Exception e) {
            final String msg = "Värdena kan inte läggas in i DB-raden";
            log.error("Problem: "+msg+", row="+toString, e);
            CommonNotification.showError(functionality, msg);
            return;
        }
        try {
            if(!p.persist()) {
                final String msg = "DB-raden kunde inte skapas";
                log.error(msg+", row="+toString);
                CommonNotification.showError(functionality, msg);
                return;
            }
        }
        catch(Exception e) {
            final String msg = "DB-raden kunde inte skapas";
            log.error("Problem: "+msg+", row="+toString, e);
            CommonNotification.showError(functionality, msg);
            return;
        }
        close();
        onPositive.run();
        CommonNotification.showAssistive(functionality, "Sparat!");
    }

}
