package general.reuse.vaadinreusable.managedapp.install;

import general.reuse.properties.HarddriveProperties;

public class InstallationProperties {

	private static final String pathToConfigFile_property = "prv.managedapp.configfile";
	private static final String pathToConfigFile_default = "/etc/prvconfig/managedapp.properties";

    private static HarddriveProperties hp = new HarddriveProperties(pathToConfigFile_property, pathToConfigFile_default);

	public static void setHarddriveProperties(HarddriveProperties hp) {
		InstallationProperties.hp = hp;
	}

	public static HarddriveProperties getHarddriveProperties() {
		return hp;
	}

	public static Integer getInt(String propertyname) throws Exception {
		return hp.getInt(propertyname);
	}

	public static Long getLong(String propertyname) throws Exception {
		return hp.getLong(propertyname);
	}

	public static Long getLong(String propertyname, Long _default) {
		return hp.getLong(propertyname, _default);
	}

	public static Long[] getLongArray(String propertyname) throws Exception {
		return hp.getLongArray(propertyname);
	}

	public static Double getDouble(String propertyname, Double _default) {
		return hp.getDouble(propertyname, _default);
	}

	public static String getString(String propertyname) throws Exception {
		return hp.getString(propertyname);
	}

	public static String[] getStringArray(String propertyname) throws Exception {
		return hp.getStringArray(propertyname);
	}

	public static Boolean getBoolean(String propertyname, final Boolean defaultValue) {
		return hp.getBoolean(propertyname, defaultValue);
	}

	public static String getString(String propertyname, final String defaultValue) {
		return hp.getString(propertyname, defaultValue);
	}

	public static String getString(HarddriveProperties hp, String propertyname, final String defaultValue) {
		return hp.getString(propertyname, defaultValue);
	}

	public static Integer getInt(String propertyname, final Integer defaultValue) {
		return hp.getCLPProperty(propertyname, defaultValue);
	}

	public static Float getFloat(String propertyname, final Float defaultValue) {
		return hp.getFloat(propertyname, defaultValue);
	}

	public static Integer[] getIntArray(String propertyname) throws Exception {
		return hp.getIntArray(propertyname);
	}

	public static void printProperties() throws Exception {
		hp.printProperties();
	}

	public static final String prv_managedapp_prefix = "prv.managedapp";
	public static final String prv_managedapp_chatactive = prv_managedapp_prefix +".chatactive";
	public static final String prv_managedapp_warnshutdown = prv_managedapp_prefix +".warnshutdown";
	/*
	public static final String jdbc_driver_class_property = prv_managedapp_prefix +".jdbc.driverclass";
	public static final String jdbc_url_property = prv_managedapp_prefix +".jdbc.url";
	public static final String jdbc_username_property = prv_managedapp_prefix +".jdbc.username";
	public static final String jdbc_password_property = prv_managedapp_prefix +".jdbc.password";
*/

	public static final String prv_managedapp_log4j_filename = "managedapp_log4j.properties";
	public static final String prv_managedapp_logoutredirect = prv_managedapp_prefix +"."+"logoutredirect";

}
