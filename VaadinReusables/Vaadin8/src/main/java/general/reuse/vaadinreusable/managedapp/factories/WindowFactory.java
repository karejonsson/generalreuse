package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.ui.Window;

public class WindowFactory {

	private static String cssname = null;

	public static void setCssname(String cssname) {
		WindowFactory.cssname = cssname;
	}
	
	public static Window styles(Window in) {
		if(cssname != null) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static Window create() {
		return styles(new Window());
	}

	public static Window create(String caption) {
		return styles(new Window(caption));
	}

}
