package general.reuse.vaadinreusable.careful;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import com.vaadin.server.StreamResource;
import com.vaadin.ui.Image;

public class CaptchaFunctions {

	public static String generateCaptcha() throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte fraction[] = (new Date()).toString().getBytes();
        md.update(fraction, 0, fraction.length);
        String out_long = Base64.getEncoder().encodeToString(md.digest());
        String out = out_long.substring(0, 8).replace('0', '2').replace('o', '3').replace('O', 'ä').replace('I', '4').replace('l', '6').replace('/', '7').replace('s', 'g').replace('S', 'G').replace('5', 'g').replace('v', '8').replace('V', '9').replace('c', 'a').replace('C', 'A').replace('w', 'f').replace('W', 'F').replace('z', 'e').replace('Z', 'E').replace('u', 'h').replace('U', 'H').replace('x', 'k').replace('X', 'K');
        //System.out.println("Genererad captcha "+out);
        return out;
	}
	
	public static List<Font> getAllFonts() {		
		List<String> defaultFontFamilies = Arrays.asList(new String[] {"Serif", "SansSerif", "Monospaced", "Dialog", "DialogInput" });
		List<Font> usedFonts = new ArrayList<Font>();
		Font all[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
		
		for(Font f : all) {
			String fam = f.getFamily();
			if(defaultFontFamilies.contains(fam)) {
				usedFonts.add(f);
			}
		}
		
		return usedFonts;
	}

	public static void setCaptchaImage(Image captchaImage, String captchaId, int captchaImageWidth, int captchaImageHeight) {
        StreamResource resource = null;

        try {
        	SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        	String filename = "myfilename-" + df.format(new Date()) + ".png";

        	final byte[] captchaChallengeAsJpeg = generateCaptchaImageByteArray(captchaId.toCharArray(), captchaImageWidth, captchaImageHeight);
        	resource = new StreamResource(
        			new StreamResource.StreamSource() {
        				@Override
        				public InputStream getStream() {
        					return new ByteArrayInputStream(captchaChallengeAsJpeg);
        				}
        			}, filename);
        }
        catch(Exception e) {
        	e.printStackTrace();
        }
		captchaImage.setSource(resource);
	}

	public static byte[] generateCaptchaImageByteArray(char code[], int captchaImageWidth, int captchaImageHeight) throws IOException {
		BufferedImage bufferedImage = new BufferedImage(captchaImageWidth, captchaImageHeight, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = bufferedImage.createGraphics();
		List<Font> fonts = getAllFonts();
		Random r = new Random();

		RenderingHints rh = new RenderingHints(
				RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		rh.put(RenderingHints.KEY_RENDERING, 
				RenderingHints.VALUE_RENDER_QUALITY);

		g2d.setRenderingHints(rh);
		
		Color c = colors[r.nextInt(18)];

		GradientPaint gp = new GradientPaint(0, 0, c, 0, captchaImageHeight/2, Color.black, true);
		g2d.setPaint(gp);
		g2d.fillRect(0, 0, captchaImageWidth, captchaImageHeight);
		g2d.setColor(new Color(255, 153, 0));

		int x = 0; 

		for (int i=0; i < code.length; i++) {
			x += 15 + (Math.abs(r.nextInt()) % 15);
			int y = 20 + Math.abs(r.nextInt()) % 35;

			Font font = getRandomFont(fonts, r);
			g2d.setFont(font);
			g2d.setColor(colors[r.nextInt(18)]);
			g2d.drawChars(code, i, 1, x, y);
		}

		g2d.dispose();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "png", baos);

		return baos.toByteArray();
	} 
	
	public static final Color colors[] = new Color[] { 
			Color.blue, Color.cyan, Color.gray, Color.green, 
			Color.red, Color.magenta, Color.orange, Color.pink,
			Color.yellow, Color.GRAY, Color.BLUE, Color.DARK_GRAY,
			Color.CYAN, Color.MAGENTA, Color.PINK, Color.ORANGE,
			Color.GRAY, Color.PINK
			};
	
	public static Font getRandomFont(List<Font> fonts, Random r) {
		Font choice = fonts.get(r.nextInt(fonts.size()));
		return new Font(choice.getFontName(), Font.BOLD, 18+r.nextInt(8));
	}

}
