package general.reuse.vaadinreusable.serverbrowse;

import general.reuse.vaadinreusable.vaadin8ish.LinkV8ish;
import general.reuse.vaadinreusable.vaadin8ish.StreamResourceV8ish;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class OperateOnOneDownloadOperation implements OperateOnOneBrowsed {

    private static Logger logg = Logger.getLogger(OperateOnOneDownloadOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnOneDownloadOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Nedladdning";
    }

    @Override
    public String getDescription() {
        return "Ladda ner fil";
    }

    @Override
    public Runnable getOperate(String filepath) {
        return () -> {
            operate(filepath);
        };
    }

    @Override
    public void operate(String filepath) {
        logg.info("filepath="+filepath);

        File f = new File(filepath);
        if(!f.exists()) {
            String msg = "Filen finns inte";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }

        if(f.isDirectory()) {
            String msg = "Det är en katalog";
            logg.info(msg+". filepath="+filepath);
            functionality.showError(msg);
            return;
        }

        DocumentLink receiver = new DocumentLink(new File(filepath));
        WindowV8ish win = new WindowV8ish();
        win.setHeight("60px");
        win.setWidth("40%");
        win.setContent(receiver);
        functionality.addWindow(win, "Fil att ladda ner");
    }

    public class DocumentLink extends LinkV8ish {

        private static final long serialVersionUID = -861816214400624227L;
        private File file;

        public DocumentLink(File file) {
            super();
            this.file = file;
            setCaption("Nedladdning av fil");
            setDescription("Hämta fil till din dator");
            //setTargetName("_blank");
        }

        @Override
        public void attach() {
            super.attach(); // Must call.

            StreamResourceV8ish.StreamSource source = new StreamResourceV8ish.StreamSource() {
                public InputStream getStream() {
                    try {
                        return new FileInputStream(file);
                    }
                    catch(Exception e) {
                        String msg = "Nedladdningen misslyckades. Underliggande felmeddelande\n"+e.getMessage();
                        logg.error(msg + ". filepath=" + file.getAbsolutePath(), e);
                        functionality.showError(msg);
                        return null;
                    }
                }
            };
            StreamResourceV8ish resource = new StreamResourceV8ish(source, file.getName());
            //resource.setMIMEType(df.getMimetype());
            resource.setCacheTime(0);
            setResource(resource);
        }
    }

}
