package general.reuse.vaadinreusable.managedapp.functionality;

import com.vaadin.ui.*;
import org.apache.log4j.MDC;
import org.apache.log4j.Logger;
import general.reuse.vaadinreusable.managedapp.install.SessionSymbols;
import general.reuse.vaadinreusable.managedapp.session.SessionRegistry;

import general.reuse.vaadinreusable.managedapp.texts.ActivityTexts;
import general.reuse.vaadinreusable.managedapp.texts.ButtonTexts;

import java.util.*;
import java.util.concurrent.Future;

import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.Page;
import com.vaadin.server.Page.Styles;
import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.factories.CompleteStyling;
import general.reuse.vaadinreusable.managedapp.factories.LabelFactory;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;

public class DefaultBaseFunctionalityProvider implements BaseFunctionalityProvider {
	
	private static final Logger logg = Logger.getLogger(DefaultBaseFunctionalityProvider.class);
	private static final int listLength = 50;

	protected UI ui = null;
	protected BaseAdminFunctionalityProvider admin = null;

	private String textualName = null;
	private String email = null;
	
	private String userid = null;

	private String userRolename = null;

	private String dateformat = "yyyy-MM-dd";
	protected String uidname = "uid";
	protected String tidname = "tid";
	
	protected Date last = new Date();

	private Map<Object, Runnable> lookup = new HashMap<>();

	public DefaultBaseFunctionalityProvider(UI ui, String dateformat, String userRolename) {
		this(ui, null, dateformat, userRolename);
	}
	public DefaultBaseFunctionalityProvider(UI ui, BaseAdminFunctionalityProvider admin, String dateformat, String userRolename) {
		this(ui, admin, dateformat, userRolename, "uid", "tid");
	}

	public DefaultBaseFunctionalityProvider(UI ui, BaseAdminFunctionalityProvider admin, String dateformat, String userRolename, String uidname, String tidname) {
		this.ui = ui;
		this.admin = admin;
		this.userRolename = userRolename;
		last = new Date();
		this.uidname = uidname;
		this.tidname = tidname;
		updateUidname();
		if(dateformat != null && dateformat.trim().length() > 2) {
			this.dateformat = dateformat;
		}
		if(ui != null) {
			ui.addShortcutListener(new ShortcutListener("Return shortcut", ShortcutAction.KeyCode.ENTER, null) {
				@Override
				public void handleAction(Object sender, Object target) {
					Runnable r = lookup.get(target);
					if (r != null) {
						r.run();
					}
				}
			});
		}
		if(admin != null) {
			admin.setFunctionality(this);
		}
	}

	public String getUserRolename() {
		return userRolename;
	}
	
	// https://blog.oio.de/2010/11/09/logging-additional-information-like-sessionid-in-every-log4j-message/
	public void updateUidname() {
		SessionRegistry.clicks.increment();
		last = new Date();
		try {
			if(uidname != null) {
				MDC.put(uidname, (Object) (userid != null ? userid : ""));
			}
			if(tidname != null) {
				MDC.put(tidname, (Object) (Thread.currentThread().getId()));
			}
		}
		catch(Throwable e) {
			logg.error("FEL", e);
			Throwable t = e.getCause();
			if(t != null) {
				t.printStackTrace();
				logg.error("Underliggande 2", t);
			}
			else {
				logg.error("Inget underliggande 2", t);
			}
		}
	}

	private Styles styles = null;

	protected void fixPage() {
		SessionRegistry.views.increment();
		if(styles != null) {
			 return;
		}
		try {
			styles = Page.getCurrent().getStyles();
			final String def = CompleteStyling.getStyles();
			final String style = def;//+pal;
			logg.info("Style = "+style);
			styles.add(style);
		}
		catch(Exception e) {}
	}

	@Override
	public void start() {
		updateUidname();
		setActivity(ActivityTexts.start, false);
		logg.info("Åter till start för "+getUsername());
		VerticalLayout vl = LayoutFactory.createVertical(getHaveMargins());
		vl.addComponent(LabelFactory.create("Funktionalitet att återgå till startläget ej tillhanda"));
		if(admin != null) {
			Button adminBtn = ButtonFactory.create(ButtonTexts.admin);
			adminBtn.addClickListener(e -> admin.openAdmin(new CommonRunnable(() -> start())));
			vl.addComponent(adminBtn);			
		}
		ui.setContent(vl);
		fixPage();
	}

	@Override
	public void addWindow(Window win, String winType) {
		updateUidname();
		//logg.info("Öppnar underordnat fönster: "+winType);
		win.center();
		addWindow(win, true, winType);
	}

	@Override
	public void addWindow(Window win, boolean modal, String winType) {
		logg.info("Öppnar underordnat fönster: "+winType);
		win.setModal(modal);
		setActivity("Fönster: "+winType, false);
        ui.addWindow(win);
	}

	@Override
	public BaseAdminFunctionalityProvider getAdminFunctionality() {
		return admin;
	}

	@Override
	public void setUsername(String userid) {
		updateUidname();
		userid = userid.trim().toUpperCase();
		this.userid = userid;
	}
	
	@Override
	public String getUsername() {
		return userid;
	}

	@Override
	public String getDateformat() {
		return dateformat;
	}
	
	public String toString() {
		StringBuffer out = new StringBuffer();
		out.append("DefaultFunctionality. UI "+(ui != null));
		out.append(", admin "+(admin != null ? "{ "+admin.toString()+" }" : "null"));
		out.append(", dateformat "+dateformat);
		out.append(", userid "+userid);
		return out.toString();
	}

	@Override
	public void setTextualName(String textualName) {
		this.textualName = textualName;
	}

	@Override
	public String getTextualName() {
		return textualName;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public CommonRunnable getRunnableForReturnToCurrentContent() {
		if(!isFunctional()) {
			return new CommonRunnable(() -> {});
		}
		String filename = Thread.currentThread().getStackTrace()[2].getFileName();
		final int linenumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
		final Component current = ui.getContent();  // Här saknas inte poolname
		final String title = getActivity();
		final boolean stateful = getStateful();
		CommonRunnable out = new CommonRunnable(() -> {
			setActivity(title, stateful);
			ui.setContent(current);
		}, "Fil "+filename+", rad "+linenumber+", aktivitet "+title+", stateful "+stateful);
		return out;
	}
	
	private String poolname = null;
	
	/*
	@Override
	public void setPoolname(String poolname) {
		this.poolname = poolname;
	}
	*/

	@Override
	final public String getPoolname() {
		// Alla anrop till DB APIet görs med det poolnamn som kommer härifrån. Om man vill rikta om Akta till flera
		// källor gör man en implementering enligt det bortkommenterade ovan.
		return poolname;
	}
	
	@Override
	public void openChat(CommonRunnable onBack) {
		updateUidname();
		setActivity(ActivityTexts.openChat, false);
		logg.info("Öppnar funktion för intern chatt - ej tillhanda");
		VerticalLayout vl = LayoutFactory.createVertical(getHaveMargins());
		vl.addComponent(LabelFactory.create("Funktionalitet för att öppna interna chatten ej tillhanda"));
		Button backBtn = ButtonFactory.create(ButtonTexts.revert);
		backBtn.addClickListener(e -> onBack.run());
		HorizontalLayout btns = LayoutFactory.createHorizontal(getHaveMargins());
		btns.addComponent(backBtn);
		if(admin != null) {
			Button adminBtn = ButtonFactory.create(ButtonTexts.admin);
			adminBtn.addClickListener(e -> admin.openAdmin(getRunnableForReturnToCurrentContent()));
			btns.addComponent(adminBtn);
		}
		vl.addComponent(btns);
		fixPage();
	}

	private byte[] imageBytes = null;

	public long chatSince = 0;

	@Override
	public void setChatSince() {
		setChatSince(System.currentTimeMillis());
	}

	public void setChatSince(long chatSince) {
		this.chatSince = chatSince;
	}

	@Override
	public long getChatSince() {
		return chatSince;
	}

	private String activity;
	private boolean stateful = false;

	@Override
	public String getActivity() {
		return activity;
	}

	@Override
	public boolean getStateful() {
		return stateful;
	}

	@Override
	public void setActivity(String activity, boolean stateful) {
		this.activity = activity;
		this.stateful = stateful;
		if(ui == null) {
			return;
		}
		ui.getPage().setTitle(activity);
		events.add(new UserInteractionEvent(new Date(), activity, stateful));
		while(events.size() > listLength) {
			events.remove(0);
		}
	}

	private ArrayList<UserInteractionEvent> events = new ArrayList<>();

	@Override
	public UserInteractionEvent[] getActivityEvents() {
		UserInteractionEvent[] out = new UserInteractionEvent[events.size()];
		for(int i = 0 ; i < events.size() ; i++) {
			out[i] = events.get(events.size()-i-1);
		}
		return out;
	}

	@Override
	public boolean isFunctional() {
		return ui != null;
	}

	@Override
	public Future<Void> access(Runnable runnable) {
		return ui.access(runnable);
	}

	@Override
	public Boolean getHaveMargins() {
		return false;
	}

	@Override
	public String getStateDescription() {
		return "Ingen beskrivning";
	}

	@Override
	public boolean possiblyWulnerable() {
		return false;
	}

	@Override
	public void setLocation(String url) {
		ui.getPage().setLocation(url);
	}

	@Override
	public Date getLast() {
		return last;
	}

	@Override
	public void addReturnKeyListener(Object o, CommonRunnable r) {
		lookup.put(o, r);
	}

	@Override
	public void clearReturnKeyListener(Object o) {
		lookup.remove(o);
	}

	@Override
	public void clearReturnKeyListeners() {
		lookup.clear();
	}

	private Map<String, String> parameters = null;

	@Override
	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	@Override
	public Map<String, String> getParameters() {
		return parameters;
	}

	@Override
	public void logout(CommonRunnable onBack) {
		fixPage();
		setActivity(ActivityTexts.logout, false);
		VerticalLayout vl = LayoutFactory.createVertical(getHaveMargins());
		vl.addComponent(LabelFactory.create("Utloggning är gjord"));
		ui.setContent(vl);
		clearAllForLogout();
	}

	private Map<String, Object> m = new HashMap<>();

	@Override
	public Object get(String k) {
		return m.get(k);
	}

	@Override
	public void put(String k, Object o) {
		m.put(k, o);
	}

	public void clearAllForLogout() {
		updateUidname();
		logg.info("Rensar upp inför utlogging");
		SessionRegistry.remove(this);
		destroyAllInternals();
	}

	@Override
	public void destroyAllInternals() {
		updateUidname();
		logg.info("Förstör intern data för användare "+getUsername(), new Throwable());
		styles = null;
		admin = null;
		textualName = null;
		email = null;
		dateformat = null;
		uidname = null;
		tidname = null;
		last = null;
		lookup = null;
		ui.getSession().setAttribute(SessionSymbols.userlookup, null);
		ui = null;
		poolname = "Något som med säkerhet är fel så man inte kommer åt databasen om det förstörts";
	}


}
