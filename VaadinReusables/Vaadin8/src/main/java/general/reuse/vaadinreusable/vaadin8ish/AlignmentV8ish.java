package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.Alignment;

public final class AlignmentV8ish {

    public static final Alignment MIDDLE_LEFT = Alignment.MIDDLE_LEFT;
    public static final Alignment MIDDLE_CENTER = Alignment.MIDDLE_CENTER;
    public static final Alignment MIDDLE_RIGHT = Alignment.MIDDLE_RIGHT;

}
