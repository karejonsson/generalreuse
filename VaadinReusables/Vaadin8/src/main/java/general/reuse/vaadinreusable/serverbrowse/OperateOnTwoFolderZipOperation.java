package general.reuse.vaadinreusable.serverbrowse;

import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class OperateOnTwoFolderZipOperation implements OperateOnTwoBrowsed_source_target {

    private static Logger logg = Logger.getLogger(OperateOnTwoExpandZipOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnTwoFolderZipOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Zippning";
    }

    @Override
    public String getDescription() {
        return "Zippar något till annan plats";
    }

    @Override
    public Runnable getOperate(File first, String second) {
        return () -> {
            operate(first, second);
        };
    }

    @Override
    public void operate(File source, String filepath) {
        logg.info("source=" + source.getAbsolutePath() + ", filepath=" + filepath);

        if(!source.exists()) {
            String msg = "Källkatalogen finns inte";
            logg.info(msg + ". source=" + source.getAbsolutePath());
            functionality.showError(msg);
            return;
        }
        if(!source.isDirectory()) {
            String msg = "Källkatalogen är inte en katalog";
            logg.info(msg + ". source=" + source.getAbsolutePath());
            functionality.showError(msg);
            return;
        }

        File f = new File(filepath);
        if (!f.exists()) {
            String msg = "Målkatalogen finns inte";
            logg.info(msg + ". filepath=" + filepath);
            functionality.showError(msg);
            return;
        }
        if (!f.isDirectory()) {
            String msg = "Målkatalogen är inte en katalog";
            logg.info(msg + ". filepath=" + filepath);
            functionality.showError(msg);
            return;
        }
        File p = f.getParentFile();
        if (!p.isDirectory()) {
            String msg = "Platsen för målzippen är inte en katalog";
            logg.info(msg + ". filepath=" + filepath);
            functionality.showError(msg);
            return;
        }
        String folder = f.getAbsolutePath().trim();
        String zipParentFolder = p.getAbsolutePath().trim();
        if(zipParentFolder.startsWith(folder)) {
            String msg = "Platsen för målzippen får inte vara i den katalog som zippas";
            logg.info(msg + ". filepath=" + filepath);
            functionality.showError(msg);
            return;
        }

        List<File> files = new ArrayList<>();
        files.add(source);

        try {
            OperateOnOneFolderZipOperation.zip(files, filepath);
        }
        catch(Exception e) {
            String msg = "Fel vid zippningen. Underliggande felmeddelande\n"+e.getMessage();
            logg.error(msg + ". filepath=" + filepath, e);
            functionality.showError(msg);
            return;
        }
    }

}