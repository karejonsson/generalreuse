package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.CheckBox;

public class CheckBoxFactory {

	private static String cssname = null;

	public static void setCssname(String cssname) {
		CheckBoxFactory.cssname = cssname;
	}
	
	public static CheckBox styles(CheckBox in) {
		if(cssname != null) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static CheckBox create(String caption) {
		return styles(new CheckBox(caption));
	}

	public static CheckBox create(String caption, VaadinIcons vi) {
		return create(vi, caption);
	}

	public static CheckBox create(VaadinIcons vi, String caption) {
		CheckBox out = new CheckBox(caption);
		out.setIcon(vi);
		return styles(out);
	}

	public static CheckBox create() {
		return styles(new CheckBox());
	}

}
