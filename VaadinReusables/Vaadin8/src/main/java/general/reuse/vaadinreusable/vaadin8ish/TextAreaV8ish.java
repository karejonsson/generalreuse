package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.TextArea;

public class TextAreaV8ish extends TextArea {
    public TextAreaV8ish() {
        super();
    }
    public TextAreaV8ish(String caption) {
        super(caption);
    }
}
