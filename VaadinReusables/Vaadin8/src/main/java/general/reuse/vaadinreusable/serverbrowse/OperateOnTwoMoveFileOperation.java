package general.reuse.vaadinreusable.serverbrowse;

import org.apache.log4j.Logger;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class OperateOnTwoMoveFileOperation implements OperateOnTwoBrowsed_source_target {

    private static Logger logg = Logger.getLogger(OperateOnTwoMoveFileOperation.class);

    private ForeignFrameworksResources functionality = null;

    public OperateOnTwoMoveFileOperation(ForeignFrameworksResources functionality) {
        this.functionality = functionality;
    }

    @Override
    public String getName() {
        return "Filflytt";
    }

    @Override
    public String getDescription() {
        return "Flytta en fil";
    }

    @Override
    public Runnable getOperate(File first, String second) {
        return () -> {
            operate(first, second);
        };
    }

    @Override
    public void operate(File first, String second) {
        if(!first.exists()) {
            String msg = "Filen att flytta finns inte";
            logg.info(msg + ". filepath=" + first.getAbsolutePath());
            functionality.showError(msg);
            return;
        }
        if(first.isDirectory()) {
            String msg = "Filen att flytta är en katalog";
            logg.info(msg + ". filepath=" + first.getAbsolutePath());
            functionality.showError(msg);
            return;
        }
        File target = new File(second);
        if(!target.exists()) {
            String msg = "Målkatalogen finns inte";
            logg.info(msg + ". filepath=" + first.getAbsolutePath());
            functionality.showError(msg);
            return;
        }
        if(!target.isDirectory()) {
            String msg = "Målkatalogen är inte en katalog";
            logg.info(msg + ". filepath=" + first.getAbsolutePath());
            functionality.showError(msg);
            return;
        }
        try {
            Path source = Paths.get(first.getCanonicalPath());
            Path targetPath = Paths.get(target.getCanonicalPath());
            Files.move(source, targetPath.resolve(source.getFileName()));
        }
        catch(Exception e) {
            String msg = "Fel vid flytt av fil. Underliggande felmeddelande\n"+e.getMessage();
            logg.error(msg + ". filepath=" + first.getAbsolutePath(), e);
            functionality.showError(msg);
            return;
        }
    }

}
