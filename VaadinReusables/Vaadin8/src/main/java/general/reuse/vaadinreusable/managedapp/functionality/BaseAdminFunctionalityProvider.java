package general.reuse.vaadinreusable.managedapp.functionality;

import com.vaadin.ui.Window;

public interface BaseAdminFunctionalityProvider {
	
	BaseFunctionalityProvider getFunctionality();
	void updateUidname();

	void start();
	void openAdmin(CommonRunnable toHere);


	void addWindow(Window win, String winType);
	void setFunctionality(BaseFunctionalityProvider baseFunctionalityProvider);
	void openUserManagement(CommonRunnable onBack);
	void openActiveManagement(CommonRunnable onBack);
	void openFileHandling(CommonRunnable onBack);

}
