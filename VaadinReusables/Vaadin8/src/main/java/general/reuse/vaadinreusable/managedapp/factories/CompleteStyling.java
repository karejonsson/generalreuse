package general.reuse.vaadinreusable.managedapp.factories;

import org.apache.log4j.Logger;

public class CompleteStyling {
	
	private static Logger logg = Logger.getLogger(CompleteStyling.class);

	public static void add(StringBuffer s, String styling, String classname) {
		String p = styling;
		if(p != null && p.trim().length() > 2) {
			p = p.replaceAll(";", ";\n\t").replaceAll("\\{", "{\n\t");
			s.append("."+classname+" "+p+"\n");
		}
	}

	public static void add(StringBuffer s, String styling, String classname, String parentclassname) {
		String p = styling;
		if(p != null && p.trim().length() > 2) {
			s.append("."+classname+", #"+parentclassname+" "+p+"\n");
		}
	}

	private static String styles = null;
	
	static {
		styles = "";
	}

	public static String getStyles() {
		return styles;
	}
	public static void setStyles(String styles ) {
		CompleteStyling.styles = styles;
	}

}
