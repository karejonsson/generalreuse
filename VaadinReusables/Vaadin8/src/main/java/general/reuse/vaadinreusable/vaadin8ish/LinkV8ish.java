package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.ui.Link;

public class LinkV8ish extends Link {

    public LinkV8ish() {
        super();
    }

    public LinkV8ish(String caption, ResourceV8ish resource) {
        super(caption, resource);
    }

}
