package general.reuse.vaadinreusable.managedapp.factories;

import com.vaadin.ui.ComboBox;

public class ComboBoxFactory {

	private static String cssname = null;

	public static void setCssname(String cssname) {
		ComboBoxFactory.cssname = cssname;
	}

	public static <T> ComboBox<T> styles(ComboBox<T> in) {
		if(cssname != null) {
			in.setPrimaryStyleName(cssname);
		}
		return in;
	}

	public static <T> ComboBox<T> create(String caption) {
		return styles(new ComboBox<T>(caption));
	}

	public static <T> ComboBox<T> create() {
		return styles(new ComboBox<T>());
	}

}
