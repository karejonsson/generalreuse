package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.event.selection.SingleSelectionListener;

public interface SingleSelectionListenerV8ish<T> extends SingleSelectionListener<T> {
}
