package general.reuse.vaadinreusable.serverbrowse;

import java.io.File;

public interface OperateOnTwoBrowsed_source_target {

    String getName();
    String getDescription();
    void operate(File first, String second);
    Runnable getOperate(File first, String second);

}
