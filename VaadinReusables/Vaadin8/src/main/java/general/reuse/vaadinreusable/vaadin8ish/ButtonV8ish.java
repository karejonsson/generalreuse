package general.reuse.vaadinreusable.vaadin8ish;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;

public class ButtonV8ish extends Button implements ComponentV8ish {
    public ButtonV8ish() {
        super();
    }
    public ButtonV8ish(String caption) {
        super(caption);
    }
    public ButtonV8ish(String caption, VaadinIcons icon) {
        super(caption, icon);
    }
    public ButtonV8ish(VaadinIcons icon) {
        super(icon);
    }

}
