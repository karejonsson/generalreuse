package general.reuse.vaadinreusable.managedapp.install;

public class DatabaseProperties {
	
	public static final String email_smtp_starttls_enable = "email.smtp.starttls.enable";
	public static final String email_smtp_auth = "email.smtp.auth";
	public static final String email_smtp_host = "email.smtp.host";
	public static final String email_smtp_port = "email.smtp.port";
	public static final String email_smtp_password = "email.smtp.password";

    public static final String login_onlyfor = "login.onlyfor";
    public static final String warningtime_millis = "warningtime.millis";

}
