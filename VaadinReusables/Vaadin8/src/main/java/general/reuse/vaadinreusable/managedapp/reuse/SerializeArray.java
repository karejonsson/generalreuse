package general.reuse.vaadinreusable.managedapp.reuse;

import java.util.ArrayList;
import java.util.List;

public class SerializeArray {

	public static int[] deserialize(String sa) {
		if(sa == null) {
			return null;
		}
		sa = sa.replaceAll("\\[", " ");
		sa = sa.replaceAll("\\]", " ");
		sa = sa.replaceAll(",", " ");
		String[] pieces = sa.split(" ");
		List<Integer> outL = new ArrayList<>();
		for(String piece : pieces) {
			if(piece == null) {
				continue;
			}
			piece = piece.trim();
			if(piece.length() == 0) {
				continue;
			}
			outL.add(Integer.parseInt(piece));
		}
		int[] out = new int[outL.size()];
		for(int i = 0 ; i < outL.size() ; i++) {
			out[i] = outL.get(i);
		}
		return out;
	}

	public static String[] deserializeCP(String sa) {
		if(sa == null) {
			return null;
		}
		sa = sa.replaceAll("\\[", " ");
		sa = sa.replaceAll("\\]", " ");
		sa = sa.replaceAll(",", " ");
		String[] pieces = sa.split(" ");
		List<String> outL = new ArrayList<>();
		for(String piece : pieces) {
			if(piece == null) {
				continue;
			}
			piece = piece.trim();
			if(piece.length() == 0) {
				continue;
			}
			outL.add(piece);
		}
		String[] out = new String[outL.size()];
		for(int i = 0 ; i < outL.size() ; i++) {
			out[i] = outL.get(i);
		}
		return out;
	}

	public static String serialize(int[] ia) {
		if(ia == null) {
			return null;
		}
		if(ia.length == 0) {
			return "[]";
		}
		if(ia.length == 1) {
			return "["+ia[0]+"]";
		}
		StringBuffer out = new StringBuffer();
		out.append("["+ia[0]);
		for(int i = 1 ; i < ia.length ; i++) {
			out.append(", "+ia[i]);
		}
		return out.toString()+"]";
	}

	public static String serialize(String[] ia) {
		if(ia == null) {
			return null;
		}
		if(ia.length == 0) {
			return "[]";
		}
		if(ia.length == 1) {
			return "["+ia[0]+"]";
		}
		StringBuffer out = new StringBuffer();
		out.append("["+ia[0]);
		for(int i = 1 ; i < ia.length ; i++) {
			out.append(", "+ia[i]);
		}
		return out.toString()+"]";
	}

}
