package general.reuse.vaadinreusable.choser;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import general.reuse.interfaces.Extractor;
import general.reuse.vaadinreusable.choser.ChoseFromListVerticalLayout.ChoiceMadeNotifier;
import general.reuse.vaadinreusable.managedapp.factories.ButtonFactory;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;
import general.reuse.vaadinreusable.managedapp.reuse.CommonNotification;

public class ChoseFromListHorizontalLayout<T> extends HorizontalLayout {
	
	private static final long serialVersionUID = -2594061578616805386L;

	public interface ToString<T> {
		String render(T t);
	}
	
	public static abstract class Searcher<T> {
		protected ChoiceMadeNotifier<T> searchersChoiceMadeNotifier = null;
		abstract public void search(List<T> list);
		final public void setChoiceMadeNotifier(ChoiceMadeNotifier<T> searchersChoiceMadeNotifier) {
			this.searchersChoiceMadeNotifier = searchersChoiceMadeNotifier;
		}
	}
	
	private List<T> list = null;
	private Extractor<T, String> renderer = null;
	private Searcher<T> searcher = null;
	private List<ComboBoxListItem<T>> cbobjects = null;
	private String title = null;
	private Runnable onCancel = null;
	private Runnable onDone = null;
	private ChoiceMadeNotifier<T> choiceMadeNotifier = null;
	
	private ComboBox<ComboBoxListItem<T>> userChoice = null;

	public ChoseFromListHorizontalLayout(List<T> list, Extractor<T, String> renderer, String title) {
		this(list, renderer, null, title, true);
	}

	public ChoseFromListHorizontalLayout(List<T> list, Extractor<T, String> renderer, String title, boolean haveButtons) {
		this(list, renderer, null, title, haveButtons);
	}

	public ChoseFromListHorizontalLayout(List<T> list, Extractor<T, String> renderer, Searcher<T> searcher, String title) {
		this(list, renderer, searcher, title, true);
	}

	public ChoseFromListHorizontalLayout(List<T> list, Extractor<T, String> renderer, Searcher<T> searcher, String title, boolean haveButtons) {
		LayoutFactory.styles(this, true);
		this.list = list;
		this.renderer = renderer;
		this.searcher = searcher;
		this.title = title;
		
		userChoice = new ComboBox<>(title);

		cbobjects = new ArrayList<>();
		list.forEach(it -> cbobjects.add(new ComboBoxListItem<>(it, renderer)));
		userChoice.setItems(cbobjects);
		
		addComponent(userChoice);

		if(haveButtons) {
			HorizontalLayout buttons = LayoutFactory.createHorizontal(true);
			Button cancelBtn = ButtonFactory.create("Avbryt");
			cancelBtn.addListener(e -> { out = null; if(onCancel != null) { onCancel.run(); } });
			buttons.addComponent(cancelBtn);
			Button doneBtn = ButtonFactory.create("Klar");
			doneBtn.addListener(e -> { out = getCurrentUserOption(); if(onDone != null) { onDone.run(); } });
			buttons.addComponent(doneBtn);
			if(searcher != null) {
				Button searchBtn = ButtonFactory.create("Sök");
				searchBtn.addListener(e -> search());
				buttons.addComponent(searchBtn);
			}
			addComponent(buttons);
		}
		else {
			if(searcher != null) {
				Button searchBtn = ButtonFactory.create("Sök");
				searchBtn.addListener(e -> search());
				addComponent(searchBtn);
			}
		}
		if(searcher != null) {
			searcher.setChoiceMadeNotifier(it -> {
				if(it == null) {
					CommonNotification.showAssistive("Inget val gjordes");
					return;
				}
				for(ComboBoxListItem<T> cbobject : cbobjects) {
					if(cbobject.getItem() == it) {
						userChoice.setSelectedItem(cbobject);
						return;
					}
				}
				CommonNotification.showError("Valet finns inte bland valmöjligheterna");
			});
		}
		setInternalWidthFull();
		setInternalHeightUndefined();
	}

	public void setEmptySelectionAllowed(boolean emptySelectionAllowed) {
		userChoice.setEmptySelectionAllowed(emptySelectionAllowed);
	}

	public void setInternalTextInputAllowed(boolean editable) {
		userChoice.setTextInputAllowed(editable);
	}

	public void setInternalWidth(String width) {
		userChoice.setWidth(width);
	}

	public void setInternalWidthFull() {
		userChoice.setWidthFull();
	}

	public void setInternalWidthUndefined() {
		userChoice.setWidthUndefined();
	}

	public void setInternalHeight(String height) {
		userChoice.setHeight(height);
	}

	public void setInternalHeightFull() {
		userChoice.setHeightFull();
	}

	public void setInternalHeightUndefined() { userChoice.setHeightUndefined(); }

	public void setInternalSizeFull() { userChoice.setSizeFull(); }

	public void setInternalSizeUndefined() { userChoice.setSizeUndefined(); }

	final public void setChoiceMadeNotifier(ChoiceMadeNotifier<T> choiceMadeNotifier) {
		this.choiceMadeNotifier = choiceMadeNotifier;
		if(choiceMadeNotifier != null) {
			userChoice.addListener(e -> {
				setChosenObject(getCurrentUserOption());
				choiceMadeNotifier.choiceMade(getCurrentUserOption());
			});
			userChoice.addValueChangeListener(e -> {
				setChosenObject(getCurrentUserOption());
				choiceMadeNotifier.choiceMade(getCurrentUserOption());
			});
		}
	}
	
	private void search() {
		searcher.search(list);
	}
	
	public void setOnCancel(Runnable onCancel) {
		this.onCancel = onCancel;
	}

	public void setOnDone(Runnable onDone) {
		this.onDone = onDone;
	}

	private T out = null;
	
	public T getChosenObject() {
		if(out == null) {
			T some = getCurrentUserOption();
			if(some != null) {
				out = some;
			}
		}
		return out;
	}

	public void setChosenObject() {
		out = getCurrentUserOption();
		setChosenObject(out);
	}

	public void setChosenObject(T obj) {
		for(ComboBoxListItem<T> cbobject : cbobjects) {
			if(cbobject.getItem().equals(obj)) {
				userChoice.setSelectedItem(cbobject);
				//System.out.println("ChoseFromListHorizontalLayout.setChosenObject(...) träff. obj="+obj);
				return;
			}
		}
		//System.out.println("ChoseFromListHorizontalLayout.setChosenObject(...) fick ingen träff. obj="+obj);
	}
	
	private T getCurrentUserOption() {
		ComboBoxListItem<T> out = null;
		try {
			Optional<ComboBoxListItem<T>> op = userChoice.getSelectedItem();
			out = op.get();
		}
		catch(Exception e) {
			//e.printStackTrace();
		}
		if(out == null) {
			return null;
		}
		return out.getItem();
	}

	private class ComboBoxListItem<T> {
		private T t = null;
		private Extractor<T, String> ts = null;
		public ComboBoxListItem(T t, Extractor<T, String> ts) {
			this.t = t;
			this.ts = ts;
		}
		public String toString() {
			return ts.get(t);
		}
		public T getItem() {
			return t;
		}
	}

}
