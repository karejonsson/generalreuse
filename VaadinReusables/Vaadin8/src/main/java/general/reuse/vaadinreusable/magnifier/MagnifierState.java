package general.reuse.vaadinreusable.magnifier;

import general.reuse.vaadinreusable.vaadin8ish.wrapp.JavaScriptComponentStateV8ish;

public class MagnifierState extends JavaScriptComponentStateV8ish {

	private static final long serialVersionUID = -2374185162037353601L;
	
	public float zoomFactor = 1.2f; 
	public String imageUrl = new String();
	public String zoomImageUrl = new String();	
    public String syncedMagnifierId = new String();
    	
}
