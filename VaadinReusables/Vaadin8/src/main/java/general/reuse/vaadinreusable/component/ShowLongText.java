package general.reuse.vaadinreusable.component;

import com.vaadin.ui.*;

public class ShowLongText extends Window {
	
	private static final long serialVersionUID = 7884499119391716345L;
	public final static double outer = 0.8;
	public final static double outer_W = outer;
	public final static double outer_H = outer;
	public final static double inner = 0.68;
	public final static double inner_W = inner;
	public final static double inner_H = inner;

	private UI ui = null;
	private TextArea layout = null;

	public ShowLongText(String text) {
		this(null, "", text, false);
	}
	public ShowLongText(UI ui, String text) {
		this(ui, "", text, false);
	}

	public ShowLongText(String frame, String text) {
		this(null, frame, text, false);
	}

	public ShowLongText(UI ui, String frame, String text) {
		this(ui, frame, text, false);
	}

	public ShowLongText(String text, boolean enabled) {
		this(null, "", "Specifikation", "Avbryt", text, enabled);
	}

	public ShowLongText(UI ui, String text, boolean enabled) {
		this(ui, "", "Specifikation", "Avbryt", text, enabled);
	}

	public ShowLongText(String frame, String text, boolean enabled) {
		this(null, "", "Specifikation", "Avbryt", text, enabled);
	}

	public ShowLongText(UI ui, String frame, String text, boolean enabled) {
		this(ui, "", "Specifikation", "Avbryt", text, enabled);
	}

	public ShowLongText(String frame, String componentText, String closeButtonText, String text, boolean enabled) {
		this(null, frame, componentText, closeButtonText, text, enabled);
	}

	public ShowLongText(UI ui, String frame, String componentText, String closeButtonText, String text, boolean enabled) {
		super(frame);
		this.ui = (ui != null) ? ui : UI.getCurrent();
		setModal(true);

		VerticalLayout content = new VerticalLayout();
		content.setSizeFull();

		layout = new TextArea(componentText);
		if(text != null) {
			layout.setValue(text);
		}
		layout.setEnabled(enabled);
		layout.setSizeFull();
		content.addComponent(layout);
		content.setExpandRatio(layout, 1.0f);

		HorizontalLayout buttons = new HorizontalLayout();
		
		Button cancelBtn = new Button(closeButtonText);
		cancelBtn.addClickListener(new Button.ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(Button.ClickEvent event) {
				close();
			}
		});
		buttons.addComponent(cancelBtn);
		
		content.addComponent(buttons);
		setContent(content);
		setWidth(""+((int) 100*outer)+"%");
		setHeight(""+((int) 100*outer)+"%");
	}
	
    public void setEnabledText(boolean enabledState) {
    	layout.setEnabled(enabledState);
    }   

}

