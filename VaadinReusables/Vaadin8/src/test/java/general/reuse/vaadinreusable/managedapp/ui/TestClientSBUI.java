package general.reuse.vaadinreusable.managedapp.ui;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import general.reuse.properties.HarddriveProperties;
import general.reuse.vaadinreusable.login.CredentialsAndGroupLoginPage;
import general.reuse.vaadinreusable.login.CredentialsAndGroupVerifyer;
import general.reuse.vaadinreusable.managedapp.access.TestCredentialsAndGroupVerifyer;
import general.reuse.vaadinreusable.managedapp.access.TestUser;
import general.reuse.vaadinreusable.managedapp.config.FunctionalityFactory;
import general.reuse.vaadinreusable.managedapp.functionality.CommonRunnable;
import general.reuse.vaadinreusable.managedapp.install.InstallationProperties;
import general.reuse.vaadinreusable.managedapp.install.SessionSymbols;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@SpringUI(path = "/")
public class TestClientSBUI extends TestClientUI {
	
	private static final long serialVersionUID = -4848154171360983992L;

    private CredentialsAndGroupVerifyer verifyer = null;

	public static void main(String[] args) {
		SpringApplication.run(TestClientSBUI.class, args);
	}
	
	public TestClientSBUI() {
	}

	@Override
	protected void init(VaadinRequest request) {
    	HarddriveProperties hp = InstallationProperties.getHarddriveProperties();
    	Map<String, String> map = new HashMap<>();
    	String adminGroup = "Administratör";
    	map.put("Administratör", adminGroup);
    	map.put("Användare", "Användare");
    	String userGroup = "Användare";
    	verifyer = new TestCredentialsAndGroupVerifyer(hp, InstallationProperties.prv_managedapp_prefix, SessionSymbols.userlookup, map);
    	onCredentialSuccess = new CommonRunnable(() -> {
				TestUser person = (TestUser) UI.getCurrent().getSession().getAttribute(SessionSymbols.userlookup);
				String username = person.getAlias();
				UI.getCurrent().getSession().setAttribute(SessionSymbols.username, person.getAlias().toUpperCase());
				adminAccessAccepted = verifyer.getAcceptedActualGroup().equals(adminGroup);
				userAccessAccepted = verifyer.getAcceptedActualGroup().equals(userGroup);

				if(adminAccessAccepted) {
					logg.info("Användaren "+username+" har Adminbehörighet");
					functionality = FunctionalityFactory.getAdmin(TestClientSBUI.this, "yyyy-MM-dd");
				}
				else if(userAccessAccepted) {
					logg.info("Användaren "+username+" har Användarbehörighet");
					functionality = FunctionalityFactory.getEngineer(TestClientSBUI.this, "yyyy-MM-dd");
				}
				else {
					logg.info("Användaren "+username+" har inte behörighet. Internt fel. Det skulle ha detekterats tidigare. Handlöst null-pointer-fel näst");
					functionality = null;
				}
				initForWork(person.getAlias(), person.getName(), person.getEmail(), request);
		});
		
		onCredentialFailure = new CommonRunnable(() -> {
				setContent(new CredentialsAndGroupLoginPage(
						verifyer, 
						onCredentialSuccess, 
						onCredentialFailure,  
						"Behörighetskontroll Phoneix meddelanden",
						"Användare",
						"Lösenord",
						"Vidare",
						"Felaktig grupptillhörighet",
						"Felaktiga behörigheter"
				));
		});

		setupVerifyer();
		onCredentialFailure.run();
	}
		
}
