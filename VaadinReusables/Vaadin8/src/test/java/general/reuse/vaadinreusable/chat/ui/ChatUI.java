package general.reuse.vaadinreusable.chat.ui;

import com.vaadin.annotations.Push;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.TextField;
import general.reuse.vaadinreusable.chat.ChatPanel;
import general.reuse.vaadinreusable.chat.ChatParticipant;
import general.reuse.vaadinreusable.vaadin8ish.ButtonV8ish;
import general.reuse.vaadinreusable.vaadin8ish.TextFieldV8ish;
import general.reuse.vaadinreusable.vaadin8ish.UIV8ish;
import general.reuse.vaadinreusable.vaadin8ish.VerticalLayoutV8ish;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SpringUI(path = "/")
@Push
public class ChatUI extends UIV8ish {

    @Override
    protected void init(VaadinRequest request) {
        setup();
    }

    public static void main(String[] args) {
        SpringApplication.run(ChatUI.class, args);
    }

    public ChatUI() {
    }

    private void setup() {
        final VerticalLayoutV8ish l = new VerticalLayoutV8ish();
        final TextFieldV8ish tf = new TextFieldV8ish("Användarnamn");
        l.addComponent(tf);
        final ButtonV8ish joinBtn = new ButtonV8ish("Börja");
        l.addComponent(joinBtn);
        //l.setSizeFull();
        joinBtn.addClickListener(e -> {
            ChatParticipant cp = new ChatParticipant() {
                //final private UI ui = ChatUI.this;
                final String username = tf.getValue();
                @Override
                public boolean hasChatAnyway() {
                    return false;
                }
                @Override
                public String getUsername() {
                    return username;
                }
                @Override
                public boolean getHaveMargins() {
                    return false;
                }
                @Override
                public boolean isFunctional() {
                    return true;
                }
                private long chatSince = 0;
                @Override
                public void setChatSince(long since) {
                    chatSince = since;
                }
                @Override
                public void access(Runnable onAccess) {
                    ChatUI.this.access(onAccess);
                }
                @Override
                public void addReturnKeyListener(TextField tf, Runnable onSend) {
                    tf.addListener(e -> onSend.run());
                }
            };
            ChatPanel wdp = new ChatPanel(cp, () -> setup());
            wdp.setSizeFull();
            ChatUI.this.setContent(wdp);
            ChatUI.this.setSizeFull();
        });
        setContent(l);
    }

}
