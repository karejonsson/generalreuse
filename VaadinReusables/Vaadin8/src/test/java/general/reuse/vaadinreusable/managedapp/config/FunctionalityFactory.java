package general.reuse.vaadinreusable.managedapp.config;

import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.vaadin8ish.UIV8ish;

public class FunctionalityFactory {

    public static BaseFunctionalityProvider getAdmin(UIV8ish ui, String dateFormat) {
        return new TestFunctionalityProvider(ui, new TestAdminFunctionalityProvider(ui), dateFormat, "Administratör");
    }

    public static BaseFunctionalityProvider getEngineer(UIV8ish ui, String dateFormat) {
        return new TestFunctionalityProvider(ui, null, dateFormat, "Ingenjör");
    }

}
