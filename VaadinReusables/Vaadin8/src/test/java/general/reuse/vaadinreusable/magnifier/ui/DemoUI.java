package general.reuse.vaadinreusable.magnifier.ui;

import java.io.InputStream;
import java.net.URI;

import general.reuse.vaadinreusable.vaadin8ish.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vaadin.server.ConnectorResource;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.SpringUI;

import general.reuse.vaadinreusable.magnifier.Magnifier;

//@Theme("demo")
//@Title("Magnifier Add-on Demo")
//@SuppressWarnings("serial")
@SpringBootApplication
@SpringUI(path="/")
public class DemoUI extends UIV8ish {
	
	private static final long serialVersionUID = 9005029920674728217L;

	/*
	@WebServlet(
        value = "/*",
        asyncSupported = true)
    @VaadinServletConfiguration(
        productionMode = false,
        ui = DemoUI.class)
    public static class Servlet extends VaadinServlet {
    }
    */

    @Override
    protected void init(final VaadinRequest request) {

        // Create Magnifier and set Size and ImageURL
        final Magnifier magnifier = new Magnifier();
        magnifier.setHeight(600, Unit.PIXELS);
        magnifier.setWidth(400, Unit.PIXELS);
        // Set the ZoomFactor
        magnifier.setZoomFactor(3.1f);

        // Left side Images
        final VerticalLayoutV8ish imageLayout = new VerticalLayoutV8ish();
        imageLayout.setMargin(true);
        imageLayout.setSpacing(true);

        boolean firstTime = true;
        //Image image = null;

        for (int i_ = 1 ; i_ <= 3 ; i_++) {
        	final int i = i_;
            // Left side images
    		StreamResourceV8ish.StreamSource source = new StreamResourceV8ish.StreamSource() {
    			public InputStream getStream() {
    				return this.getClass().getClassLoader().getResourceAsStream(""+i+".jpeg");
    			}
    		};
    		StreamResourceV8ish resource = new StreamResourceV8ish(source, "sida_"+i+".jpeg");

            final ImageV8ish image = new ImageV8ish(null, resource);
            image.setHeight(300, Unit.PIXELS);
            image.setWidth(200, Unit.PIXELS);
            image.addClickListener((event) -> {
                magnifier.setImageUrl(getImageUrl(DemoUI.this, image));
                magnifier.setZoomImageUrl(getImageUrl(DemoUI.this, image));
            });
            imageLayout.addComponents(image);

            // set default magnifier image
            if(false && firstTime) {
                magnifier.setImageUrl(getImageUrl(DemoUI.this, image));
                // OPTIONAL: Set the ZoomImageUrl
                magnifier.setZoomImageUrl(getImageUrl(DemoUI.this, image));
                firstTime = false;
            }
        }

        final VerticalLayoutV8ish magnifierLayout = new VerticalLayoutV8ish();
        magnifierLayout.setStyleName("demoContentLayout");
        magnifierLayout.setSizeFull();
        magnifierLayout.addComponent(magnifier);
        magnifierLayout.setComponentAlignment(magnifier, AlignmentV8ish.MIDDLE_CENTER);

        final HorizontalSplitPanelV8ish hsplit = new HorizontalSplitPanelV8ish();
        hsplit.setFirstComponent(imageLayout);
        hsplit.setSecondComponent(magnifierLayout);

        // Set the position of the splitter as percentage
        hsplit.setSplitPosition(20, Sizeable.UNITS_PERCENTAGE);

        setContent(hsplit);
    }
	public static String getImageUrl(UIV8ish ui, ImageV8ish image) {
		URI location = ui.getLocation();
		String scheme = location.getScheme();
		String authority = location.getAuthority();
		String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
		Integer uiId = image.getUI().getUIId();
		String cid = image.getConnectorId();
		String filename = "";
		if(image.getSource() instanceof ConnectorResource) {
			filename = ((ConnectorResource) image.getSource()).getFilename();
			//throw new IllegalArgumentException("Requires image with ConnectorResource");
		}
		String out = scheme + "://" + authority + contextPath + "/APP/connector/" + uiId + "/" + cid + "/source/" + filename;
		System.out.println("out="+out);
		return out;
	}

    public static void main(String[] args) {
        SpringApplication.run(DemoUI.class, args);
    }

}