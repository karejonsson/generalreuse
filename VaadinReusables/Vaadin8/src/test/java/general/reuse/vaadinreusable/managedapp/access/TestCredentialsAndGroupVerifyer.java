package general.reuse.vaadinreusable.managedapp.access;

import com.vaadin.ui.UI;
import general.reuse.properties.HarddriveProperties;
import general.reuse.vaadinreusable.login.CredentialsAndGroupVerifyer;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.Set;

public class TestCredentialsAndGroupVerifyer implements CredentialsAndGroupVerifyer {

    private static final Logger logg = Logger.getLogger(TestCredentialsAndGroupVerifyer.class);

    private String propertyBeginning = null;
    private HarddriveProperties hp = null;
    private String userlookup = null;
    private Map<String, String> groups;

    public TestCredentialsAndGroupVerifyer(HarddriveProperties hp, String propertyBeginning, String userlookup, Map<String, String> groups) {
        this.propertyBeginning = propertyBeginning;
        this.hp = hp;
        this.userlookup = userlookup;
        this.groups = groups;
    }

    @Override
    public boolean accepted(String username, String password) {
        return true;
    }

    @Override
    public String[] possibleGroups() {
        Set<String> keys = groups.keySet();
        String[] out = new String[keys.size()];
        int idx = 0;
        for(String key : keys) {
            out[idx++] = key;
        }
        return out;
    }

    public static boolean developmentConfiguration(HarddriveProperties hp, String propertyBeginning, String userlookup) {
        TestUser user = new TestUser("kajo","Kåre Jonsson","kare.jonsson@prv.se");
        UI.getCurrent().getSession().setAttribute(userlookup, user);
        return true;
    }

    @Override
    public boolean partOfGroup(String group, String username) {
        logg.trace("partOfGroup(Group "+group+", username "+username+")");
        if(developmentConfiguration(hp, propertyBeginning, userlookup)) {
            return true;
        }
        return true;
    }

    private String acceptedGroup;

    @Override
    public void setAcceptedGroup(String acceptedGroup) {
        this.acceptedGroup = acceptedGroup;
    }

    @Override
    public String getAcceptedActualGroup() {
        return groups.get(acceptedGroup);
    }

}
