package general.reuse.vaadinreusable.managedapp.config;

import com.vaadin.ui.TextField;
import general.reuse.vaadinreusable.chat.ChatPanel;
import general.reuse.vaadinreusable.chat.ChatParticipant;
import general.reuse.vaadinreusable.managedapp.functionality.BaseAdminFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.functionality.CommonRunnable;
import general.reuse.vaadinreusable.managedapp.functionality.DefaultBaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.texts.ActivityTexts;
import general.reuse.vaadinreusable.vaadin8ish.TextFieldV8ish;
import general.reuse.vaadinreusable.vaadin8ish.UIV8ish;
import org.apache.log4j.Logger;

public class TestFunctionalityProvider extends DefaultBaseFunctionalityProvider {

	private static Logger logg = Logger.getLogger(TestFunctionalityProvider.class);

	public TestFunctionalityProvider(UIV8ish ui, BaseAdminFunctionalityProvider adminFunctionality, String dateformat, String userRolename) {
		super(ui, adminFunctionality, dateformat, userRolename);
	}

	public String toString() {
		return "PhoenixFunctionality: " + super.toString();
	}

	@Override
	public void openChat(CommonRunnable onBack) {
		updateUidname();
		setActivity(ActivityTexts.openChat, false);
		logg.info("Öppnar chatt");
		ChatParticipant chatter = new ChatParticipant() {

			@Override
			public boolean hasChatAnyway() {
				return TestFunctionalityProvider.this.getAdminFunctionality() != null;
			}

			@Override
			public String getUsername() {
				return TestFunctionalityProvider.this.getUsername();
			}

			@Override
			public boolean getHaveMargins() {
				return TestFunctionalityProvider.this.getHaveMargins();
			}

			@Override
			public boolean isFunctional() {
				return TestFunctionalityProvider.this.isFunctional();
			}

			@Override
			public void setChatSince(long l) {
				TestFunctionalityProvider.this.setChatSince(l);
			}

			@Override
			public void access(Runnable accessor) {
				TestFunctionalityProvider.this.access(accessor);
			}

			@Override
			public void addReturnKeyListener(TextField textField, Runnable onBack) {
				TestFunctionalityProvider.this.addReturnKeyListener(textField, new CommonRunnable(onBack));
			}
		};
		ChatPanel wdp = new ChatPanel(chatter, onBack);
		wdp.setSizeFull();
		ui.setContent(wdp);
		ui.setSizeFull();
		fixPage();
	}

	@Override
	final public void setUsername(String userid) {
		updateUidname();
		super.setUsername(userid);
	}

	@Override
	public Boolean getHaveMargins() {
		updateUidname();
		return false;
	}

}

