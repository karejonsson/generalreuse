package general.reuse.vaadinreusable.component.ui;

import com.google.gwt.dom.client.Style;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import general.reuse.vaadinreusable.component.AddToForm;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;
import general.reuse.vaadinreusable.managedapp.functionality.BaseAdminFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.functionality.CommonRunnable;
import general.reuse.vaadinreusable.managedapp.functionality.UserInteractionEvent;
import general.reuse.vaadinreusable.ui.SimplifiedContentUI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.Future;

@SpringBootApplication
@SpringUI(path="/")
public class AddToFormTestUI extends UI {

    private static final long serialVersionUID = 3593424878292602936L;

    private VaadinRequest request;

    public VaadinRequest getRequest() {
        return request;
    }

    @Override
    protected void init(VaadinRequest request) {
        this.request = request;
    }
    public AddToFormTestUI() {
        VerticalLayout vl = LayoutFactory.createVertical(true);
        vl.setSizeFull();
        setContent(vl);
        BaseFunctionalityProvider bfp = new BaseFunctionalityProvider() {

            @Override
            public BaseAdminFunctionalityProvider getAdminFunctionality() {
                return null;
            }

            @Override
            public CommonRunnable getRunnableForReturnToCurrentContent() {
                return null;
            }

            @Override
            public void start() {

            }

            @Override
            public void addWindow(Window win, String winType) {

            }

            @Override
            public void addWindow(Window win, boolean modal, String winType) {

            }

            @Override
            public void setUsername(String username) {

            }

            @Override
            public String getUsername() {
                return null;
            }

            @Override
            public void updateUidname() {

            }

            @Override
            public String getDateformat() {
                return null;
            }

            @Override
            public void setTextualName(String username) {

            }

            @Override
            public String getTextualName() {
                return null;
            }

            @Override
            public void setEmail(String email) {

            }

            @Override
            public String getEmail() {
                return null;
            }

            @Override
            public String getPoolname() {
                return null;
            }

            @Override
            public String getUserRolename() {
                return null;
            }

            @Override
            public void setChatSince() {

            }

            @Override
            public long getChatSince() {
                return 0;
            }

            @Override
            public void openChat(CommonRunnable onBack) {

            }

            @Override
            public String getActivity() {
                return null;
            }

            @Override
            public boolean getStateful() {
                return false;
            }

            @Override
            public void setActivity(String activity, boolean stateful) {

            }

            @Override
            public UserInteractionEvent[] getActivityEvents() {
                return new UserInteractionEvent[0];
            }

            @Override
            public boolean isFunctional() {
                return false;
            }

            @Override
            public Future<Void> access(Runnable runnable) {
                return null;
            }

            @Override
            public Boolean getHaveMargins() {
                return null;
            }

            @Override
            public String getStateDescription() {
                return null;
            }

            @Override
            public boolean possiblyWulnerable() {
                return false;
            }

            @Override
            public void setLocation(String s) {

            }

            @Override
            public void destroyAllInternals() {

            }

            @Override
            public Date getLast() {
                return null;
            }

            @Override
            public void addReturnKeyListener(Object o, CommonRunnable r) {

            }

            @Override
            public void clearReturnKeyListener(Object o) {

            }

            @Override
            public void clearReturnKeyListeners() {

            }

            @Override
            public void setParameters(Map<String, String> parameters) {

            }

            @Override
            public Map<String, String> getParameters() {
                return null;
            }

            @Override
            public void logout(CommonRunnable onBack) {

            }

            @Override
            public Object get(String k) {
                return null;
            }

            @Override
            public void put(String k, Object o) {

            }
        };
        AddToForm.addBooleanField(
            vl,
            null,
            null,
            () -> true,
            c -> { },
            "CAPTION 1",
            true
        );
        AddToForm.addStringField(
            vl,
            null,
            null,
            () -> "Sträng",
            c -> { },
            "CAPTION 1",
            true
        );
        AddToForm.addSearchableChoiceField(
            bfp,
            vl,
            null,
            null,
            () -> "H",
            c -> { },
            Arrays.asList(new String[]{"A", "B", "C"}),
            c -> "HEJ",
            "ETT",
            c -> "SYMBOL",
            "TVÅ",
            c -> "TEXT",
            "CAPTION",
            true
            );
        AddToForm.addBooleanField(
            vl,
            null,
            null,
            () -> false,
            c -> { },
            "CAPTION 2",
            true
        );
    }

    public static void main(String[] args) {
        SpringApplication.run(AddToFormTestUI.class, args);
    }

}
