package general.reuse.vaadinreusable.managedapp.access;

public class TestUser {

    private String alias;
    private String name;
    private String email;

    public TestUser(String alias, String name, String email) {
        this.alias = alias;
        this.name = name;
        this.email = email;
    }

    public String getAlias() {
        return alias;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

}
