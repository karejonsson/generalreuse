package general.reuse.vaadinreusable.serverbrowse;

import java.io.File;

public class PrintPreferred {

    public static void main(String[] args) {
        for(File f : FilesystemDataProviderHack.getPreferred()) {
            System.out.println("Föredragen: "+f.toPath());
        }
    }

}
