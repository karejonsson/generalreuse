package general.reuse.vaadinreusable.ui;

import com.vaadin.ui.MenuBar;
import general.reuse.vaadinreusable.vaadin8ish.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;

@SpringBootApplication
@SpringUI(path="/")
public class SimplifiedContentUITest extends SimplifiedContentUI {
	
	private static final long serialVersionUID = 3593424878292602936L;
	
	private VaadinRequest request;

	public VaadinRequest getRequest() {
		return request;
	}

	@Override
	protected void init(VaadinRequest request) {
		this.request = request;
		setup(new LabelV8ish("INIT"));
	}
	
	private MenuBarV8ish.MenuItem system = null;
	private MenuBarV8ish.MenuItem particular = null;
	
	public SimplifiedContentUITest() {
	}
	
	public void setup(ComponentV8ish c) {
    	System.out.println("setup");
		super.setup(c);
	}	
	
    public void setupGeneralMenubar() {
    	//System.out.println("setupGeneralMenubar");
    	if(system != null) {
    		return;
    	}
		system = menubar.addItem("System", null);
		system.addItem("Systematiskt", new MenuBarV8ish.Command() {
            private static final long serialVersionUID = 4483013525105015694L;
            public void menuSelected(MenuBar.MenuItem selectedItem) {
            	setWorkContent(new LabelV8ish("GENERA MBO"));
            }  
        });
    }
    
    public void setupParticularMenubar() {
    	//System.out.println("setupParticularMenubar");
    	if(particular != null) {
    		return;
    	}
    	particular = menubar.addItem("Särskild", null);
		particular.addItem("Särskiljande", new MenuBarV8ish.Command() {
			private static final long serialVersionUID = 3725756735100541361L;
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.openSubwindow(new WindowV8ish("Particular MBO"));
            }
        });
		particular.addItem("H1 100", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelRightmost(new LabelV8ish("H1 100"), 100);
            }  
        });
		particular.addItem("H2 200", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelRightmost(new LabelV8ish("H2 200"), 200);
            }  
        });
		particular.addItem("H1 -", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelRightmost(new LabelV8ish("H1 -"));
            }  
        });
		particular.addItem("H2 -", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelRightmost(new LabelV8ish("H2 -"));
            }  
        });
		particular.addItem("M1 100", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelMiddle(new LabelV8ish("M1 100"), 100);
            }  
        });
		particular.addItem("M2 200", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelMiddle(new LabelV8ish("M2 200"), 200);
            }  
        });
		particular.addItem("M1 -", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelMiddle(new LabelV8ish("M1 -"));
            }  
        });
		particular.addItem("M2 -", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelMiddle(new LabelV8ish("M2 -"));
            }  
        });
		particular.addItem("M synlig", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelMiddleVisible(true);
            }  
        });
		particular.addItem("M osynlig", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelMiddleVisible(false);
            }  
        });
		particular.addItem("H synlig", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelRightmostVisible(true);
            }  
        });
		particular.addItem("H osynlig", new MenuBarV8ish.Command() {
			public void menuSelected(MenuBarV8ish.MenuItem selectedItem) {
				SimplifiedContentUITest.this.setBarLevelRightmostVisible(false);
            }  
        });
    }
    
    public static void main(String[] args) {
        SpringApplication.run(SimplifiedContentUITest.class, args);
    }

}
