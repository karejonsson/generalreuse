package general.reuse.vaadinreusable.grid;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import general.reuse.vaadinreusable.vaadin8ish.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.ServiceException;
import com.vaadin.server.SessionDestroyEvent;
import com.vaadin.server.SessionDestroyListener;
import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.SpringUI;

import general.reuse.vaadinreusable.grid.GridFiltering;
import general.reuse.vaadinreusable.ui.SimplifiedContentUI;

@SpringBootApplication
@SpringUI(path="/")
public class GridFilteringTest extends SimplifiedContentUI {
	
	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = GridFilteringTest.class)
	public static class Servlet extends VaadinServlet implements SessionInitListener, SessionDestroyListener {
		
		private static final long serialVersionUID = 5576554614414878206L;

		@Override
	    protected void servletInitialized() throws ServletException {
			super.servletInitialized();
	        getService().addSessionInitListener(this); 
	        getService().addSessionDestroyListener(this);
		}
		
	    @Override
	    public void sessionInit(SessionInitEvent event)
	            throws ServiceException {
	    }

	    @Override
	    public void sessionDestroy(SessionDestroyEvent event) {
	    }
	}

    public static void main(String[] args) {
        SpringApplication.run(GridFilteringTest.class, args);
    }

    private static final long serialVersionUID = -5887982355605043938L;
	private VaadinRequest request;

	public VaadinRequest getRequest() {
		return request;
	}

	@Override
	protected void init(VaadinRequest request) {
		this.request = request;
		setWidth("100%");
		setContent(new GridFilteringLayout());
	}
	
	public GridFilteringTest() {
	}
	
    public void setupGeneralMenubar() {
    }
    
    public void setupParticularMenubar() {
    }

	final static class Person {
	    private String firstname;
	    private String lastname;
	    private String email;
	    private Date birth;
	    
	    public Person(String firstname, String lastname, String email, Date birth) {
	        this.firstname = firstname;
	        this.lastname = lastname;
	        this.email = email;
	        this.birth = birth;
	    }
	    public String getFirstName() {
	        return firstname;
	    }
	    public String getLastName() {
	        return lastname;
	    }
	    public String getEmail() {
	        return email;
	    }
	    public Date getBirth() {
	        return birth;
	    }
	}
	
	final static class PersonGrid extends GridV8ish<Person> {
		private static final long serialVersionUID = 8208173989772358942L;

		public PersonGrid() {
	        List<Person> persons = new ArrayList<>();
	        persons.add(new Person("foo", "Any", "1.name@mail.org", new Date(1204567090123l)));
	        persons.add(new Person("bar", "Name", "2.name@mail.org", new Date(1214567190123l)));
	        persons.add(new Person("foobar", "Will", "3.name@mail.org", new Date(1224567291123l)));
	        persons.add(new Person("Barbie", "Do", "4.name@mail.org", new Date(1234567391123l)));
	        persons.add(new Person("Barry", "For", "5.name@mail.org", new Date(1244567491123l)));
	        persons.add(new Person("Ken", "The", "6.name@mail.org", new Date(1254567591123l)));
	        persons.add(new Person("Kenny", "Time", "7.name@mail.org", new Date(1264567691123l)));
	        persons.add(new Person("Ben", "We", "8.name@mail.org", new Date(1274567791123l)));
	        persons.add(new Person("Benny", "Test", "9.name@mail.org", new Date(1284567891123l)));

	        addColumn(row -> {
		        return new LabelV8ish(row.getFirstName());
			}).setCaption("First");
	        addColumn(row -> {
	        	TextFieldV8ish out = new TextFieldV8ish();
	        	out.setValue(row.getLastName());
	        	return out;
			}).setCaption("Last");
	        addColumn(Person::getEmail).setCaption("Email");
	        addColumn(row -> {
	        	DateFieldV8ish df = new DateFieldV8ish();
	        	Date d = row.getBirth();
	        	df.setValue(LocalDate.of(d.getYear()+1900, d.getMonth()+1, d.getDay()+1));
	        	df.setDateFormat("yyyy-MM-dd");
	        	return df;
			}).setCaption("Birth");
	        
	        setItems(persons);
	    }
	    
	}
	
    public static class GridFilteringLayout extends VerticalLayoutV8ish {
		private static final long serialVersionUID = -8524909192710591010L;
		private PersonGrid personGrid;
        public GridFilteringLayout() {
	        personGrid = new PersonGrid();
	        GridFiltering.addColumnFilters(personGrid);
	        addComponentsAndExpand(personGrid);
	        setWidth("100%");
    	}
    }

}
