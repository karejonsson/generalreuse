package general.reuse.vaadinreusable.managedapp.ui;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import general.reuse.vaadinreusable.managedapp.factories.LabelFactory;
import general.reuse.vaadinreusable.managedapp.texts.DescriptionTexts;

@SpringUI(path = "/utloggad")
public class TestClientUIClose extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        getPage().setTitle(DescriptionTexts.appname+", Utloggad");
        setContent(LabelFactory.create("Sessionen är avslutad. Stäng fliken"));
    }

}
