package general.reuse.vaadinreusable.managedapp.ui;

import com.vaadin.annotations.Push;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.VerticalLayout;
import general.reuse.properties.HarddriveProperties;
import general.reuse.vaadinreusable.managedapp.factories.LabelFactory;
import general.reuse.vaadinreusable.managedapp.factories.LayoutFactory;
import general.reuse.vaadinreusable.managedapp.functionality.BaseFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.functionality.CommonRunnable;
import general.reuse.vaadinreusable.managedapp.install.InstallationProperties;
import general.reuse.vaadinreusable.managedapp.install.SessionSymbols;
import general.reuse.vaadinreusable.managedapp.session.SessionRegistry;
import general.reuse.vaadinreusable.vaadin8ish.UIV8ish;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Push
public abstract class TestClientUI extends UIV8ish {

	private static final long serialVersionUID = 2386688493398037234L;

	protected static Logger logg = null;

	private static final String userid = "booter";
	private static String uidname = null;
	private static String tidname = null;

    static {
    	/*
    	try {
			Class.forName("org.apache.log4j.MDC");
		}
    	catch(Exception e) {
    		e.printStackTrace();
		}
    	 */
 		String message = "--";
    	logg = Logger.getLogger("TestClientUI");
    	try {
    		InputStream is = HarddriveProperties.getStream(InstallationProperties.prv_managedapp_log4j_filename, TestClientUI.class);
    		if(is != null) {
    			message = "Använder extern parameterfil";
				Properties prop = new Properties();
				prop.load(is);
				PropertyConfigurator.configure(prop);
    		}
    		else {
    			message = "Faller tillbaka på inbyggd parameterfil";
        		is = HarddriveProperties.getStream("log4j.properties", TestClientUI.class);
        		if(is != null) {
					Properties prop = new Properties();
					prop.load(is);
					PropertyConfigurator.configure(prop);
        		}
    		}
    	}
    	catch(Exception e) { }

		if(uidname == null) {
			uidname = "uid";
			tidname = "tid";
		}

     	logg.info(message);
       	System.setProperty("log4j.defaultInitOverride", "true");
    }

    public TestClientUI() {
		logg.info("Lägger till lyssnare för frikoppling av session. functionality="+functionality);
		addDetachListener(new DetachListener() {
			private static final long serialVersionUID = 2130748488474937314L;
			public void detach(DetachEvent event) {
				functionality.updateUidname();
				logg.info("detach händelse");
				SessionRegistry.closeMechanicallyAndLogout(functionality);
			}
		});
		setSizeFull();
    }

    private boolean handelClosedState_isOkToContinue() {
    	Boolean opened = SessionRegistry.openedForLogin.getValue();
		if(opened != null && !opened) {
			logg.info("Inloggning när systemet är stängt. functionality="+functionality);
			VerticalLayout vl = LayoutFactory.createVertical(functionality.getHaveMargins());
			vl.addComponent(LabelFactory.create("Systemet är för tillfället inte tillgängligt."));
			vl.addComponent(LabelFactory.create("Inloggningen är aktiv för att administratörer skall kunna slå på tillgängligheten efter omstart."));
			vl.addComponent(LabelFactory.create("Försök igen om en stund eller kontakta driften."));
			setContent(vl);
			return false;
		}
		else {
			return true;
		}
	}

	protected String adminActualADGroup = "AdminGroup";
    protected String adminVisibleGroup = "Adminstratör";

	protected String userActualADGroup = "UserGroup";
    protected String userVisibleGroup = "Användare";

	protected Boolean adminAccessAccepted = null;
	protected Boolean userAccessAccepted = null;

	protected void setupVerifyer() {
    	Map<String, String> map = new HashMap<>();

    	map.put(adminVisibleGroup, adminActualADGroup);
    	map.put(userVisibleGroup, userActualADGroup);
	}

	protected BaseFunctionalityProvider functionality = null;

	protected CommonRunnable onCredentialSuccess = null;
	protected CommonRunnable onCredentialFailure = null;

	protected void initForWork(String alias, String textualName, String email, VaadinRequest request) {
		VaadinSession session = VaadinSession.getCurrent();
		session.setAttribute(SessionSymbols.userfunctionality, functionality);
		functionality.setUsername(alias);
		functionality.updateUidname();
		functionality.setTextualName(textualName);
		if(!handelClosedState_isOkToContinue()) {
			return;
		}
		functionality.setEmail(email);
		functionality.start();
		SessionRegistry.add(functionality);

		JavaScript.getCurrent().addFunction("aboutToClose",
				(arg) -> {
					SessionRegistry.closeMechanicallyAndLogout(functionality);
					functionality.setLocation(VaadinServlet.getCurrent().getServletContext().getContextPath() + "/utloggad");
				});

		Page.getCurrent().getJavaScript().execute(
				"window.onbeforeunload = function (e) { var e = e || window.event; aboutToClose(); return; };");

	}

}
