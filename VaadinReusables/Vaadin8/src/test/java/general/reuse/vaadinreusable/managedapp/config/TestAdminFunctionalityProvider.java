package general.reuse.vaadinreusable.managedapp.config;

import com.vaadin.ui.Window;
import general.reuse.vaadinreusable.managedapp.functionality.CommonRunnable;
import general.reuse.vaadinreusable.managedapp.functionality.DefaultBaseAdminFunctionalityProvider;
import general.reuse.vaadinreusable.managedapp.navigation.NavigationPanel;
import general.reuse.vaadinreusable.managedapp.navigation.UsersActivePanel;
import general.reuse.vaadinreusable.managedapp.texts.ActivityTexts;
import general.reuse.vaadinreusable.serverbrowse.DefaultForeignFrameworksResources;
import general.reuse.vaadinreusable.serverbrowse.ServerFilesystemManipulationPanel;
import general.reuse.vaadinreusable.vaadin8ish.UIV8ish;
import general.reuse.vaadinreusable.vaadin8ish.WindowV8ish;
import org.apache.log4j.Logger;

public class TestAdminFunctionalityProvider extends DefaultBaseAdminFunctionalityProvider {

	private static Logger logg = Logger.getLogger(TestAdminFunctionalityProvider.class);

	public TestAdminFunctionalityProvider(UIV8ish ui) {
		super(ui);
	}
	
	public String toString() {
		return "PhoenixAdminFunctionality: "+super.toString();
	}

	@Override
	public void openAdmin(CommonRunnable onBack) {
		updateUidname();
		getFunctionality().setActivity(ActivityTexts.openAdmin, false);
		logg.info("Öppnar administrationsvyn åt "+getFunctionality().getUsername());
		NavigationPanel np = new NavigationPanel(this, onBack);
		np.setSizeFull();
		ui.setContent(np);
		ui.setSizeFull();
	}

	@Override
	public void openActiveManagement(CommonRunnable onBack) {
		updateUidname();
		getFunctionality().setActivity(ActivityTexts.openActiveManagement, false);
		logg.info("Öppnar funktionen att hantera aktiva användare åt "+getFunctionality().getUsername());
		UsersActivePanel dp = new UsersActivePanel(this, onBack);
		dp.setSizeFull();
		ui.setContent(dp);
		ui.setSizeFull();
	}

	@Override
	public void openFileHandling(CommonRunnable onBack) {
		updateUidname();
		getFunctionality().setActivity(ActivityTexts.openFileHandling, true);
		logg.info("Öppnar funktionen att hantera filsystemet åt "+getFunctionality().getUsername());
		DefaultForeignFrameworksResources affr = new DefaultForeignFrameworksResources() {
			@Override
			public void addWindow(Window win) {
				TestAdminFunctionalityProvider.this.addWindow(win, null);
			}

			@Override
			public void addWindow(Window win, String winType) {
				TestAdminFunctionalityProvider.this.addWindow(win, winType);
			}

		};
		ServerFilesystemManipulationPanel fmp = new ServerFilesystemManipulationPanel(affr, onBack);
		fmp.setSizeFull();
		ui.setContent(fmp);
		ui.setSizeFull();
	}

}
