package general.reuse.vaadinreusable.serverbrowse;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

import org.vaadin.filesystemdataprovider.FilesystemData;

@SpringBootApplication
@SpringUI(path = "/")
public class FilesystemDataProviderHackUI extends UI {

    // https://vaadin.com/forum/thread/17359246/vaadin-8-grid-item-click-issue
    // This happens when the widgetset is compile with old version of Vaadin and run with another newer version.
    // It can be fixed by upgrading the Vaadin plugin and recompiling the widgetset.
    private static Logger logg = Logger.getLogger(FilesystemDataProviderHackUI.class);

    @Override
    protected void init(VaadinRequest request) {
    }

    public static void main(String[] args) {
        SpringApplication.run(FilesystemDataProviderHackUI.class, args);
    }

    public FilesystemDataProviderHackUI() throws IOException {
        setContent(new ServerFilesystemManipulationPanel());
    }

}
