package general.reuse.tunneling;

public class TunnelForwardLParameters {

	public int tunnel_lport = -1;
	public String tunnel_rhost = null;
	public int tunnel_rport = -1;
    
	public String login_user = null;
	public String login_domain = null;
	public int ssh_port = 22;
	public String login_password = null;
	
	public int getTunnel_lport() {
		return tunnel_lport;
	}

	public void setTunnel_lport(int tunnel_lport) {
		this.tunnel_lport = tunnel_lport;
	}

	public String getTunnel_rhost() {
		return tunnel_rhost;
	}

	public void setTunnel_rhost(String tunnel_rhost) {
		this.tunnel_rhost = tunnel_rhost;
	}

	public int getTunnel_rport() {
		return tunnel_rport;
	}

	public void setTunnel_rport(int tunnel_rport) {
		this.tunnel_rport = tunnel_rport;
	}

	public String getLogin_user() {
		return login_user;
	}

	public void setLogin_user(String login_user) {
		this.login_user = login_user;
	}

	public String getLogin_domain() {
		return login_domain;
	}

	public void setLogin_domain(String login_domain) {
		this.login_domain = login_domain;
	}

	public int getSsh_port() {
		return ssh_port;
	}

	public void setSsh_port(int ssh_port) {
		this.ssh_port = ssh_port;
	}

	public String getLogin_password() {
		return login_password;
	}

	public void setLogin_password(String login_password) {
		this.login_password = login_password;
	}

	public boolean hasAll() {
		if(tunnel_lport < 1) {
			return false;
		}
		if(tunnel_rhost == null) {
			return false;
		}
		if(tunnel_rhost.length() < 5) {
			return false;
		}
		if(tunnel_rport < 1) {
			return false;
		}
		if(login_user == null) {
			return false;
		}
		if(login_user.length() < 1) {
			return false;
		}
		if(login_domain == null) {
			return false;
		}
		if(login_domain.length() < 1) {
			return false;
		}
		if(ssh_port < 1) {
			return false;
		}
		if(login_password == null) {
			return false;
		}
		if(login_password.length() < 2) {
			return false;
		}
		return true;
	}
	
	public String toString() {
		return 
				"    login_user \""+login_user+"\"\n"+
				"    login_domain \""+login_domain+"\"\n"+
				"    login_password \""+login_password+"\"\n"+
				"    tunnel_lport \""+tunnel_lport+"\"\n"+
				"    tunnel_rhost \""+tunnel_rhost+"\"\n"+
				"    tunnel_rport \""+tunnel_rport+"\"\n"+
				"    ssh_port \""+ssh_port+"\"\n";
	}

}
