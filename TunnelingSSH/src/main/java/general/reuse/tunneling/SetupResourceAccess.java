package general.reuse.tunneling;

import general.reuse.properties.HarddriveProperties;

public class SetupResourceAccess {
	
	private static TunnelForwardLEncapsulation jdbc_tfle = null;

	public static final String postfix_ssh_open = ".ssh.open";

	public static final String postfix_ssh_login_user = ".ssh.login.user";
	public static final String postfix_ssh_login_domain = ".ssh.login.domain";
	public static final String postfix_ssh_login_password = ".ssh.login.password";

	public static final String postfix_ssh_tunnel_lport = ".ssh.tunnel.lport";
	public static final String postfix_ssh_tunnel_rhost = ".ssh.tunnel.rhost";
	public static final String postfix_ssh_tunnel_rport = ".ssh.tunnel.rport";

	public static final String postfix_ssh_ssh_port = ".ssh.ssh.port";

	public static void setup(HarddriveProperties hp, String prefix) {
		if(hp.getBoolean(prefix+postfix_ssh_open, false)) {

			TunnelForwardLParameters jdbc_tflp = new TunnelForwardLParameters();
			try {
				jdbc_tflp.setLogin_domain(hp.getString(prefix+postfix_ssh_login_domain));
				jdbc_tflp.setLogin_password(hp.getString(prefix+postfix_ssh_login_password));
				jdbc_tflp.setLogin_user(hp.getString(prefix+postfix_ssh_login_user));
				jdbc_tflp.setTunnel_lport(hp.getInt(prefix+postfix_ssh_tunnel_lport));
				jdbc_tflp.setTunnel_rport(hp.getInt(prefix+postfix_ssh_tunnel_rport));
				jdbc_tflp.setTunnel_rhost(hp.getString(prefix+postfix_ssh_tunnel_rhost));
				jdbc_tflp.setSsh_port(hp.getInt(prefix+postfix_ssh_ssh_port, 22));
			}
			catch(Exception e) {
				System.out.println("Tillhandahållna parametrar räcker ej för tunneln för JDBC.\n"+jdbc_tflp);
				e.printStackTrace();
				return;
			}
			
			if(!jdbc_tflp.hasAll()) {
				System.out.println("Erforderliga parametrar saknas för tunneln för JDBC.\n"+jdbc_tflp);
				return;
			}
			
			try {
				jdbc_tfle = new TunnelForwardLEncapsulation(jdbc_tflp);
			} catch (Exception e1) {
				System.out.println("Parametrar saknas för att öppna tunneln för JDBC.\n"+jdbc_tflp);
				e1.printStackTrace();
				return;
			}
			
			if(!jdbc_tfle.openTunnel()) {
				System.out.println("Det gick inte att öppna SSH-tunneln för JDBC.\n"+jdbc_tflp);
				return;
			}
	
			System.out.println("SSH-tunneln för JDBC öppnad.");
		}
		else {
			System.out.println("SSH-tunneln för JDBC skall inte öppnas.");
		}
		
	}
	
	public static void teardown() {
		if(jdbc_tfle != null) {
			jdbc_tfle.closeTunnel();
			System.out.println("SSH-tunneln för JDBC stängd.");
		}
	}



}
