package general.reuse.gaminginteraction.reuse;

public class PolygonCalcules {

	public static int getMax(int[] array) {
		if(array.length == 1) {
			return array[0];
		}
		int out = array[0];
		for(int i = 1 ; i < array.length ; i++) {
			out = Math.max(out, array[i]);
		}
		return out;
	}
	
	public static int getMaxIndex(int[] array) {
		if(array.length == 1) {
			return 0;
		}
		int max = array[0];
		int out = 0;
		for(int i = 1 ; i < array.length ; i++) {
			int here = Math.max(max, array[i]);
			if(here >= max) {
				max = here;
				out = i;
			}
		}
		return out;
	}
	
	public static int getMin(int[] array) {
		if(array.length == 1) {
			return array[0];
		}
		int out = array[0];
		for(int i = 1 ; i < array.length ; i++) {
			out = Math.min(out, array[i]);
		}
		return out;
	}
	
	public static int getMinIndex(int[] array) {
		if(array.length == 1) {
			return 0;
		}
		int min = array[0];
		int out = 0;
		for(int i = 1 ; i < array.length ; i++) {
			int here = Math.min(min, array[i]);
			if(here < min) {
				min = here;
				out = i;
			}
		}
		return out;
	}
	
	public static int[] centralize(int[] array) {
		return add(array, -1*middle(array));
	}
	
	public static int[] add(int[] array, int offset) {
		int[] out = new int[array.length];
		for(int i = 0 ; i < array.length ; i++) {
			out[i] = array[i]+offset;
		}
		return out;
	}
	
	public static int span(int[] array) {
		return getMax(array) - getMin(array);
	}
	
	public static int middle(int[] array) {
		return (getMax(array) + getMin(array))/2;
	}
	
	public static int[] sPolyXArray = { 9, 16, 25, 33, 26, 35, 25, 13, 9, 0, 4, 0, 9 };
	public static int[] sPolyYArray = { 0, 5, 1, 8, 13, 20, 31, 28, 31, 22, 16, 7, 0 };

	public static void main(String[] args) {
		int idx = getMinIndex(sPolyXArray);
		System.out.println(""+idx+" -> "+sPolyXArray[idx]);
	}
	
}
