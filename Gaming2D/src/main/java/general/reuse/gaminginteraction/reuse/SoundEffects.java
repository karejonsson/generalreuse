package general.reuse.gaminginteraction.reuse;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import general.reuse.gaminginteraction.board.GameBoard;

public class SoundEffects {

	public static void play(String soundToPlay) {
		try {
			// soundLocation = new URL(soundToPlay);

			// Stores a predefined audio clip
			Clip clip = AudioSystem.getClip();

			// Holds a stream of a definite length
			AudioInputStream inputStream;

			inputStream = AudioSystem
					.getAudioInputStream(GameBoard.class.getClassLoader().getResourceAsStream(soundToPlay));// soundLocation);

			// Make audio clip available for play
			clip.open(inputStream);

			// Define how many times to loop
			clip.loop(0);

			// Play the clip
			clip.start();
		}
		catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		catch (UnsupportedAudioFileException e1) {
			e1.printStackTrace();
		}
		catch (IOException e1) {
		}
		catch (LineUnavailableException e1) {
			e1.printStackTrace();
		} 
		catch (Exception e1) {
		}
	}
	
}
