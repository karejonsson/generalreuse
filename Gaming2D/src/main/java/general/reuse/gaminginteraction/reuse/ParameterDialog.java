package general.reuse.gaminginteraction.reuse;

import javax.swing.*;

import general.reuse.gaminginteraction.board.Gametime;

import java.awt.*;

public class ParameterDialog extends JDialog {
	private final JTextField gameTime = new JTextField(8);
	private final JTextField realTime = new JTextField(8);
	private Gametime gametime = null;
	public ParameterDialog(Frame owner, Gametime gametime) {
		super(owner, "Parameters", true);
		this.gametime = gametime;
		gameTime.setText(""+gametime.getGametime());
		realTime.setText(""+gametime.getUpdatepace());
		getContentPane().setLayout(new GridLayout(0,2,5,5));
		getContentPane().add(new JLabel("Fysisk tid:"));
		getContentPane().add(gameTime);
		getContentPane().add(new JLabel("Speltakt:"));
		getContentPane().add(realTime);
		JButton okBtn = new JButton("OK");
		okBtn.addActionListener(e -> { ParameterDialog.this.dispose(); ok(gameTime.getText(), realTime.getText()); } );
		getContentPane().add(okBtn);
		pack();
		centerOnScreen(this);
		show();

	}
	
	private void ok(String gameTime, String realTime) {
		gametime.setGametime(Integer.parseInt(gameTime));
		gametime.setUpdatepace(Integer.parseInt(realTime));
		System.out.println("Nytt tillstånd "+gametime);
	}
	
	public String getGametime() { return gameTime.getText(); }
	public String getRealtime() { return realTime.getText(); }
	
	public static void main(String[] args) {
		JFrame owner = new JFrame("Parameters");
		owner.setLocation(-1000, -1000);
		owner.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		owner.show();
		Gametime gt = new Gametime();
		ParameterDialog dialog = new ParameterDialog(owner, gt);
		System.out.println("Dispose");
	}

	public static void centerOnScreen(Window window) {
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		window.setLocation(
				(d.width - window.getSize().width) / 2,
				(d.height - window.getSize().height) / 2);
	}

}
