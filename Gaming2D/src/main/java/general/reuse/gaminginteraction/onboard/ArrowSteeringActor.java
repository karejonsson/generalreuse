package general.reuse.gaminginteraction.onboard;

import java.awt.Rectangle;
import java.awt.Shape;

public interface ArrowSteeringActor extends Shape {
	double getRotationAngle();
	Rectangle getBoundingRectangle();
	void setXVelocity(double i);
	void setYVelocity(double i);
	double getXCenter();
	double getYCenter();
	void move();
	void collisionStationary(StationaryObject on);
	void collisionMoving();
	void setFree(StationaryObject on);
}
