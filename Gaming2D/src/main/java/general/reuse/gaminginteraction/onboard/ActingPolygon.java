package general.reuse.gaminginteraction.onboard;

import java.awt.*;
import java.awt.geom.Rectangle2D;

import general.reuse.gaminginteraction.board.ActingPolygonBoardLimitPolicies;
import general.reuse.gaminginteraction.board.DeadActorBoardLimitPolicies;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;

public abstract class ActingPolygon extends Polygon {
	
	public static class SlideOverEdgesPolicies implements ActingPolygonBoardLimitPolicies {

		private int boardWidth;
		private int boardHeight;

		public SlideOverEdgesPolicies(int boardWidth, int boardHeight) {
			this.boardWidth = boardWidth;
			this.boardHeight = boardHeight;
		}
		
		@Override
		public void outLeft(ActingPolygon actor) {
			//System.out.println("SlideOverEdgesPolicies.outLeft");
			actor.setXCenter(boardWidth);
		}

		@Override
		public void outRight(ActingPolygon actor) {
			//System.out.println("SlideOverEdgesPolicies.outRight");
			actor.setXCenter(0);
		}

		@Override
		public void outDown(ActingPolygon actor) {
			//System.out.println("SlideOverEdgesPolicies.outDown");
			actor.setYCenter(0);
		}

		@Override
		public void outUp(ActingPolygon actor) {
			//System.out.println("SlideOverEdgesPolicies.outUp");
			actor.setYCenter(boardHeight);
		}
	};

	public static class StayWithinEdgesPolicies implements ActingPolygonBoardLimitPolicies {

		private int boardWidth;
		private int boardHeight;

		public StayWithinEdgesPolicies(int boardWidth, int boardHeight) {
			this.boardWidth = boardWidth;
			this.boardHeight = boardHeight;
		}
		
		@Override
		public void outLeft(ActingPolygon actor) {
			//System.out.println("StayWithinEdgesPolicies.outLeft");
			actor.setXCenter(0);
		}

		@Override
		public void outRight(ActingPolygon actor) {
			//System.out.println("StayWithinEdgesPolicies.outRight");
			actor.setXCenter(boardWidth);
		}

		@Override
		public void outDown(ActingPolygon actor) {
			//System.out.println("StayWithinEdgesPolicies.outDown");
			actor.setYCenter(boardHeight);
		}

		@Override
		public void outUp(ActingPolygon actor) {
			//System.out.println("StayWithinEdgesPolicies.outUp");
			actor.setYCenter(0);
		}
	};

	private static final long serialVersionUID = 3505389210924047587L;

	// Determines the speed the actor moves
	protected double xVelocity = 0;
	protected double yVelocity = 0;
	protected double unfree_xVelocity = 0;
	protected double unfree_yVelocity = 0;

	// Center of actor	
	private double centerX;
	private double centerY;

	// Width and height of polygon	
	private int polygonWidth;
	private int polygonWidthHalf;

	private int polygonHeight;
	private int polygonHeightHalf;

	// Defines if the actor should rotate
	protected double rotationAngle = 0;
	protected double movingAngle = 0;

	//GameBoard board;
	protected int boardWidth;
	protected int boardHeight;
	private int angleStep;
	protected ActingPolygonBoardLimitPolicies boardPolicies;

	public ActingPolygon(int[] polyXArray, int[] polyYArray, int middleX, int middleY, int boardWidth, int boardHeight, int angleStep) {	
		this(polyXArray, polyYArray, middleX, middleY, boardWidth, boardHeight, angleStep, new SlideOverEdgesPolicies(boardWidth, boardHeight));
	}

	// Creates a new actor	
	public ActingPolygon(int[] polyXArray, int[] polyYArray, int middleX, int middleY, int boardWidth, int boardHeight, int angleStep, ActingPolygonBoardLimitPolicies boardPolicies) {	
		// Creates a Polygon by calling the super or parent class Polygon	
		super(
				PolygonCalcules.centralize(polyXArray), 
				PolygonCalcules.centralize(polyYArray), 
				polyXArray.length);
		this.boardPolicies = boardPolicies;
		
		int[] actualXArray = PolygonCalcules.add(PolygonCalcules.centralize(polyXArray), Math.min(boardWidth-PolygonCalcules.span(polyXArray), Math.max(middleX, -PolygonCalcules.span(polyXArray))));
		int[] actualYArray = PolygonCalcules.add(PolygonCalcules.centralize(polyYArray), Math.min(boardHeight-PolygonCalcules.span(polyYArray), Math.max(middleY, -PolygonCalcules.span(polyYArray))));

		polygonWidth = PolygonCalcules.span(actualXArray);
		polygonWidthHalf = polygonWidth/2;
		polygonHeight = PolygonCalcules.span(actualYArray);
		polygonHeightHalf = polygonHeight/2;

		this.boardWidth = boardWidth;
		this.boardHeight = boardHeight;
		this.angleStep = angleStep;

		int span = PolygonCalcules.span(actualXArray);
		centerX = Math.max(span, Math.min(boardWidth-span, middleX));
		span = PolygonCalcules.span(actualYArray);
		centerY = Math.max(span, Math.min(boardHeight-span, middleY));
		//System.out.println("ActingPolygon centerX "+centerX+", centerY "+centerY);
	}

	// Gets & sets the values for the x & y center of actor
	public double getXCenter() { return centerX; }
	public double getYCenter() { return centerY; }
	public void setXCenter(double xCent) { this.centerX = xCent; }
	public void setYCenter(double yCent) { this.centerY = yCent; }
	public void increaseXPos(double incAmt) { this.centerX += incAmt; }
	public void increaseYPos(double incAmt) { this.centerY += incAmt; }	

	// Gets & sets the x & y for upper left hand corner of actor
	public int getPolygonWidth() { return polygonWidth; }
	public int getPolygonHeight() { return polygonHeight; }

	// Gets, sets, increases and decreases the Polygon velocity
	public double getXVelocity() { return xVelocity; }
	public double getYVelocity() { return yVelocity; }
	public void setXVelocity(double xVel) { this.xVelocity = xVel; }
	public void setYVelocity(double yVel) { this.yVelocity = yVel; }
	
	public double getUnfree_xVelocity() {
		return unfree_xVelocity;
	}

	public void setUnfree_xVelocity(double unfree_xVelocity) {
		this.unfree_xVelocity = unfree_xVelocity;
	}

	public double getUnfree_yVelocity() {
		return unfree_yVelocity;
	}

	public void setUnfree_yVelocity(double unfree_yVelocity) {
		this.unfree_yVelocity = unfree_yVelocity;
	}

	public void increaseXVelocity(double xVelInc) { this.xVelocity += xVelInc; }
	public void increaseYVelocity(double yVelInc) { this.yVelocity += yVelInc; }
	public void decreaseXVelocity(double xVelDec) { this.xVelocity -= xVelDec; }
	public void decreaseYVelocity(double yVelDec) { this.yVelocity -= yVelDec; }

	// Set and increase the actor movement angle
	public void setMovingAngle(double moveAngle) { this.movingAngle = moveAngle; }
	public double getMovingAngle() { return movingAngle; }
	public void increaseMovingAngle(double moveAngle) { this.movingAngle += moveAngle; }

	// Calculate the actor angle of movement

	// This is trigonometry at work
	// By taking the cos of the angle I get the opposite value of x
	// Angle * Math.PI / 180 converts degrees into radians
	public double cosOfAngle(double angle) {	
		return (double) (Math.cos(angle * Math.PI / 180));	
	}

	// By taking the sin of the angle I get the adjacent value of y
	// Angle * Math.PI / 180 converts degrees into radians 
	public double sinOfAngle(double angle) {	
		return (double) (Math.sin(angle * Math.PI / 180));	
	}

	// Gets & increases or decreases the rotation angle of the actor
	// Increase the rotation angle by 5, but don't let it go above 360
	public double getRotationAngle() { 
		return rotationAngle;
	}

	public void increaseRotationAngle() { 
		if(getRotationAngle() >= 360-angleStep) { 
			rotationAngle = 0;
		}
		else { 
			rotationAngle += angleStep;
		}
	}

	// Decrease the rotation angle by 5, but don't let it go below 0
	public void decreaseRotationAngle() { 	
		if(getRotationAngle() < 0) {
			rotationAngle = 360-angleStep;
		}	
		else { 
			rotationAngle -= angleStep;
		}	
	}

	// NEW FIX Artificial rectangle that is used for collision detection
	public Rectangle getBounds() {
		Rectangle out = new Rectangle((int) getXCenter() - polygonWidthHalf, (int) getYCenter() - polygonHeightHalf, getPolygonWidth(), getPolygonHeight());
		//System.out.println("ActingPolygon.getBounds out "+out);
		return out;
	}

	public Rectangle2D getBounds2D() {
		Rectangle out = new Rectangle((int) getXCenter() - polygonWidthHalf, (int) getYCenter() - polygonHeightHalf, getPolygonWidth(), getPolygonHeight());
		//System.out.println("ActingPolygon.getBounds2D out "+out);
		return out;
	}

	// Gets the actors nose X vector -------------------
	public double getPolygonNoseX() {
		// x2 = this.getXCenter() + cos(rotationAngle) * 14
		return this.getXCenter() + Math.cos(rotationAngle) * polygonWidthHalf;
	}

	// Gets the actors nose Y vector --------------------
	public double getPolygonNoseY() {
		// x2 = this.getXCenter() + cos(rotationAngle) * 14
		// y2 = this.getYCenter() + sin(rotationAngle) * 14
		return this.getYCenter() + Math.sin(rotationAngle) * polygonHeightHalf;
	}

	private StationaryObject free = null;
	
	public boolean isFree() {
		return free == null;
	}

	public void setFree(StationaryObject free) {
		//(new Throwable()).printStackTrace();
		this.free = free;
	}

	public void move() {
		if(free == null) {
			//System.out.println("ActingPolygon.move "+isFree()+", "+this.getXCenter());
			// Increase the x origin value based on current velocity 
			increaseXPos(getXVelocity());
	
			// If the actor goes off the board flip it to the other side of the board
			if(this.getXCenter() < 0.0) {
				boardPolicies.outLeft(this);
			} else if (this.getXCenter() > boardWidth){
				boardPolicies.outRight(this);
			}
	
			// Increase the x origin value based on current velocity 
			increaseYPos(getYVelocity());
	
			// If the actor goes off the board flip it to the other side of the board
			if(this.getYCenter() < 0.0){
				boardPolicies.outUp(this);
			} 
			else if (this.getYCenter() > boardHeight){				
				boardPolicies.outDown(this);
			}
		}
		else {
			increaseXPos(-1*getUnfree_xVelocity());
			increaseYPos(-1*getUnfree_yVelocity());
			Rectangle actorRectangle = free.getBounds();
			Rectangle myRectangle = getBounds();
			
			if(!myRectangle.intersects(actorRectangle)) {
				free = null;
			}
		}
	}
	
}