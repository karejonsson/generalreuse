package general.reuse.gaminginteraction.onboard;

import java.awt.Polygon;
import java.awt.Rectangle;

import general.reuse.gaminginteraction.board.PolygonTouchActorPolicies;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;
import general.reuse.gaminginteraction.reuse.SoundEffects;

public class StationaryPolygon extends Polygon implements StationaryObject {

	private static final long serialVersionUID = -3092717511677381813L;
	
	public static final PolygonTouchActorPolicies dissapearActorTouchPolicy = new PolygonTouchActorPolicies() {
		@Override
		public void touch(StationaryPolygon polygon, DeadActor deadActor) {
			SoundEffects.play(explodeFile);
			polygon.onScreen = false;
		}
	};
	
	public static final PolygonTouchActorPolicies remainActorTouchPolicy = new PolygonTouchActorPolicies() {
		@Override
		public void touch(StationaryPolygon polygon, DeadActor deadActor) {
			SoundEffects.play(explodeFile);
		}
	};
	
	public static final String explodeFile = "explode.wav";

	// Width and height of polygon	
	private int polygonWidth;

	private int polygonHeight;

	// Upper left hand corner of polygon
	private double uLeftXPos;
	private double uLeftYPos;

	// Keep track of whether actor is on screen
	private boolean onScreen = true;
	private PolygonTouchActorPolicies actorTouchPolicy = null;

	public StationaryPolygon(
			int[] polyXArray, 
			int[] polyYArray,
			int middleX,
			int middleY,
			int boardWidth, int boardHeight) {	
		this(polyXArray,  polyYArray, middleX, middleY, boardWidth, boardHeight, remainActorTouchPolicy);
	}
		
	// Creates a new actor	
	public StationaryPolygon(
			int[] polyXArray, 
			int[] polyYArray,
			int middleX,
			int middleY,
			int boardWidth, int boardHeight,
			PolygonTouchActorPolicies actorTouchPolicy) {	
		// Creates a Polygon by calling the super or parent class Polygon	
		super(
				PolygonCalcules.add(PolygonCalcules.centralize(polyXArray), Math.min(boardWidth-PolygonCalcules.span(polyXArray), Math.max(middleX, -PolygonCalcules.span(polyXArray)))), 
				PolygonCalcules.add(PolygonCalcules.centralize(polyYArray), Math.min(boardHeight-PolygonCalcules.span(polyYArray), Math.max(middleY, -PolygonCalcules.span(polyYArray)))), 
				polyXArray.length);
		this.actorTouchPolicy = actorTouchPolicy;
		
		polygonWidth = PolygonCalcules.span(polyXArray);
		polygonHeight = PolygonCalcules.span(polyYArray);

		int span = PolygonCalcules.span(polyXArray);
		uLeftXPos = Math.max(-span, middleX-span/2);	

		span = PolygonCalcules.span(polyYArray);
		uLeftYPos = Math.max(-span, middleY-span/2);
	}

	// Gets & sets the x & y for upper left hand corner of actor
	public double getuLeftXPos() { return uLeftXPos; }
	public double getuLeftYPos() { return uLeftYPos; }
	public void setuLeftXPos(double xULPos) { this.uLeftXPos = xULPos; }
	public void setuLeftYPos(double yULPos) { this.uLeftYPos = yULPos; }

	// Gets & sets the x & y for upper left hand corner of actor
	public int getPolygonWidth() { return polygonWidth; }
	public int getPolygonHeight() { return polygonHeight; }

	// NEW FIX Artificial rectangle that is used for collision detection
	public Rectangle getBounds() {
		Rectangle out = new Rectangle((int) uLeftXPos, (int) uLeftYPos, getPolygonWidth(), getPolygonHeight());
		//System.out.println("StationaryPolygon getBounds "+out);
		return out;
	}

	@Override
	public boolean isOnScreen() {
		return onScreen;
	}

	@Override
	public void setOnScreen(boolean visible) {
		onScreen = visible;
	}

	public void interact_actor(ArrowSteeringActor actor) {

		// NEW Check if theShip hits a Rock
		if(actor != null) {
			Rectangle myRectangle = getBounds();
			Rectangle actorRectangle = actor.getBoundingRectangle();
	
			if(myRectangle.intersects(actorRectangle)) {
				// NEW play explosion if ship is hit
				SoundEffects.play(explodeFile);
				actor.collisionStationary(this);
				actor.setFree(this);
			}
		}
	}

	@Override
	public void interact_actor(DeadActor deadActor) {
		//System.out.println("ExplodingPolygon.interact_actor detektera "+deadActor);
		Rectangle myRectangle = getBounds();

		// NEW Check if theShip hits a Rock
		if(deadActor != null) {
			Rectangle actorRectangle = deadActor.getBounds();
	
			if(myRectangle.intersects(actorRectangle)) {
				actorTouchPolicy.touch(this, deadActor);
			}
		}
	}	

}