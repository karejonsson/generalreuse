package general.reuse.gaminginteraction.onboard;

public interface Steerable {
	void rotateLeft();
	void rotateRight();
	void forwards();
	void backwards();
	void shoot();
}
