package general.reuse.gaminginteraction.onboard;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.ArrayList;

import general.reuse.gaminginteraction.board.DeadActorBoardLimitPolicies;
import general.reuse.gaminginteraction.board.Gametime;
import general.reuse.gaminginteraction.board.PhysicsUpdatePolicies;
import general.reuse.gaminginteraction.board.SteererTouchActorPolicies;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;
import general.reuse.gaminginteraction.reuse.SoundEffects;

public class NonActingMover extends Polygon implements DeadActor {

	private static final long serialVersionUID = 2135017804358313902L;
	
	// Used to change the direction of the asteroid when
	// it hits something and determines how fast it moves
	private double xDirection = 1;
	private double yDirection = 1;

	// Define rock height and width
	private int moverLeftmostIndex;
	private int moverUpmostIndex;
	private int moverWidth;
	private int moverHeight;
	private int moverMinX;
	private int moverMaxX;
	private int moverMinY;
	private int moverMaxY;
	
	public static DeadActorBoardLimitPolicies reboundAllSidesPolicy = new DeadActorBoardLimitPolicies() {
		@Override
		public void outLeft(DeadActor actor) {
			((NonActingMover) actor).redirectHorizontally();
		}

		@Override
		public void outRight(DeadActor actor) {
			((NonActingMover) actor).redirectHorizontally();
		}

		@Override
		public void outDown(DeadActor actor) {
			((NonActingMover) actor).redirectVertically();
		}

		@Override
		public void outUp(DeadActor actor) {
			((NonActingMover) actor).redirectVertically();
		}	
	};
	
	public static SteererTouchActorPolicies reboundAllDirectionsPolicy = new SteererTouchActorPolicies() {
		@Override
		public void touchLeft(DeadActor object, ArrowSteeringActor actor) {
			//System.out.println("NonActingMover.interact_actor left "+actor);
			SoundEffects.play(explodeFile);
			actor.collisionMoving();
		}

		@Override
		public void touchRight(DeadActor object, ArrowSteeringActor actor) {
			//System.out.println("NonActingMover.interact_actor right "+actor);
			SoundEffects.play(explodeFile);
			actor.collisionMoving();
		}

		@Override
		public void touchDown(DeadActor object, ArrowSteeringActor actor) {
			//System.out.println("NonActingMover.interact_actor down "+actor);
			SoundEffects.play(explodeFile);
			actor.collisionMoving();
		}

		@Override
		public void touchUp(DeadActor object, ArrowSteeringActor actor) {
			//System.out.println("NonActingMover.interact_actor up "+actor);
			SoundEffects.play(explodeFile);
			actor.collisionMoving();
		}
	};
	
	public static PhysicsUpdatePolicies defaultPhysicsPolicies = new PhysicsUpdatePolicies() {

		@Override
		public double nextXVelocity(double currentVelocity, long timeElapsed) {
			return currentVelocity;
		}

		@Override
		public double nextYVelocity(double currentVelocity, long timeElapsed) {
			return currentVelocity;
		}
		
	};
	
	private DeadActorBoardLimitPolicies boardPolicies;
	private SteererTouchActorPolicies steererPolicies;
	private PhysicsUpdatePolicies physicsPolicies;

	// NEW Keep track of whether Rock is on screen
	private boolean onScreen = true;
	
	private Gametime gametime = null;

	// NEW Sound file names
	// The JavaSound api allows wavs, au, aiff files
	private static String explodeFile = "explode.wav";
	
	public NonActingMover(
			int[] polyXArray, int[] polyYArray, int startX, int startY, 
			int xDirection, int yDirection, 
			int boardWidth, int boardHeight,
			Gametime gametime) {
		this(polyXArray, polyYArray, startX, startY, xDirection, yDirection, boardWidth, boardHeight, gametime,
				reboundAllSidesPolicy, reboundAllDirectionsPolicy, defaultPhysicsPolicies);
	}
	
	private double[] polyXArrayD;
	private double[] polyYArrayD;
	
	public NonActingMover(
			int[] polyXArray, int[] polyYArray, int startX, int startY, 
			int xDirection, int yDirection, 
			int boardWidth, int boardHeight,
			Gametime gametime,
			DeadActorBoardLimitPolicies boardPolicies,
			SteererTouchActorPolicies steererPolicies,
			PhysicsUpdatePolicies physicsPolicies) {
		// Creates a Polygon by calling the super or parent class of Rock Polygon
		super(
				PolygonCalcules.add(PolygonCalcules.centralize(polyXArray), Math.min(boardWidth-PolygonCalcules.span(polyXArray)/2, Math.max(startX, PolygonCalcules.span(polyXArray)/2))), 
				PolygonCalcules.add(PolygonCalcules.centralize(polyYArray), Math.min(boardHeight-PolygonCalcules.span(polyYArray)/2, Math.max(startY, PolygonCalcules.span(polyYArray)/2))), 
				polyXArray.length);

		polyXArrayD = new double[xpoints.length];
		polyYArrayD = new double[ypoints.length];
		for(int i = 0 ; i < polyXArray.length ; i++) {
			polyXArrayD[i] = (double) xpoints[i];
			polyYArrayD[i] = (double) ypoints[i];
		}
		
		moverLeftmostIndex = PolygonCalcules.getMinIndex(polyXArray);
		//System.out.println("moverLeftmostIndex "+moverLeftmostIndex);
		moverUpmostIndex = PolygonCalcules.getMinIndex(polyYArray);
		//System.out.println("moverUpmostIndex "+moverUpmostIndex);
		moverWidth = PolygonCalcules.span(polyXArray);
		moverHeight = PolygonCalcules.span(polyYArray);
		moverMinX = 0;
		moverMinY = 0;
		moverMaxX = boardWidth-moverWidth;
		moverMaxY = boardHeight-moverHeight;
		//System.out.println("NonActingMover polyXArray "+Arrays.toString(polyXArray)+", polyYArray "+Arrays.toString(polyYArray));
		//System.out.println("NonActingMover moverMinX "+moverMinX+", moverMinY "+moverMinY+", moverMaxX "+moverMaxX+", moverMaxY "+moverMaxY);
		
		// Randomly generate a speed for the Polygon
		this.xDirection = xDirection;
		this.yDirection = yDirection;
		this.gametime = gametime;
		timePreviousExecution = gametime.getGametime();
		this.boardPolicies = boardPolicies;
		this.steererPolicies = steererPolicies;
		this.steererPolicies = steererPolicies;
		this.physicsPolicies = physicsPolicies;
	}

	// Creates a bounding rectangle for collision checking
	public Rectangle getBounds() {
		return new Rectangle(super.xpoints[moverLeftmostIndex], super.ypoints[moverUpmostIndex], moverWidth, moverHeight);
	}

	// NEW move receives SpaceShip and Torpedos now
	public void interact_deadActor(DeadActor otherRock) {
		Rectangle thisRockRectangle = getBounds();
		Rectangle otherRockRectangle = otherRock.getBounds();
		if(otherRockRectangle.intersects(thisRockRectangle)) {
			// Switch the direction the rocks are moving on impact
			double tempXDirection = this.xDirection;
			double tempYDirection = this.yDirection;

			this.xDirection = otherRock.getXDirection();
			this.yDirection = otherRock.getYDirection();

			otherRock.setXDirection(tempXDirection);
			otherRock.setYDirection(tempYDirection);
		}
	}
	
	// NEW move receives SpaceShip and Torpedos now
	public void interact_stationary(StationaryObject sobj) {
		Rectangle thisRockRectangle = getBounds();
		Rectangle stationarysRectangle = sobj.getBounds();
		if(stationarysRectangle.intersects(thisRockRectangle)) {
			//System.out.println("Block "+stationarysRectangle+", bumling "+thisRockRectangle);
			if(Math.abs(stationarysRectangle.getMaxX()-thisRockRectangle.getMaxX()) > 
			   Math.abs(stationarysRectangle.getMaxY()-thisRockRectangle.getMaxY())) {
				//System.out.println("Skärning X-led "+(ctr++));
				// Switch in X-direction
				xDirection = -xDirection;
				return;
			}
			// Switch in Y-direction
			//System.out.println("Skärning Y-led "+(ctr++));
			yDirection = -yDirection;
		}
	}
	
	public void interact_actor(ArrowSteeringActor actor) {
		Rectangle meBox = getBounds();

		// NEW Check if theShip hits a Rock
		if(actor != null) {
			Rectangle actorBox = actor.getBoundingRectangle();
	
			if(meBox.intersects(actorBox)) {
				Double upTouch = null;
				Double downTouch = null;
				if(meBox.getCenterX() > actorBox.getMinX() && meBox.getCenterX() < actorBox.getMaxX()) {
					// I am at the right place horizontally
					if(meBox.getCenterY() < actorBox.getCenterY()) {
						// I am above
						if(meBox.getMaxY() > actorBox.getMinY()) {
							// My lowest is lower than his highest
							//System.out.println("Beröring ovanifrån");
							//steererPolicies.touchDown(this, actor);
							downTouch = meBox.getMaxY() - actorBox.getMinY();
						}
					}
					if(meBox.getCenterY() > actorBox.getCenterY()) {
						// I am below
						if(meBox.getMinY() < actorBox.getMaxY()) {
							// My highest is higher than his lowest
							//System.out.println("Beröring underifrån");
							//steererPolicies.touchUp(this, actor);
							upTouch = actorBox.getMaxY() - meBox.getMinY();
						}
					}
				}
				if(upTouch == null && downTouch != null) {
					steererPolicies.touchDown(this, actor);
				}
				if(upTouch != null && downTouch == null) {
					steererPolicies.touchUp(this, actor);
				}
				if(upTouch != null && downTouch != null) {
					if(upTouch > downTouch) {
						steererPolicies.touchUp(this, actor);
					}
					else {
						steererPolicies.touchDown(this, actor);
					}
				}
			}
			if(meBox.getCenterY() > actorBox.getMinY() && meBox.getCenterY() < actorBox.getMaxY()) {
				Double leftTouch = null;
				Double rightTouch = null;
				// I am at the right place vertically
				if(meBox.getCenterX() < actorBox.getCenterX()) {
					// I am to the left
					if(meBox.getMaxX() > actorBox.getMinX()) {
						// My rightest is righter than his leftest
						//System.out.println("Beröring från vänster");
						//steererPolicies.touchLeft(this, actor);
						leftTouch = meBox.getMaxX() - actorBox.getMinX();
					}
				}
				if(meBox.getCenterX() > actorBox.getCenterX()) {
					// I am to the left
					if(meBox.getMaxX() < actorBox.getMinX()) {
						// My rightest is righter than his leftest
						//System.out.println("Beröring från höger");
						//steererPolicies.touchRight(this, actor);
						rightTouch = actorBox.getMinX() - meBox.getMaxX(); 
					}
				}
				if(leftTouch == null && rightTouch != null) {
					steererPolicies.touchRight(this, actor);
				}
				if(leftTouch != null && rightTouch == null) {
					steererPolicies.touchLeft(this, actor);
				}
				if(leftTouch != null && rightTouch != null) {
					if(leftTouch > rightTouch) {
						steererPolicies.touchLeft(this, actor);
					}
					else {
						steererPolicies.touchRight(this, actor);
					}
				}
			}
		}
	}
	
	public void interact_torpedoes(ArrayList<PhotonTorpedo> torpedos) {
		Rectangle rectangle = getBounds();

		for(PhotonTorpedo torpedo : torpedos) {
			// Make sure the Torpedo is on the screen
			if(torpedo.onScreen) {
				// NEW Check if a torpedo hits a Rock
				if(rectangle.contains(torpedo.getXCenter(), torpedo.getYCenter())) {
					setOnScreen(false);
					torpedo.onScreen = false;
					
					// NEW play explosion sound if rock is destroyed
					SoundEffects.play(explodeFile);
				}
			}
		}
	}
	
	private long timePreviousExecution;
	
	public void move() {
		// Get the upper left and top most point for the Polygon
		// This will be dynamic later on
		int uLeftXPos = super.xpoints[moverLeftmostIndex];
		int uLeftYPos = super.ypoints[moverUpmostIndex];
		//System.out.println("NonActingMover.move uLeftXPos "+uLeftXPos+", uLeftYPos "+uLeftYPos);

		// If the Rock hits a wall it will go in the opposite direction
		if(uLeftXPos < moverMinX) {
			boardPolicies.outLeft(this);
		}
		if(uLeftXPos > moverMaxX) {
			boardPolicies.outRight(this);
		}
		if(uLeftYPos < moverMinY) {
			boardPolicies.outUp(this);
		}
		if(uLeftYPos > moverMaxY) {
			boardPolicies.outDown(this);
		}

		long currentGametime = gametime.getGametime();
		
		// Change the values of the points for the Polygon
		for (int i = 0; i < super.xpoints.length; i++) {
			xDirection = physicsPolicies.nextXVelocity(xDirection, currentGametime-timePreviousExecution);
			polyXArrayD[i] += xDirection;
			polyYArrayD[i] += yDirection;
			super.xpoints[i] = (int) polyXArrayD[i];
			super.ypoints[i] = (int) polyYArrayD[i];
		}
		timePreviousExecution = currentGametime;
	}
	
	@Override
	public void setOnScreen(boolean b) {
		onScreen = b;
	}

	@Override
	public boolean isOnScreen() {
		return onScreen;
	}

	@Override
	public void setXDirection(double xDirection) {
		System.out.println("NonActingMover.setXDirection "+xDirection);
		this.xDirection = xDirection;
	}

	@Override
	public void setYDirection(double yDirection) {
		System.out.println("NonActingMover.setyDirection "+yDirection);
		this.yDirection = yDirection;
	}

	@Override
	public double getXDirection() {
		return xDirection;
	}

	@Override
	public double getYDirection() {
		return yDirection;
	}
	
	public double getMovingAngle() {
		return getAngleInRadians(yDirection, xDirection);
	}
	
	public static double getAngleInRadians(double ydir, double xdir) {
		ydir = -ydir;
		if(xdir == 0) {
			return Math.PI/2;
		}
		double out = Math.atan(ydir/xdir);
		if(xdir < 0) {
			out += Math.PI;
		}
		if(out > Math.PI) {
			out -= 2*Math.PI;
		}
		return out;
	}
	
	public void redirectVertically() {
		yDirection = -yDirection;
	}

	public void redirectHorizontally() {
		xDirection = -xDirection;
	}
	
	
	
	public static void main(String[] args) {
		double y = 1.0;
		double x = -1.0;
		System.out.println(getAngleInRadians(1.0, 1.0));
		System.out.println(getAngleInRadians(1.0, -1.0));
		System.out.println(getAngleInRadians(-1.0, 1.0));
		System.out.println(getAngleInRadians(-1.0, -1.0));
		System.out.println(getAngleInRadians(-1.0, -100.0));
	}

	public void setMovingAngle(double angle) {
		//System.out.println("NonActingMover.setMovingAngle 1 xDirection "+xDirection+", yDirection "+yDirection+", angle "+angle);
		double magn = Math.sqrt(Math.pow(yDirection, 2) + Math.pow(xDirection, 2));
		yDirection = (-magn*Math.sin(angle));
		xDirection = (magn*Math.cos(angle));
		//System.out.println("NonActingMover.setMovingAngle 2 xDirection "+xDirection+", yDirection "+yDirection);
	}

}