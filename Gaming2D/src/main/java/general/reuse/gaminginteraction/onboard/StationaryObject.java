package general.reuse.gaminginteraction.onboard;

import java.awt.Shape;

public interface StationaryObject extends Shape {

	boolean isOnScreen();
	void setOnScreen(boolean visible);
	void interact_actor(ArrowSteeringActor actor);
	void interact_actor(DeadActor deadActor);

}
