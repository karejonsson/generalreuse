package general.reuse.gaminginteraction.onboard;

import java.awt.Shape;
import java.util.ArrayList;

public interface DeadActor extends Shape {

	void setOnScreen(boolean b);
	boolean isOnScreen();
	void interact_deadActor(DeadActor otherRock);
	void interact_actor(ArrowSteeringActor actor);
	void interact_torpedoes(ArrayList<PhotonTorpedo> torpedos);
	void move();
	void setXDirection(double xDirection);
	void setYDirection(double yDirection);
	double getXDirection();
	double getYDirection();
	void interact_stationary(StationaryObject stationary);
	
}
