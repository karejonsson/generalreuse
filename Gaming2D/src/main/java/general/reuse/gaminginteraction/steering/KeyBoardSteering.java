package general.reuse.gaminginteraction.steering;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import general.reuse.gaminginteraction.board.GameBoard;
import general.reuse.gaminginteraction.onboard.ArrowSteeringActor;
import general.reuse.gaminginteraction.onboard.PhotonTorpedo;
import general.reuse.gaminginteraction.onboard.Steerable;
import general.reuse.gaminginteraction.reuse.SoundEffects;

public class KeyBoardSteering implements KeyListener, Runnable {

	// NEW Location of sound files
	private String thrustFile = "thrust.au";
	private String laserFile = "laser.aiff";
	
	private Steerable actor = null;
	
	public KeyBoardSteering(Steerable actor) {
		this.actor = actor;
	}
	// Used to check if a key is being held down
	public boolean keyHeld = false;

	// Gets the keycode for the key being held down
	public int keyHeldCode;

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			//System.out.println("Forward");
			SoundEffects.play(thrustFile);
			keyHeldCode = e.getKeyCode();
			keyHeld = true;
		}
		else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			//System.out.println("Backward");
			keyHeldCode = e.getKeyCode();
			keyHeld = true;
		}
		else if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			//System.out.println("Rotate Right");
			keyHeldCode = e.getKeyCode();
			keyHeld = true;
		} else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			//System.out.println("Rotate Left");
			keyHeldCode = e.getKeyCode();
			keyHeld = true;
		}
		else if(e.getKeyCode() == KeyEvent.VK_SPACE || e.getKeyCode() == KeyEvent.VK_ENTER) {
			SoundEffects.play(laserFile);
			actor.shoot();
		}
		
	}

	// When the key is released this informs the code that
	// the key isn't being held down
	public void keyReleased(KeyEvent e) {
		keyHeld = false;
	}

	@Override
	public void run() {
		if (keyHeld == true && keyHeldCode == KeyEvent.VK_RIGHT) {
			actor.rotateRight();
		} else if (keyHeld == true && keyHeldCode == KeyEvent.VK_LEFT) {
			actor.rotateLeft();
		} else if (keyHeld == true && keyHeldCode == KeyEvent.VK_UP) {
			actor.forwards();
		} else if (keyHeld == true && keyHeldCode == KeyEvent.VK_DOWN) {
			actor.backwards();
		}
	}

}
