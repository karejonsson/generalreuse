package general.reuse.gaminginteraction.board;

class RepaintTheBoard implements Runnable {

	private Gametime gametime;
	private Runnable board;
	private Runnable player;

	public RepaintTheBoard() {
		setSteering(null);
		setBoard(null);
		setGametime(null);
	}
	
	public void setSteering(Runnable r) {
		if(r != null) {
			player = r;
		}
		else {
			player = new Runnable() {
				@Override
				public void run() {
				}
			};
		}
	}

	public void setBoard(Runnable r) {
		if(r != null) {
			board = r;
		}
		else {
			board = new Runnable() {
				@Override
				public void run() {
				}
			};
		}
	}

	public void setGametime(Gametime gametime) {
		if(gametime != null) {
			this.gametime = gametime;
		}
		else {
			gametime = new Gametime();
		}
	}

	@Override
	public void run() {
		board.run();
		player.run();
		gametime.timeForwards();
		//System.out.println("RepaintTheBoard.run "+gametime.getGametime());
	}

}
