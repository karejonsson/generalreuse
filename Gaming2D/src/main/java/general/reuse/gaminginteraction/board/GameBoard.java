package general.reuse.gaminginteraction.board;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

//Will hold all of my Rock objects

import java.util.ArrayList;
import java.util.List;
//Runs commands after a given delay
import java.util.concurrent.ScheduledThreadPoolExecutor;

//Defines time units. In this case TimeUnit.MILLISECONDS
import java.util.concurrent.TimeUnit;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import general.reuse.gaminginteraction.onboard.ArrowSteeringActor;
import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.PhotonTorpedo;
import general.reuse.gaminginteraction.onboard.StationaryObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class GameBoard extends JFrame implements Runnable, TorpedoInserter {

	private static final long serialVersionUID = 1L;
	
	public ArrayList<DeadActor> deadActors;
	public ArrayList<StationaryObject> stationaries;
	
	// Height and width of the game board

	public int boardWidth;
	public int boardHeight;

	// Holds every PhotonTorpedo I create
	public ArrayList<PhotonTorpedo> torpedos = new ArrayList<PhotonTorpedo>();

	private GameDrawingPanel gamePanel;
	ArrowSteeringActor actor = null;
	private GameBoardState state = null;
	private StationaryObjectFactory factory;
	private Gametime gametime = null;

	public GameBoard(
			Gametime gametime,
			int boardWidth, 
			int boardHeight, 
			ArrayList<DeadActor> rocks, 
			ArrayList<StationaryObject> stationaries,
			StationaryObjectFactory factory) {
		this.gametime = gametime;
		this.boardWidth = boardWidth;
		this.boardHeight = boardHeight;
		this.deadActors = rocks;
		this.stationaries = stationaries;
		this.factory = factory;
	
		// Define the defaults for the JFrame
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		gamePanel = new GameDrawingPanel(this);
		this.add(gamePanel, BorderLayout.CENTER);
		this.setSize(boardWidth, boardHeight+50);
		initMenuBar();
		//play();
	}
	
	private JMenuItem clear_item = null;
	private JMenuItem stop_item = null;
	private JMenuItem play_item = null;
	private JMenuItem timing_item = null;
	private JMenuItem save_item = null;
	private JMenuItem draw_item = null;
	private JMenuItem load_item = null;
	private JMenuItem exit_item = null;
	
	private void initMenuBar() {
    	JMenuBar menubar = new JMenuBar();

    	JMenu file = new JMenu("File");
        menubar.add(file);
        
    	JMenu blocks = new JMenu("Blocks");
        menubar.add(blocks);
        
    	JMenu execute = new JMenu("Execute");
        menubar.add(execute);
        
    	clear_item = new JMenuItem("Clear");
    	clear_item.addActionListener((ActionEvent event) -> {
    		clear();
        });
    	blocks.add(clear_item);
    	
    	load_item = new JMenuItem("Load");
    	load_item.addActionListener((ActionEvent event) -> {
    		load();
        });
    	file.add(load_item);
    	
    	save_item = new JMenuItem("Save");
    	save_item.addActionListener((ActionEvent event) -> {
    		save();
        });
    	file.add(save_item);
    	
    	exit_item = new JMenuItem("Exit");
    	exit_item.addActionListener((ActionEvent event) -> {
    		exit();
        });
    	file.add(exit_item);
    	
    	play_item = new JMenuItem("Play");
    	play_item.addActionListener((ActionEvent event) -> {
    		play();
        });
    	execute.add(play_item);
    	
    	timing_item = new JMenuItem("Timing");
    	timing_item.addActionListener((ActionEvent event) -> {
    		timing();
        });
    	execute.add(timing_item);
    	
    	stop_item = new JMenuItem("Stop");
    	stop_item.addActionListener((ActionEvent event) -> {
    		stop();
        });
    	execute.add(stop_item);
    	
    	draw_item = new JMenuItem("Draw");
    	draw_item.addActionListener((ActionEvent event) -> {
    		draw();
        });
    	blocks.add(draw_item);
    	
        setJMenuBar(menubar);
        run();
	}
	
	private void exit() {
		System.exit(-1);
	}

	private void stop() {
		System.out.println("Stop");
		state = GameBoardState.STOPPED;
		gamePanel.removeMouseListener(mouseListener);
		pause();
		clear_item.setEnabled(true);
		stop_item.setEnabled(false);
		play_item.setEnabled(true);
		timing_item.setEnabled(true);
		save_item.setEnabled(true);
		draw_item.setEnabled(true);
		load_item.setEnabled(true);
	}
	
	private void updateStationaries(int x, int y) {
		List<StationaryObject> toRemove = new ArrayList<StationaryObject>();
		for(StationaryObject stationary : stationaries) {
			if(stationary.contains(x, y)) {
				toRemove.add(stationary);
			}
		}
		if(toRemove.size() > 0) {
			// It is a remove action
			for(StationaryObject stationary : toRemove) {
				stationaries.remove(stationary);
			}
			gamePanel.repaint();
			return; 
		}
		// It is an add action
		stationaries.add(factory.get(x, y, boardWidth, boardHeight));
		gamePanel.repaint();
	}
	
	private MouseListener mouseListener = new MouseListener() {
		@Override
		public void mouseClicked(MouseEvent e) {
			System.out.println("mouseClicked");
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			System.out.println("mouseEntered");
		}

		@Override
		public void mouseExited(MouseEvent e) {
			System.out.println("mouseExited");
		}

		@Override
		public void mousePressed(MouseEvent e) {
			System.out.println("mousePressed");
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			System.out.println("mouseReleased");
			updateStationaries(e.getPoint().x, e.getPoint().y);
		}
	};

	private void draw() {
		System.out.println("Draw");
		state = GameBoardState.DRAWING;
		gamePanel.addMouseListener(mouseListener);
		clear_item.setEnabled(true);
		stop_item.setEnabled(false);
		play_item.setEnabled(true);
		timing_item.setEnabled(true);
		save_item.setEnabled(true);
		draw_item.setEnabled(false);
		load_item.setEnabled(true);
	}

	private void play() {
		System.out.println("Play");
		state = GameBoardState.PLAYING;
		gamePanel.removeMouseListener(mouseListener);
		start();
		clear_item.setEnabled(false);
		stop_item.setEnabled(true);
		play_item.setEnabled(false);
		timing_item.setEnabled(false);
		save_item.setEnabled(false);
		draw_item.setEnabled(false);
		load_item.setEnabled(false);
	}
	
	private void timing() {
	    JDialog.setDefaultLookAndFeelDecorated(true);
	    Long[] selectionValues = { 3l, 4l, 6l, 8l, 10l, 12l, 16l, 20l, 25l, 30l, 40l, 50l, 60l, 80l, 100l, 140l, 200l, 300l, 500l, 700l, 1000l, 1500l, 2000l, 4000l };
	    String initialSelection = "Dogs";
	    Long selection = (Long) JOptionPane.showInputDialog(null, "Millisekunder per simuleringsvarv",
	        "Simuleringstid", JOptionPane.QUESTION_MESSAGE, null, selectionValues, gametime.getUpdatepace());
	    if(selection != null) {
		    gametime.setUpdatepace(selection);
	    }
	}

	private void save() {
		System.out.println("Save");
		JFileChooser fileChooser = new JFileChooser();
		if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			if(file != null) {
				try {
					FileOutputStream f = new FileOutputStream(file);
					ObjectOutputStream o = new ObjectOutputStream(f);

					// Write objects to file
					o.writeObject(stationaries);

					o.close();
					f.close();
				} catch (Exception e) {
					System.out.println("Error initializing stream");
				}
			}
		}
	}

	private void load() {
		System.out.println("Load");
		JFileChooser fileChooser = new JFileChooser();
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			if(file != null) {
				try {
					FileInputStream fi = new FileInputStream(file);
					ObjectInputStream oi = new ObjectInputStream(fi);

					// Read objects
					ArrayList<StationaryObject> otherstationaries = (ArrayList<StationaryObject>) oi.readObject();
					
					oi.close();
					fi.close();
					
					stationaries.clear();
					gamePanel.removeAll();
					stationaries.addAll(otherstationaries);
					gamePanel.repaint();
				} catch (Exception e) {
					System.out.println("Error initializing stream");
				}
			}
		}
	}

	private void clear() {
		System.out.println("Clear");
		stationaries.clear();
		gamePanel.removeAll();
		gamePanel.repaint();
	}
	
	private ScheduledThreadPoolExecutor executor;
	private RepaintTheBoard repainter = new RepaintTheBoard();
	
	public void setSteering(Runnable player) {
		repainter.setSteering(player);
	}
	
	public void start() {
		play_item.setEnabled(false);
		timing_item.setEnabled(false);
		//System.out.println("GameBoard.start ");
		// Used to execute code after a given delay
		// The attribute is corePoolSize - the number of threads to keep in
		// the pool, even if they are idle
		if(executor == null) {
			executor = new ScheduledThreadPoolExecutor(5);
		}

		// Method to execute, initial delay, subsequent delay, time unit
		repainter.setBoard(this);
		repainter.setGametime(gametime);
		executor.scheduleAtFixedRate(repainter, 0L, gametime.getUpdatepace(), gametime.getTimeUnits());

		// Show the frame
		this.setVisible(true);
	}

	public void pause() {
		if(executor == null) {
			return;
		}

		// Method to execute, initial delay, subsequent delay, time unit
		executor.shutdown();
		executor = null;

		// Show the frame
		this.setVisible(true);
	}

	@Override
	public void run() {
		repaint();
	}

	@Override
	public void add(double centerX, double centerY, double movingAngle) {
		torpedos.add(new PhotonTorpedo(boardWidth, boardHeight, centerX, centerY, movingAngle));
	}

	public void setActor(ArrowSteeringActor actor) {
		this.actor = actor;
	}

}

