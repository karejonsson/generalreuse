package general.reuse.gaminginteraction.board;

import general.reuse.gaminginteraction.onboard.ActingPolygon;

public interface ActingPolygonBoardLimitPolicies {

	void outLeft(ActingPolygon actor);
	void outRight(ActingPolygon actor);
	void outDown(ActingPolygon actor);
	void outUp(ActingPolygon actor);

}
