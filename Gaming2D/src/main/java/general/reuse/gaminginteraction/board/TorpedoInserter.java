package general.reuse.gaminginteraction.board;

public interface TorpedoInserter {
	void add(double centerX, double centerY, double movingAngle);
}
