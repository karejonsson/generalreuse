package general.reuse.gaminginteraction.board;

import java.util.concurrent.TimeUnit;

public class Gametime {
	
	private long gametime = System.currentTimeMillis();
	
	public long getGametime() {
		return gametime;
	}
	
	public void setGametime(long gametime) {
		this.gametime = gametime;
	}
	
	private long updatepace = 20;
	
	public long getUpdatepace() {
		return updatepace;
	}
	
	public void setUpdatepace(long updatepace) {
		this.updatepace = updatepace;
	}

	public void timeForwards() {
		gametime += updatepace;
	}
	
	private TimeUnit tu = TimeUnit.MILLISECONDS;
	
	public TimeUnit getTimeUnits() {
		return tu;
	}
	
	public void setTimeUnits(TimeUnit tu) {
		this.tu = tu;
	}
	
	public String toString() {
		return "Gametime{gametime="+gametime+", updatepace="+updatepace+", tu="+tu+"}";
	}
	
}
