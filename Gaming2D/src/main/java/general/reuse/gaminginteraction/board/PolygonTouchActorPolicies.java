package general.reuse.gaminginteraction.board;

import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.StationaryPolygon;

public interface PolygonTouchActorPolicies {

	void touch(StationaryPolygon polygon, DeadActor actor);

}
