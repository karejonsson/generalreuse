package general.reuse.gaminginteraction.board;

import general.reuse.gaminginteraction.onboard.StationaryObject;

public interface StationaryObjectFactory {

	public StationaryObject get(int x, int y, int boardWidth, int boardHeight);
	
}
