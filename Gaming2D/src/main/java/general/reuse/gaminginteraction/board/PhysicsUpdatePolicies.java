package general.reuse.gaminginteraction.board;

public interface PhysicsUpdatePolicies {
	
	double nextXVelocity(double currentVelocity, long timeElapsed);
	double nextYVelocity(double currentVelocity, long timeElapsed);

}
