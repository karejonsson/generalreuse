package general.reuse.gaminginteraction.board;

import general.reuse.gaminginteraction.onboard.DeadActor;

public interface DeadActorBoardLimitPolicies {

	void outLeft(DeadActor actor);
	void outRight(DeadActor actor);
	void outDown(DeadActor actor);
	void outUp(DeadActor actor);
	
}
