package general.reuse.gaminginteraction.board;

import general.reuse.gaminginteraction.onboard.ArrowSteeringActor;
import general.reuse.gaminginteraction.onboard.DeadActor;

public interface SteererTouchActorPolicies {

	void touchLeft(DeadActor touching, ArrowSteeringActor steerer);
	void touchRight(DeadActor touching, ArrowSteeringActor steerer);
	void touchDown(DeadActor touching, ArrowSteeringActor steerer);
	void touchUp(DeadActor touching, ArrowSteeringActor steerer);

}
