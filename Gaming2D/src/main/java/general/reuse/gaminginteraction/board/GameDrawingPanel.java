package general.reuse.gaminginteraction.board;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;

import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.PhotonTorpedo;
import general.reuse.gaminginteraction.onboard.StationaryObject;

class GameDrawingPanel extends JComponent {

	private static final long serialVersionUID = -1775299305694745894L;

	private GameBoard board;

	public GameDrawingPanel(GameBoard board) {
		this.board = board;
		setSize(board.boardWidth, board.boardHeight);
	}

	public void activateDeadActors() {
		for (DeadActor rock : board.deadActors) {
			rock.setOnScreen(true);
		}
	}

	public void paint(Graphics g) {
		//System.out.println("GameDrawingPanel.paint 1");
		// Allows me to make many settings changes in regards to graphics
		Graphics2D graphicSettings = (Graphics2D) g;
		AffineTransform identity = new AffineTransform();

		// Draw a black background that is as big as the game board
		graphicSettings.setColor(Color.BLACK);
		graphicSettings.fillRect(0, 0, getWidth(), getHeight());

		// Set rendering rules
		graphicSettings.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// Set the drawing color to white
		graphicSettings.setPaint(Color.WHITE);

		// Cycle through all of the Rock objects
		int rocksOnScreen = 0;

		for(DeadActor deadActor : board.deadActors) {
			if(deadActor.isOnScreen()) {
				rocksOnScreen++;
				for(DeadActor otherDeadActor : board.deadActors) {
					if(otherDeadActor.isOnScreen()) {
						if(deadActor != otherDeadActor) {
							deadActor.interact_deadActor(otherDeadActor);
						}
						deadActor.interact_actor(board.actor);
						deadActor.interact_torpedoes(board.torpedos);
					}
				}
				for(StationaryObject stationary : board.stationaries) {
					if(stationary.isOnScreen()) {
						deadActor.interact_stationary(stationary);
						stationary.interact_actor(deadActor);
					}
				}
				deadActor.move();
				graphicSettings.draw(deadActor);
			}
		}
		for(StationaryObject stationary : board.stationaries) {
			if(stationary.isOnScreen()) {
				stationary.interact_actor(board.actor);
				graphicSettings.draw(stationary);
			}
		}

		if(rocksOnScreen == 0) {
			// System.out.println("All dead actors hit");
			//this.activateDeadActors();
		}

		if(board.actor != null) {
			board.actor.move();
			graphicSettings.translate(board.actor.getXCenter(), board.actor.getYCenter());

			// Rotates the polygon
			graphicSettings.rotate(Math.toRadians(board.actor.getRotationAngle()));
			//System.out.println("GameDrawingPanel.paint draw");
			graphicSettings.draw(board.actor);
		}

		// Draw torpedos
		for (PhotonTorpedo torpedo : board.torpedos) {
			torpedo.move();
			if (torpedo.onScreen) {
				// Stroke the polygon torpedo on the screen
				graphicSettings.setTransform(identity);

				// Changes the torpedos center x & y vectors
				graphicSettings.translate(torpedo.getXCenter(), torpedo.getYCenter());
				graphicSettings.draw(torpedo);
			}
		}
	}

}
