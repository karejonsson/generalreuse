package general.reuse.gaminginteraction.board;

public enum GameBoardState {
	
	PLAYING("Playing"), STOPPED("Stopped"), DRAWING("Drawing"); 
	
	private String name;
	GameBoardState(String name) {
		this.name = name;
	}
	public String toString() {
		return name;
	}

}
