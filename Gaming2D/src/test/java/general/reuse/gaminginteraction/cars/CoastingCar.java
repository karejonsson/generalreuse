package general.reuse.gaminginteraction.cars;

import java.awt.Rectangle;
import java.util.Arrays;

import general.reuse.gaminginteraction.onboard.ActingPolygon;
import general.reuse.gaminginteraction.onboard.ArrowSteeringActor;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.onboard.Steerable;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;

public class CoastingCar extends ActingPolygon implements ArrowSteeringActor, Steerable {

	private static final long serialVersionUID = -8602504861932868033L;
	
	public static int[] polyXArray = {-15, 11, 15, 11, -15, -15};
	public static int[] polyYArray = {10, 10, 0, -10, -10, 10};

	public CoastingCar(int boardWidth, int boardHeight) {
		super(
				PolygonCalcules.centralize(polyXArray), 
				PolygonCalcules.centralize(polyYArray), 
				(int) (boardWidth/2),
				(int) (boardHeight/2),
				boardWidth, boardHeight, 3);
	}

	@Override
	public Rectangle getBoundingRectangle() {
		return getBounds();
	}

	@Override
	public void rotateLeft() {
		//System.out.println("Framåt "+goingForwards);
		int dir = goingForwards ? 1 : -1;
		double speed = dir*Math.sqrt(Math.pow(xVelocity, 2) + Math.pow(yVelocity, 2));
		if(goingForwards) {
			decreaseRotationAngle();
		}
		else {
			increaseRotationAngle();
		}
		xVelocity = speed * cosOfAngle(rotationAngle);
		yVelocity = speed * sinOfAngle(rotationAngle);
	}

	@Override
	public void rotateRight() {
		//System.out.println("Framåt "+goingForwards);
		int dir = goingForwards ? 1 : -1;
		double speed = dir*Math.sqrt(Math.pow(xVelocity, 2) + Math.pow(yVelocity, 2));
		if(goingForwards) {
			increaseRotationAngle();
		}
		else {
			decreaseRotationAngle();
		}
		xVelocity = speed * cosOfAngle(rotationAngle);
		yVelocity = speed * sinOfAngle(rotationAngle);
	}
	
	private boolean goingForwards = true;

	@Override
	public void forwards() {
		setMovingAngle(getRotationAngle());
		double c = cosOfAngle(getMovingAngle());
		double s = sinOfAngle(getMovingAngle());
		increaseXVelocity(c * 0.1);
		increaseYVelocity(s * 0.1);
		c = cosOfAngle(rotationAngle);
		s = sinOfAngle(rotationAngle);
		goingForwards = 
				(s*yVelocity >= 0) && 
				((c*s >= 0 && xVelocity*yVelocity >= 0) || 
				 (c*s <= 0 && xVelocity*yVelocity <= 0)) ;
		//System.out.println("Car forwards c "+c+", s "+s+", xVelocity "+xVelocity+", yVelocity "+yVelocity+", goingForwards "+goingForwards+", rotationAngle "+rotationAngle);
	}

	@Override
	public void backwards() {
		setMovingAngle(getRotationAngle());
		double c = cosOfAngle(getMovingAngle());
		double s = sinOfAngle(getMovingAngle());
		decreaseXVelocity(c * 0.1);
		decreaseYVelocity(s * 0.1);
		c = cosOfAngle(rotationAngle);
		s = sinOfAngle(rotationAngle);
		goingForwards = 
				(s*yVelocity >= 0) && 
				((c*s >= 0 && xVelocity*yVelocity >= 0) || 
				 (c*s <= 0 && xVelocity*yVelocity <= 0)) ;
		//System.out.println("Car backwards c "+c+", s "+s+", xVelocity "+xVelocity+", yVelocity "+yVelocity+", goingForwards "+goingForwards+", rotationAngle "+rotationAngle);
	}

	@Override
	public void collisionStationary(StationaryObject sobj) {
		setUnfree_xVelocity(getXVelocity());
		setUnfree_yVelocity(getYVelocity());
		setFree(sobj);
		setXVelocity(0);
		setYVelocity(0);
	}

	@Override
	public void collisionMoving() {
		setXVelocity(0);
		setYVelocity(0);
	}
	
	public static void main(String[] args) {
		System.out.println("MIN polyXArray "+PolygonCalcules.getMin(polyXArray));
		System.out.println("MAX polyXArray "+PolygonCalcules.getMax(polyXArray));
		
		int boardWidth = 1400;
		System.out.println("polyXArray "+Arrays.toString(polyXArray));
		int[] central = PolygonCalcules.centralize(polyXArray);
		System.out.println("Central "+Arrays.toString(central));
		int[] array = PolygonCalcules.add(central, (int) (boardWidth/2));
		System.out.println("Array "+Arrays.toString(array));
	}

	@Override
	public void shoot() {
	}

}
