package general.reuse.gaminginteraction.cars;

import general.reuse.gaminginteraction.board.StationaryObjectFactory;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.onboard.StationaryPolygon;

public class Block {

	public static int[] sPolyXArray = { 0, 24, 24, 0, 0 };
	public static int[] sPolyYArray = { 24, 24, 0, 0, 24 };

	public static StationaryObjectFactory factory = new StationaryObjectFactory() {
		@Override
		public StationaryObject get(int x, int y, int boardWidth, int boardHeight) {
			return new StationaryPolygon(
					Block.sPolyXArray, 
					Block.sPolyYArray, 
					x, y, boardWidth, boardHeight);
		}
	};

}
