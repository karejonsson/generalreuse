package general.reuse.gaminginteraction.cars;

public class SluggishCar extends CoastingCar {

	private static final long serialVersionUID = -8602504861932868033L;
	
	public static int[] polyXArray = {-15, 11, 15, 11, -15, -15};
	public static int[] polyYArray = {10, 10, 0, -10, -10, 10};
	
	private int speed;

	public SluggishCar(int boardWidth, int boardHeight, int speed) {
		super(boardWidth, boardHeight);
		this.speed = speed;
	}
	
	@Override
	public void forwards() {
		xVelocity = speed*cosOfAngle(rotationAngle);
		yVelocity = speed*sinOfAngle(rotationAngle);
	}

	@Override
	public void backwards() {
		xVelocity = -speed*cosOfAngle(rotationAngle);
		yVelocity = -speed*sinOfAngle(rotationAngle);
	}

	@Override
	public void move() {
		super.move();
		if(isFree()) {
			setXVelocity(0);
			setYVelocity(0);
		}
	}

}
