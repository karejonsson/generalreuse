package general.reuse.gaminginteraction.cars;

import java.util.ArrayList;

import general.reuse.gaminginteraction.asteriods.Rock;
import general.reuse.gaminginteraction.board.GameBoard;
import general.reuse.gaminginteraction.board.Gametime;
import general.reuse.gaminginteraction.board.StationaryObjectFactory;
import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.NonActingMover;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.onboard.StationaryPolygon;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;
import general.reuse.gaminginteraction.steering.KeyBoardSteering;

public class SluggishCarApp {

	public static final int boardWidth = 1400;
	public static final int boardHeight = 900;

	public static void setup(int noOfStationarys, int noOfRocks) {	
		ArrayList<DeadActor> rocks = new ArrayList<DeadActor>();	
		ArrayList<StationaryObject> stationaries = new ArrayList<StationaryObject>();	
		
		Gametime gametime = new Gametime();
		
		for (int i = 0; i < noOfRocks; i++) {
			// Find a random x & y starting point
			// The span part is on there to keep the Rock on the screen
			int randomStartXPos = (int) (Math.random() * (boardWidth - PolygonCalcules.span(Rock.sPolyXArray)) + 1);
			int randomStartYPos = (int) (Math.random() * (boardHeight - PolygonCalcules.span(Rock.sPolyYArray)) + 1);

			// Add the Rock object to the ArrayList based on the attributes sent
			rocks.add(new NonActingMover(
					Rock.sPolyXArray, 
					Rock.sPolyYArray, 
					randomStartXPos,
					randomStartYPos,
					((int) (Math.random()*2.5))+1, 
					((int) (Math.random()*2.5))+1, 
					boardWidth, 
					boardHeight,
					gametime));
		}
		
		for (int i = 0; i < noOfStationarys; i++) {
			// Find a random x & y starting point
			// The span part is on there to keep the Block on the screen
			int randomStartXPos = (int) (Math.random() * (boardWidth - PolygonCalcules.span(Block.sPolyXArray)) + 1);
			int randomStartYPos = (int) (Math.random() * (boardHeight - PolygonCalcules.span(Block.sPolyYArray)) + 1);

			// Add the Rock object to the ArrayList based on the attributes sent
			stationaries.add(Block.factory.get(randomStartXPos, randomStartYPos, boardWidth, boardHeight));
		}
		
		GameBoard board = new GameBoard(gametime, boardWidth, boardHeight, rocks, stationaries, Block.factory);
		SluggishCar car = new SluggishCar(boardWidth, boardHeight, 3);
		board.setActor(car);
		KeyBoardSteering steering = new KeyBoardSteering(car);
		board.addKeyListener(steering);
		board.setSteering(steering);
		board.setTitle("Sluggish Car driving");
		board.start();

	}

	public static void main(String[] args) {	
		setup(15, 0);
	}

}
