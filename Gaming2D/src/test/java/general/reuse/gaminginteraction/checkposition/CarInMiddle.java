package general.reuse.gaminginteraction.checkposition;

import java.util.ArrayList;

import general.reuse.gaminginteraction.board.GameBoard;
import general.reuse.gaminginteraction.board.Gametime;
import general.reuse.gaminginteraction.cars.Block;
import general.reuse.gaminginteraction.cars.CoastingCar;
import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.onboard.StationaryPolygon;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;
import general.reuse.gaminginteraction.steering.KeyBoardSteering;

public class CarInMiddle {

	public static final int boardWidth = 600;
	public static final int boardHeight = 600;

	public static void main(String[] args) {	
		ArrayList<DeadActor> rocks = new ArrayList<DeadActor>();
		ArrayList<StationaryObject> stationaries = new ArrayList<StationaryObject>();	
			
		Gametime gametime = new Gametime();
		
		stationaries.add(new StationaryPolygon(
				Block.sPolyXArray,
				Block.sPolyYArray,
				0,
				0,
				boardWidth, 
				boardHeight));

		stationaries.add(new StationaryPolygon(
				Block.sPolyXArray,
				Block.sPolyYArray,
				500,
				500,
				boardWidth, 
				boardHeight));
	
		
		GameBoard board = new GameBoard(gametime, boardWidth, boardHeight, rocks, stationaries, Block.factory);
		CoastingCar car = new CoastingCar(boardWidth, boardHeight);
		board.setActor(car);
		KeyBoardSteering steering = new KeyBoardSteering(car);
		board.addKeyListener(steering);
		board.setSteering(steering);
		board.setTitle("Car in middle");
		board.start();

	}

}
