package general.reuse.gaminginteraction.checkposition;

import java.util.ArrayList;

import general.reuse.gaminginteraction.asteriods.Rock;
import general.reuse.gaminginteraction.board.GameBoard;
import general.reuse.gaminginteraction.board.Gametime;
import general.reuse.gaminginteraction.cars.Block;
import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.NonActingMover;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;

public class RockAtZeroVertical {

	public static final int boardWidth = 100;
	public static final int boardHeight = 100;

	public static void main(String[] args) {	
		ArrayList<DeadActor> rocks = new ArrayList<DeadActor>();
		ArrayList<StationaryObject> stationaries = new ArrayList<StationaryObject>();	
			
		Gametime gametime = new Gametime();
		
		rocks.add(new NonActingMover(
				Rock.sPolyXArray,
				Rock.sPolyYArray,
				0,
				0,
				0, 
				1, 
				boardWidth, 
				boardHeight,
				gametime));

		GameBoard board = new GameBoard(gametime, boardWidth, boardHeight, rocks, stationaries, Block.factory);
		board.setTitle("Block still");
		board.start();
	}

}