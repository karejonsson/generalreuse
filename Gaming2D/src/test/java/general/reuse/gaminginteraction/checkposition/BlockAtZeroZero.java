package general.reuse.gaminginteraction.checkposition;

import java.util.ArrayList;
import java.util.Arrays;

import general.reuse.gaminginteraction.board.GameBoard;
import general.reuse.gaminginteraction.board.Gametime;
import general.reuse.gaminginteraction.cars.Block;
import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.NonActingMover;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.onboard.StationaryPolygon;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;

public class BlockAtZeroZero {

	public static final int boardWidth = 100;
	public static final int boardHeight = 100;

	public static void main(String[] args) {	
		ArrayList<DeadActor> rocks = new ArrayList<DeadActor>();
		ArrayList<StationaryObject> stationaries = new ArrayList<StationaryObject>();	
			
		Gametime gametime = new Gametime();
		
		stationaries.add(new StationaryPolygon(
				Block.sPolyXArray,
				Block.sPolyYArray,
				0,
				0,
				boardWidth, 
				boardHeight));

		GameBoard board = new GameBoard(gametime, boardWidth, boardHeight, rocks, stationaries, Block.factory);
		board.setTitle("Block still");
		board.start();
	}
	
	public static void _main(String[] args) {
		System.out.println(Arrays.toString(PolygonCalcules.centralize(Block.sPolyXArray)));
	}

}

