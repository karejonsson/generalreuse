package general.reuse.gaminginteraction.checkcollision;

import java.util.ArrayList;

import general.reuse.gaminginteraction.asteriods.Rock;
import general.reuse.gaminginteraction.board.GameBoard;
import general.reuse.gaminginteraction.board.Gametime;
import general.reuse.gaminginteraction.cars.Block;
import general.reuse.gaminginteraction.cars.CoastingCar;
import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.NonActingMover;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;
import general.reuse.gaminginteraction.steering.KeyBoardSteering;

public class OverlayRockCar {

	public static final int boardWidth = 200;
	public static final int boardHeight = 200;

	public static void main(String[] args) {	
		ArrayList<DeadActor> rocks = new ArrayList<DeadActor>();
		ArrayList<StationaryObject> stationaries = new ArrayList<StationaryObject>();	
			
		// Find a random x & y starting point
		// The -40 part is on there to keep the Rock on the screen
		int startXPos = boardWidth/2;
		int startYPos = boardHeight/2;
		
		Gametime gametime = new Gametime();

		// Add the Rock object to the ArrayList based on the attributes sent
		rocks.add(new NonActingMover(
				Rock.sPolyXArray, 
				Rock.sPolyYArray, 
				startXPos,
				startYPos,
				0,
				0,
				boardWidth, 
				boardHeight,
				gametime));
	
		GameBoard board = new GameBoard(gametime, boardWidth, boardHeight, rocks, stationaries, Block.factory);
		CoastingCar car = new CoastingCar(boardWidth, boardHeight);
		board.setActor(car);
		KeyBoardSteering steering = new KeyBoardSteering(car);
		board.addKeyListener(steering);
		board.setSteering(steering);
		board.setTitle("Overlay rock and car");
		board.start();
	
	}

}
