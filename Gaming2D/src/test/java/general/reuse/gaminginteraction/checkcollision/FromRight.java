package general.reuse.gaminginteraction.checkcollision;

import java.util.ArrayList;

import general.reuse.gaminginteraction.asteriods.Rock;
import general.reuse.gaminginteraction.board.GameBoard;
import general.reuse.gaminginteraction.board.Gametime;
import general.reuse.gaminginteraction.cars.Block;
import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.NonActingMover;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.onboard.StationaryPolygon;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;

public class FromRight {

	public static final int boardWidth = 250;
	public static final int boardHeight = 100;
	public static final int amountOfRocks = 1;
	public static final int amountOfStationarys = 1;

	public static void main(String[] args) {	
		ArrayList<DeadActor> rocks = new ArrayList<DeadActor>();
		ArrayList<StationaryObject> stationaries = new ArrayList<StationaryObject>();	
			
		// Find a random x & y starting point
		// The -40 part is on there to keep the Rock on the screen
		int startXPosRock = 180;
		int startYPosRock = 20;
		
		Gametime gametime = new Gametime();

		// Add the Rock object to the ArrayList based on the attributes sent
		rocks.add(new NonActingMover(
				Rock.sPolyXArray, 
				Rock.sPolyYArray, 
				startXPosRock,
				startYPosRock,
				-1,
				0,
				boardWidth, 
				boardHeight,
				gametime));
	

		int startXPosBlock = 50;
		int startYPosBlock = 20;

		// Add the Rock object to the ArrayList based on the attributes sent
		stationaries.add(new StationaryPolygon(
				Block.sPolyXArray, 
				Block.sPolyYArray, 
				startXPosBlock,
				startYPosBlock,
				boardWidth, 
				boardHeight));

		GameBoard board = new GameBoard(gametime, boardWidth, boardHeight, rocks, stationaries, Block.factory);
		board.setTitle("Rock from right");
		board.start();
	}

}
