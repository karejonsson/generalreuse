package general.reuse.gaminginteraction.test;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ExperimentWithSchedulation {

	public static void main(String[] args) throws InterruptedException {
		ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(5);
		ScheduledFuture<?> f = executor.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				System.out.println("a");
			}
			
		}, 0L, 1000L, TimeUnit.MILLISECONDS);
		System.out.println("-");
		Thread.currentThread().sleep(10000);
		System.out.println("-");
		f.cancel(false);
		f = executor.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				System.out.println("b");
			}
			
		}, 0L, 1000L, TimeUnit.MILLISECONDS);
	}

}
