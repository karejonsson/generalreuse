package general.reuse.gaminginteraction.breakthrough;

import java.util.ArrayList;
import java.util.List;

import general.reuse.gaminginteraction.board.GameBoard;
import general.reuse.gaminginteraction.board.Gametime;
import general.reuse.gaminginteraction.board.PolygonTouchActorPolicies;
import general.reuse.gaminginteraction.cars.Block;
import general.reuse.gaminginteraction.onboard.ActingPolygon;
import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.NonActingMover;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.onboard.StationaryPolygon;
import general.reuse.gaminginteraction.reuse.SoundEffects;
import general.reuse.gaminginteraction.steering.KeyBoardSteering;

public class SluggishRaquetApp {

	public static final int boardWidth = 600;
	public static final int boardHeight = 600;
	public static final int amountOfRocks = 1;
	public static final int amountOfStationarys = 1;

	public static void main(String[] args) {	
		ArrayList<DeadActor> rocks = new ArrayList<DeadActor>();
		ArrayList<StationaryObject> stationaries = new ArrayList<StationaryObject>();	
			
		// Find a random x & y starting point
		// The -40 part is on there to keep the Rock on the screen
		int startXPos = 580;
		int startYPos = 400;
		
		int randomXPos = 30;
		int randomYPos = 90;
		
		Gametime gametime = new Gametime();
		PolygonTouchActorPolicies polTochActorPoliciesNoPoints = new PolygonTouchActorPolicies() {
			@Override
			public void touch(StationaryPolygon polygon, DeadActor actor) {
				SoundEffects.play(StationaryPolygon.explodeFile);
				polygon.setOnScreen(false);
				addToInvisiblesNoPoint(polygon);
			}
		};

		PolygonTouchActorPolicies polTochActorPoliciesGetPoints = new PolygonTouchActorPolicies() {
			@Override
			public void touch(StationaryPolygon polygon, DeadActor actor) {
				SoundEffects.play(StationaryPolygon.explodeFile);
				polygon.setOnScreen(false);
				addToInvisiblesGetPoint(polygon);
			}
		};

		for(int h = 0 ; h < 20 ; h++) {
			for(int v = 0 ; v < 2 ; v++) {
				// Add the Rock object to the ArrayList based on the attributes sent
				stationaries.add(new StationaryPolygon(
						CutEdgesBlock.sPolyXArray, 
						CutEdgesBlock.sPolyYArray, 
						randomXPos+h*28,
						randomYPos+v*28,
						boardWidth, 
						boardHeight,
						polTochActorPoliciesGetPoints));
			}
			for(int v = 2 ; v < 5 ; v++) {
				// Add the Rock object to the ArrayList based on the attributes sent
				stationaries.add(new StationaryPolygon(
						Block.sPolyXArray, 
						Block.sPolyYArray, 
						randomXPos+h*28,
						randomYPos+v*28,
						boardWidth, 
						boardHeight,
						polTochActorPoliciesNoPoints));
			}
		}
		
		// Add the Rock object to the ArrayList based on the attributes sent
		NonActingMover ball = new NonActingMover(
				Ball.sPolyXArray, 
				Ball.sPolyYArray, 
				startXPos,
				startYPos,
				-1,
				-1,
				boardWidth, 
				boardHeight,
				gametime,
				new HoleDownDeadActorBoardLimitPolicies(),
				new PositionDrivenTouchActorPolicies() {
				},
				NonActingMover.defaultPhysicsPolicies);
		
		rocks.add(ball);

		GameBoard board = new GameBoard(gametime, boardWidth, boardHeight, rocks, stationaries, Block.factory);
		SluggishRaquet raquet = new SluggishRaquet(boardWidth, boardHeight, 1.5, 
				new ActingPolygon.StayWithinEdgesPolicies(boardWidth, boardHeight)
				);
		board.setActor(raquet);

		//KeyBoardSteering steering = new KeyBoardSteering(raquet);
		SluggishRaquetSteeringByFollow steering = new SluggishRaquetSteeringByFollow(board, ball, raquet);
		//board.addKeyListener(steering);
		board.setSteering(steering);
		board.setTitle("Sluggish raquet playing");
		board.start();

		board.setTitle("Break on through");
		board.start();
	}
	
	public static void gameOver(GameBoard board, KeyBoardSteering steering) {
		System.exit(0);
	}
	
	private static int ctrGetPoint = 0;
	
	private static void addToInvisiblesGetPoint(StationaryPolygon polygon) {
		ctrGetPoint++;
		System.out.println("Antal poänggivande träffar "+ctrGetPoint);
		if(ctrGetPoint >= 40) {
			System.out.println("Klart!");
		}
	}

	private static List<StationaryPolygon> invisiblesNoPoint = new ArrayList<>();
	private static int ctrNoPoint = 0;
	
	private static void addToInvisiblesNoPoint(StationaryPolygon polygon) {
		ctrNoPoint++;
		System.out.println("Antal träffar "+ctrNoPoint);
		invisiblesNoPoint.add(polygon);
		while(invisiblesNoPoint.size() > 25) {
			int idx = (int) Math.random()*invisiblesNoPoint.size();
			StationaryPolygon somePolygon = invisiblesNoPoint.remove(idx);
			somePolygon.setOnScreen(true);
		}
	}


}
