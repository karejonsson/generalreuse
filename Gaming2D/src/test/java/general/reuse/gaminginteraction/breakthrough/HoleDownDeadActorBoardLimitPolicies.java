package general.reuse.gaminginteraction.breakthrough;

import general.reuse.gaminginteraction.board.DeadActorBoardLimitPolicies;
import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.NonActingMover;

public class HoleDownDeadActorBoardLimitPolicies implements DeadActorBoardLimitPolicies {

	@Override
	public void outLeft(DeadActor actor) {
		((NonActingMover) actor).redirectHorizontally();
	}

	@Override
	public void outRight(DeadActor actor) {
		((NonActingMover) actor).redirectHorizontally();
	}

	@Override
	public void outDown(DeadActor actor) {
		SluggishRaquetApp.gameOver(null, null);
	}

	@Override
	public void outUp(DeadActor actor) {
		((NonActingMover) actor).redirectVertically();
	}	

}
