package general.reuse.gaminginteraction.breakthrough;

import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import general.reuse.gaminginteraction.board.GameBoard;
import general.reuse.gaminginteraction.onboard.NonActingMover;

public class SluggishRaquetSteeringByFollow implements Runnable {

	private GameBoard board;
	private NonActingMover ball;
	private SluggishRaquet raquet;
	
	public SluggishRaquetSteeringByFollow(GameBoard board, NonActingMover ball, SluggishRaquet raquet) {
		this.board = board;
		this.ball = ball;
		this.raquet = raquet;
	}
	
	public static final double off = 0; // minus -> vänster sida, plus -> höger

	@Override
	public void run() {
		double rx = raquet.getXCenter();
		Rectangle br = ball.getBounds();
		double bx = br.getCenterX();
		if(bx < rx+off) {
			raquet.rotateLeft();
		}
		if(bx > rx+off) {
			raquet.rotateRight();
		}
	}

}
