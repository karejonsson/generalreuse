package general.reuse.gaminginteraction.breakthrough;

import general.reuse.gaminginteraction.board.ActingPolygonBoardLimitPolicies;
import general.reuse.gaminginteraction.onboard.ActingPolygon;

public class SluggishRaquet extends CoastingRaquet {

	private static final long serialVersionUID = -8602504861932868033L;
	
	public SluggishRaquet(int boardWidth, int boardHeight, double speed) {
		super(boardWidth, boardHeight, speed, new ActingPolygon.StayWithinEdgesPolicies(boardWidth, boardHeight));
	}

	public SluggishRaquet(int boardWidth, int boardHeight, double speed, ActingPolygonBoardLimitPolicies boardPolicies) {
		super(boardWidth, boardHeight, speed, boardPolicies);
	}
	
	@Override
	public void move() {
		super.move();
		if(isFree()) {
			setXVelocity(0);
			setYVelocity(0);
		}
	}

}
