package general.reuse.gaminginteraction.breakthrough;

import java.awt.Rectangle;
import java.util.Arrays;

import general.reuse.gaminginteraction.board.ActingPolygonBoardLimitPolicies;
import general.reuse.gaminginteraction.onboard.ActingPolygon;
import general.reuse.gaminginteraction.onboard.ArrowSteeringActor;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.onboard.Steerable;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;

public class CoastingRaquet extends ActingPolygon implements ArrowSteeringActor, Steerable {

	private static final long serialVersionUID = -8602504861932868033L;
	private double speed = 0;
	
	public CoastingRaquet(int boardWidth, int boardHeight, double speed) {
		this(boardWidth, boardHeight, speed, new ActingPolygon.SlideOverEdgesPolicies(boardWidth, boardHeight));
	}
	
	public CoastingRaquet(int boardWidth, int boardHeight, double speed, ActingPolygonBoardLimitPolicies boardPolicies) {
		super(
				PolygonCalcules.centralize(RaquetPolygon.sPolyXArray), 
				PolygonCalcules.centralize(RaquetPolygon.sPolyYArray), 
				(int) (boardWidth/2 - (PolygonCalcules.getMax(RaquetPolygon.sPolyXArray))/2),
				boardHeight - PolygonCalcules.getMax(RaquetPolygon.sPolyYArray) - 15,
				boardWidth, boardHeight, 3, boardPolicies);
		this.speed = speed;
	}

	@Override
	public Rectangle getBoundingRectangle() {
		return getBounds();
	}

	@Override
	public void rotateLeft() {
		setMovingAngle(getRotationAngle());
		double c = cosOfAngle(getMovingAngle());
		double s = sinOfAngle(getMovingAngle());
		decreaseXVelocity(c * speed);
		decreaseYVelocity(s * speed);
		c = cosOfAngle(rotationAngle);
		s = sinOfAngle(rotationAngle);
		goingForwards = 
				(s*yVelocity >= 0) && 
				((c*s >= 0 && xVelocity*yVelocity >= 0) || 
				 (c*s <= 0 && xVelocity*yVelocity <= 0)) ;
		//System.out.println("Car backwards c "+c+", s "+s+", xVelocity "+xVelocity+", yVelocity "+yVelocity+", goingForwards "+goingForwards+", rotationAngle "+rotationAngle);
	}

	@Override
	public void rotateRight() {
		setMovingAngle(getRotationAngle());
		double c = cosOfAngle(getMovingAngle());
		double s = sinOfAngle(getMovingAngle());
		increaseXVelocity(c * speed);
		increaseYVelocity(s * speed);
		c = cosOfAngle(rotationAngle);
		s = sinOfAngle(rotationAngle);
		goingForwards = 
				(s*yVelocity >= 0) && 
				((c*s >= 0 && xVelocity*yVelocity >= 0) || 
				 (c*s <= 0 && xVelocity*yVelocity <= 0)) ;
		//System.out.println("Car forwards c "+c+", s "+s+", xVelocity "+xVelocity+", yVelocity "+yVelocity+", goingForwards "+goingForwards+", rotationAngle "+rotationAngle);
	}
	
	private boolean goingForwards = true;

	@Override
	public void forwards() {
	}

	@Override
	public void backwards() {
	}

	@Override
	public void collisionStationary(StationaryObject sobj) {
		setUnfree_xVelocity(getXVelocity());
		setUnfree_yVelocity(getYVelocity());
		setFree(sobj);
		setXVelocity(0);
		setYVelocity(0);
	}

	@Override
	public void collisionMoving() {
		setXVelocity(0);
		setYVelocity(0);
	}
	
	public static void main(String[] args) {
		System.out.println("MIN polyXArray "+PolygonCalcules.getMin(RaquetPolygon.sPolyXArray));
		System.out.println("MAX polyXArray "+PolygonCalcules.getMax(RaquetPolygon.sPolyYArray));
		
		int boardWidth = 1400;
		System.out.println("polyXArray "+Arrays.toString(RaquetPolygon.sPolyXArray));
		int[] central = PolygonCalcules.centralize(RaquetPolygon.sPolyYArray);
		System.out.println("Central "+Arrays.toString(central));
		int[] array = PolygonCalcules.add(central, (int) (boardWidth/2));
		System.out.println("Array "+Arrays.toString(array));
	}

	@Override
	public void shoot() {
	}

}
