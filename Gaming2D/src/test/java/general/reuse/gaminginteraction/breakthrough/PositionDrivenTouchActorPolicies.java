package general.reuse.gaminginteraction.breakthrough;

import java.awt.Rectangle;

import general.reuse.gaminginteraction.board.SteererTouchActorPolicies;
import general.reuse.gaminginteraction.onboard.ArrowSteeringActor;
import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.NonActingMover;

public class PositionDrivenTouchActorPolicies implements SteererTouchActorPolicies {

	@Override
	public void touchLeft(DeadActor object, ArrowSteeringActor actor) {
		NonActingMover objT = (NonActingMover) object;
		objT.redirectHorizontally();
	}

	@Override
	public void touchRight(DeadActor object, ArrowSteeringActor actor) {
		((NonActingMover) object).redirectHorizontally();
	}

	@Override
	public void touchDown(DeadActor object, ArrowSteeringActor actor) {
		NonActingMover objT = (NonActingMover) object;
		SluggishRaquet actT = (SluggishRaquet) actor;
		double objectAngle = objT.getMovingAngle();
		Rectangle actorsPlace = actT.getBounds();
		double actorCenter = actorsPlace.getCenterX();
		double objectCenter = objT.getBounds().getCenterX();
		double halfActorWidth = actorsPlace.getWidth() / 2;
		double fraction = (objectCenter - actorCenter)/halfActorWidth; // Vänster negativt, höger positivt
		double orientingAngle = -0.5*Math.PI - fraction*0.1*Math.PI;
		double reboundAngle = Math.PI+(2*orientingAngle - objectAngle);
		objT.setMovingAngle(fixAngle(reboundAngle, 0.5));
	}

	@Override
	public void touchUp(DeadActor object, ArrowSteeringActor actor) {
		((NonActingMover) object).redirectVertically();
	}
	
	public static double fixAngle(double angle, double limit) {
		double tmp = angleWithinOneRound(angle);
		if(tmp < limit) {
			tmp = limit;
		}
		if(tmp > (Math.PI-limit)) {
			tmp = Math.PI-limit;
		}
		return tmp;
	}
	
	public static double angleWithinOneRound(double angle) {
		return angle % (2*Math.PI);
	}
	
	public static void main(String[] args) {
		System.out.println(angleWithinOneRound(0.05));
		System.out.println(angleWithinOneRound(Math.PI-0.05));
		System.out.println(angleWithinOneRound(-0.05));
		System.out.println(angleWithinOneRound(-(Math.PI-0.05)));
	}

}
