package general.reuse.gaminginteraction.asteriods;

import java.util.ArrayList;

import general.reuse.gaminginteraction.board.GameBoard;
import general.reuse.gaminginteraction.board.Gametime;
import general.reuse.gaminginteraction.board.StationaryObjectFactory;
import general.reuse.gaminginteraction.cars.Block;
import general.reuse.gaminginteraction.onboard.DeadActor;
import general.reuse.gaminginteraction.onboard.NonActingMover;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.onboard.StationaryPolygon;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;
import general.reuse.gaminginteraction.steering.KeyBoardSteering;

public class AsteroidsApp {

	public static final int boardWidth = 1000;
	public static final int boardHeight = 750;
	public static final int amountOfRocks = 7;
	public static final int amountOfStationarys = 10;

	public static void main(String[] args) {	
		ArrayList<DeadActor> rocks = new ArrayList<DeadActor>();
		ArrayList<StationaryObject> stationaries = new ArrayList<StationaryObject>();
		
		Gametime gametime = new Gametime();
			
		for (int i = 0; i < amountOfRocks; i++) {
			// Find a random x & y starting point
			// The span part is on there to keep the Rock on the screen
			int randomStartXPos = (int) (Math.random() * (boardWidth - PolygonCalcules.span(Rock.sPolyXArray)) + 1);
			int randomStartYPos = (int) (Math.random() * (boardHeight - PolygonCalcules.span(Rock.sPolyYArray)) + 1);

			// Add the Rock object to the ArrayList based on the attributes sent
			rocks.add(new NonActingMover(
					Rock.sPolyXArray,
					Rock.sPolyYArray,
					randomStartXPos,
					randomStartYPos,
					((int) (Math.random()*2.5))+1, 
					((int) (Math.random()*2.5))+1, 
					boardWidth, 
					boardHeight,
					gametime));
		}
		
		for (int i = 0; i < amountOfStationarys; i++) {
			// Find a random x & y starting point
			// The span part is on there to keep the Block on the screen
			int randomStartXPos = (int) (Math.random() * (boardWidth - PolygonCalcules.span(Block.sPolyXArray)) + 1);
			int randomStartYPos = (int) (Math.random() * (boardHeight - PolygonCalcules.span(Block.sPolyYArray)) + 1);

			// Add the Block object to the ArrayList based on the attributes sent
			stationaries.add(Block.factory.get(randomStartXPos, randomStartYPos, boardWidth, boardHeight));
		}

		GameBoard board = new GameBoard(gametime, boardWidth, boardHeight, rocks, stationaries, Block.factory);
		SpaceShip spaceship = new SpaceShip(boardWidth, boardHeight, board);
		board.setActor(spaceship);
		KeyBoardSteering steering = new KeyBoardSteering(spaceship);
		board.addKeyListener(steering);
		board.setSteering(steering);
		board.setTitle("Java Asteroids");
		board.start();
	}

}
