package general.reuse.gaminginteraction.asteriods;

import java.awt.Rectangle;

import general.reuse.gaminginteraction.board.TorpedoInserter;
import general.reuse.gaminginteraction.onboard.ActingPolygon;
import general.reuse.gaminginteraction.onboard.ArrowSteeringActor;
import general.reuse.gaminginteraction.onboard.PhotonTorpedo;
import general.reuse.gaminginteraction.onboard.StationaryObject;
import general.reuse.gaminginteraction.onboard.Steerable;
import general.reuse.gaminginteraction.reuse.PolygonCalcules;

public class SpaceShip extends ActingPolygon implements ArrowSteeringActor, Steerable {

	private static final long serialVersionUID = -8602504861932868033L;
	
	public static int[] polyXArray = {-13, 14, -13, -5, -13};
	public static int[] polyYArray = {-15, 0, 15, 0, -15};
	
	private TorpedoInserter torpedoinserter;

	public SpaceShip(int boardWidth, int boardHeight, TorpedoInserter torpedoinserter) {
		super(PolygonCalcules.centralize(polyXArray), PolygonCalcules.centralize(polyYArray), boardWidth/2, boardHeight/2, boardWidth, boardHeight, 5);
		this.torpedoinserter = torpedoinserter;
	}

	@Override
	public Rectangle getBoundingRectangle() {
		return getBounds();
	}

	@Override
	public void rotateLeft() {
		decreaseRotationAngle();
	}

	@Override
	public void rotateRight() {
		increaseRotationAngle();
	}

	@Override
	public void forwards() {
		setMovingAngle(getRotationAngle());
		increaseXVelocity(cosOfAngle(getMovingAngle()) * 0.1);
		increaseYVelocity(sinOfAngle(getMovingAngle()) * 0.1);
	}

	@Override
	public void backwards() {
		setMovingAngle(getRotationAngle());
		decreaseXVelocity(cosOfAngle(getMovingAngle()) * 0.1);
		decreaseYVelocity(sinOfAngle(getMovingAngle()) * 0.1);
	}

	@Override
	public void collisionStationary(StationaryObject on) {
		setXCenter(boardWidth / 2);
		setYCenter(boardHeight / 2);
		setXVelocity(0);
		setYVelocity(0);
	}

	@Override
	public void collisionMoving() {
		setXCenter(boardWidth / 2);
		setYCenter(boardHeight / 2);
		setXVelocity(0);
		setYVelocity(0);
	}

	@Override
	public void shoot() {
		torpedoinserter.add(getPolygonNoseX(), getPolygonNoseY(), getRotationAngle());
	}

}
