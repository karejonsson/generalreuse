package general.reuse.metrics;

import java.awt.Color;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import general.reuse.metrics.registration.LongAccumulatorWithInterval;

public class LongAccumulatorWithIntervalSwingTest {

	public static void main(String[] arg) { 
		LongAccumulatorWithInterval metric = new LongAccumulatorWithInterval("Användning", "Antal", "Tid", 500, "yyyy-MM-dd HH:mm:ss");
    	metric.start();
		
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					LongAccumulatorWithIntervalTest.updates(metric);
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
		
		final int width = 800;
		final int heigth = 650;
		
		JPanel panel = new JPanel(); 
		panel.setSize(width,heigth);
		panel.setBackground(Color.CYAN); 
		//ImageIcon icon = new ImageIcon(arg); 
		final JLabel label = new JLabel(); 
		//label.setIcon(icon); 
		panel.add(label);
		
		JFrame st = new JFrame();
		st.setSize(width, heigth);
		
		st.getContentPane().add(panel);
		st.setVisible(true);
		
		TimerTask task = new TimerTask() {
			public void run() {
				try {
					//System.out.println("Uppdaterar på skärmen");
					label.setIcon(new ImageIcon(metric.getPNGBytearray(width, heigth, 60000l)));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(task, 1740, 1740);
	}

}
