package general.reuse.metrics.frame;

import general.reuse.metrics.DoubleAverageWithIntervalTest;
import general.reuse.metrics.registration.DoubleAverageWithInterval;

public class DoubleAverageFrameLauncherTest {
	
	public static void main(String[] args) throws InterruptedException {
		DoubleAverageFrameLauncher frame = new DoubleAverageFrameLauncher("Användning", "Antal", "Tid", 500, "yyyy-MM-dd HH:mm:ss");
		frame.prepareLocation(500, 40);
		frame.start(250, 300, 1740, 60000l);

		DoubleAverageWithInterval metric = frame.getMetric();
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				DoubleAverageWithIntervalTest.updates(metric);
			}
		});
		t.start();

		Thread.sleep(120000l);
		
		t.interrupt();
		frame.stop();
	}

}
