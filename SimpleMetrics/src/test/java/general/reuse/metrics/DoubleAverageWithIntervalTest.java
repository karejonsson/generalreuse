package general.reuse.metrics;

import general.reuse.metrics.registration.DoubleAverageWithInterval;
import general.reuse.metrics.registration.LongAccumulatorWithInterval;

public class DoubleAverageWithIntervalTest {

    public static void main(String args[]) throws InterruptedException {
    	run();
    }

    public static void run() throws InterruptedException {
    	DoubleAverageWithInterval metric = new DoubleAverageWithInterval("En titel", "Antal", "Tid", 2000, "yyyy-MM-dd HH:mm:ss");
    	metric.start();
    	
        System.out.println("TimerTask started");

        updates(metric);
        
        metric.stop();

        System.out.println("TimerTask cancelled");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    public static void updates(DoubleAverageWithInterval metric) {
        long ctr = 0;
        for(long l = 0 ; l < 100000 ; l++) {
    		//System.out.println("CounterWithIntervalTest updates");
        	ctr++;
        	ctr %= 628;
        	double sinarg = ctr / 100;
        	double sin = Math.sin(sinarg);
        	try {
        		Thread.sleep((int) (20+15*sin));
        	}
        	catch(InterruptedException ie) {
        		break;
        	}
        	metric.addValue(20+15*sin);
        }
    }

}
