package general.reuse.metrics.registration;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.data.time.TimePeriodValue;
import org.jfree.data.time.TimePeriodValues;
import org.jfree.data.time.TimePeriodValuesCollection;

public abstract class MetricWithInterval implements Serializable {
	
	private static final long serialVersionUID = 6632334503913000784L;

	private String sequence;
	private String verticalText;
	private String horizontalText;
	private long intervalmillis;
	private transient TimerTask task = null;
	private transient Timer timer = null;
	private long starttimemillis = 0l;
	private TimePeriodValues serie = null;
	private String dateformat = "yyyy/MM/dd HH:mm";
		
	public MetricWithInterval(String sequence, String verticalText, String horizontalText, long intervalmillis, String dateformat) {
		this.sequence = sequence;
		this.verticalText = verticalText;
		this.horizontalText = horizontalText;
		this.intervalmillis = intervalmillis;
		this.dateformat = dateformat;
		starttimemillis = System.currentTimeMillis();
		serie = new TimePeriodValues(sequence);
	}

	public boolean start() {
		if(task != null) {
			return false;
		}
		task = new TimerTask() {
			public void run() {
				MetricWithInterval.this.exec();
			}
		};
        timer = new Timer(true);
        timer.scheduleAtFixedRate(task, intervalmillis, intervalmillis);
        return true;
	}
	
	public boolean stop() {
		if(task == null) {
			return false;
		}
		timer.cancel();
		timer = null;
		task.cancel();
		task = null;
		return true;
	}
	
	public boolean save(File f) {
		if(task != null) {
			return false;
		}
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		boolean out = true;
		try {
			fos = new FileOutputStream(f);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
		} 
		catch(Exception e) {
			e.printStackTrace();
			out = false; 
		}
		try {
			if(oos != null) {
				oos.close();	
			}
		}
		catch(Exception e) { 
			out = false;
		}
		oos = null;
		try {
			if(fos != null) {
				fos.close();	
			}
		}
		catch(Exception e) { 
			out = false;
		}
		fos = null;
		return out;
	}

	public byte[] getBytes() {
		if(task != null) {
			return null;
		}
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput oo = null;
		byte[] out = null;
		try {
			oo = new ObjectOutputStream(bos);   
			oo.writeObject(this);
			oo.flush();
			out = bos.toByteArray();
		} 
		catch(Exception e) {
			out = null;
		}
		finally {
			try {
				bos.close();
			} 
			catch (IOException ex) {
			}
			try {
				oo.close();
			} 
			catch (IOException ex) {
			}
		}		
		return out;
	}
	
	public static MetricWithInterval load(File f) {
		MetricWithInterval out = null;
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			fis = new FileInputStream(f);
			ois = new ObjectInputStream(fis);
			out = (MetricWithInterval) ois.readObject();
		} 
		catch(Exception i) {
			System.out.println("MetricWithInterval could not be read");
			i.printStackTrace();
		} 
		try {
			if(ois != null) {
				ois.close();	
			}
		}
		catch(Exception e) { 
			out = null;
		}
		ois = null;
		try {
			if(fis != null) {
				fis.close();	
			}
		}
		catch(Exception e) { 
			out = null;
		}
		fis = null;
		return out;
	}
	
	public static MetricWithInterval recreateFromBytes(byte[] in) {
		ByteArrayInputStream bis = null;
		ObjectInput oi = null;
		MetricWithInterval out = null;
		try {
			bis = new ByteArrayInputStream(in);
			oi = new ObjectInputStream(bis);
			out = (MetricWithInterval) oi.readObject(); 
		} 
		catch(Exception e) {
		}
		try {
			if(oi != null) {
				oi.close();
			}
		}
		catch(IOException ex) {
		}
		try {
			if(bis != null) {
				bis.close();
			}
		}
		catch(IOException ex) {
		}
		return out;
	}
	
	private void exec() {
		//System.out.println("Metric exec");
		SimpleTimePeriod period = new SimpleTimePeriod(new Date(starttimemillis), new Date());
		starttimemillis = System.currentTimeMillis();
		TimePeriodValue item = new TimePeriodValue(period, getValue());
		serie.add(item);
		cropNotification();
	}	
	
	protected abstract Number getValue();
	protected abstract void cropNotification();
	
	private JFreeChart createChart(String title, long timebackmillis) throws CloneNotSupportedException {
        final TimePeriodValuesCollection data = new TimePeriodValuesCollection();
        try {
        	int lower = Math.max(0, serie.getItemCount()-((int)(timebackmillis/intervalmillis)));
        	int upper = serie.getItemCount();
        	TimePeriodValues used = new TimePeriodValues(sequence);
        	for(int i = lower ; i < upper ; i++) {
        		TimePeriodValue item = serie.getDataItem(i);
        		used.add(item);
        	}
        	data.addSeries(used);
        }
        catch(Exception e) { 
        	e.printStackTrace();
        	return null;
        }
        
        final JFreeChart chart = ChartFactory.createTimeSeriesChart(
                title,
                horizontalText, 
                verticalText, 
                data,
                //PlotOrientation.VERTICAL,
                true,
                true,
                false
            );

        XYPlot plot = (XYPlot)chart.getPlot();
        DateAxis axis = (DateAxis)plot.getDomainAxis();
        axis.setDateFormatOverride( new SimpleDateFormat(dateformat));
		return chart;
	}
	
	public byte[] getPNGBytearray(int width, int height, long timebackmillis) throws IOException, CloneNotSupportedException {
	    JFreeChart chart = createChart(sequence, timebackmillis);
	    if(chart == null) {
	    	return null;
	    }
	    chart.setTitle(sequence);
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    ChartUtils.writeChartAsPNG(out, chart, width, height);
		return out.toByteArray();
	}

	public byte[] getJPEGBytearray(int width, int height, long timebackmillis) throws IOException, CloneNotSupportedException {
	    JFreeChart chart = createChart(sequence, timebackmillis);
	    if(chart == null) {
	    	return null;
	    }
	    chart.setTitle(sequence);
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    ChartUtils.writeChartAsPNG(out, chart, width, height);
		return out.toByteArray();
	}

}
