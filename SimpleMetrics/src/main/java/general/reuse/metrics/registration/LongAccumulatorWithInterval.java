package general.reuse.metrics.registration;

public class LongAccumulatorWithInterval extends MetricWithInterval {

	private static final long serialVersionUID = -3501716882669025556L;

	public LongAccumulatorWithInterval(String sequence, String verticalText, String horizontalText, long intervalmillis, String dateformat) {
		super(sequence, verticalText, horizontalText, intervalmillis, dateformat);
	}

	private long accumulation = 0;
	
	public void addValue(long value) {
		accumulation += value;
	}

	@Override
	protected Number getValue() {
		return accumulation;
	}

	@Override
	protected void cropNotification() {
		accumulation = 0;
	}

}
