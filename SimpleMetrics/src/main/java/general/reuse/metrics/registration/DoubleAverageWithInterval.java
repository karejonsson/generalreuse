package general.reuse.metrics.registration;

public class DoubleAverageWithInterval extends MetricWithInterval {

	private static final long serialVersionUID = 7836301073307077686L;

	public DoubleAverageWithInterval(String sequence, String verticalText, String horizontalText, long intervalmillis, String dateformat) {
		super(sequence, verticalText, horizontalText, intervalmillis, dateformat);
	}

	private double accumulation = 0;
	private int counter = 0;
	
	public void addValue(double value) {
		accumulation += value;
		counter++;
		//System.out.println("ADD "+getValue());
	}

	@Override
	protected Number getValue() {
		if(counter == 0) {
			return null;			
		}
		return accumulation/counter;
	}

	@Override
	protected void cropNotification() {
		//System.out.println("CROP "+getValue());
		accumulation = 0;
		counter = 0;
	}

}