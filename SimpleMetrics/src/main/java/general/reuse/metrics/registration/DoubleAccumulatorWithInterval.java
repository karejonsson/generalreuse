package general.reuse.metrics.registration;

public class DoubleAccumulatorWithInterval extends MetricWithInterval {

	private static final long serialVersionUID = 164187877556350808L;

	public DoubleAccumulatorWithInterval(String sequence, String verticalText, String horizontalText, long intervalmillis, String dateformat) {
		super(sequence, verticalText, horizontalText, intervalmillis, dateformat);
	}

	private double accumulation = 0;
	
	public void addValue(double value) {
		accumulation += value;
	}

	@Override
	protected Number getValue() {
		return accumulation;
	}

	@Override
	protected void cropNotification() {
		accumulation = 0;
	}

}
