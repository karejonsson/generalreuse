package general.reuse.metrics.registration;

public class IntegerAccumulatorWithInterval extends MetricWithInterval {

	private static final long serialVersionUID = -4077729823514135318L;

	public IntegerAccumulatorWithInterval(String sequence, String verticalText, String horizontalText, long intervalmillis, String dateformat) {
		super(sequence, verticalText, horizontalText, intervalmillis, dateformat);
	}

	private int accumulation = 0;
	
	public void addValue(int value) {
		accumulation += value;
	}

	@Override
	protected Number getValue() {
		return accumulation;
	}

	@Override
	protected void cropNotification() {
		accumulation = 0;
	}

}
