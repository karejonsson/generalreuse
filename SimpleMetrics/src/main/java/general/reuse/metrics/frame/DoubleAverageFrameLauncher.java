package general.reuse.metrics.frame;

import java.awt.Color;
import java.awt.Point;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import general.reuse.metrics.registration.DoubleAverageWithInterval;

public class DoubleAverageFrameLauncher {
	
	private String sequence;
	private String verticalText;
	private String horizontalText;
	private long intervalmillis;
	private String dateformat;
	
	public DoubleAverageFrameLauncher(String sequence, String verticalText, String horizontalText, long intervalmillis, String dateformat) {
		this.sequence = sequence;
		this.verticalText = verticalText;
		this.horizontalText = horizontalText;
		this.intervalmillis = intervalmillis;
		this.dateformat = dateformat;
	}
	
	private Point position = null;
	
	public void prepareLocation(Point position) {
		this.position = position;
	}
	
	public void prepareLocation(int hor, int ver) {
		this.position = new Point(hor, ver);
	}
	
	private JFrame st = null;
	private TimerTask task = null;
	private Timer timer = null;
	private DoubleAverageWithInterval metric = null;
	
	public void start(final int width, final int heigth, long rate, long timespan) {
		
		metric = new DoubleAverageWithInterval(sequence, verticalText, horizontalText, intervalmillis, dateformat);
    	metric.start();

		JPanel panel = new JPanel(); 
		panel.setSize(heigth, width);
		panel.setBackground(Color.CYAN); 
		//ImageIcon icon = new ImageIcon(arg); 
		final JLabel label = new JLabel(); 
		//label.setIcon(icon); 
		panel.add(label);
		
		st = new JFrame();
		st.setSize(heigth, width);
		
		st.getContentPane().add(panel);
		st.setVisible(true);
		if(position != null) {
			st.setLocation(position);
		}
		
		task = new TimerTask() {
			public void run() {
				try {
					//System.out.println("Uppdaterar på skärmen");
					label.setIcon(new ImageIcon(metric.getPNGBytearray(heigth, width, timespan)));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		timer = new Timer(true);
        timer.scheduleAtFixedRate(task, rate, rate);
	}
	
	public DoubleAverageWithInterval getMetric() {
		return metric;
	}

	public void stop() {
		if(st != null) {
			st.setVisible(false);
			st.dispose();
		}
		if(task != null) {
			task.cancel();
		}
		if(timer != null) {
			timer.cancel();
			timer.purge();
		}
		if(metric != null) {
			metric.stop();
		}
	}
	
}
