package general.reuse.webmanipulation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

public class Download {

    public static byte[] getBytes(String url) throws UnsupportedEncodingException, MalformedURLException {
    	return getBytes(new URL(url));
    }

    public static byte[] getBytes(URL url) throws UnsupportedEncodingException {
    	ByteArrayOutputStream out = null;
        URLConnection conn = null;
        InputStream in = null;

        try {
            conn = url.openConnection();
            out = getByteStream(conn.getInputStream());
        } 
        catch (Exception e) { 
            e.printStackTrace();
        } 
        finally {
            try {
                if (in != null) {
                    in.close(); 
                }
                if (out != null) {
                    out.close();
                }
            } 
            catch (IOException ioe) {
            }
        }
        if(out != null) {
        	return out.toByteArray();
        }
        return null;
    }
    
    public static ByteArrayOutputStream getByteStream(InputStream in) throws IOException {
    	ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];

        int numRead;

        while ((numRead = in.read(buffer)) != -1) {
            out.write(buffer, 0, numRead);
        }
        return out;
    }
 	
    public static byte[] getBytes(InputStream in) throws IOException {
    	return getByteStream(in).toByteArray();
    }
 	
	public static String getString(String url, String encoding) {
		try {
			String out = new String(getBytes(url), encoding);
			//System.out.println(out);
			return out;
		}
		catch(Exception e) {}
		return null;
	}

	public static String getString(URL url, String encoding) {
		try {
			String out = new String(getBytes(url), encoding);
			//System.out.println(out);
			return out;
		}
		catch(Exception e) {}
		return null;
	}
	
	public static void initForPromiscuousDownloads() throws NoSuchAlgorithmException {
		X509TrustManager[] trustAllCerts = new X509TrustManager[] {
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
					public void checkClientTrusted(X509Certificate[] certs, String authType) {
					}
					public void checkServerTrusted(X509Certificate[] certs, String authType) {
					}
				}
		};

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		try {
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {

			//@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}

		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		
	}
	
	private static final String HEXES = "0123456789abcdef";

	public static String bytesToCharacterHexPresentation(byte[] raw) {
	    final StringBuilder hex = new StringBuilder(2 * raw.length);
	    for (final byte b : raw) {
	        hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
	    }
	    return hex.toString();
	}
	
	public static void print(String content, String sequence) {
		Integer idxs[] = HtmlLinkOperations.indexesOf(content, sequence);
		System.out.println("check = "+idxs.length+" "+sequence);
	}
	
	public static void main(String args[]) throws UnsupportedEncodingException, MalformedURLException, NoSuchAlgorithmException {
		//String from = "https://lagrummet.se/hit-vander-du-dig/myndighetersansvar";
		//String to = "http://www.domstol.se/templates/DV_InfoPage.aspx?id=740";
		//String to = "http://rkrattsbaser.gov.se/sfst?bet=1736:0123 1";
		//String to = "http://rkrattsbaser.gov.se/sfst?bet=2003:389";
		//String result = HtmlLinkOperations.concatenateUrlsWithParameterConversionOfLink(from, to);
		System.setProperty("jsse.enableSNIExtension", "false");
		initForPromiscuousDownloads();
		String urls = "http://www1.lagr1.dev.dom.se/rattsinformation/rattspraxis";
		//System.out.println("OUT: "+result);
		byte cont[] = getBytes(urls);
		String page = new String(cont);
		System.out.println("SIDA: "+page);
		print(page, "js/readspeaker");
		print(page, "href");
		print(page, "rattsinformation");
		print(page, "ul class");
		print(page, "<li>");
		print(page, "</li>");
		print(page, "<ul>");
		print(page, "</ul>");
		print(page, "Hos Domstolsverket kan du");
		print(page, "<article>");
		print(page, "</article>");
		print(page, "div");
	}

}
