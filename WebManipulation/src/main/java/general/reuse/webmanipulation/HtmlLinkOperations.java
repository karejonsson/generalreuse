package general.reuse.webmanipulation;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlLinkOperations {

	public static String concatenateUrls(String source, String link) {
		try {
			URL url1 = new URL(source);
			URL url2 = new URL(url1, link);
			return url2.toString();
		}
		catch(Exception e) {
			System.err.println("ReadLinkStructure.concatenateUrls("+source+", "+link+")");
			e.printStackTrace();
			return null;
		}
	}
	
	public static String concatenateUrlsWithParameterConversionOfLink(String source, String link) {
		try {
			URL url1 = new URL(source);
			URL url2 = new URL(url1, encodeOnlyParametersOfUrl(link));
			String out = url2.toString();
			//System.out.println("HtmlLinkOperations.concatenateUrlsWithParameterConversionOfLink "+source+" + "+link+" -> "+out);
			return out;
		}
		catch(Exception e) {
			System.err.println("HtmlLinkOperations.concatenateUrlsWithParameterConversionOfLink("+source+", "+link+")");
			e.printStackTrace();
			return null;
		}
	}
	
	public static String encodeOnlyParametersOfUrl(String urls) throws UnsupportedEncodingException, MalformedURLException, URISyntaxException {
		final int idx = urls.indexOf("?");
		if(idx == -1) {
			return urls;
		}
		//URL url= new URL(urls);
		//URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
		//return uri.toASCIIString();
		//return urls.substring(0, idx)+"?"+URLEncoder.encode(urls.substring(idx+1), "UTF-8");
		
		/*
		String parms_orig[] = urls.substring(idx+1).split("&");
		String parms_fixed[] = new String[parms_orig.length];
		for(int i = 0 ; i < parms_orig.length ; i++) {
			int equalpos = parms_orig[i].indexOf('=');
			if(equalpos == -1) {
				parms_fixed[i] = parms_orig[i];
			}
			else {
				parms_fixed[i] = parms_orig[i].substring(0, equalpos)+"="+URLEncoder.encode(parms_orig[i].substring(equalpos+1));
			}
		}
		StringBuffer sb = new StringBuffer();
		sb.append(parms_fixed[0]);
		for(int i = 1 ; i < parms_fixed.length ; i++) {
			sb.append("&"+parms_fixed[i]);
		}
		
		return urls.substring(0, idx)+"?"+sb.toString();
		*/
		return urls.replaceAll(" ", "+");
 	}

	public static final String linkpattern = "<a[^>]+href=[\"']?([\"'>]+)[\"']?[^>]*>(.*?)</a>";
	
	public static Vector<String> getRelativeHrefLinks(String page) {
		Pattern linkPattern = Pattern.compile(linkpattern,  Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
		Matcher pageMatcher = null;
		try {
			pageMatcher = linkPattern.matcher(page);
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		ArrayList<String> links = new ArrayList<String>();
		//int ctr = 0;
		Vector<String> out = new Vector<String>();
		while(pageMatcher.find()){
			String link = pageMatcher.group();
		    //System.out.println("XX-> "+(++ctr)+" "+link);
		    links.add(link);
		    String href = getHrefFromLink(link).replaceAll("&amp;", "&");
		    //System.out.println("To URL decoding "+href.replaceAll("&amp;", "&"));
		    String visualText = getVisibleTextFromLink(link);
		    //System.out.println("href="+href);
		    out.add(href);
		}
		return out;
	}
	
	public static final String iframepattern = "<iframe[^>]+src=[\"']?([\"'>]+)[\"']?[^>]*>(.*?)</iframe>";
	
	public static Vector<String> getRelativeSrcLinks(String page) {
		Pattern linkPattern = Pattern.compile(iframepattern,  Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
		Matcher pageMatcher = null;
		try {
			pageMatcher = linkPattern.matcher(page);
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		ArrayList<String> links = new ArrayList<String>();
		//int ctr = 0;
		Vector<String> out = new Vector<String>();
		while(pageMatcher.find()){
			String link = pageMatcher.group();
		    //System.out.println("XX-> "+(++ctr)+" "+link);
		    links.add(link);
		    String href = getSrcFromLink(link).replaceAll("&amp;", "&");
		    //System.out.println("To URL decoding "+href.replaceAll("&amp;", "&"));
		    String visualText = getVisibleTextFromLink(link);
		    //System.out.println("href="+href);
		    out.add(href);
		}
		return out;
	}
	
	public static String getHrefFromLink(String htmllink) {
		return getAttributeFromLink(htmllink, "href");
	}
	
	public static String getSrcFromLink(String htmllink) {
		return getAttributeFromLink(htmllink, "src");
	}
	
	public static String getAttributeFromLink(String htmllink, String attribute) {
		int idxHref = htmllink.toLowerCase().indexOf(attribute.toLowerCase());
		if(idxHref == -1) {
			return null;
		}
		int idx1Hyphen = htmllink.substring(idxHref).indexOf("\"")+idxHref;
		//System.out.println("------"+html+" 1 "+idx1Hyphen);
		int idx2Hyphen = -1;
		int len = attribute.length();
		if(idx1Hyphen == idxHref-1) {
			idx1Hyphen = htmllink.substring(idxHref+len).indexOf("'")+idxHref+len;
			idx2Hyphen = htmllink.substring(idx1Hyphen+1).indexOf("'")+idx1Hyphen+1;
		}
		else {
			idx2Hyphen = htmllink.substring(idx1Hyphen+1).indexOf("\"")+idx1Hyphen+1;			
		}
		//System.out.println(" 1 "+idx1Hyphen+"    :    2 "+idx2Hyphen);
		String ref = htmllink.substring(idx1Hyphen+1, idx2Hyphen);
		return ref;
	}
	
	public static String getVisibleTextFromLink(String htmllink) {
		int idxEndHref = Math.max(htmllink.indexOf("\">"), htmllink.indexOf("'>"));
		int idxEndA = htmllink.indexOf("</");
		return htmllink.substring(idxEndHref+2, idxEndA);
	}
	
	public static Vector<String> getALinks(String page, String sourceUrl)  {
		Vector<String> rlinks = getRelativeHrefLinks(page);
		if(rlinks == null) {
			return null;
		}

		Vector<String> out = new Vector<String>();
		for(String r : rlinks) {
			String nextUrl = concatenateUrlsWithParameterConversionOfLink(sourceUrl, r);
			if(!out.contains(nextUrl)) {
				out.add(nextUrl);
			}
		}
		return out;
	}
	
	public static Vector<String> getIframeLinks(String page, String sourceUrl)  {
		Vector<String> rlinks = getRelativeSrcLinks(page);
		if(rlinks == null) {
			return null;
		}

		Vector<String> out = new Vector<String>();
		for(String r : rlinks) {
			String nextUrl = concatenateUrlsWithParameterConversionOfLink(sourceUrl, r);
			if(!out.contains(nextUrl)) {
				out.add(nextUrl);
			}
		}
		return out;
	}
	
	public static String getDomain(String url) {
		try {
			Integer[] slashPlaces = indexesOf(url,"/");
			if(slashPlaces.length == 2) {
				return url.substring(slashPlaces[1]+1).trim();
			}
			return url.substring(slashPlaces[1]+1, slashPlaces[2]).trim();
		}
		catch(Exception e) {
			return null;
		}
	}
	
	public static Integer[] indexesOf(String content, String sequence) {
		Vector<Integer> out = new Vector<Integer>();
		int idx = -1;
		while((idx = content.indexOf(sequence, idx+1)) != -1) {
			out.add(new Integer(idx));
		}
		Integer[] anArray = new Integer[out.size()];
		return out.toArray(anArray);
	}


}
