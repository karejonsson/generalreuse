package general.reuse.webmanipulation;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

//import sun.security.ssl.SSLSocketImpl;

public class AdvancedHTTPS {
	
	public static boolean isHttpsUrl(String url) {
		return url.toLowerCase().trim().startsWith("https:");
	}
	
	public static boolean isMisconfigured(String url) {
		HttpsURLConnection httpsconn = null;
		try {
			httpsconn = getSingleHandshakeProtocolHostedHttpsConnection(url);
	        int code = httpsconn.getResponseCode();
			return false;
		}
		catch(Exception e) {
			String message = e.getMessage();
			//System.out.println("isMisconfigured "+url);
			return message.contains("handshake alert") && message.contains("unrecognized_name");
		}
		finally {
			try { if(httpsconn != null) { httpsconn.disconnect(); } } catch(Exception e) { }
		}
	}

	public static HttpsURLConnection getSingleHandshakeProtocolHostlessHttpsConnection(String url) throws IOException, KeyManagementException, NoSuchAlgorithmException {
		return getSingleHandshakeProtocolHostlessHttpsConnection(url, null);
	}
	
	public static HttpsURLConnection getSingleHandshakeProtocolHostedHttpsConnection(String url) throws IOException, KeyManagementException, NoSuchAlgorithmException {
		return getSingleHandshakeProtocolHostedHttpsConnection(url, null);
	}
	
	public static HttpsURLConnection getSingleHandshakeProtocolHostlessHttpsConnection(String url, String protocol) throws IOException, KeyManagementException, NoSuchAlgorithmException {
		URL website = new URL(url);

		URLConnection connection = website.openConnection();
		HttpsURLConnection httpsUrlConnection = (HttpsURLConnection) connection;
		SSLSocketFactory sslSocketFactory = createTrustAllSslSingleHandshakeProtocolHostlessSocketFactory(protocol);
		httpsUrlConnection.setSSLSocketFactory(sslSocketFactory);
		return httpsUrlConnection;
	}

	public static HttpsURLConnection getSingleHandshakeProtocolHostedHttpsConnection(String url, String protocol) throws IOException, KeyManagementException, NoSuchAlgorithmException {
		URL website = new URL(url);

		URLConnection connection = website.openConnection();
		HttpsURLConnection httpsUrlConnection = (HttpsURLConnection) connection;
		SSLSocketFactory sslSocketFactory = createTrustAllSslSingleHandshakeProtocolHostedSocketFactory(protocol);
		httpsUrlConnection.setSSLSocketFactory(sslSocketFactory);
		return httpsUrlConnection;
	}

	private static SSLSocketFactory createTrustAllSslSingleHandshakeProtocolHostlessSocketFactory(String protocol) throws NoSuchAlgorithmException, KeyManagementException {
		TrustManager[] byPassTrustManagers = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}

			public void checkClientTrusted(X509Certificate[] chain, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] chain, String authType) {
			}
		} };
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(null, byPassTrustManagers, new SecureRandom());
		return getInternalSingleHandshakeProtocolHostlessFactoryForJavaVersion(sslContext, protocol);
	}
	
	private static SSLSocketFactory createTrustAllSslSingleHandshakeProtocolHostedSocketFactory(String protocol) throws NoSuchAlgorithmException, KeyManagementException {
		TrustManager[] byPassTrustManagers = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}

			public void checkClientTrusted(X509Certificate[] chain, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] chain, String authType) {
			}
		} };
		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(null, byPassTrustManagers, new SecureRandom());
		return getInternalSingleHandshakeProtocolHostedFactoryForJavaVersion(sslContext, protocol);
	}
	
	private static SSLSocketFactory getInternalSingleHandshakeProtocolHostlessFactoryForJavaVersion(SSLContext sslContext, String protocol) {
		//System.out.println("xy1z23");
		if(System.getProperty("java.version").contains("1.7")) {
			return new InternalSingleHandshakeProtocolHostlessSocketFactoryJava7(sslContext.getSocketFactory(), protocol);
		} 
		//return sslContext.getSocketFactory();
		return new InternalSingleHandshakeProtocolHostlessSocketFactoryJavaNot7(sslContext.getSocketFactory(), protocol);
	}

	private static SSLSocketFactory getInternalSingleHandshakeProtocolHostedFactoryForJavaVersion(SSLContext sslContext, String protocol) {
		return new InternalSingleHandshakeProtocolHostedSocketFactory(sslContext.getSocketFactory(), protocol);
	}

	private static class InternalSingleHandshakeProtocolHostlessSocketFactoryJava7 extends SSLSocketFactory {

		private SSLSocketFactory sf = null;
		private String protocol = null;

		public InternalSingleHandshakeProtocolHostlessSocketFactoryJava7(SSLSocketFactory sf, String _protocol) {
			this.sf = sf;
			this.protocol = _protocol;
		}

		@Override
		public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
			//return new SSLSocket(s, host, port, autoClose);
			SSLSocket out = (SSLSocket) sf.createSocket(s, host, port, autoClose);
			/*
			String[] supprot = out.getSupportedProtocols();
			System.out.println("Antal Supporterade protokoll "+supprot.length);
			for(int i = 0 ; i < supprot.length ; i++) {
				System.out.println("Supporterat protokoll "+supprot[i]);
			}
			*/
			if(protocol != null) {
				//System.out.println("Använder "+protocol+" för host "+host);
				out.setEnabledProtocols(new String[] { protocol });
				// Tog bort följande i maj 2020 för att kompilera med java 11.
				//((SSLSocketImpl) out).setHost(null);
			}
			else {
				//System.out.println("Ändrar inte protokoll för host "+host);
			}
			return out;
		}

		@Override
		public String[] getDefaultCipherSuites() {
			return sf.getDefaultCipherSuites();
		}

		@Override
		public String[] getSupportedCipherSuites() {
			return sf.getSupportedCipherSuites();
		}

		@Override
		public Socket createSocket(String arg0, int arg1) throws IOException, UnknownHostException {
			Socket out = sf.createSocket(arg0, arg1);
			System.out.println("2 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1);
		}

		@Override
		public Socket createSocket(InetAddress arg0, int arg1) throws IOException {
			Socket out = sf.createSocket(arg0, arg1);
			System.out.println("3 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1);
		}

		@Override
		public Socket createSocket(String arg0, int arg1, InetAddress arg2, int arg3)
				throws IOException, UnknownHostException {
			Socket out = sf.createSocket(arg0, arg1, arg2, arg3);
			System.out.println("4 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1, arg2, arg3);
		}

		@Override
		public Socket createSocket(InetAddress arg0, int arg1, InetAddress arg2, int arg3) throws IOException {
			Socket out = sf.createSocket(arg0, arg1, arg2, arg3);
			System.out.println("5 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1, arg2, arg3);
		}
	}
	
	private static class InternalSingleHandshakeProtocolHostlessSocketFactoryJavaNot7 extends SSLSocketFactory {

		private SSLSocketFactory sf = null;
		private String protocol = null;

		public InternalSingleHandshakeProtocolHostlessSocketFactoryJavaNot7(SSLSocketFactory sf, String _protocol) {
			this.sf = sf;
			this.protocol = _protocol;
		}

		@Override
		public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
			//return new SSLSocket(s, host, port, autoClose);
			SSLSocket out = (SSLSocket) sf.createSocket(s, null, port, autoClose);
			/*
			String[] supprot = out.getSupportedProtocols();
			System.out.println("Antal Supporterade protokoll "+supprot.length);
			for(int i = 0 ; i < supprot.length ; i++) {
				System.out.println("Supporterat protokoll "+supprot[i]);
			}
			*/
			if(protocol != null) {
				//System.out.println("Använder "+protocol+" för host "+host);
				out.setEnabledProtocols(new String[] { protocol });
				//((SSLSocketImpl) out).setHost(host);
			}
			else {
				//System.out.println("Ändrar inte protokoll för host "+host);
			}
			return out;
		}

		@Override
		public String[] getDefaultCipherSuites() {
			return sf.getDefaultCipherSuites();
		}

		@Override
		public String[] getSupportedCipherSuites() {
			return sf.getSupportedCipherSuites();
		}

		@Override
		public Socket createSocket(String arg0, int arg1) throws IOException, UnknownHostException {
			Socket out = sf.createSocket(arg0, arg1);
			System.out.println("2 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1);
		}

		@Override
		public Socket createSocket(InetAddress arg0, int arg1) throws IOException {
			Socket out = sf.createSocket(arg0, arg1);
			System.out.println("3 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1);
		}

		@Override
		public Socket createSocket(String arg0, int arg1, InetAddress arg2, int arg3)
				throws IOException, UnknownHostException {
			Socket out = sf.createSocket(arg0, arg1, arg2, arg3);
			System.out.println("4 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1, arg2, arg3);
		}

		@Override
		public Socket createSocket(InetAddress arg0, int arg1, InetAddress arg2, int arg3) throws IOException {
			Socket out = sf.createSocket(arg0, arg1, arg2, arg3);
			System.out.println("5 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1, arg2, arg3);
		}
	}

	private static class InternalSingleHandshakeProtocolHostedSocketFactory extends SSLSocketFactory {

		private SSLSocketFactory sf = null;
		private String protocol = null;

		public InternalSingleHandshakeProtocolHostedSocketFactory(SSLSocketFactory sf, String _protocol) {
			this.sf = sf;
			this.protocol = _protocol;
		}

		@Override
		public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
			//return new SSLSocket(s, host, port, autoClose);
			SSLSocket out = (SSLSocket) sf.createSocket(s, host, port, autoClose);
			/*
			String[] supprot = out.getSupportedProtocols();
			System.out.println("Antal Supporterade protokoll "+supprot.length);
			for(int i = 0 ; i < supprot.length ; i++) {
				System.out.println("Supporterat protokoll "+supprot[i]);
			}
			*/
			if(protocol != null) {
				//System.out.println("Använder "+protocol+" för host "+host);
				out.setEnabledProtocols(new String[] { protocol });
				//((SSLSocketImpl) out).setHost(host);
			}
			else {
				//System.out.println("Ändrar inte protokoll för host "+host);
			}
			return out;
		}

		@Override
		public String[] getDefaultCipherSuites() {
			return sf.getDefaultCipherSuites();
		}

		@Override
		public String[] getSupportedCipherSuites() {
			return sf.getSupportedCipherSuites();
		}

		@Override
		public Socket createSocket(String arg0, int arg1) throws IOException, UnknownHostException {
			Socket out = sf.createSocket(arg0, arg1);
			System.out.println("2 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1);
		}

		@Override
		public Socket createSocket(InetAddress arg0, int arg1) throws IOException {
			Socket out = sf.createSocket(arg0, arg1);
			System.out.println("3 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1);
		}

		@Override
		public Socket createSocket(String arg0, int arg1, InetAddress arg2, int arg3)
				throws IOException, UnknownHostException {
			Socket out = sf.createSocket(arg0, arg1, arg2, arg3);
			System.out.println("4 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1, arg2, arg3);
		}

		@Override
		public Socket createSocket(InetAddress arg0, int arg1, InetAddress arg2, int arg3) throws IOException {
			Socket out = sf.createSocket(arg0, arg1, arg2, arg3);
			System.out.println("5 "+out.getClass().getName());
			return out;
			//return sf.createSocket(arg0, arg1, arg2, arg3);
		}
	}
}