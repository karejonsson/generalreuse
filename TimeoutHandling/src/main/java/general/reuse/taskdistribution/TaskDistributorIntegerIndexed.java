package general.reuse.taskdistribution;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaskDistributorIntegerIndexed {

	private static final ExecutorService execpool;
	private static final int cpus;
	static {
		cpus = Runtime.getRuntime().availableProcessors();
		execpool = Executors.newFixedThreadPool(cpus);
	}

	private static void execute(Runnable task) {
		execpool.execute(task);
	}

	public static void shutdown() {
		execpool.shutdown();
	}
	
	private int finalIndexNonInclusive;
	private Processable processable;
	
	public interface Processable {
		void process(int idx);
	}

	private TaskDistributorIntegerIndexed(int finalIndexNonInclusive, Processable processable) {
		this.finalIndexNonInclusive = finalIndexNonInclusive;
		this.processable = processable;
	}

	public void start() {
		int targetedCpus = cpus < finalIndexNonInclusive ? cpus : 1;

		final CountDownLatch latches = new CountDownLatch(targetedCpus);
		int sizeOfInterval = (finalIndexNonInclusive + targetedCpus - 1) / targetedCpus;
		for(int cpuIdx = 0; cpuIdx < targetedCpus; cpuIdx++) {
			final int startIndexInclusive = cpuIdx * sizeOfInterval;
			int tmp = (cpuIdx + 1) * sizeOfInterval;
			final int endIndexNonInclusive = tmp <= finalIndexNonInclusive ? tmp : finalIndexNonInclusive;
			Runnable task = new Runnable() {
				@Override
				public void run() {
					for(int idx = startIndexInclusive ; idx < endIndexNonInclusive ; idx++) {
						processable.process(idx);
					}
					latches.countDown();
				}
			};
			TaskDistributorIntegerIndexed.execute(task);
		}
		try {
			latches.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	public static void process(int finalIndexNonInclusive, Processable processable) {
		new TaskDistributorIntegerIndexed(finalIndexNonInclusive, processable).start();
	}

	public static void main(String args[]) {
		new TaskDistributorIntegerIndexed(12, new Processable() {

			@Override
			public void process(int idx) {
				System.out.println("Index "+idx);
			}
			
		}).start();
		System.out.println("Efter");
	}
	
}
