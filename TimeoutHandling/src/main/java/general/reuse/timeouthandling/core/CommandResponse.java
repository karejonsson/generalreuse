package general.reuse.timeouthandling.core;

import java.io.PrintWriter;
import java.io.StringWriter;

public class CommandResponse<SpecialException extends Exception, Responsetype> extends TaskResponse<SpecialException, Responsetype> {
	private String stderr = null;
	private boolean withSudo = false;
	private String command = null;
	
	public CommandResponse(String command) {
		this.command = command;
	}
	public String getStderr() {
		return stderr;
	}
	public void setStderr(String stderr) {
		this.stderr = stderr;
	}
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString());
		if(stderr == null) {
			sb.append("No stderr\n");
		}
		else {
			if(stderr.length() < 1) {
				sb.append("No stderr\n");
			}
			else {
				sb.append("Stderr: "+stderr+"\n");
			}
		}
		if(withSudo) {
			sb.append("With sudo\n");
		}
		else {
			sb.append("Not with sudo");
		}
		sb.append("Command: "+command+"\n");
		return sb.toString();
	}
	public boolean isWithSudo() {
		return withSudo;
	}
	public void setWithSudo(boolean withSudo) {
		this.withSudo = withSudo;
	}
	public static String getString(Throwable t) {
		if(t == null) {
			return "";
		}
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		return sw.toString();
	}
}
