package general.reuse.timeouthandling.core;

import java.io.PrintWriter;
import java.io.StringWriter;

public class TaskResponse <SpecialException extends Exception, Responsetype> {
	private Integer exitStatus;
	private Responsetype response;
	private Exception exception;
	private SpecialException specialException;
	
	public TaskResponse() {
	}
	public boolean getGeneralOutcome() {
		if(exitStatus == null) {
			return false;
		}
		return exitStatus == 0;
	}

	public Integer getExitStatus() {
		return exitStatus;
	}
	public void setExitStatus(Integer exitStatus) {
		this.exitStatus = exitStatus;
	}
	public Responsetype getResponse() {
		return response;
	}
	public void setResponse(Responsetype response) {
		this.response = response;
	}
	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}
	public String toString() {
		StringBuffer sb = new StringBuffer();
		if(exitStatus == null) {
			sb.append("No exit status\n");
		}
		else {
			sb.append("Exit status: "+exitStatus+"\n");
		}
		if(exception == null) {
			sb.append("No exception thrown\n");
		}
		else {
			sb.append("Exception stack trace:\n-------------"+getString(exception)+"\n----------------");
		}
		if(specialException == null) {
			sb.append("No special exception thrown\n");
		}
		else {
			sb.append("Special exception stack trace:\n-------------"+getString(specialException)+"\n----------------");
		}
		if(response == null) {
			sb.append("No response\n");
		}
		else {
			sb.append("response "+response);
		}
		return sb.toString();
	}
	public void setSpecialException(SpecialException je) {
		this.specialException = je;
	}
	public SpecialException getSpecialException() {
		return specialException;
	}
	public static String getString(Throwable t) {
		if(t == null) {
			return "";
		}
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		return sw.toString();
	}
}
