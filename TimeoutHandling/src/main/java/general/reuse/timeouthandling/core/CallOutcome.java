package general.reuse.timeouthandling.core;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class CallOutcome<SpecialException extends Exception, Responsetype> {
	
	private boolean timedOut = false;
	private TimeoutException timeoutException = null;
	private boolean execution = false;
	private ExecutionException executionException = null;
	private boolean outcome = true;
	private Exception outcomeException = null;
	private TaskResponse<SpecialException, Responsetype> cr = null;
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		if(timedOut) {
			sb.append("Timed out with exception "+CommandResponse.getString(timeoutException)+"\n");
		}
		else {
			sb.append("No timeout"+"\n");
		}
		if(execution) {
			sb.append("Execution exception "+CommandResponse.getString(executionException)+"\n");
		}
		else {
			sb.append("No execution exception"+"\n");
		}
		if(outcome) {
			sb.append("Outcome exception "+CommandResponse.getString(outcomeException)+"\n");
		}
		else {
			sb.append("Bad outcome"+"\n");
		}
		sb.append("----------------- Command:\n"+cr);
		return sb.toString();
	}
	
	public CallOutcome(TaskResponse<SpecialException, Responsetype> cr) {
		this.cr = cr;
	}
	
	public TaskResponse<SpecialException, Responsetype> getCommandResponse() {
		return cr;
	}

	public void setTimedOut(boolean b) {
		timedOut = b;
	}

	public void setTimeoutException(TimeoutException e) {
		this.timeoutException = e;
	}

	public void setExecution(boolean b) {
		execution = b;
	}

	public void setExecutionException(ExecutionException e) {
		executionException = e;
	}

	public void setOutcome(boolean b) {
		outcome = b;
	}

	public void setOutcomeException(Exception e) {
		outcomeException = e;
	}
	
	public boolean getGeneralOutcome() {
		if(outcome) {
			return cr.getGeneralOutcome();
		}
		if((!timedOut) && (!execution)) {
			return cr.getGeneralOutcome();
		}
		return false;
	}


}
