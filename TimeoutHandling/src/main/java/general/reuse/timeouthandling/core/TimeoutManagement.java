package general.reuse.timeouthandling.core;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class TimeoutManagement {

	public static <Responsetype, SpecialException extends Exception> CallOutcome<SpecialException, Responsetype> invoke(final Runnable runnable, long timeout,
			TimeUnit timeUnit, final CallOutcome<SpecialException, Responsetype> cr) throws Exception {
		invoke(new Callable<Responsetype>() {

			public Responsetype call() throws Exception {
				runnable.run();
				return null;
			}
		}, timeout, timeUnit, cr);
		return cr;
	} 

	private static <Responsetype, SpecialException extends Exception> Responsetype invoke(Callable<Responsetype> callable, long timeout,
			TimeUnit timeUnit, final CallOutcome<SpecialException, Responsetype> cr) throws Exception {
		final ExecutorService executor = Executors.newSingleThreadExecutor();
		final Future<Responsetype> future = executor.submit(callable);
		executor.shutdown(); // This does not cancel the already-scheduled task.
		try {
			return future.get(timeout, timeUnit);
		} catch (TimeoutException e) {
			// remove this if you do not want to cancel the job in progress
			// or set the argument to 'false' if you do not want to interrupt
			// the thread
			future.cancel(true);
			cr.setTimedOut(true);
			cr.setTimeoutException(e);
		} catch (ExecutionException e) {
			// unwrap the root cause
			cr.setExecution(true);
			cr.setExecutionException(e);
		}
		return null;
	}

}
