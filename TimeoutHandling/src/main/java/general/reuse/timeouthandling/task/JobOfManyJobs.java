package general.reuse.timeouthandling.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JobOfManyJobs implements Runnable {
	
	private List<TaskMonitorer> tasks; 
	private long timeout; 
	private Long[] timeoutReattempts; 
	private ResourceManagement rm;
	private int referenceMax = 0;
	private int tasksDoneInCurrentList = 0;
	private int currentPhase = -1;
	private String phaseMessage = null;
	private boolean hasExecuted = false;
	private List<TaskMonitorer> requeued = null;
	private boolean done = false;
	
	public JobOfManyJobs(List<TaskMonitorer> tasks, long timeout, Long[] timeoutReattempts, ResourceManagement rm) {
		this.tasks = tasks;
		this.timeout = timeout;
		this.timeoutReattempts = timeoutReattempts;
		this.rm = rm;
	}

	@Override
	public void run() {
		hasExecuted = true;
		if(rm != null) {
			if(!rm.isActive()) {
				rm.start();
			}
			if(!rm.isActive()) {
				System.out.println("Cannot initiate resources");
			}
		}
		List<TaskMonitorer> failed = new ArrayList<TaskMonitorer>();
		requeued = failed;
		
		int consequtiveFailures = 0;
		
		referenceMax = tasks.size();
		
		tasksDoneInCurrentList = 0;
		currentPhase = 0;
		phaseMessage = "First attempt. "+tasks.size()+" elements";
		
		for(TaskMonitorer task : tasks) {
			task.run(0, timeout);
			if(!task.isDone()) {
				failed.add(task);
				consequtiveFailures++;
				currentPhase++;
				phaseMessage = "First attempt. "+tasks.size()+" elements. "+failed.size()+" requeued.";
			}
			else {
				consequtiveFailures = 0;
			}
			if(consequtiveFailures > 2) {
				if(rm != null) {
					rm.stop();
					rm.start();
					if(!rm.isActive()) {
						System.out.println("Cannot initiate resources. Time "+(new Date()));
					}
				}
				consequtiveFailures = 0;
			}
			tasksDoneInCurrentList++;
		}
		
		//System.out.println("Failed tasks "+failed.size());
		
		consequtiveFailures = 0;
		
		List<TaskMonitorer> failedAgain = new ArrayList<TaskMonitorer>();
		requeued = failedAgain;
		
		int doneReattempts = 0;
		tasksDoneInCurrentList = 0;
		
		while(failed.size() > 0 && doneReattempts < timeoutReattempts.length) {
			
			tasksDoneInCurrentList = 0;
			referenceMax = failed.size();
			currentPhase++;
			phaseMessage = "Repetition "+(1+doneReattempts)+". "+failed.size()+" elements.";
			
			System.out.println("Reattempt with "+failed.size()+" remaining tasks");

			for(TaskMonitorer task : failed) {
				task.run(doneReattempts, timeoutReattempts[doneReattempts]);
				if(!task.isDone()) {
					failedAgain.add(task);
					consequtiveFailures++;
					currentPhase++;
					phaseMessage = "Repetition "+(1+doneReattempts)+". "+failed.size()+" elements. "+failedAgain.size()+" requeued.";
				}
				else {
					consequtiveFailures = 0;
				}
				if(consequtiveFailures > 2) {
					if(rm != null) {
						rm.stop();
						rm.start();
						if(!rm.isActive()) {
							System.out.println("Cannot initiate resources. Time "+(new Date()));
						}
					}
					consequtiveFailures = 0;
				}
				tasksDoneInCurrentList++;
			}
			
			doneReattempts++;
			failed = failedAgain;
			failedAgain = new ArrayList<TaskMonitorer>();
			requeued = failedAgain;
		}
		
		System.out.println("Efter antal "+failed.size()+", doneReattempts "+doneReattempts);
		printFractionDoneParameters();
		
		for(TaskMonitorer terminallyFailed : failed) {
			terminallyFailed.onTerminallyFailed();
		}

		System.out.println("Efter allt");
		printFractionDoneParameters();
		
		done = true;
	}
	
	/*
tasksDoneInCurrentList 0
((double) tasksDoneInCurrentList) 0.0
referenceMax 819279
(requeued == null ? 0 : requeued.size()) 0
(referenceMax+(requeued == null ? 0 : requeued.size())) 819279
getFractionDone() 0.0
	 */
	
	public int getPhase() {
		return currentPhase;
	}
	
	public int getTasksDoneInCurrentList() {
		return tasksDoneInCurrentList;
	}
	
	public String getPhaseMessage() {
		return phaseMessage;
	}
	
	public double getFractionDoneOfCurrentList() {
		double out = ((double) tasksDoneInCurrentList) / (referenceMax+(requeued == null ? 0 : requeued.size()));
		//System.out.println("JobOfManyJobs.getFractionDone() -> "+out+", referenceMax "+referenceMax+", tasksDoneInCurrentList "+tasksDoneInCurrentList);
		return out;
	}
	
	public void printFractionDoneParameters() {
		System.out.println("tasksDoneInCurrentList "+tasksDoneInCurrentList);
		System.out.println("((double) tasksDoneInCurrentList) "+((double) tasksDoneInCurrentList));
		System.out.println("referenceMax "+referenceMax);
		System.out.println("(requeued == null ? 0 : requeued.size()) "+(requeued == null ? 0 : requeued.size()));
		System.out.println("(referenceMax+(requeued == null ? 0 : requeued.size())) "+(referenceMax+(requeued == null ? 0 : requeued.size())));
		System.out.println("getFractionDone() "+getFractionDoneOfCurrentList());
	}
	
	public boolean getHasExecutedStatus() {
		return hasExecuted;
	}
	
	public boolean isDone() {
		return done;
	}
	
}
