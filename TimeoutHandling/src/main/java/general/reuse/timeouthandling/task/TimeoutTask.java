package general.reuse.timeouthandling.task;

public interface TimeoutTask {
	
	void runTimeconsuming(int attemptNo) throws InterruptedException ;
	void onFinishedBeQuick(int attemptNo);
	void onTimeoutBeQuick(int attemptNo);
	void onTerminallyFailed();
	boolean isDone();

}
