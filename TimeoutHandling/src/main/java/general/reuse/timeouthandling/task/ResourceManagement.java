package general.reuse.timeouthandling.task;

public interface ResourceManagement {
	
	boolean isActive();
	void stop();
	void start();
	boolean isProbablyOK();

}
