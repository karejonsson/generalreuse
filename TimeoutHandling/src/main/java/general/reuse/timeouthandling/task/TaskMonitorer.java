package general.reuse.timeouthandling.task;

import java.util.concurrent.TimeUnit;

import general.reuse.timeouthandling.core.CallOutcome;
import general.reuse.timeouthandling.core.TaskResponse;
import general.reuse.timeouthandling.core.TimeoutManagement;

public class TaskMonitorer {
	
	private TimeoutTask tot;
	private boolean isDone = false;
	
	public TaskMonitorer(TimeoutTask tot) {
		this.tot = tot;
	}

	public void run(final int attemptNo, long millisAllowed) {
		if(isDone) {
			return;
		}
		final TaskResponse<Exception, Void> tr = new TaskResponse<Exception, Void>();
		final CallOutcome<Exception, Void> out = new CallOutcome<Exception, Void>(tr);
		
		Runnable r = new Runnable() {
			@Override
			public void run() {
				try {
					tot.runTimeconsuming(attemptNo);
					isDone = tot.isDone();
				} catch (InterruptedException e) {
				}
			}
		};
		
		try {
			TimeoutManagement.invoke(r, millisAllowed, TimeUnit.MILLISECONDS, out);
		} catch (Exception e) {
		}
		
		if(isDone) {
			tot.onFinishedBeQuick(attemptNo);
		}
		else {
			tot.onTimeoutBeQuick(attemptNo);
		}
	}
	
	public boolean isDone() {
		return isDone;
	}
	
	public void onTerminallyFailed() {
		tot.onTerminallyFailed();
	}

}
