package general.reuse.timeouthandling.monitortask;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import general.reuse.timeouthandling.task.JobOfManyJobs;

public class ProgressWebReporter {

    public static void run(JobOfManyJobs jomj, int port, String title) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new MyHandler(jomj, title));
        server.setExecutor(null); // creates a default executor
        server.start();
        jomj.run();
        server.stop(0);
    }

    static class MyHandler implements HttpHandler {
    	
    	private JobOfManyJobs jomj = null;
    	private String title = null;
    	
    	public MyHandler(JobOfManyJobs jomj, String title) {
    		this.jomj = jomj;
    		this.title = title;
    	}
    	
        @Override
        public void handle(HttpExchange t) throws IOException {
        	int progress = (int) (100*jomj.getFractionDoneOfCurrentList());
            String response = title+": "+jomj.getPhaseMessage()+". Progress "+progress+"%. Done "+jomj.getTasksDoneInCurrentList()+".";
            response = response.replaceAll("\\.\\.", ".");
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
    
    public static void main(String args[]) {
    	String r = "abc .. efg";
    	r = r.replaceAll("\\.\\.", ".");
    	System.out.println("HEJ: "+r);
    }
    
}