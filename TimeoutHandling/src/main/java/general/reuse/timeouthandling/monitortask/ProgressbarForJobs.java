package general.reuse.timeouthandling.monitortask;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import general.reuse.timeouthandling.task.JobOfManyJobs;

public class ProgressbarForJobs extends JPanel implements ActionListener, PropertyChangeListener {
	
	private JProgressBar progressBar;
	private JButton startButton;
	private JTextArea taskOutput;
	private Task task;

	private JobOfManyJobs jomj = null;
	
	public ProgressbarForJobs(JobOfManyJobs jomj, boolean interactive) {
		super(new BorderLayout());

		this.jomj = jomj;

		if(interactive) {
			//Create the demo's UI.
			startButton = new JButton("Start");
			startButton.setActionCommand("start");
			startButton.addActionListener(this);
		}

		progressBar = new JProgressBar(0, 100);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);

		taskOutput = new JTextArea(5, 20);
		taskOutput.setMargin(new Insets(5,5,5,5));
		taskOutput.setEditable(false);

		JPanel panel = new JPanel();
		if(interactive) {
			panel.add(startButton);
		}
		panel.add(progressBar);

		add(panel, BorderLayout.PAGE_START);
		add(new JScrollPane(taskOutput), BorderLayout.CENTER);
		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

	}
	
	/**
	 * Invoked when the user presses the start button.
	 */
	public void actionPerformed(ActionEvent evt) {
		if(startButton != null) {
			startButton.setEnabled(false);			
		}
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		//Instances of javax.swing.SwingWorker are not reusuable, so
		//we create new instances as needed.
		//SwingUtilities.invokeLater(jomj);
        Thread t = new Thread(jomj);
        t.start();
		task = new Task();
		task.addPropertyChangeListener(this);
		task.execute();
	}
	
	private int previousPhase = -1;

	/**
     * Invoked when task's progress property changes.
     */
    public void propertyChange(PropertyChangeEvent evt) {
        if ("progress".equals(evt.getPropertyName())) {
            int progress = (Integer) evt.getNewValue();
            progressBar.setValue(progress);
            int phase = jomj.getPhase();
        	if(phase != previousPhase) {
                taskOutput.append(jomj.getPhaseMessage()+"\n");
                previousPhase = phase;
        	}

            //taskOutput.append(String.format(
            //        "Completed %d%% of task.\n", task.getProgress()));
        } 
    }
    
    private JFrame frame = null;

	private void createAndShowGUI(String title, JobOfManyJobs jomj, boolean interactive) {
		//Create and set up the window.
		frame = new JFrame(title);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		//Create and set up the content pane.
		setOpaque(true); //content panes must be opaque
		frame.setContentPane(this);

		//Display the window.
		frame.pack();
		frame.setVisible(true);
		if(!interactive) {
			actionPerformed(null);
		}
	}
	
	public void closeGUI() {
		if(frame != null) {
			frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
		}
	}
	
	public static void run(String title, JobOfManyJobs jomj, boolean interactive) {
		//Schedule a job for the event-dispatching thread:
		//creating and showing this application's GUI.
		ProgressbarForJobs newContentPane = new ProgressbarForJobs(jomj, interactive);
		Runnable r = new Runnable() {
			public void run() {
				newContentPane.createAndShowGUI(title, jomj, interactive);
			}
		};
		javax.swing.SwingUtilities.invokeLater(r);
        while(!jomj.isDone()) {
            //Sleep for up to one second.
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignore) {}
            //System.out.println("ProgressbarForJobs.run: I snurran");
        }
        //System.out.println("ProgressbarForJobs.run: Ute ur snurran");
        newContentPane.closeGUI();
        //System.out.println("ProgressbarForJobs.run: Efter nedtagningen av GUIt.");
	}

	class Task extends SwingWorker<Void, Void> {
		/*
		 * Main task. Executed in background thread.
		 */
		@Override
		public Void doInBackground() {
			
            while(!jomj.isDone()) {
                //Sleep for up to one second.
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignore) {}
                int progress = (int) (100*jomj.getFractionDoneOfCurrentList());
                setProgress(progress);
            }
			return null;
		}
		
		/*
		 * Executed in event dispatching thread
		 */
		@Override
		public void done() {
			Toolkit.getDefaultToolkit().beep();
			setCursor(null); //turn off the wait cursor
			taskOutput.append("Done!\n");
			if(startButton != null) {
				startButton.setEnabled(true);
			}			
			if(startButton == null) {
				//System.exit(0);
			}			
		}

	}

}
