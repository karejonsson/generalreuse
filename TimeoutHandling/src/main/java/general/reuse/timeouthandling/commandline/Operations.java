package general.reuse.timeouthandling.commandline;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import general.reuse.timeouthandling.core.CallOutcome;
import general.reuse.timeouthandling.core.CommandResponse;

public class Operations {

	public static final String stringend_UNIX = "\n";
	public static final String stringend_WINDOWS = "\r\n";
	public static String stringend = stringend_UNIX;
	
	public static void logMessage(StringBuffer sb, String message) {
		sb.append(message);
		System.out.println(message);
	}

	public static String getMd5sum(Session session, String file, int timeout, StringBuffer report) {
		String command = "md5sum "+file;
		logMessage(report, "Skall bestämma MD5-summa på fil. Kommando <"+command+">"+stringend);
		CallOutcome<JSchException, String[]> callr = null;
		
		try {
			callr = ExecuteJsch.runCommand(session, command, timeout);

			if(!callr.getGeneralOutcome()) {
				logMessage(report, "Det blev fel vid körningen av kommandot:"+stringend+callr.toString());
				return null;
			}
		}
		catch(Exception e) {
			logMessage(report, "Fel vid invokeringen."+stringend+CommandResponse.getString(e)+stringend);
			return null;
		}
		
		String identifyingFilenamePart = file;
		if(file.contains("~")) {
			if(file.contains("/")) {
				identifyingFilenamePart = file.substring(file.lastIndexOf("/"));
			}
		}

		try {
			CommandResponse<JSchException, String[]> cr = (CommandResponse<JSchException, String[]>) callr.getCommandResponse();
			String lines[] = cr.getResponse();
			for(int i = 0 ; i < lines.length ; i++) {
				String line = lines[i];
				if(line == null) {
					continue;
				}
				if(line.contains(identifyingFilenamePart)) {
					String linesplit[] = lines[i].split(" ");
					if(linesplit == null) {
						continue;
					}
					if(linesplit.length < 1) {
						continue;
					}
					return linesplit[0];
				}
			}
			logMessage(report, "Hittade inte rätt rad"+stringend+callr.toString()+stringend);
			return null;
		}
		catch(Exception e) {
			logMessage(report, "Fel vid framvaskningen av svaret."+stringend+CommandResponse.getString(e)+stringend);
		}
		return null;
	}
	
	public static Boolean fileExists(Session session, String file, int timeout, StringBuffer report) {
		String command = "ls -l "+file;
		logMessage(report, "Skall bestämma om fil finns. Kommando <"+command+">"+stringend);
		
		CallOutcome<JSchException, String[]> callr = null;
		try {
			callr = ExecuteJsch.runCommand(session, command, timeout);

			if(!callr.getGeneralOutcome()) {
				logMessage(report, "Det blev fel vid körningen av kommandot:"+stringend+callr.toString());
				return null;
			}
		}
		catch(Exception e) {
			logMessage(report, "Fel vid invokeringen."+stringend+CommandResponse.getString(e)+stringend);
			return null;
		}
		
		String identifyingFilenamePart = file;
		if(file.contains("~")) {
			if(file.contains("/")) {
				identifyingFilenamePart = file.substring(file.lastIndexOf("/"));
			}
		}

		try {
			CommandResponse<JSchException, String[]> cr = (CommandResponse<JSchException, String[]>) callr.getCommandResponse();
			String lines[] = cr.getResponse();
			for(int i = 0 ; i < lines.length ; i++) {
				String line = lines[i];
				if(line == null) {
					continue;
				}
				//System.out.println("# "+lines[i]);
				if(line.contains(identifyingFilenamePart)) {
					String linesplit[] = line.split(" ");
					if(linesplit == null) {
						continue;
					}
					if(linesplit.length < 5) {
						continue;
					}
					//System.out.println("Raden är "+lines[i]);
					return true;
					//System.out.println("MD5-summan är "+md5sum);
				}
			}
			
			logMessage(report, "Hittade inte rätt rad"+stringend+callr.toString()+stringend);
			return false;
		}
		catch(Exception e) {
			logMessage(report, "Fel vid framvaskningen av svaret."+stringend+CommandResponse.getString(e)+stringend);
		}
		return null;
	}
	
	public static Long getFilesize(Session session, String file, int timeout, StringBuffer report) {
		String command = "ls -l "+file;
		logMessage(report, "Skall bestämma storlek på fil. Kommando <"+command+">"+stringend);
		
		CallOutcome<JSchException, String[]> callr = null;
		try {
			callr = ExecuteJsch.runCommand(session, command, timeout);

			if(!callr.getGeneralOutcome()) {
				logMessage(report, "Det blev fel vid körningen av kommandot:"+stringend+callr.toString());
				return null;
			}
		}
		catch(Exception e) {
			logMessage(report, "Fel vid invokeringen."+stringend+CommandResponse.getString(e)+stringend);
			return null;
		}
		
		String identifyingFilenamePart = file;
		if(file.contains("~")) {
			if(file.contains("/")) {
				identifyingFilenamePart = file.substring(file.lastIndexOf("/"));
			}
		}

		try {
			CommandResponse<JSchException, String[]> cr = (CommandResponse<JSchException, String[]>) callr.getCommandResponse();
			String lines[] = cr.getResponse();
			for(int i = 0 ; i < lines.length ; i++) {
				String line = lines[i];
				if(line == null) {
					continue;
				}
				//System.out.println("# "+lines[i]);
				if(line.contains(identifyingFilenamePart)) {
					String linesplit[] = line.split(" ");
					if(linesplit == null) {
						continue;
					}
					if(linesplit.length < 5) {
						continue;
					}
					//System.out.println("Raden är "+lines[i]);
					return Long.parseLong(linesplit[4]);
					//System.out.println("MD5-summan är "+md5sum);
				}
			}
			
			logMessage(report, "Hittade inte rätt rad"+stringend+callr.toString()+stringend);
			return null;
		}
		catch(Exception e) {
			logMessage(report, "Fel vid framvaskningen av svaret."+stringend+CommandResponse.getString(e)+stringend);
		}
		return null;
	}
	
	public static Boolean removeFile(Session session, String file, int timeout, StringBuffer report) {
		String command = "rm -f "+file;
		report.append("Kommando <"+command+">"+stringend);
		
		CallOutcome<JSchException, String[]> callr = null;
		try {
			callr = ExecuteJsch.runCommand(session, command, timeout);

			if(!callr.getGeneralOutcome()) {
				logMessage(report, "Det blev fel vid körningen av kommandot:"+stringend+callr.toString());
				return false;
			}
		}
		catch(Exception e) {
			logMessage(report, "Fel vid invokeringen."+stringend+CommandResponse.getString(e)+stringend);
			return false;
		}
		
		return true;
	}
	
	public static Boolean removeFileAsSudo(Session session, String file, int timeout, String passwd, StringBuffer report) throws Exception {
		String command = "rm -f "+file;
		logMessage(report, "Kommando <"+command+">"+stringend);
		
		CallOutcome<JSchException, String[]> callr = null;
		try {
			callr = ExecuteJsch.runSudoCommand(session, command, passwd, timeout);

			if(!callr.getGeneralOutcome()) {
				logMessage(report, "Det blev fel vid körningen av kommandot:"+stringend+callr.toString());
				return false;
			}
		}
		catch(Exception e) {
			logMessage(report, "Fel vid invokeringen."+stringend+CommandResponse.getString(e)+stringend);
			return false;
		}
		
		return true;
	}
	
	public static Long getFreeDiscspace(Session session, String file, int timeout, StringBuffer report) {
		String command = "df -kh --block-size=1 "+file+" | tail -1";
		logMessage(report, "Kommando <"+command+">"+stringend);
		
		CallOutcome<JSchException, String[]> callr = null;
		try {
			callr = ExecuteJsch.runCommand(session, command, timeout);

			if(!callr.getGeneralOutcome()) {
				logMessage(report, "Det blev fel vid körningen av kommandot:"+stringend+callr.toString());
				return null;
			}
		}
		catch(Exception e) {
			logMessage(report, "Fel vid invokeringen."+stringend+CommandResponse.getString(e)+stringend);
			return null;
		}

		try {
			CommandResponse<JSchException, String[]> cr = (CommandResponse<JSchException, String[]>) callr.getCommandResponse();
			String lines[] = cr.getResponse();
			for(int i = 0 ; i < lines.length ; i++) {
				String line = lines[i];
				if(line == null) {
					continue;
				}
				while(line.contains("  ")) {
					line = line.replaceAll("  ", " ");
				}
				String linesplit[] = line.split(" ");
				if(linesplit == null) {
					continue;
				}
				if(linesplit.length < 6) {
					continue;
				}
				//System.out.println("Plats "+lines[i].indexOf("\n")+", längd "+lines[i].length());
				//System.out.println("# "+lines[i]+" -> split[5] = "+split[5]+", antal "+split.length);
				//System.out.println("# "+line+" -> split[5] = "+split[5]+", antal "+split.length);
				if(linesplit[5].contains(file)) {
					//System.out.println("Raden är "+lines[i]);
					return Long.parseLong(linesplit[3]);
					//System.out.println("MD5-summan är "+md5sum);
				}
			}
			
			logMessage(report, "Hittade inte rätt rad"+stringend+callr.toString()+stringend);
			return null;
		}
		catch(Exception e) {
			logMessage(report, "Fel vid framvaskningen av svaret."+stringend+CommandResponse.getString(e)+stringend);
		}

		return null;
	}

}
