package general.reuse.timeouthandling.commandline;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import general.reuse.timeouthandling.core.CallOutcome;
import general.reuse.timeouthandling.core.CommandResponse;
import general.reuse.timeouthandling.core.TimeoutManagement;

public class ExecuteJsch {

	private static void handleCommandRespose(CommandResponse<JSchException, String[]> cr,
			ChannelExec channel, String sudopass, String promptedpass) {

		Vector<String> stdout = new Vector<String>();
		OutputStream stderr = new ByteArrayOutputStream();
		channel.setErrStream(stderr);
		channel.setPty(true);
		try {

			channel.connect();
			InputStream in = channel.getInputStream();
			if (sudopass != null) {
				//System.out.println("ExecuteJsch.handleCommandResponse : sudopass "+ sudopass);
				OutputStream out = channel.getOutputStream();
				out.write((sudopass + "\n").getBytes());
				out.flush();
			}

			byte[] tmp = new byte[1024];
			while (true) {
				String stdoutavailable = null;
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0) {
						//System.out.println("ExecuteJsch.handleCommandResponse i < 0 break");
						break;
					}
					stdoutavailable = new String(tmp, 0, i);
					//System.out.println("ExecuteJsch.handleCommandResponse stdoutavailable = <"+ stdoutavailable + ">");
					stdout.add(stdoutavailable);
				}
				if (channel.isClosed()) {
					if (in.available() > 0) {
						//System.out.println("ExecuteJsch.handleCommandResponse in.available() > 0 continue");
						continue;
					}
					cr.setExitStatus(channel.getExitStatus());
					// System.out.println("exit-status: "+channel.getExitStatus());
					//System.out.println("ExecuteJsch.handleCommandResponse setExitStatus");
					break;
				}
				try {
					//System.out.println("ExecuteJsch.handleCommandResponse sleep(1000)");
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
				if (stdoutavailable != null) {
					if(stdoutavailable.trim().endsWith(" password:")) {
						if (promptedpass != null) {
							//System.out.println("ExecuteJsch.handleCommandResponse : promptedpass "+ promptedpass);
							OutputStream out = channel.getOutputStream();
							out.write((promptedpass + "\n").getBytes());
							out.flush();
						}
					}
					if(stdoutavailable.trim().contains("Are you sure you want to continue connecting (yes/no)")) {
						//System.out.println("ExecuteJsch.handleCommandResponse : asked to continue ");
						OutputStream out = channel.getOutputStream();
						out.write(("yes" + "\n").getBytes());
						out.flush();
					}
				}
				stdoutavailable = null;
			}
			//System.out.println("ExecuteJsch.handleCommandResponse - efter while -> disconnect)");
			channel.disconnect();
			String lines[] = new String[stdout.size()];

			cr.setResponse(lines);
			for (int i = 0; i < stdout.size(); i++) {
				lines[i] = stdout.elementAt(i);
				//System.out.println("ExecuteJsch.handleCommandResponse lines[i] = "+ lines[i]);

				if (lines[i].contains("ECDSA") && lines[i].contains("Are you sure you want to continue connecting (yes/no)")) {
					//System.out.println("JAAAAA på plats " + i);
					channel.getOutputStream().write("yes\n".getBytes("UTF-8"));
				}
			}

		} catch (Exception e) {
			cr.setException(e);
		}

		//Thread.currentThread().stop();
	}

	public static CommandResponse<JSchException, String[]> runCommand(final Session session,
			final String command) throws JSchException {

		final CommandResponse<JSchException, String[]> out = new CommandResponse<JSchException, String[]>(command);

		ChannelExec channel = (ChannelExec) session.openChannel("exec");
		channel.setCommand(command);

		handleCommandRespose(out, channel, null, null);

		return out;
	}

	public static CallOutcome<JSchException, String[]> runCommand(final Session session,
			final String command, long timeout) {

		final CommandResponse<JSchException, String[]> cr = new CommandResponse<JSchException, String[]>(command);

		Runnable r = new Runnable() {
			public void run() {
				try {
					ChannelExec channel = (ChannelExec) session
							.openChannel("exec");
					channel.setCommand(command);

					handleCommandRespose(cr, channel, null, null);
				} catch (JSchException je) {
					cr.setSpecialException(je);
				}
			}
		};

		final CallOutcome<JSchException, String[]> out = new CallOutcome<JSchException, String[]>(cr);
		try {
			TimeoutManagement.invoke(r, timeout, TimeUnit.MILLISECONDS, out);
		} catch (Exception e) {
			out.setOutcome(false);
			out.setOutcomeException(e);
		}

		return out;
	}

	public static CommandResponse<JSchException, String[]> runPasswordPromptedCommand(
			final Session session, final String command,
			final String promptedpass) throws JSchException {

		final CommandResponse<JSchException, String[]> cr = new CommandResponse<JSchException, String[]>(command);
		// cr.setWithSudo(true); // Varför?

		ChannelExec channel = (ChannelExec) session.openChannel("exec");
		channel.setCommand(command);

		handleCommandRespose(cr, channel, null, promptedpass);

		return cr;
	}

	public static CallOutcome<JSchException, String[]> runPasswordPromptedCommand(
			final Session session, final String command,
			final String promptedpass, long timeout) {

		final CommandResponse<JSchException, String[]> cr = new CommandResponse<JSchException, String[]>(command);
		// cr.setWithSudo(true); // Varför?

		Runnable r = new Runnable() {
			public void run() {
				try {
					ChannelExec channel = (ChannelExec) session.openChannel("exec");
					channel.setCommand(command);

					handleCommandRespose(cr, channel, null, promptedpass);
				} catch (JSchException je) {
					cr.setSpecialException(je);
				}
			}
		};

		final CallOutcome<JSchException, String[]> out = new CallOutcome<JSchException, String[]>(cr);
		try {
			TimeoutManagement.invoke(r, timeout, TimeUnit.MILLISECONDS, out);
		} catch (Exception e) {
			out.setOutcome(false);
			out.setOutcomeException(e);
		}

		return out;
	}

	public static CommandResponse<JSchException, String[]> runSudoCommand(final Session session,
			final String command, final String sudopass) throws JSchException {

		final CommandResponse<JSchException, String[]> cr = new CommandResponse<JSchException, String[]>(command);
		cr.setWithSudo(true);

		ChannelExec channel = (ChannelExec) session.openChannel("exec");
		channel.setCommand("sudo -S -p '' " + command);

		handleCommandRespose(cr, channel, sudopass, null);

		return cr;
	}

	public static CallOutcome<JSchException, String[]> runSudoCommand(final Session session, final String command, final String sudopass, long timeout) {

		final CommandResponse<JSchException, String[]> cr = new CommandResponse<JSchException, String[]>(command);
		cr.setWithSudo(true);

		Runnable r = new Runnable() {
			public void run() {
				try {
					ChannelExec channel = (ChannelExec) session.openChannel("exec");
					channel.setCommand("sudo -S -p '' " + command);

					handleCommandRespose(cr, channel, sudopass, null);
				} catch (JSchException je) {
					cr.setSpecialException(je);
				}
			}
		};

		final CallOutcome<JSchException, String[]> out = new CallOutcome<JSchException, String[]>(cr);
		try {
			TimeoutManagement.invoke(r, timeout, TimeUnit.MILLISECONDS, out);
		} catch (Exception e) {
			out.setOutcome(false);
			out.setOutcomeException(e);
		}

		return out;
	}

	public static CommandResponse runPasswordPromptedSudoCommand(
			final Session session, final String command, final String sudopass,
			final String promptedpass) throws JSchException {

		final CommandResponse cr = new CommandResponse(command);
		cr.setWithSudo(true);

		ChannelExec channel = (ChannelExec) session.openChannel("exec");
		channel.setCommand("sudo -S -p '' " + command);

		handleCommandRespose(cr, channel, sudopass, promptedpass);

		return cr;
	}

	public static CallOutcome<JSchException, String[]> runPasswordPromptedSudoCommand(
			final Session session, final String command, final String sudopass,
			final String promptedpass, long timeout) {

		final CommandResponse<JSchException, String[]> cr = new CommandResponse<JSchException, String[]>(command);
		cr.setWithSudo(true);

		Runnable r = new Runnable() {
			public void run() {
				try {
					ChannelExec channel = (ChannelExec) session.openChannel("exec");
					channel.setCommand("sudo -S -p '' " + command);

					handleCommandRespose(cr, channel, sudopass, promptedpass);
				} catch (JSchException je) {
					cr.setSpecialException(je);
				}
			}
		};

		final CallOutcome<JSchException, String[]> out = new CallOutcome<JSchException, String[]>(cr);
		try {
			TimeoutManagement.invoke(r, timeout, TimeUnit.MILLISECONDS, out);
		} catch (Exception e) {
			out.setOutcome(false);
			out.setOutcomeException(e);
		}

		return out;
	}

}
