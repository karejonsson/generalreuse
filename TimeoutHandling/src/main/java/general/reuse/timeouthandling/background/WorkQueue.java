package general.reuse.timeouthandling.background;

import java.util.LinkedList;
import java.util.List;

public class WorkQueue
{
    private static int nThreads = 0;
    private static PoolWorker[] threads;
    private static LinkedList<WorkQueueRepresentation> queue;
    
    public static List<WorkQueueRepresentation> getQueue() {
    	return queue;
    }
    
    public static void clearWaiting() {
    	queue.clear();
    }

    public static void initialize(int threadCount) {
    	if(nThreads != 0) {
    		return;
    	}
    	WorkQueue.nThreads = threadCount;
        queue = new LinkedList<WorkQueueRepresentation>();
        threads = new PoolWorker[nThreads];

        for (int i=0; i<nThreads; i++) {
            threads[i] = new PoolWorker(queue);
            threads[i].start();
        }
    }
    
    public static PoolWorkerProtector[] getPoolWorkers() {
    	PoolWorkerProtector[] out = new PoolWorkerProtector[nThreads];
    	for(int i = 0 ; i < nThreads ; i++) {
    		out[i] = new PoolWorkerProtector(threads[i]);
    	}
    	return out;
    }
    
    public static void remove(String name) {
    	for(int idx = 0 ; idx < queue.size() ; idx++) {
    		try {
    			WorkQueueRepresentation wqr = queue.get(idx);
    			if(wqr.getName().compareTo(name) == 0) {
    				queue.remove(idx);
    				return;
    			}
    		}
    		catch(Exception e) {
    		}
    	}
    }
    
    public static void requestStop(String name) {
    	for(int idx = 0 ; idx < threads.length ; idx++) {
    		try {
    			WorkQueueRepresentation wqr = threads[idx].getQueueRepresentation();
    			if(wqr.getName().compareTo(name) == 0) {
    				wqr.requestStop();
    			}
    		}
    		catch(Exception e) {
    		}
    	}
    }
    
    public static int getThreadPoolCount() {
    	return threads.length;
    }

    public static void execute(WorkTask wt) {
        synchronized(queue) {
        	for(int i = 0 ; i < queue.size() ; i++) {
        		WorkQueueRepresentation qr = queue.get(i);
        		if(qr.getName().equals(wt.getName())) {
        			qr.setWorktask(wt);
        			qr.setInQueueUpdateDate();
        			return;
        		}
        	}
        	WorkQueueRepresentation qr = new WorkQueueRepresentation(wt);
        	qr.setEnqueueDate();
            queue.addLast(qr);
            queue.notify();
        }
    }

    public static String getLog(String name) {
    	for(int i = 0 ; i < queue.size() ; i++) {
    		WorkQueueRepresentation qr = queue.get(i);
    		if(qr.getName().compareTo(name) != 0) {
    			return "Väntar på exekvering";
    		}
    	}
    	for(int i = 0 ; i < nThreads ; i++) {
    		WorkQueueRepresentation qr = threads[i].getQueueRepresentation();
    		if(qr == null) {
    			continue;
    		}
    		if(qr.getName().compareTo(name) != 0) {
    			continue;
    		}
    		return qr.get();
    	}
    	return "Inget asynkront arbete under namnet "+name;
    }

    
    private static List<WorkQueueRepresentation> finished = new LinkedList<WorkQueueRepresentation>();
    
    public static List<WorkQueueRepresentation> getRecentlyFinished() {
    	return finished;
    }
    
    public static void addFinished(WorkQueueRepresentation qr) {
        finished.add(qr);
        while(finished.size() > 20) {
        	finished.remove(0);
        }
    }
    
    public static void clearFinished() {
    	finished.clear();
    }
            
}