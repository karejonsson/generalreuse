package general.reuse.timeouthandling.background;

import java.util.ArrayList;
import java.util.List;

public class DefaultMultiPurposeLogger implements MultiPurposeLogger {
	
	private List<String> mem = new ArrayList<String>();
	private int lines;
	
	public DefaultMultiPurposeLogger(int lines) {
		this.lines = lines;
	}
	
	@Override
	public  void add(String log) {
		synchronized (this) {
			while(mem.size() > lines) {
				mem.remove(0);
			}
			mem.add(log);
		}
	}

	@Override
	public String get() {
		synchronized (this) {
			StringBuffer out = new StringBuffer();
			for(String row : mem) {
				out.append(row+"\n");
			}
			return out.toString();
		}
	}

	@Override
	public void clear() {
		synchronized (this) {
			mem.clear();
		}
	}
	
}
