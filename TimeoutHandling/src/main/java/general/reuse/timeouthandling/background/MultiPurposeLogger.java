package general.reuse.timeouthandling.background;

public interface MultiPurposeLogger {

	public void add(String log);
	public String get();
	public void clear();

}
