package general.reuse.timeouthandling.background;

public interface NotifyDelegator {
	
	public void notifySuccess(String message);
	public void notifyError(String message);
	
}

