package general.reuse.timeouthandling.background;

public abstract class WorkTask {

	protected MultiPurposeLogger memoryLog = null;
	private NotifyDelegator notifyer = null;

	public void notifyGood(String message) {
		if(notifyer != null) {
			notifyer.notifySuccess(message);
		}
	}
	public void notifyBad(String message) {
		if(notifyer != null) {
			notifyer.notifyError(message);
		}
	}
	public void setMemoryLog(MultiPurposeLogger memoryLog) {
		this.memoryLog = memoryLog;
	}
	
	private String name = null;
	public WorkTask(String name, NotifyDelegator notifyer) {
		this.name = name;
		this.notifyer = notifyer;
	}
	public String getName() {
		return name;
	}
	public void log(String msg) {
		if(memoryLog != null) {
			memoryLog.add(msg);
		}
	}
	public String getLog() {
		if(memoryLog != null) {
			return memoryLog.get();
		}
		return null;
	}
	
	public void clearLog() {
		if(memoryLog != null) {
			memoryLog.clear();
		}
	}
	
	public abstract void execute();
	
	private boolean stopWhenPossible = false;
	
	public void requestStop() {
		stopWhenPossible = true;
	}
	
	public boolean stopIsRequested() {
		return stopWhenPossible;
	}
	
	private String progress = "0%";
	
	public String get15CharProgress() {
		return progress;
	}
	
	public void set15CharProgress(String prog) {
		if(prog == null) {
			progress = null;
			return;
		}
		progress = prog.substring(0, Math.min(15, prog.length()));
	}
	
	public void set15ProgressPercent(double prog) {
		//System.out.println("Worktask.set10CharProgress("+prog+")");
		if(prog < 0) {
			progress = "< start";
			return;
		}
		if(prog > 1) {
			progress = "> slut";
			return;
		}
		progress = ""+(((double) ((int) (prog*1000)))/10)+"%";
	}
	
	public void set15ProgressPercent(int above, int below) {
		//System.out.println("Worktask.set10CharProgress("+prog+")");
		if(above < 0) {
			progress = "< start";
			return;
		}
		if(above > below) {
			progress = "> slut";
			return;
		}
		double prog = ((double) above)/ below;
		progress = ""+(((double) ((int) (prog*1000)))/10)+"%";
	}
	
	public void set15ProgressCombo(int above, int below) {
		//System.out.println("Worktask.set10CharProgress("+prog+")");
		if(above < 0) {
			progress = "< start";
			return;
		}
		if(above > below) {
			progress = "> slut";
			return;
		}
		double prog = ((double) above)/ below;
		String tmp = ""+above+"/"+below+" "+(((double) ((int) (prog*1000)))/10)+"%";
		progress = tmp.substring(0, Math.min(15,  tmp.length()));
	}
	
	public void set15ProgressRatio(int above, int below) {
		//System.out.println("Worktask.set10CharProgress("+prog+")");
		if(above < 0) {
			progress = "< start";
			return;
		}
		if(above > below) {
			progress = "> slut";
			return;
		}
		progress = ""+above+"/"+below;
	}
	
	public static NotifyDelegator getDefaultNotifyDelegator() {
		return null;
	}
	
}

