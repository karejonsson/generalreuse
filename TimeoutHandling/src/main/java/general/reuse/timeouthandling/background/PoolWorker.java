package general.reuse.timeouthandling.background;

import java.util.LinkedList;

class PoolWorker extends Thread {
	
    private static int jobctr = 0;
	
	private WorkQueueRepresentation qr;
	private LinkedList<WorkQueueRepresentation> queue = null;
	
	public PoolWorker(LinkedList<WorkQueueRepresentation> queue) {
		this.queue = queue;
	}

	public void run() {

        while (true) {
            synchronized(queue) {
                while (queue.isEmpty()) {
                    try {
                        queue.wait();
                    }
                    catch (InterruptedException ignored) { }
                } 
                qr = queue.removeFirst();
                qr.setJobNr(jobctr++);
            }

            // If we don't catch RuntimeException, 
            // the pool could leak threads
        	WorkTask wt = qr.getWorktask();
            try {
            	qr.clear();
                qr.setStartDate();
            	//wt.setMemoryLog(qr);
                wt.execute();
            }
            catch (RuntimeException e1) {
        		e1.printStackTrace();
            	try {
            		wt.notifyBad("Misslyckades\n"+qr.get());
            	}
            	catch(Exception e2) {
            		/*
	// Ett detach-problem enligt nedan kan leda hit. Har att göra med att Notfieringen kommer från
	// en annan tråd och sen tror jag det har att göra med att Jetty och websockets 
	// har något problem men det har jag inte undersökt.

Exception in thread "Thread-33" com.vaadin.ui.UIDetachedException
	at com.vaadin.ui.UI.access(UI.java:1441)
	at se.dom.rinfo.feedmanager.main.NewExecutionWindow$3.notifyError(NewExecutionWindow.java:112)
	at se.dom.rinfo.feedmanager.async.framework.WorkTask.notifyBad(WorkTask.java:17)
	at se.dom.rinfo.feedmanager.async.framework.PoolWorker.run(PoolWorker.java:40)
            		 */
            		e2.printStackTrace();
            	}
            }
            qr.setEndDate();
            WorkQueue.addFinished(qr);
            qr = null;
        }
    }
	
	public String getLog() {
		return qr.get();
	}
	
	public String getWorkTaskName() {
		if(qr == null) {
			return null;
		}
		return qr.getName();
	}
	
	public WorkQueueRepresentation getQueueRepresentation() {
		return qr;
	}
	
	public String toString() {
		if(qr == null) {
			return "Inaktiv";
		}
		return 
				"Namn: "+qr.getName()+"\n"+
				"Inköat: "+qr.getEnqueueDate()+"\n"+
				(qr.getInQueueUpdateDate() != null ? ("Uppdaterat: "+qr.getInQueueUpdateDate()+"\n") : "")+
				"Startat: "+qr.getStartDate()+"\n"+
				"Jobnummer: "+qr.getJobNr()+"\n"+
				"Log: "+qr.get()+"\n";
	}
	
	public Integer getJobNr() {
		if(qr == null) {
			return null;
		}
		return qr.getJobNr();
	}
	
	public void requestStop() {
		qr.requestStop();
	}
	
	public boolean isExecuting() {
		return (qr != null);
	}
	
}
