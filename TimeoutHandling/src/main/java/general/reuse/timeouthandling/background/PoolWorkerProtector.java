package general.reuse.timeouthandling.background;

import java.util.Date;

public class PoolWorkerProtector {
	
	private PoolWorker pw;
	private Integer initialJobNr = null;
	
	public PoolWorkerProtector(PoolWorker pw) {
		this.pw = pw;
		initialJobNr = pw.getJobNr();
	}

	public Integer getJobNr() {
		return pw.getJobNr();
	}
	
	public String toString() {
		return pw.toString();
	}
	
	public String getLog() {
		Integer currentJobNr = getJobNr();
		if(currentJobNr == null) {
			return "Jobbet är inte aktivt."+((initialJobNr != null) ? " Ursprungligen utfördes "+initialJobNr : "");
		}
		WorkQueueRepresentation wqr = pw.getQueueRepresentation();
		if(wqr == null) {
			return "Inget jobb utförs i denna tråd."+((initialJobNr != null) ? " Ursprungligen utfördes "+initialJobNr : "");
		}
		return wqr.get();
	}
	
	public String getName() {
		Integer currentJobNr = getJobNr();
		if(currentJobNr == null) {
			return "Jobbet är inte aktivt."+((initialJobNr != null) ? " Ursprungligen "+initialJobNr : "");
		}
		if(currentJobNr != initialJobNr) {
			return "Nu pågår ett annat jobb i denna tråd."+
					((initialJobNr != null) ? " Nyss "+initialJobNr : "")+
					((currentJobNr != null) ? " Nu "+currentJobNr : "");
		}
		WorkQueueRepresentation wqr = pw.getQueueRepresentation();
		if(wqr == null) {
			return null;
		}
		return wqr.getName();		
	}
	
	public Date getEnqueueDate() {
		Integer currentJobNr = getJobNr();
		if(currentJobNr == null) {
			return null;
		}
		if(currentJobNr != initialJobNr) {
			return null;
		}
		WorkQueueRepresentation wqr = pw.getQueueRepresentation();
		if(wqr == null) {
			return null;
		}
		return wqr.getEnqueueDate();
	}
	
	public Date getInQueueUpdateDate() {
		Integer currentJobNr = getJobNr();
		if(currentJobNr == null) {
			return null;
		}
		if(currentJobNr != initialJobNr) {
			return null;
		}
		WorkQueueRepresentation wqr = pw.getQueueRepresentation();
		if(wqr == null) {
			return null;
		}
		return wqr.getInQueueUpdateDate();
	}
	
	public Date getStartDate() {
		Integer currentJobNr = getJobNr();
		if(currentJobNr == null) {
			return null;
		}
		if(currentJobNr != initialJobNr) {
			return null;
		}
		WorkQueueRepresentation wqr = pw.getQueueRepresentation();
		if(wqr == null) {
			return null;
		}
		return wqr.getStartDate();
	}
	
	public void requestStop() {
		pw.requestStop();
	}
	
	public String get10CharProgress() {
		Integer currentJobNr = getJobNr();
		if(currentJobNr == null) {
			return null;
		}
		if(currentJobNr != initialJobNr) {
			return null;
		}
		WorkQueueRepresentation wqr = pw.getQueueRepresentation();
		if(wqr == null) {
			return null;
		}
		return wqr.get15CharProgress();
	}
	
	public boolean isExecuting() {
		return pw.isExecuting();
	}
	
}
