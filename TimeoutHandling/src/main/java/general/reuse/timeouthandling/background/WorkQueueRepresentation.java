package general.reuse.timeouthandling.background;

import java.util.Date;

public class WorkQueueRepresentation /* implements MultiPurposeLogger */ {
	
	private Date enqueueDate = null;
	public Date getEnqueueDate() {
		return enqueueDate;
	}
	public void setEnqueueDate(Date enqueueDate) {
		this.enqueueDate = enqueueDate;
	}
	public void setEnqueueDate() {
		setEnqueueDate(new Date());
	}
	
	private Date inQueueUpdateDate = null;
	public Date getInQueueUpdateDate() {
		return inQueueUpdateDate;
	}
	public void setInQueueUpdateDate(Date inQueueUpdateDate) {
		this.inQueueUpdateDate = inQueueUpdateDate;
	}
	public void setInQueueUpdateDate() {
		setInQueueUpdateDate(new Date());
	}
	
	private Date startDate = null;
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public void setStartDate() {
		setStartDate(new Date());
	}

	private Date endDate = null;
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public void setEndDate() {
		setEndDate(new Date());
	}

	private WorkTask worktask = null;

	public WorkTask getWorktask() {
		return worktask;
	}

	public void setWorktask(WorkTask worktask) {
		this.worktask = worktask;
	}
	
	public String getName() {
		if(worktask == null) {
			return null;
		}
		return worktask.getName();
	} 
	
	public WorkQueueRepresentation(WorkTask worktask) {
		this.worktask = worktask;
	}
	
	public void add(String log) {
		worktask.log(log);
	}

	public String get() {
		return worktask.getLog();
	}

	public void clear() {
		worktask.clearLog();
	}
	
	public void requestStop() {
		worktask.requestStop();
		add("Stop requested");
	}
	
	public String toString() {
		return "Queuer "+getName();
	}
	
	private int jobNr; 
	
	void setJobNr(int jobNr) {
		this.jobNr = jobNr;
	}
	
	public int getJobNr() {
		return jobNr;
	}
	
	public String get15CharProgress() {
		return worktask.get15CharProgress();
	}
	
}