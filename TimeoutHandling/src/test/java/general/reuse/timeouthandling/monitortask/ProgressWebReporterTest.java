package general.reuse.timeouthandling.monitortask;

import java.util.ArrayList;
import java.util.List;

import general.reuse.timeouthandling.task.DummyResourceManagement;
import general.reuse.timeouthandling.task.JobOfManyJobs;
import general.reuse.timeouthandling.task.StupidTask;
import general.reuse.timeouthandling.task.TaskMonitorer;

public class ProgressWebReporterTest {

	public static void main(String args[]) throws Exception {
		List<TaskMonitorer> tasks = new ArrayList<TaskMonitorer>();
		long loops = 100;
		for(int i = 0 ; i < loops ; i++) {
			tasks.add(new TaskMonitorer(new StupidTask(i, 50, 200)));
		}
		
		JobOfManyJobs worker = new JobOfManyJobs(tasks, 200l, new Long[] {200l, 400l, 1500l, 3000l}, new DummyResourceManagement());
		ProgressWebReporter.run(worker, 8001, "title");
		System.out.println("Klart");
	}

}
