package general.reuse.timeouthandling.task;

public class DummyResourceManagement implements ResourceManagement {

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public void stop() {
		System.out.println("Stoppar resurser");
	}

	@Override
	public void start() {
		System.out.println("Startar resurser");
	}

	@Override
	public boolean isProbablyOK() {
		return true;
	}

}
