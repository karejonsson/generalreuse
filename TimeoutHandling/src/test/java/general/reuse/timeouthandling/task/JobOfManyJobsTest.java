package general.reuse.timeouthandling.task;

import java.util.ArrayList;
import java.util.List;

public class JobOfManyJobsTest {

	public void test() {
		List<TaskMonitorer> tasks = new ArrayList<TaskMonitorer>();
		long loops = 15;
		for(int i = 0 ; i < loops ; i++) {
			tasks.add(new TaskMonitorer(new StupidTask(i, 50, 2000)));
		}
		
		JobOfManyJobs worker = new JobOfManyJobs(tasks, 200, new Long[] {200l, 400l, 700l, 1500l, 3000l}, new DummyResourceManagement());
		long before = System.currentTimeMillis();
		worker.run();
		long after = System.currentTimeMillis();
		
		System.out.println("Total extratid "+(after - before - (2*loops)));
	}
	
}
