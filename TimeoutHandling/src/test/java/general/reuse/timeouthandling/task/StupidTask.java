package general.reuse.timeouthandling.task;

public class StupidTask implements TimeoutTask {
	
	private long millissleep;
	private long median;
	private long stddev;
	private long sum = 0;
	private int jobnr = 0;
	private boolean done = false;
	
	public StupidTask(int jobnr, long median, long stddev) {
		this.median = median;
		this.stddev = stddev;
		
		millissleep = (long) (median + Math.random()*stddev);
		this.jobnr = jobnr;
	}

	@Override
	public void runTimeconsuming(int attemptNo) throws InterruptedException {
		try {
			Thread.sleep(millissleep);
			sum += millissleep;
			done = Math.random() < 0.4;
			if(!done) {
				System.out.println("Jobb "+jobnr+" som väntade "+millissleep+" på försök "+attemptNo+" blev klart men hade inte lyckats.");
			}
		} catch (InterruptedException e) {
			sum += millissleep;
			throw e;
		}
	}

	@Override
	public void onFinishedBeQuick(int attemptNo) {
		System.out.println("Klar med jobb "+jobnr+" som väntade "+millissleep+" på försök "+attemptNo);
		millissleep = (long) (median + Math.random()*stddev);
	}

	@Override
	public void onTimeoutBeQuick(int attemptNo) {
		System.out.println("Timeout för jobb "+jobnr+" som väntade "+millissleep+" på försök "+attemptNo);
		millissleep = (long) (median + Math.random()*stddev);
	}

	@Override
	public void onTerminallyFailed() {
		System.out.println("Slutligt misslyckande för jobb "+jobnr+" som sammanlagt väntade "+sum);
	}

	@Override
	public boolean isDone() {
		return done;
	}

}
